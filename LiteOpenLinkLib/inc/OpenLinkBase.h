/*!
 * \file OpenLinkBase.h
 * \date Created: 9/11/2018 12:39:25 PM
 * \author Author: jwalsh
 */


#ifndef OPENLINKBASE_H_
#define OPENLINKBASE_H_

/*****		User Includes		*****/
#include <stdint.h>

/*****		User Defines		*****/
#define OL_LIB_REV_MAJOR			ONEKEY_LIB_REVSION_MAJOR		/**<Library Revision -- MAJOR */
#define OL_LIB_REV_MINOR			ONEKEY_LIB_REVSION_MINOR		/**<Library Revision -- MINOR */
#define PRODUCT_FW_RELEASE			0x00		/**<enumerated value */
#define OL_MAX_MSG_LEN				254         /**<includes header and checksum length.  max total msg bytes. */
#define OL_PACKET_DATA_START_BYTE	3U          /**<location in packet where payload data starts */
#define OL_PACKET_CRC_LEN			2U          /**<location in packet where length byte resides */
#define OL_PACKET_MAX_DATA_LEN		(OL_MAX_MSG_LEN - OL_PACKET_DATA_START_BYTE - OL_PACKET_CRC_LEN)    /**<maximum packet size */

#define OL_MM_ADDRESS_SIZE			2U		/**<2 bytes */
#define OL_PASSWORD_ENABLE_SIZE		8U      /**<8 bytes */
#define OL_BLE_MAC_ADDRESS_SIZE		6U      /**<6 bytes */

#define PRODUCT_CODE				0x9B	/**<Tool product ID code prefix */
#define OL_RUNTIME_DATA_SIZE		26U     /**<26 bytes */

#define OL_PACKET_LEN_OFFSET		(OL_PACKET_CRC_LEN + OL_PACKET_DATA_START_BYTE)  /**< Byte in a packet where the packet lenght is located, 0 indexed from beginning */

#define BGM_11S_NACK_ACK_MODE		1			/**<Define this if the BLE module is the BGM11s only one of these 3 can be defined at a time*/
//#define BLE_113_NACK_ACK_MODE		1			//Define this if the BLE module is the BLE113+
//#define BLE_113_NACK_ACK_SHORT	1			//Define this if the BLE module is the BLE113+ and your tool doesn't include the SID byte in ack/naks (why?)
#ifdef BGM_11S_NACK_ACK_MODE
#define ACK_NAK_LEN 4                           /**<4 bytes */
#elif BLE_113_NACK_ACK_MODE
#define ACK_NAK_LEN 2                           /**<2 bytes */
#else
#define ACK_NAK_LEN 1                           /**<1 byte */
#endif

#define OL_TIMER_INTERVAL_MS		1			/**<number of milliseconds for each OL_TIMER tick.  TODO: consider making this a registrable parameter to the struct */
#define SYSTEM_ENDIANESS_LITTLE		1			/**<Define for a little endian system, comment out of big endian */

#define READ	0								/**<RW flag READ value */
#define WRITE	1								/**<RW flag WRITE value  probably could make these an enum then you could ONLY pass 1 or 0..*/

#define HYDRAULIC_EVENT_PASS		0x11		/**<Event Log pass value */
#define HYDRAULIC_EVENT_FAIL		0xF0		/**<Event Log fail value */




/*****	OL Base Enumerations	*****/
// enumerated list of communications errors
// typedef enum
// {
// 	eCommsErr_NONE = 0,
// 	eCommsErr_TX_byte_timeout,			// This error is set by the PE Physical Interface Handler
// 	eCommsErr_TX_Invalid_packet,			// This error is set by the PE Control SM when a MASTER is asked to load a packet but the packet format is bad
// 	eCommsErr_packet_RX_timeout,			// This error is set by the PE Physical Interface Handler and PE Control SM
// 	eCommsErr_bad_RX_cmd_byte,			// This error is set by the PE Packet Framer
// 	eCommsErr_bad_RX_length,				// This error is set by the PE Control SM and the PE Packet Framer
// 	eCommsErr_bad_RX_crc,					// This error is set by the PE Packet Framer
// 	eCommsErr_bad_RX_READ_request,		// This error is set by the PE Control SM
// 	eCommsErr_bad_RX_WRITE_request,		// This error is set by the PE Control SM
// 	eCommsErr_bad_RX_APP_CMD_Request,		// This error is set by the PE Control SM
// 	eCommsErr_Not_in_map_Request,			// This error is set by the PE Control SM
// 	eCommsErr_Insufficient_Privelege,		// This error is set by the PE Control SM
// 	eCommsErr_no_RX_buffer_available,		// This error is set by the PE Control SM and the PE Packet Framer
// 	eCommsErr_PI_RX_overflow,				// This error is set by the Tool to M18 Physical Interface
// 	eCommsErr_PI_RX_Com_Err,				// This error is set by the Tool to M18 Physical Interface
// 	eCommsErr_PI_RX_BitFrameErr,			// This error is set by the Tool to M18 Physical Interface
// 	eCommsErr_MAX_ERROR_VAL
// } eCommsErr;
//
//
//
//  /*!@brief eOL_Sources enum of all allowable message Sources (Uart, Battery Terminal interface, etc) */
//  typedef enum
//  {
// 	 BT_UART = 0,					/** BlueTooth UART  */
// 	 BATTERY_TERMINAL				/** Battery Terminal communications interface */
//  } eOL_Sources;
/*!@brief eTool_Lock_State enum of Tool Lockout Status */
// typedef enum
// {
// 	LOCKED = 0,						/** Tool Lockout - Locked */
// 	UNLOCKED						/** Tool Lockout - UnLocked */
// }eTool_Lock_State;

/*! \brief eNAK_ERROR_CODES
*	A listing of all the nak with reason "reason" codes.
*/
typedef enum
{
	eNAK_CODE_INVALD=0,
	eNAK_CODE_PERMISSION_DENIED,
	eNAK_CODE_ILLEGAL_MEMORY_ADDRESS,
	eNAK_CODE_ILLEGAL_EXT_MEMORY_ADDRESS,
	eNAK_CODE_MALFORMED_PACKET,
	eNAK_CODE_UNKNOWN_MSG_TYPE,
	eNAK_CODE_ILLEGAL_VALUE,
	eNAK_CODE_BUFFER_FULL,
	eNAK_CODE_PERSISTENT_STORAGE_ERROR,
	eNAK_CODE_FEATURE_NOT_SUPPORTED,
	eNAK_CODE_DEFERRED_TIMEOUT,
	eNAK_CODE_UNKNOWN_SPECIFIER,
	eNAK_CODE_UNKNOWN_CLASS,
	eNAK_CODE_UNKNOWN_INSTANCE,
	eNAK_CODE_UNKNOWN_PROPERTY,
	eNAK_CODE_GARBLED_PACKET,
	eNAK_CODE_UNKNOWN_ERROR,
	eNAK_CODE_MAX_VALUE
}eNAK_ERROR_CODES;

/*! \brief NVM_OL_base_data packed struct of EEPROM page 1 data. MIGHT NOT NEED< COMMENTED FOR NOW
*
*	First a table of data that is not updated often (if ever) and won't require NVM rotation
*   may decide to read /write these directly to NVM, removing this structure, or most of it anyway
*/
// typedef struct
// {   //fixed data used during operation
// 	uint16_t mem_map_rev;				/**< RTC time, updated by OL Library, then incremented by Tool counter */
// 	uint16_t extended_mem_offset;		/**< Cumulative trigger pull count.  Loaded from NVM, updated in RAM */
// 	uint8_t mpbid[5];					/**< Tool MPID */
// 	uint32_t sw_pn;						/**< Service state of tool.  See \ref eTool_Service_State typdef - updated by tool, read by library */
// 	uint32_t fw_rev;					/**< Number of service cycles remaining - updated by tool, read by library */
// 	uint32_t born_on_date;				/**< Date of manufacture/initial programming.  Programmed during manufacturing */
// 	uint32_t first_use;					/**< Date of first trigger pull */
// 	uint8_t bt_dev_id[6];			    /**< not sure if this is used for anything */
// 	uint32_t Tool_PCBA_part_number;		/**< PCBA revision/lot/etc/ number for the tool PCBA.  Programmed during Manufacturing.  <i> Optional, ideal if it can be done </i> */
// 	uint8_t enable_authentication;		/**< Most recent Rx or Tx comm error detected.  written by both Tool and Library, read by Library */
//
// 	uint8_t ble_mac_address[6];			/**< MAC address of BLE module, not the same as bt_dev_id */
// 	uint16_t OL_core_version;			/**< OL core version supported by this tool */
// //	uint16_t Encrypt_key[16];			/**< Encryption key programmed during manufacturing.  Unique for each tool */	//shouldn't need to transmit this, ever!
//
// }NVM_OL_base_data;

//extern NVM_OL_base_data OL_base_data;

/*! \brief NVM_OL_runtime_data
*	First a table of data that is updated often /kept track of real time, some is from NVM, some is not.
*/
typedef struct
{
	//runtime data
	uint32_t NUM_OL_TX_PACKETS;			/**< number of OpenLink Packets Transmitted */
	uint32_t NUM_OL_RX_PACKETS;			/**< number of OpenLink Packets Received */
	uint32_t NUM_OL_TX_PACKETS_ERRS;	/**< number of OpenLink Packets Total OL TX Errors */
	uint32_t NUM_OL_RX_PACKETS_ERRS;	/**< number of OpenLink Packets Total OL RX Errors */
	uint8_t NUM_OL_LAST_ERROR;			/**< number of OpenLink Packets Most recent OL Error */
	//uint8_t[5] SECURITY_MODE;
	uint8_t CURRENT_PRIVILEGE_LEVEL;	/**< The current privilege level for the session the valid values are eMM_Sec_Lev values **/
	int32_t LAST_TIME_USED;				/**< Most Recent Time used- \b Note: may not be needed in this, load on init, store regularly */
	//int32_t DATE_TIME_REF;			/**< DATE_REIM_REF is in the shared structure, so not in runtime data*/
	uint16_t EXTENDED_MEMORY_OFFSET;    /**< overflow holder for extended memory reads.  Holds the two high bytes of 32-bit address */
	struct
	{
		uint8_t REV_major;
		uint8_t REV_minor;
		uint16_t REV_build;
	} FIMWARE_REVISION;					/**< FW revision will be created on demand from a hardcoded value and shared data param for this product */
	uint8_t PASSWORD_ENABLE[8];			/**< Holds the most recent password sent to the tool to set security level.  not store to NVM */
	uint8_t BLE_MAC_ADDRESS[6];			/**< BLE MAC address.  Held in ram only.  Future tools should make this persistent for pairing */
	uint32_t PCBA_PART_NUMBER;			/**< PCBA part number of TOOL main board, programmed at manufacturing */
	volatile uint8_t FORWARDED_MESSAGE_SID_BYTE; /**< If a message is forwarded from the battery terminal to the BLE module, this holds the SID for forwarding responses. */
	volatile uint8_t MAILBOX_SID_BYTE;			/**< if a message used the mailbox, the SID is stored here, else it is 0 */
	volatile uint8_t MAILBOX_MSG_SOURCE;			/**< The source of the mailbox write command, used for routing the response */
	union {
		struct{
			uint8_t metco_PW_set	: 1;	/**< the METCO pw has been set, else use default */
			uint8_t service_PW_set	: 1;	/**< the service pw has been set, else use default */
			uint8_t admin_PW_set	: 1;	/**< the user admin pw has been set, else use default */
			uint8_t user_PW_set		: 1;	/**< the user pw has been set, else use default */
			uint8_t mpbid_set		: 4;	/**< the MPBID has been written to eeprom, don't use default  (not implemented yet!, ) */
		}__attribute__((packed));		// pack so individual fields don't align on even boundaries and pad as a result.
		uint8_t USE_DEFAULT_PWS;		//need to refactor this name, since I am adding non-pw stuff to it
	};

}NVM_OL_runtime_data;

extern NVM_OL_runtime_data OL_runtime_data;

/** convienience Structures and Unions**/

/*! \brief union Uint16Bytes - byte representation for uint_16t */

typedef union{
	uint32_t uint16_val;                /**< upper 16 bits */
	struct {
		#ifdef SYSTEM_ENDIANESS_LITTLE
		uint8_t b0;
		uint8_t b1;
		#else
		uint8_t b1;
		uint8_t b0;
		#endif
	};                                  /**< define endianess */
	uint8_t byte_array[2];              /**< access as an array */
}Uint16Bytes;


/*! \brief union Uint32Bytes - byte representation for uint_32t */

typedef union{
	uint32_t uint32_val;                /**< 32 bit value */
	struct {
		#ifdef SYSTEM_ENDIANESS_LITTLE
		uint8_t b0;
		uint8_t b1;
		uint8_t b2;
		uint8_t b3;
		#else
		uint8_t b3;
		uint8_t b2;
		uint8_t b1;
		uint8_t b0;
		#endif
	};
	uint8_t byte_array[4];      /**< access as byte array */
}Uint32Bytes;

/*! \brief union Uint64Bytes - byte representation for uint_64t */
typedef union{
	uint64_t uint64_val;            /**< 64 bit value */
	struct {
		#ifdef SYSTEM_ENDIANESS_LITTLE
		uint8_t b0;
		uint8_t b1;
		uint8_t b2;
		uint8_t b3;
		uint8_t b4;
		uint8_t b5;
		uint8_t b6;
		uint8_t b7;
		#else
		uint8_t b7;
		uint8_t b6;
		uint8_t b5;
		uint8_t b4;
		uint8_t b3;
		uint8_t b2;
		uint8_t b1;
		uint8_t b0;
		#endif
	};
	uint8_t byte_array[8];          /**< access as byte array */
}Uint64Bytes;


void lolCheckSecurityTimeout();

/** convienience Functions **/

void OL_U32_2_Buff(uint32_t val, uint8_t* buffer,uint8_t reverse_endianess);
void OL_U16_2_Buff(uint16_t val, uint8_t* buffer,uint8_t reverse_endianess);
uint32_t OL_Bytes2U32(uint8_t* data);

#endif /* OPENLINKBASE_H_ */