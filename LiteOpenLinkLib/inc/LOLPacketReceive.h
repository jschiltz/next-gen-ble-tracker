/*!
 * \file LOLPacketReceive.h
 *
 * \author Original by:  Al Lukowitz
 *
 * \date 2/07/2020       (AL) Original Creation
 */


#ifndef LOLPACKETRECEIVE_H_
#define LOLPACKETRECEIVE_H_

#include "OpenLinkBase.h"
#include "LOLPacketTransmit.h"

eCommsErr lolPacketCsumCheck(uint8_t * packet);
void lolProcessRXBuffer(uint8_t channelNumber);
void lolCheckRXBuffer();

#endif /* LOLPACKETRECEIVE_H_ */
