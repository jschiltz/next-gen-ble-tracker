/*!
 * \file  LOLPacketTransmit.h
 *
 * \author Original by:  Al Lukowitz
 *
 * \date 1/28/2020       (AL) Original Creation
 */


#ifndef LOLPACKETTRANSMIT_H_
#define LOLPACKETTRANSMIT_H_

#include "LOLConfig.h"

#define SIDBLEBITPOS    1
#define SIDBITS         0x3F

void lolCheckTimeout();
uint16_t lolGenerateChecksum(uint8_t * inputData, uint8_t length);
uint8_t lolGetNextSequenceID(uint8_t channelNumber);

extern channelVals lolTransaction[NUMCHANNELS];

#endif /* LOLPACKETTRANSMIT_H_ */
