/*
 * LOL_Globals.h
 *
 * Holds the config data that is project independent
 *
 * Created: 5/26/2020
 *  Author: Al Lukowitz
 */


#ifndef LOLGLOB_H_
#define LOLGLOB_H_

/*****************************************************************************
 * Define required by LOL
 *****************************************************************************/
#define NUMCHANNELS         2               /**< number of physical channels, to be set by end user */

/*****************************************************************************
 * DEFAULT SECURITY PASSWORDS
 *****************************************************************************/
#define USERPW          0x1111111111111111      /**< Default User Password */
#define ADMINPW         0x2222222222222222      /**< Default Admin Password */
#define SERVICEPW       0x4444444444444444      /**< Default Service Password */
#define METCOPW         0x3333333333333333      /**< Default METCO Password */

#define DEFAULTSECURITYTIMEOUT  2*60*1000   /**< Default security timeout is set to 2 minutes */

#endif  // LOLGLOB_H_