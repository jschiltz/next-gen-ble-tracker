/**
 * \file LOLConfig.h
 *
 * \brief Holds all of the config data for the Lite Open Link, LOL, project
 *
 * \details The host project will need to include this file to interface with the LOL project
 *
 *\author Original by:  Al Lukowitz
 *\date 1/29/2020       (AL) Original Creation
 */

/** \mainpage Lite Open Link Library
 *
 * \section intro_sec Introduction
 *
 * Lite Open Link provides simple OpenLink communications processing.  LOL provides
 * helper functions for sending ACK/NAK, local memory access and extended memory access.
 * The Physical Interface is provided by the host process.  LOL will calculate checksums and
 * the Sequence IDs.
 *
 * \section usage_sec Usage
 *  Include the file \ref  LOLConfig.h in order to have the function prototypes, Library Definitions and
 *  Error definitions. \n
 *
 * \section prototypes_sec Function Prototypes
 *  The host process must call \ref LOLInitChannel at initialization once for each channel.\n
 *  The host process must call \ref LOLTimerTick once every millisecond.\n
 *
 * \subsection transmit_sec Transmit Functions
 *  The host application must provide a \ref Tool_data_tx function that will transmit the output packet over the physical interface.\n
 *  The host application must call \ref uartTxCallback after a packet transmission is complete.\n
 *  The host application must call \ref eCommsErr LOLSend when it needs to transmit an OL packet.\n
 *
 * \subsection receive_set Receive Functions
 *  The host application must call \ref uartRxCallback upon reception of a packet\n
 *
 * \subsection helper_sec Helper Functions
 *
 * The host application can call \ref LOLSendACK to send and ACK or NAK.\n
 * The host application can call \ref LOLSendLocalMemAccessExt to generate an extended local memory access request.\n
 * The host application can call \ref LOLSendLocalMemAccess to generate a local memory access request.\n
 *
 * \section Revision_History Revision History
 *
 * \par 0.1 - Initial Commit
 */


#ifndef LOLCONFIG_H_
#define LOLCONFIG_H_

#include <stdbool.h>
#include "stdint.h"
#include <string.h>
#include "LOL_Globals.h"

#define TX_OVERHEAD_SIZE    5       /**< 5 bytes, Message Type, SID, Length and checksum */
#define NAKWDATASIZE        2       /**< 2 bytes, a NAK with data must have 2 bytes in the payload section */

#define MESSAGEIDPOS       0        /**< Position of the Message Type byte in an OL packet */
#define MESSAGESIDPOS      1        /**< Position of the SID byte in an OL packet */
#define PAYLOADLENGTHPOS   2        /**< Position of the payload length byte in an OL packet */
#define PAYLOADPOS         3        /**< Starting byte of the packet payload ("data") */
#define HEADERSIZE         3        /**< Size of the OL header, Msg Type, SID, payload length */
#define CHKSUMBYTE1OFFSET  3        /**< Added to the payload length to get position of 1st checksum byte in an OL packet */
#define CHKSUMBYTE2OFFSET  4        /**< Added to the payload length to get position of 2nd checksum byte in an OL packet */
#define ACKNACKLENGTH      2        /**< Size of a simple ack/nak OL message */
#define NORESPONSEEXPECTED 0        /**< Set the timeout timer to 0 to show LOL is not expecting a response */
#define PASSWORDLENGTH     8        /**< All passwords are 8 bytes long */
#define CHANGEPASSWORDLENGTH 10     /**< size of the message to change a password */
#define PAYLOADADDRPOS0    0        /**< Location of the high byte for the virtual memory map address */
#define PAYLOADADDRPOS1    1        /**< Location of the low byte for the virtual memory map address */
#define PAYLOADDATAPOS     2        /**< Location where the data starts in the payload for a common memory map */

#define READWRITEBIT    0x01        /**< Used to determine if this is a read or write request */

/*****************************************************************************
 * BOOLEAN TYPES
 *****************************************************************************/
#ifndef TRUE
    #define TRUE 	1U                  /**< Boolean True */
#endif

#ifndef FALSE
     #define FALSE 	0U                  /**< Boolean False */
#endif

/*****************************************************************************
 * NAK TYPES
 *****************************************************************************/
#define NAKPERMISSIONDENIED         0x01    /**< NAK message because requester does not have the correct permissions level */
#define NAKILLEGALMEMORYADDRESS     0x02    /**< NAK message because the memory address is not valid */
#define NAKILLEGALEXTMEMORYADDRESS  0x03    /**< NAK message because the extended memory address is not valid */
#define NAKMALFORMEDPACKET          0x04    /**< NAK message because the packet is not valid, noise? */
#define NAKUNKNOWNMSGTYPE           0x05    /**< NAK message because the message type is not supported */
#define NAKILLEGALVALUE             0x06    /**< NAK message because the value is illegal */
#define NAKBUFFERFULL               0x07    /**< NAK message because the buffer is full */
#define NAKPERSISTENTSTORAGEERROR   0x08    /**< NAK message because of a persistent storage error */
#define NAKFEATURENOTSUPPORTED      0x09    /**< NAK message because the feature is not supported */
#define NAKDEFERREDTIMEOUT          0x0A    /**< NAK message because a defer timed out */
#define NAKUNKNOWNSPECIFIER         0x0B    /**< NAK message because of an unknown specifier */
#define NAKUNKNOWNCLASS             0x0C    /**< NAK message because of an unknown class */
#define NAKUNKNOWNINSTANCE          0x0D    /**< NAK message because of an unknown instance */
#define NAKUNKNOWNPROPERTY          0x0E    /**< NAK message because of an unknown property */
#define NAKGARBLEDPACKET            0x0F    /**< NAK message because of a garbled packet */
#define NAKUNKNOWNERROR             0x10    /**< NAK message because of something unknown */
#define NAKMAXMESSAGE               0x10    /**< maximum NAK message */

#define READOLCOREVER               0x0069  /**< Address of Open Link Core Version in Common Memory Map */
#define OLCOREVER                   0x0200  /**< LOL Open Link Core Version */


//TODO - update the comms errors, delete what is not needed, add new if needed
/*! \enum eCommsErr An enumeration of errors that can occur during communications */
typedef enum
{
    eCommsErr_NONE = 0,                 /**< No Error  */
    eCommsErr_TX_byte_timeout,          /**< Tx transmission timeout                                - NP     */
    eCommsErr_TX_Invalid_packet,        /**< Tx packet error (bad pointer, invalid length, etc.)    - NP     */
    eCommsErr_packet_RX_timeout,        /**< Rx Receive time out  waiting for data                  - NP     */
    eCommsErr_bad_RX_cmd_byte,          /**< Rx of an invalid/unsupported command                   - MT     */
    eCommsErr_bad_RX_length,            /**< Rx packet length does not match packet length          - MT     */
    eCommsErr_bad_RX_crc,               /**< Rx packet fails checksum                               - MT     */
    eCommsErr_bad_RX_READ_request,      /**< Rx of read request of invalid/write only address       - MT     */
    eCommsErr_bad_RX_WRITE_request,     /**< Rx of write request of invalid/read only address       - MT     */
    eCommsErr_bad_RX_APP_CMD_Request,   /**< Rx of unknown Application level request                - MT     */
    eCommsErr_Not_in_map_Request,       /**< Rx of Memory Map request outside of MM region          - MT     */
    eCommsErr_Insufficient_Privelege,   /**< Rx of command requiring higher security privilege      - MT     */
    eCommsErr_no_RX_buffer_available,   /**< Rx of packet but Rx buffers are full                   - MT/NP  */
    eCommsErr_PI_RX_overflow,           /**< Rx overflow error                                      - NP     */
    eCommsErr_PI_RX_Com_Err,            /**< Rx generic/undefined Comms error                       - NP     */
    eCommsErr_PI_RX_BitFrameErr,        /**< Rx framing error                                       - NP     */
    eCommsErr_OL_byte_Transferred,      /**< Rx buffer case, indicates a single byte was successfully handed over to data buffer that is sent to OL_packet_rx - NP   */
    eCommsErr_OL_packet_Transferred,    /**< Rx buffer case, indicates a complete error free packet was handed over to data buffer that is sent to OL_packet_rx - NP */
    eCommsErr_TX_in_use,                /** The transmitter is in use                                        */
    eCommsErr_TX_Oversized_packet,      /** The transmitter is in use                                        */
    eCommsErr_Invalid_Channel,			/** Channel larger than NUMCHANNELS - 1 was passed in 				 */
    eCommsErr_MAX_ERROR_VAL             /**< Standard enum max value parameter for expansion                 */
} eCommsErr;

//TODO do we need more or less of these
/*! \enum eOL_MsgTypes enum of all allowable message types in a packet */
typedef enum
{
    eOPENLINK_BACKDOOR = 0x00,      //DO NOT USE, IF Detected in production code, pull fire alarm and scream!
    eLOCAL_MEM_ACCESS = 0x01,
    eLOL_MASTER_TEST = 0x02,        // LOL only - used to start a master side test
    eLOCAL_MEM_ACCESS_EXTENDED = 0x03,
    eRESERVED2 = 0x04,
    eOBJECT_ACCESS = 0x05,
    eOBJECT_INFO = 0x06,
    eGET_MAX_PKT_LEN = 0x11,
    eRESERVED3 = 0x12,
    eRESET_COMMS = 0x13,
    eAUTHENTICATION = 0x14,
    eRESERVED4 = 0x15,
    eRESERVED5 = 0x20,
    eRESERVED6 = 0x21,
    eAUTOBAUD_SYNC = 0x55,    // (Previously NOP)
    eCHARGER_CURRENT_INFO = 0x60,
    eMAXIMUM_CELL_VOLTAGE = 0x61,
    ePACK_STATUS_REQUEST = 0x62,
    eADAPTER_PACKET_TARGET = 0x70,
    eMANUFACTURING_TEST = 0x71,
    eMANUFACTURING_TEST_RESP = 0x72,
    eRESERVED7 = 0x73,
    eTRIAL_OTA_MESSAGES = 0x74,
    eBLE_ADVERTISEMENT = 0x75,
    eBLE_CONNECTION_STATUS = 0x76,
    eBLE_GENERIC_CMD = 0x77,
    eBLE_SET_BEACON_PARAMETERS = 0x78,
    eBLE_SET_LED_STATE = 0x79,
    eBLE_EVENT_NOTIFICATION = 0x7A,
    eBLE_DISCONNECT = 0x7B,
    eBLE_SET_SHIPPING_ADV_MODE = 0x7C,
    eBLE_SET_TOOL_TIME = 0x7D,
    eBLE_SYNC_TOOL_PHONE = 0x7E,
    eBLE_GET_FW_VERSION = 0x7F,
    eACK = 0x80,
    eACK_WITH_DATA = 0x81,
    eNAK = 0x82,
    eNAK_WITH_DATA = 0x83,
    eDEFERRED = 0x85,
    eCELL_VOLTAGE_REPORTING = 0x90,
    eOL_MsgTypes_MAX
} eOL_MsgTypes;

/*! \enum eOL_SecTypes enum of all allowable password common memory map addresses */
typedef enum
{
    eUSER_PASSWORD = 0x0043,
    eADMIN_PASSWORD = 0x004B,
    eSERVICE_PASSWORD = 0x0053,
    eMETCO_PASSWORD = 0x005B
} eOL_SecTypes;

/*! \enum eMM_Sec_Lev define the security levels */
typedef enum
{
    eMM_Sec_Lev__ALL,           // MUST always be the 1st entry
    eMM_Sec_Lev__USER,
    eMM_Sec_Lev__USER_ADMIN,
    eMM_Sec_Lev__SERVICE,
    eMM_Sec_Lev__MANUFACTURING,
    eMM_Sec_Lev__NONE,          // MUST always be the last entry
    eMM_Sec_Lev__MAX_SEC_LEVELS
} eMM_Sec_Lev;

/*! void (*errorCallback)(eCommsErr errorMessage)
 * \brief Error callback provided by the host to process a communications error
 *
 * \param[in] errorMessage - reason the error callback is being called
 * \param[in] channelNumber - number of the channel that generated the error
 */
typedef void (*errorCallback)(eCommsErr errorMessage, uint8_t channelNumber);

/*! (*completeCallback)(bool ackNack, uint8_t * payloadPtr, uint8_t payloadLength)
 * \brief Complete callback provided by the host
 *
 * \param[in] ackNack - true for an ACK, false for a NAK
 * \param[in] payloadPtr - pointer to the payload data
 * \param[in] payloadLength - length of the payload data
 * \param[in] channelNumber - number of the channel that received the message
 */
typedef void (*completeCallback)(bool ackNack, uint8_t * payloadPtr, uint8_t payloadLength, uint8_t channelNumber);

/*! (*slaveCallback)(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID, uint8_t payloadLength)
 * \brief callback function for processing an incoming slave message
 *
 * \param[in] payloadPtr - pointer to the received payload data
 * \param[in] messageID - received message
 * \param[in] SID - sequence ID of the message
 * \param[in] payloadLength - length of the payload data
 * \param[in] channelNumber - number of the channel that received the message
 */
typedef void (*slaveCallback)(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID, uint8_t payloadLength, uint8_t channelNumber);

/*! \struct slaveVals
* Structure that will be required for the host to provide for each OL command it will respond to
*/
typedef struct
{
    slaveCallback   rxSlaveCallback;        /**< provided callback to process received slave message */
    eMM_Sec_Lev     messageReadSecurity;    /**< required security level for a read of the message ID */
    eMM_Sec_Lev     messageWriteSecurity;   /**< required security level for a write of the message ID */
    uint8_t         messageID;              /**< allowed message ID */
    uint16_t        localMemAddr;           /**< local memory address, only used for a local memory address, 0x01 */
} slaveVals;

/*! \struct sendVals
* Structure that will be sent as output to the host's physical interface
*/
typedef struct
{
    errorCallback       txErrorCallBack;        /**< provided callback if there was an error sending the packet, for open link only errors */
    completeCallback    txCompleteCallback;     /**< provided callback if the message was completed, this will include any nacks */
    uint16_t            timeoutTime;            /**< time to wait for the transmit response, in milliseconds, if set to zero no response is expected */
    uint8_t             channelNum;             /**< number of the channel being used for communication */
    eOL_MsgTypes        OLMessage;              /**< message to be sent */
    uint8_t             *data;                  /**< pointer to the payload bytes to be sent */
    uint8_t             payloadLength;          /**< Payload length */
    bool                write;                  /**< true for a write transaction, false for a read transaction */
    bool                BLE;                    /**< True if the message is inteded for a BLE */
} sendVals;

/*! \struct ackTransactionVals
* Structure to be used when sending an ACK
*/
typedef struct
{
    uint8_t         channelNum;             /**< number of the channel being used for communication */
    uint8_t         SID;                    /**< System ID of the current in flight message that was sent */
    uint8_t         *data;                  /**< pointer to the data to be sent - set to zero for 0x80, ack with no data */
    uint8_t         payloadLength;          /**< Payload length */
    bool            ackNack;                /**< True for an Ack, false for a Nack */
} ackTransactionVals;

/*! \struct channelVals
* Structure that will be store the transactions data, one per channel
*/
typedef struct
{
    errorCallback       txErrorCallBack;        /**< provided callback if there was an error sending the packet, for open link only errors */
    completeCallback    txCompleteCallback;     /**< provided callback if the message was completed, this will include and nacks */
    const slaveVals*    slaveValues;            /**< Pointer to the array of valid OL commands for this slave channel */
    uint8_t *           txBuffer;               /**< Pointer to the transmit buffer for this channel */
    uint8_t *           rxBuffer;               /**< Pointer to the receive buffer for this channel */
    uint32_t            securityTimeLeft;       /**< time left until until security level is reset to the lowest level, in milliseconds */
    uint16_t            timeLeft;               /**< time left until until timeout is called, in milliseconds */
    uint16_t            timeoutTime;            /**< time to wait for the transmit response, in milliseconds, if set to zero no response is expected */
    uint8_t             numSlaveValues;         /**< Number of OL commands this channel will respond to */
    uint8_t             txBufferSize;           /**< Size of the transmit buffer for this channel */
    uint8_t             rxBufferSize;           /**< Size of the receive buffer for this channel */
    uint8_t             channelNum;             /**< number of the channel being used for communication */
    uint8_t             SID;                    /**< System ID of the current in flight message that was sent */
    uint8_t             nextSID;                /**< What the next SID will be based on */
    uint8_t             packetLength;           /**< length of received packet to be processed */
    eMM_Sec_Lev         channelSecurity;        /**< Security level of the channel */
    bool                inFlight;               /**< Do we have a packet in flight?? */
    bool                dataRX;                 /**< Received data to be processed */
} channelVals;

// Defines for mailboxing
#define MAILBOXVIRTUALADDRESS1  0
#define MAILBOXVIRTUALADDRESS2  1
#define MAILBOXMESSAGETYPE      2
#define MAILBOXSID              3
#define TXMAILBOXOVERHEAD       3

/*! \struct mailboxVals
* Structure that will be store the required values to properly execute a mailbox request
*/
typedef struct
{
    uint8_t         originalSID;            /**< SID value that will be used in the ACK/NAK back to the mailbox originator */
    uint8_t         BLESID;                 /**< SID value that was originally intended for the BLE, it will be used when the response data is requested */
    uint8_t         *data;                  /**< pointer to the data that was returned */
    uint8_t         length;                 /**< number of bytes that were returned */
} mailboxVals;

// Provided for the host
bool LOLCompare(const uint8_t *s1, const uint8_t *s2, uint32_t len);
void LOLChangeSecurity(uint8_t channelNumber, eMM_Sec_Lev newSecurityLevel, uint32_t timeoutTime);
void LOLTimerTick(void);
void LOLInitChannel(uint8_t channelNumber, uint8_t * txBuffer, uint8_t txBuffSize, uint8_t * rxBuffer, uint8_t rxBuffSize, const slaveVals * slaveArray, uint8_t numSlaveEntries);
eCommsErr LOLSend(sendVals * sendValues);
eCommsErr LOLSendACK(ackTransactionVals * sendValues);
eCommsErr LOLSendLocalMemAccess(sendVals * sendValues);
eCommsErr LOLSendLocalMemAccessExt(sendVals * sendValues);
uint64_t OL_Bytes2U64(uint8_t* data);

// The Host will need to provide this
/** \fn	eCommsErr Tool_data_tx(uint8_t *data, uint8_t data_len, uint8_t target);
*
* \brief    Tool side function used by the library to transmit data
*
* \details  This function is exposed from the main calling program. It is expected to
*			be called by the library to send OL packets for data transmission
*
*			\param data		- input array/pointer of bytes
*			\param data_len	- length of input array
*			\param target	- source of packet, channel number
*			\return			- a eCommsErr with the result of processing the send request.
*/
eCommsErr Tool_data_tx(uint8_t *data, uint8_t packet_len, uint8_t target); // Function to send generic data out via the target source.

// The Host will need to provide this
/** \fn	void debug_assert(bool expression);
*
* \brief    Tool side function used by the library to handle assertions
*
* \details  This function is exposed from the main calling program. It is expected to
*			be called by the library to handle assertions
*
*			\param expression	- the expression to be asserted
*			\return			- void
*/
void debug_assert(bool expression); // Function to handle assertions and trip watchdog

#endif /* LOLCONFIG_H_ */
