/*!
 * \file OpenLinkBase.c
 * \date Created: 9/11/2018 12:39:25 PM
 * \author Author: jwalsh
 */
#include "OpenLinkBase.h"
#include "LOLPacketTransmit.h"
#include "LOLPacketReceive.h"

/** Library Globals **/
NVM_OL_runtime_data OL_runtime_data;    /**< runtime data */

const uint32_t LIB_DATE=0x20191030;     /**< release date */
const uint32_t LIB_TIME=1572495494U;    /**< release time */


/**
 * \fn LOLTimerTick()
 *
 * \brief Called by the host to provide timing for LOL.
 *
*/
void LOLTimerTick()
{
    //TODO - add Privilege Manager
    lolCheckSecurityTimeout();

    // Check receive Buffers
    lolCheckRXBuffer();

    // Check Transmission timeout
    lolCheckTimeout();

    //TODO - Update the POSIX clock

	return;
}

/*********************************************************************************//*!
 * \fn LOLChangeSecurity(uint8_t channelNumber, eMM_Sec_Lev newSecurityLevel, uint32_t timeoutTime)
 *
 * \brief Change the security level for the channel and start the security level timer
 *
 * \param[in] channelNumber the channel to initialize
 * \param[in] newSecurityLevel the new desired level of security for the channel
 * \param[in] timeoutTime the amount of time in milliseconds until the security level is changed to eMM_Sec_Lev__ALL
 *
 ************************************************************************************/
void LOLChangeSecurity(uint8_t channelNumber, eMM_Sec_Lev newSecurityLevel, uint32_t timeoutTime)
{
    if(channelNumber > NUMCHANNELS - 1)
    {
        // Invalid channel, assert!
        debug_assert(false);
        return;	// Will only be hit in unit test environment
    }
    
    // is the security level valid?
    if (newSecurityLevel < eMM_Sec_Lev__MAX_SEC_LEVELS)
    {
        lolTransaction[channelNumber].channelSecurity = newSecurityLevel;
        lolTransaction[channelNumber].securityTimeLeft = timeoutTime;
    }
}

/**
 * \fn lolCheckSecurityTimeout()
 *
 * \brief Reset security level if the timer has expired, this is on a per channel basis
 *
 * \param[in] none
*/
void lolCheckSecurityTimeout()
{
    uint8_t i;
    for (i = 0; i < NUMCHANNELS; i++)
    {
        if (lolTransaction[i].securityTimeLeft > 0)
        {
            if (--lolTransaction[i].securityTimeLeft == 0)
            {
                // We have timed out, reset channel security to the lowest level
                lolTransaction[i].channelSecurity = eMM_Sec_Lev__ALL;
            }
        }
    }
}

/** Libary Helper Functions **/


/**
 * \fn OL_Bytes2U64(uint8_t* data)
 *
 * \brief Convert uint8_t array values to uint64_t little endian
 *
*/
uint64_t OL_Bytes2U64(uint8_t* data)
{
uint64_t result = 0x0000000000000000;

    result |= (uint64_t) (*(data+7)) << 56;
    result |= (uint64_t) (*(data+6)) << 48;
    result |= (uint64_t) (*(data+5)) << 40;
    result |= (uint64_t) (*(data+4)) << 32;
    result |= (uint64_t) (*(data+3)) << 24;
    result |= (uint64_t) (*(data+2)) << 16;
    result |= (uint64_t) (*(data+1)) << 8;
    result |= (uint64_t) (*(data+0));

	return result;
}

/**
 * \fn OL_Bytes2U32(uint8_t* data)
 *
 * \brief Convert uint8_t to uint32_t
 *
*/
uint32_t OL_Bytes2U32(uint8_t* data)
{
	uint32_t result = 0x00000000;

	result |= (uint32_t) (*(data+0)) << 24;
	result |= (uint32_t) (*(data+1)) << 16;
	result |= (uint32_t) (*(data+2)) << 8;
	result |= (uint32_t) (*(data+3));

	return result;
}

/**
 * \fn OL_U32_2_Buff(uint32_t val, uint8_t* buffer,uint8_t reverse_endianess)
 *
 * \brief change endianess of uint32_t value
 *
*/
void OL_U32_2_Buff(uint32_t val, uint8_t* buffer,uint8_t reverse_endianess)
{
	uint32_t b0,b1,b2,b3;
	if(reverse_endianess==TRUE)
	{
		//val= __bigendian__(val);  should make this a macro to call like this
		b0 = (val & 0x000000ff) << 24U;
		b1 = (val & 0x0000ff00) << 8U;
		b2 = (val & 0x00ff0000) >> 8U;
		b3 = (val & 0xff000000) >>24U;

		val = b0 | b1 | b2 | b3;
	}

	//Now put it in the buffer:

	*(buffer+0) = (val >> 24)  & 0xFF;
	*(buffer+1) = (val >> 16)  & 0xFF;
	*(buffer+2) = (val >> 8) & 0xFF;
	*(buffer+3) = val & 0xFF;

}

/**
 * \fn OL_U16_2_Buff(uint16_t val, uint8_t* buffer,uint8_t reverse_endianess)
 *
 * \brief change endianess of uint16_t value
 *
*/
void OL_U16_2_Buff(uint16_t val, uint8_t* buffer,uint8_t reverse_endianess)
{
	if(reverse_endianess==TRUE)
	{
		val= (((val & 0x00FF) << 8) | ((val & 0xFF00) >> 8));	//just swap the bytes
	}

	//Now put it in the buffer:

	*(buffer+0) = (val >> 8) & 0xFF;
	*(buffer+1) = val & 0xFF;

}

/*********************************************************************************//*!
 * \fn LOLCompare(const uint8_t *s1, const uint8_t *s2, uint32_t len)
 *
 * \brief Verify the contents of two arrays in a secure maner
 *
 * \param[in] s1 pointer to 1st array to be compared
 * \param[in] s2 pointer to 2nd array to be compared
 * \param[in] len length of the arrays to be compared
 *
 * \return bool return true if they are equal, false if they are different
 *
 ************************************************************************************/
bool LOLCompare(const uint8_t *s1, const uint8_t *s2, uint32_t len)
{
    uint32_t x = 0;
    volatile uint8_t result = 0; //setting to volatile to get the compiler to keep its mitts off of my memory accesses
    //Don't directly compare s1 and s2. Operations must be done in constant time to preserve security.
    for( x = 0; x< len; x++)
    {
        result |= (s1[x] ^ s2[x]);
    }

    return result == 0 ? true : false; //return true if result is 0, return false otherwise.
}
