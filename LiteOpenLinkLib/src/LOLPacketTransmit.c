 /*********************************************************************************//*!
 * \file  LOLPacketTransmit.c
 *
 * \brief Generate Open Link Packet to be sent to the correct channel of the Physical Interface for transmission.
 * \details Host processor will call LOLSendPacket, or an associated helper function, with a pointer to a structure that contains:
 *   - A pointer to the Master error callback that takes the error code as input
 *   - A pointer to the Master complete callback that takes a pointer to the returned data buffer as input
 *   - A unit8_t Channel number that indicates the channel that is being used
 *   - A Message type and payload
 *   - A pointer to any data that is to be sent, NULL if none
 *   - A Boolean to show if this is a read or a write
 *   - A Boolean for the Tool/BLE routing bit (MSB of SID)
 *
 * Using this information generate a packet to be sent to the Host’s Physical Interface.
 *
 * Include functions to:
 * - generate checksum
 * - generate Sequence ID (SID)
 * - helper function to create/send LocalMemoryAccess[0x01]
 * - helper function to create/send ExtendedMemoryAccess[0x03])
 * - helper functions for creating Naks and Acks
 *
 *\author Original by:  Al Lukowitz
 *
 *\date 1/28/2020       (AL) Original Creation
  *
 *\copyright Copyright (c) 2020 by Milwaukee Tool. All rights reserved.
 ************************************************************************************/
#include "LOLPacketTransmit.h"

/** \var channelVals lolTransaction[NUMCHANNELS]
    \brief An array holding the status of master transacations for each channel
*/
channelVals lolTransaction[NUMCHANNELS];

/**
 * \fn LOLInitChannel(uint8_t channelNumber, uint8_t * txBuffer, uint8_t txBuffSize, uint8_t * rxBuffer, uint8_t rxBuffSize, const slaveVals * slaveArray, uint8_t numSlaveEntries)
 *
 * \brief Used by the host to initialize a channel's transaction packet.  This must be called
 *        before transmission of packets.
 *
 * \param[in] channelNumber the channel to initialize
 * \param[in] txBuffer pointer to the host provided transmit buffer
 * \param[in] txBuffSize the size of the host provided transmit buffer
 * \param[in] rxBuffer pointer to the host provided receive buffer
 * \param[in] rxBuffSize the size of the host provided receive buffer
 * \param[in] slaveArray pointer to the slaveVals array used to process received OL messages
 * \param[in] numSlaveEntries the size of the host provided slaveArray
*/
void LOLInitChannel(uint8_t channelNumber, uint8_t * txBuffer, uint8_t txBuffSize, uint8_t * rxBuffer, uint8_t rxBuffSize, const slaveVals * slaveArray, uint8_t numSlaveEntries)
{
    if (channelNumber <= NUMCHANNELS - 1)
    {
        // Valid channel, initialze it!
        lolTransaction[channelNumber].channelNum = channelNumber;
        lolTransaction[channelNumber].inFlight = false;
        lolTransaction[channelNumber].nextSID = 0x01;
        lolTransaction[channelNumber].SID = 0x01;
        lolTransaction[channelNumber].txErrorCallBack = NULL;
        lolTransaction[channelNumber].txCompleteCallback = NULL;
        lolTransaction[channelNumber].channelSecurity = eMM_Sec_Lev__ALL;
        lolTransaction[channelNumber].dataRX = false;
        lolTransaction[channelNumber].packetLength = 0;
        lolTransaction[channelNumber].txBuffer = txBuffer;
        lolTransaction[channelNumber].txBufferSize = txBuffSize;
        lolTransaction[channelNumber].rxBuffer = rxBuffer;
        lolTransaction[channelNumber].rxBufferSize = rxBuffSize;
        lolTransaction[channelNumber].slaveValues = slaveArray;
        lolTransaction[channelNumber].numSlaveValues = numSlaveEntries;
        lolTransaction[channelNumber].timeoutTime = NORESPONSEEXPECTED;
        lolTransaction[channelNumber].timeLeft = 0;
        lolTransaction[channelNumber].securityTimeLeft = 0;
    }
    else
    {
        // Invalid channel, assert!
        debug_assert(false);
        return;	// Will only be hit in unit test environment
    }
}

/**
 * \fn LOLSendACK(ackTransactionVals *)
 *
 * \brief Used by the host and by LOL to generate all NACK and ACK messages using the data provided.
 *
 * \param[in] sendValues Pointer to the ackTransactionVals structure that contains all the information about the ACK/NAK
 *
 * \return eCommsErr - either eCommsErr_NONE or the error
*/
eCommsErr LOLSendACK(ackTransactionVals * sendValues)
{
    uint8_t packetLength=ACKNACKLENGTH;   // Most will be simple Ack or Nack

    if(sendValues->channelNum > NUMCHANNELS - 1)
    {
        // Invalid channel, assert!
        debug_assert(false);
        return eCommsErr_Invalid_Channel;	// Will only be hit in unit test environment
    }

    if (lolTransaction[sendValues->channelNum].inFlight == true)
    {
        // In use, let the sender know - they can always try again later
        return eCommsErr_TX_in_use;
    }

    lolTransaction[sendValues->channelNum].txBuffer[MESSAGESIDPOS] = sendValues->SID;

    if (sendValues->payloadLength > 0)
    {
        // Make sure our buffer is big enough
        if (sendValues->payloadLength + TX_OVERHEAD_SIZE >= lolTransaction[sendValues->channelNum].txBufferSize)
        {
            return eCommsErr_TX_Oversized_packet;
        }
        else if (sendValues->ackNack == true)
        {
            lolTransaction[sendValues->channelNum].txBuffer[MESSAGEIDPOS] = eACK_WITH_DATA;
        }
        else
        {
            // NAK with data needs to be 2 bytes long, if not complain!!
            if (sendValues->payloadLength != 2)
            {
                return eCommsErr_TX_Invalid_packet; // Packet is not valid if it is not 2 bytes long
            }
            else if (sendValues->data[0] != NAKMAXMESSAGE)
            {
                // NAKMAXMESSAGE must be in the first data position for a NAK with data
                return eCommsErr_TX_Invalid_packet; // Packet is not valid
            }
            else if (sendValues->data[1] > NAKMAXMESSAGE)
            {
                // NAK message must be less than or equal to NAKMAXMESSAGE
                return eCommsErr_TX_Invalid_packet; // Packet is not valid
            }
            lolTransaction[sendValues->channelNum].txBuffer[MESSAGEIDPOS] = eNAK_WITH_DATA;
        }

        // Move payload length over
        lolTransaction[sendValues->channelNum].txBuffer[PAYLOADLENGTHPOS] = sendValues->payloadLength;

        // Move the payload over
        for (int i = 0; i < sendValues->payloadLength; i++)
        {
            lolTransaction[sendValues->channelNum].txBuffer[i+PAYLOADPOS] = sendValues->data[i];
        }

        // Generate the checksum
        uint16_t csum = lolGenerateChecksum(lolTransaction[sendValues->channelNum].txBuffer, sendValues->payloadLength+HEADERSIZE);
        lolTransaction[sendValues->channelNum].txBuffer[sendValues->payloadLength+CHKSUMBYTE1OFFSET] = (uint8_t)(csum >> 8);
        lolTransaction[sendValues->channelNum].txBuffer[sendValues->payloadLength+CHKSUMBYTE2OFFSET] = (uint8_t)(csum);

        packetLength = TX_OVERHEAD_SIZE + sendValues->payloadLength;
    }
    else
    {
        // Just send the ack/nack and sid message
        if (sendValues->ackNack == true)
        {
            lolTransaction[sendValues->channelNum].txBuffer[MESSAGEIDPOS] = eACK;
        }
        else
        {
            lolTransaction[sendValues->channelNum].txBuffer[MESSAGEIDPOS] = eNAK;
        }
    }
    // There should not be a response
    lolTransaction[sendValues->channelNum].timeoutTime = NORESPONSEEXPECTED;
    return (Tool_data_tx(lolTransaction[sendValues->channelNum].txBuffer,packetLength,sendValues->channelNum));    // this is a tool provided function
}

/**
 * \fn LOLSend(sendVals *)
 *
 * \brief Used by the host and by LOL to generate an open link message to be sent.  It calls
 *        the host provided function Tool_data_tx() to interface with the physical layer.
 *
 * \param[in] sendValues Pointer to the sendVals structure that holds the values to be transmitted
 *
 * \return eCommsErr - either eCommsErr_NONE or the error
*/
eCommsErr LOLSend(sendVals * sendValues)
{
    if(sendValues->channelNum > NUMCHANNELS - 1)
    {
        // Invalid channel, assert!
        debug_assert(false);
        return eCommsErr_Invalid_Channel;	// Will only be hit in unit test environment
    }

    if (lolTransaction[sendValues->channelNum].inFlight == true)
    {
        // In use, let the sender know - they can always try again later
        return eCommsErr_TX_in_use;
    }
    else if (sendValues->payloadLength + TX_OVERHEAD_SIZE >= lolTransaction[sendValues->channelNum].txBufferSize)
    {
        return eCommsErr_TX_Oversized_packet;
    }

	eCommsErr err;
	uint16_t csum;
    uint8_t i;

    // Generate the message to be transmitted
    lolTransaction[sendValues->channelNum].txBuffer[MESSAGEIDPOS] = sendValues->OLMessage;

    // Set time to wait for the ACK/NAK response
    lolTransaction[sendValues->channelNum].timeoutTime = sendValues->timeoutTime;

    // Generate the SID
    lolTransaction[sendValues->channelNum].SID = lolGetNextSequenceID(sendValues->channelNum);
    lolTransaction[sendValues->channelNum].SID += (uint8_t)sendValues->write;                   // set read/write bit
    lolTransaction[sendValues->channelNum].SID += (uint8_t)(sendValues->BLE << SIDBLEBITPOS);   // set BLE bit
    lolTransaction[sendValues->channelNum].txBuffer[MESSAGESIDPOS] = lolTransaction[sendValues->channelNum].SID;

    // Generate the pay load length
    lolTransaction[sendValues->channelNum].txBuffer[PAYLOADLENGTHPOS] = sendValues->payloadLength;

    // Move the payload over
    for (i = 0; i < sendValues->payloadLength; i++)
    {
        lolTransaction[sendValues->channelNum].txBuffer[i+3] = sendValues->data[i];
    }

    // Generate the checksum
    csum = lolGenerateChecksum(lolTransaction[sendValues->channelNum].txBuffer, sendValues->payloadLength+HEADERSIZE);

    lolTransaction[sendValues->channelNum].txBuffer[sendValues->payloadLength+CHKSUMBYTE1OFFSET] = (uint8_t)(csum >> 8);
    lolTransaction[sendValues->channelNum].txBuffer[sendValues->payloadLength+CHKSUMBYTE2OFFSET] = (uint8_t)(csum);

    lolTransaction[sendValues->channelNum].inFlight = true;

    lolTransaction[sendValues->channelNum].txErrorCallBack = sendValues->txErrorCallBack;
    lolTransaction[sendValues->channelNum].txCompleteCallback = sendValues->txCompleteCallback;

	err=Tool_data_tx(lolTransaction[sendValues->channelNum].txBuffer,sendValues->payloadLength+TX_OVERHEAD_SIZE,sendValues->channelNum);    // this is a tool provided function

	return err;
}

/**
 * \fn LOLSendLocalMemAccessExt(sendVals *)
 *
 * \brief Uses the input data to generate an extended local memory access request by calling LOLSend
 *
 * \param[in] sendValues Pointer to the sendVals structure that holds the values to be transmitted
 *
 * \return eCommsErr - either eCommsErr_NONE or the error
*/
eCommsErr LOLSendLocalMemAccessExt(sendVals * sendValues)
{
    sendValues->OLMessage = eLOCAL_MEM_ACCESS_EXTENDED;
    return(LOLSend(sendValues));
}

/**
 * \fn LOLSendLocalMemAccess(sendVals *)
 *
 * \brief Uses the input data to generate a local memory access request by calling LOLSend
 *
 * \param[in] sendValues Pointer to the sendVals structure that holds the values to be transmitted
 *
 * \return eCommsErr - either eCommsErr_NONE or eCommsErr_bad_RX_crc
*/
eCommsErr LOLSendLocalMemAccess(sendVals * sendValues)
{
    sendValues->OLMessage = eLOCAL_MEM_ACCESS;
    return(LOLSend(sendValues));
}

/**
 * \fn lolGenerateChecksum(uint8_t * , uint8_t)
 *
 * \brief Return the uint16_t checksum for the packet
 *
 * \param[in] inputData Pointer to the data array
 * \param[in] length Number of entries in the data array
 *
 * \return uint16_t generated checksum
*/
uint16_t lolGenerateChecksum(uint8_t * inputData, uint8_t length)
{
    uint16_t csum=0;
    uint8_t i;
    for (i = 0; i < length; i++)
    {
        csum += inputData[i];
    }
	return csum;
}

/**
 * \fn lolGetNextSequenceID(uint8_t)
 *
 * \brief Return the next sequence ID to be used
 *
 * \param[in] channelNumber the channel that needs the new sequence ID
 *
 * \return uint8_t nextSID
*/
uint8_t lolGetNextSequenceID(uint8_t channelNumber)
{
    if(channelNumber > NUMCHANNELS - 1)
    {
        // Invalid channel, assert!
        debug_assert(false);
        return 0xFF;	// Will only be hit in unit test environment
    }

    lolTransaction[channelNumber].nextSID += 1U;
    lolTransaction[channelNumber].nextSID &= SIDBITS;
    if (lolTransaction[channelNumber].nextSID == 0U)
    {
        lolTransaction[channelNumber].nextSID = 1U;
    }
    return lolTransaction[channelNumber].nextSID << 2;
}

/**
 * \fn uartTxCallback(uint8_t)
 *
 * \brief Called when transmission is complete, show the TX line is available and set the timeout flag
 *
 * \param[in] channel the channel that has completed transmission of data
*/
void uartTxCallback(uint8_t channel)
{
    if(channel > NUMCHANNELS - 1)
    {
        // Invalid channel, assert!
        debug_assert(false);
        return;	// Will only be hit in unit test environment
    }

    lolTransaction[channel].inFlight = false;
    lolTransaction[channel].timeLeft = lolTransaction[channel].timeoutTime;
}

/**
 * \fn lolCheckTimeout()
 *
 * \brief Check to see if there has been a timeout
 *
 * \param[in] none
*/
void lolCheckTimeout()
{
    uint8_t i;
    for (i = 0; i < NUMCHANNELS; i++)
    {
        if (lolTransaction[i].timeLeft > 0)
        {
            if (--lolTransaction[i].timeLeft == 0)
            {
                if (lolTransaction[i].txErrorCallBack != NULL)
                {
                    // We have timed out, call our error handler, if we have one, with the timeout message
                    lolTransaction[i].txErrorCallBack(eCommsErr_TX_byte_timeout, i);
                }
            }
        }
    }
}
