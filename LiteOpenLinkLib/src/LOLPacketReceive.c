 /*********************************************************************************//*!
 * \file  LOLPacketReceive.c
 *
 * \brief Process a received Open Link Packet
 * \details Check that the received packet is formatted correctly and processes appropriately \n
 * If the message type is a NAK or an ACK
 * -     Check if the master is waiting for it, if not forget about it
 * -     If so, check if the packet is valid, if not call the master’s error callback processing
 * -     Otherwise, call the master’s complete callback with the message attached
 *
 * For all other message types perform slave processing
 * -     Verify the packet is valid
 * -     Verify the sender has the correct privilege level
 * -     Send message to the Host for processing
 *
 *
 *\author Original by:  Al Lukowitz
 *
 *\date 2/07/2020       (AL) Original Creation
  *
 *\copyright Copyright (c) 2020 by Milwaukee Tool. All rights reserved.
 ************************************************************************************/
#include "LOLPacketReceive.h"

/*********************************************************************************//*!
 * \fn uartRxCallback(uint8_t * inputData, uint8_t channelNumber, uint8_t length)
 *
 * \brief Called from the receive interrupt, move the received data to the rxBuffer for
 *        processing.  Set appropriate flag and length in the lolTransaction structure.
 *
 * \param[in] inputData - pointer to the received packet
 * \param[in] channelNumber - channel number to process
 * \param[in] length - amount of data to be processed
 *
 ************************************************************************************/
void uartRxCallback(uint8_t * inputData, uint8_t channelNumber, uint8_t length)
{
    uint8_t packetlength = inputData[PAYLOADLENGTHPOS] + TX_OVERHEAD_SIZE;  // Add 5 for the  header and checksum

    if(channelNumber > NUMCHANNELS - 1)
    {
        // Invalid channel, assert!
        debug_assert(false);
        return;	// Will only be hit in unit test environment
    }

    if((inputData[MESSAGEIDPOS] == eNAK ) || (inputData[MESSAGEIDPOS] == eACK))
    {
        packetlength = ACKNACKLENGTH;
    }

    if (lolTransaction[channelNumber].dataRX == false)
    {
        // Do we have room for the packet?
        if (packetlength <= lolTransaction[channelNumber].rxBufferSize)
        {
            // Move the data, set the flag, and hit the road
            memcpy(lolTransaction[channelNumber].rxBuffer, inputData, packetlength);
            lolTransaction[channelNumber].dataRX = true;
            lolTransaction[channelNumber].packetLength = length;
        }
        // else - just forget about it, we don't have the room for it
    }
}

/*********************************************************************************//*!
 * \fn lolCheckRXBuffer()
 *
 * \brief Did we get a buffer that needs to be processed?  If so, process it!
 *
 ************************************************************************************/
void lolCheckRXBuffer()
{
    uint8_t i;
    for (i = 0; i < NUMCHANNELS; i++)
    {
        if (lolTransaction[i].dataRX == true)
        {
            lolProcessRXBuffer(i);
            lolTransaction[i].dataRX = false;
        }
    }
}

/*********************************************************************************//*!
 * \fn lolProcessRXBuffer(uint8_t channelNumber)
 *
 * \brief Process received data
 *
 * \param[in] channelNumber - channel number to process
 *
 ************************************************************************************/
void lolProcessRXBuffer(uint8_t channelNumber)
{
    if(channelNumber > NUMCHANNELS - 1)
    {
            // Invalid channel, assert!
            debug_assert(false);
            return;	// Will only be hit in unit test environment
    }

    eCommsErr err=eCommsErr_NONE;
    uint8_t packetlength = lolTransaction[channelNumber].rxBuffer[PAYLOADLENGTHPOS] + TX_OVERHEAD_SIZE;


    if((lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eNAK ) || (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eACK) ||
       (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eNAK_WITH_DATA ) || (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eACK_WITH_DATA))
    {
        // Master Processing!
        // Is this one we are waiting for?
        if ((lolTransaction[channelNumber].timeLeft > 0) && (lolTransaction[channelNumber].SID == lolTransaction[channelNumber].rxBuffer[MESSAGESIDPOS]))
        {
            // We've been waiting for you!
            lolTransaction[channelNumber].timeLeft = 0;     // we are not waiting for it anymore!
            // Is this an ACK or a NACK, we need to know because there will be no length byte
            if((lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eNAK) || (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eACK))
            {
                // Simple ack/nak, just send it back to the caller
                if (lolTransaction[channelNumber].txCompleteCallback != NULL)
                {
                    bool ackNak = (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eACK ? true : false);
                    lolTransaction[channelNumber].txCompleteCallback(ackNak, NULL, 0, channelNumber);
                }
            }
            else
            {
                // ack or nak with data
                // Make sure it is a valid packet

                // verify checksum
                err = lolPacketCsumCheck(lolTransaction[channelNumber].rxBuffer);
                if(err!=eCommsErr_NONE)
                {
                    if (lolTransaction[channelNumber].txErrorCallBack != NULL)
                    {
                        lolTransaction[channelNumber].txErrorCallBack(err, channelNumber);
                    }
                }
                // Check the length next
                else if (packetlength != lolTransaction[channelNumber].packetLength)
                {
                    if (lolTransaction[channelNumber].txErrorCallBack != NULL)
                    {
                        lolTransaction[channelNumber].txErrorCallBack(eCommsErr_bad_RX_length, channelNumber);
                    }
                }
                else
                {
                    // Valid packet, send it back to the host
                    if (lolTransaction[channelNumber].txCompleteCallback != NULL)
                    {
                        bool ackNak = (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eACK_WITH_DATA ? true : false);
                        lolTransaction[channelNumber].txCompleteCallback(ackNak, &lolTransaction[channelNumber].rxBuffer[PAYLOADPOS], lolTransaction[channelNumber].rxBuffer[PAYLOADLENGTHPOS], channelNumber);
                    }
                }
            }
        }
        // Nothing else to do, hit the road.  If we were not waiting for this message, we will quietly forget about it
    }
    else
    {
        uint8_t nakPayload[2];     // Prepare NAK, just incase!!
        // Slave processing
        ackTransactionVals nackVals;
        nackVals.ackNack = false;
        nackVals.channelNum = channelNumber;
        nackVals.data = nakPayload;
        nackVals.payloadLength = 2;
        nakPayload[0] = NAKMAXMESSAGE;
        nackVals.SID = lolTransaction[channelNumber].rxBuffer[MESSAGESIDPOS];

        // Is this a valid packet?

        // verify checksum
        err = lolPacketCsumCheck(lolTransaction[channelNumber].rxBuffer);
        // if err != eCommsErr_NONE ignore it, we don't trust the SID to NAK it
        if(err==eCommsErr_NONE)
        {
            // Check the length next
            if (packetlength != lolTransaction[channelNumber].packetLength)
            {
                // Nack it!
                nakPayload[1] = NAKMALFORMEDPACKET;
                LOLSendACK(&nackVals);
            }
            else
            {
                bool found = false;
                uint8_t receivedMessageID = lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS];
                uint16_t localMemoryAddress = (lolTransaction[channelNumber].rxBuffer[PAYLOADPOS] << 8) + lolTransaction[channelNumber].rxBuffer[PAYLOADPOS+1];

                // if this is a Open Link Core Version Read Request we will handle it ourselves!
                if (receivedMessageID == eLOCAL_MEM_ACCESS && localMemoryAddress == READOLCOREVER)
                {
                    uint16_t ackVal = OLCOREVER;
                    // Check if this is a read, a write will need a NAK!!
                    if (lolTransaction[channelNumber].rxBuffer[MESSAGESIDPOS] & READWRITEBIT == 0x01)
                    {
                        nakPayload[1] = NAKPERMISSIONDENIED;
                    }
                    else
                    {
                        // Send the ack with data
                        nackVals.ackNack = true;
                        nackVals.data[0] = ackVal >> 8;
                        nackVals.data[1] = ackVal;
                    }
                    LOLSendACK(&nackVals);
                }
                else
                {
                    // Valid packet, does our host want it?
                    for (int i = 0; i < lolTransaction[channelNumber].numSlaveValues; i++)
                    {
                        if (lolTransaction[channelNumber].slaveValues[i].messageID == receivedMessageID)
                        {
                            // LOCAL_MEM_ACCESS is special
                            if (receivedMessageID == eLOCAL_MEM_ACCESS)
                            {
                                // Then we need to check the address in the first two payload bytes

                                if (localMemoryAddress != lolTransaction[channelNumber].slaveValues[i].localMemAddr)
                                {
                                    continue;   // not the one we want, skip the rest of the loop
                                }
                            }

                            // check security!!
                            eMM_Sec_Lev requiredSecurity = lolTransaction[channelNumber].slaveValues[i].messageReadSecurity;
                            if (lolTransaction[channelNumber].rxBuffer[MESSAGESIDPOS] & READWRITEBIT == 0x01)
                            {
                                // Then we need the write security level
                                requiredSecurity = lolTransaction[channelNumber].slaveValues[i].messageWriteSecurity;
                            }

                            if (requiredSecurity == eMM_Sec_Lev__NONE)
                            {
                                // if the security level is eMM_Sec_Lev__NONE, just hit the road, we don't want to respond to it!!
                                return;
                            }

                            if (lolTransaction[channelNumber].channelSecurity >= requiredSecurity)
                            {
                                // The host wants this one, call the callback!!
                                lolTransaction[channelNumber].slaveValues[i].rxSlaveCallback(&lolTransaction[channelNumber].rxBuffer[PAYLOADPOS],
                                                                                             lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS],
                                                                                             lolTransaction[channelNumber].rxBuffer[MESSAGESIDPOS],
                                                                                             lolTransaction[channelNumber].rxBuffer[PAYLOADLENGTHPOS],
                                                                                             channelNumber);
                            }
                            else
                            {
                                // Nack it!
                                nakPayload[1] = NAKPERMISSIONDENIED;
                                LOLSendACK(&nackVals);
                            }
                            found = true;
                            break;
                        }
                    }
                    if (found == false)
                    {
                        // NAK IT!!
                        if (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eLOCAL_MEM_ACCESS)
                        {
                            nakPayload[1] = NAKILLEGALMEMORYADDRESS;
                        }
                        else if (lolTransaction[channelNumber].rxBuffer[MESSAGEIDPOS] == eLOCAL_MEM_ACCESS_EXTENDED)
                        {
                            nakPayload[1] = NAKILLEGALEXTMEMORYADDRESS;
                        }
                        else
                        {
                            nakPayload[1] = NAKUNKNOWNMSGTYPE;
                        }
                        LOLSendACK(&nackVals);
                    }
                }
            }
        }
    }
}

/*********************************************************************************//*!
 * \fn lolPacketCsumCheck(uint8_t * inputPacket)
 *
 * \brief Verify the received packet's checksum is valid, if not return eCommsErr_bad_RX_crc
 *
 * \details - This is not to be used on eACK or eNAK, they don't have checksums
 *
 * \param[in] inputPacket - pointer to the received input packet
 *
 * \return eCommsErr - either eCommsErr_NONE or eCommsErr_bad_RX_crc
 *
 ************************************************************************************/
eCommsErr lolPacketCsumCheck(uint8_t * inputPacket)
{
    eCommsErr err = eCommsErr_NONE;
    uint16_t receivedChecksum = inputPacket[inputPacket[PAYLOADLENGTHPOS] + CHKSUMBYTE1OFFSET];
    receivedChecksum = receivedChecksum << 8;
    receivedChecksum += inputPacket[inputPacket[PAYLOADLENGTHPOS] + CHKSUMBYTE2OFFSET];
    uint16_t checkSum = lolGenerateChecksum(inputPacket, inputPacket[PAYLOADLENGTHPOS] + HEADERSIZE);
    if (checkSum != receivedChecksum)
    {
        err = eCommsErr_bad_RX_crc;
    }
    return err;
}
