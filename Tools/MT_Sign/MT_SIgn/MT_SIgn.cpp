/*
 ============================================================================
 Name        : MT_Sign.cpp
 Author      : Joseph Getz
 Version     :
 Copyright   : Your copyright notice
 Description :
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "crc32.h"
#include "validation.h"
#include "time.h"


unsigned char bin_array[65536 * 4];

const unsigned char MT_SIGNING_KEY[] = "MilwaukeeToolKey";

clock_t begin, end;

volatile uint8_t view_signed_output_data = 0;
volatile uint8_t go_for_signing = 0;

extern volatile uint8_t view_signed_output_data;
extern volatile uint8_t go_for_signing;

/*sorted Argument data variables*/
char* input_bin_file_path;
char* output_bin_file_path;
char* signing_key;

unsigned int upper_address;
unsigned long array_address;
unsigned long lowest_address = (65536 * 8);
unsigned long highest_address = 0;
unsigned long working_address = 0;

int main(int argc, char* argv[])
{
    FILE* fSigned;
    FILE* fBin;

    long int linenumber = 0;

    char* binfilename = 0;
    char* signedfilename = 0;

    char file_value;

    uint32_t file_length;

    uint32_t Product_Id;
    uint32_t Timestamp;
    uint32_t Firmware_Version;
    uint32_t Hardware_Version;
    uint32_t Compatability_Version;
    uint32_t Destination;
    uint32_t Length;
    uint32_t Fingerprint;
    uint32_t Header_Signature;

    uint32_t crc32;
    uint32_t retval;

    CRC32_T CRC32_State;
    uint32_t index = 0;
    uint32_t index_data = 0;

    time_t seconds;

    printf("\nProgram Name is = %s", argv[0]);
    validate_arguments(argc, argv);
    if (!go_for_signing)
    {
        printf("\nProgram Terminated\n");
        return 0;
    }
    else
    {
        binfilename = input_bin_file_path;
        signedfilename = output_bin_file_path;

        if (NULL == signing_key)
        {
            signing_key = (char*)MT_SIGNING_KEY;
        }
    }

    printf("\nBin Filename = %s", binfilename);
    printf("\nSigned Bin Filename = %s", signedfilename);

    // Check whether signing key pointer pointing to MT_SIGNING_KEY
    // If signing pointer pointing to MT_SIGNING_KEY then we are using MT_SIGNING_KEY and don't print it.
    if ((const char*)MT_SIGNING_KEY != signing_key)
    {
        printf("\nSigning Key = %s", signing_key);
    }
    else
    {
        printf("\nDefault Signing Key = *******");
    }

    seconds = time(NULL);


    /*****************************************************************************************************************/
    /*                                                                                                               */
    /*         Process BIN File                                                                                      */
    /*                                                                                                               */
    /*         Open BIN file and drop into an array. Copy structures for easy access                                 */
    /*                                                                                                               */
    /*****************************************************************************************************************/
    printf("\n\n****************************************");
    printf("\n*           Open BIN file              *");
    printf("\n****************************************");

#pragma warning(suppress : 4996)
    fBin = fopen(binfilename, "rb");

    if (fBin == NULL)
    {
        /* Unable to open file hence exit */
        printf("\n\nUnable to open BIN file.\n");
        printf("Please check whether file exists and you have read/write privilege.\n");
        return 0;
    }

    fseek(fBin, 0, SEEK_END);
    file_length = ftell(fBin);
    fseek(fBin, 0, SEEK_SET);

    //Mark all locations as erased to start
    memset(bin_array, 0xFF, sizeof(bin_array));
    linenumber = 0;

    retval = 1;
    index = 0;

    for(index = 0; index < file_length; index++)
    {
        // Replace all occurrence of word from current line
        fread(&file_value, 1, 1, fBin);
        bin_array[index] = file_value;
    }

    printf("\n---Number of bytes processed = 0x%08x", index);
    printf("\n---BIN file processed");
    fclose(fBin);
    printf("\n---BIN file closed");

    file_length = index;


    printf("\n\n****************************************");
    printf("\n*            Current Header            *");
    printf("\n****************************************");

    Product_Id = (uint32_t)*((uint32_t*)(&bin_array[0]));
    Timestamp = (uint32_t)*((uint32_t*)(&bin_array[4]));
    Firmware_Version = (uint32_t)*((uint32_t*)(&bin_array[8]));
    Hardware_Version = (uint32_t)*((uint32_t*)(&bin_array[12]));
    Compatability_Version = (uint32_t)*((uint32_t*)(&bin_array[16]));
    Destination = (uint32_t)*((uint32_t*)(&bin_array[20]));
    Length = (uint32_t)*((uint32_t*)(&bin_array[24]));
    Fingerprint = (uint32_t)*((uint32_t*)(&bin_array[92]));
    Header_Signature = (uint32_t)*((uint32_t*)(&bin_array[192]));

    if (Product_Id == 0x20008000)
    {
        printf("\n\r\n\r!!! WRONG APPLICATION VERSION. USE AN OTA VERSION OF THE APPLICATION !!!\n\r\n\r");
        return(0);
    }
    printf("\n---Milwaukee Tool Bootloader Structure");
    printf("\n------Product ID = 0x%08x", Product_Id);
    printf("\n------Timestamp = 0x%08x", Timestamp);
    printf("\n------Firmware Version = 0x%08x", Firmware_Version);
    printf("\n------Hardware Version = 0x%08x", Hardware_Version);
    printf("\n------Compatability Version = 0x%08x", Compatability_Version);
    printf("\n------Destination = 0x%08x", Destination);
    printf("\n------Length = 0x%08x", Length);
    printf("\n------Image Fingerprint = 0x%08x", Fingerprint);
    printf("\n------Header Fingerprint = 0x%08x", Header_Signature);
    printf("\n------Size of Header = 256");


    printf("\n\n****************************************");
    printf("\n*           Updating Header            *");
    printf("\n****************************************");

    Timestamp = seconds;
    bin_array[7] = (Timestamp & 0xFF000000) >> 24;
    bin_array[6] = (Timestamp & 0x00FF0000) >> 16;
    bin_array[5] = (Timestamp & 0x0000FF00) >> 8;
    bin_array[4] = (Timestamp & 0xFF);


    Length = file_length - 256;
    bin_array[27] = (Length & 0xFF000000) >> 24;
    bin_array[26] = (Length & 0x00FF0000) >> 16;
    bin_array[25] = (Length & 0x0000FF00) >> 8;
    bin_array[24] = (Length & 0xFF);

    /*****************************************************************************************************************/
    /*                                                                                                               */
    /*         Process CRC32                                                                                         */
    /*                                                                                                               */
    /*         Process CRC32 on the program starting at 100 and going to the last byte of the array                  */
    /*                                                                                                               */
    /*         Ths goes into the Image Fingerprint variable                                                          */
    /*                                                                                                               */
    /*         The CRC32 is calculated by a CRC32 of the MT_SIGNING_KEY, then timestamp, then image                  */
    /*                                                                                                               */
    /*****************************************************************************************************************/

    CRC32_Init(&CRC32_State);
    printf("\n\n****************************************");
    printf("\n*          Calculating CRC32           *");
    printf("\n****************************************");
    printf("\n---CRC32 Initialization = 0x%08lx", CRC32_State);
    printf("\n---CRC32 Process Length = %i bytes", Length);

    CRC32_ProcessMessage(&CRC32_State, (unsigned char*)signing_key, strlen(signing_key));    //"Milwaukee_Tool_Key";
    printf("\n---CRC32 KEY Processed. Length = %i", strlen(signing_key));
    CRC32_ProcessMessage(&CRC32_State, (unsigned char*)&Timestamp, sizeof(Timestamp));         //Timestamp;
    printf("\n---CRC32 Timestamp Processed. Length = %i", sizeof(Timestamp));
    CRC32_ProcessMessage(&CRC32_State, &bin_array[256 + 32], Length - 32); //Bin array
    printf("\n---CRC32 Program Array Processed. Length = %i", Length);

    crc32 = CRC32_GetResult(&CRC32_State);

    Fingerprint = crc32;

    printf("\n---CRC32 = 0x%08x", crc32);

    bin_array[95] = (Fingerprint & 0xFF000000) >> 24;
    bin_array[94] = (Fingerprint & 0x00FF0000) >> 16;
    bin_array[93] = (Fingerprint & 0x0000FF00) >> 8;
    bin_array[92] = (Fingerprint & 0xFF);

    printf("\n---CRC32 Image Fingerprint written");
    printf("\n---CRC32 Finished");

    /*****************************************************************************************************************/
    /*                                                                                                               */
    /*         Process CRC32                                                                                         */
    /*                                                                                                               */
    /*         Process CRC32 on the entire header structure                                                          */
    /*****************************************************************************************************************/
    CRC32_Init(&CRC32_State);

    printf("\n\n****************************************");
    printf("\n*  Calculating Header Signature CRC32  *");
    printf("\n****************************************");
    printf("\n---CRC32 Initialization = 0x%08x", CRC32_State);
    printf("\n---CRC32 Process Length = 192 bytes");

    CRC32_ProcessMessage(&CRC32_State, &bin_array[0], 192); //Bin array

    printf("\n---CRC32 Header Array Processed");

    crc32 = CRC32_GetResult(&CRC32_State);

    printf("\n---CRC32 = 0x%08x", crc32);
    printf("\n---CRC32 written");
    printf("\n---CRC32 Finished");

    bin_array[195] = (crc32 & 0xFF000000) >> 24;
    bin_array[194] = (crc32 & 0x00FF0000) >> 16;
    bin_array[193] = (crc32 & 0x0000FF00) >> 8;
    bin_array[192] = (crc32 & 0xFF);

    printf("\n\n****************************************");
    printf("\n*            Updated Header            *");
    printf("\n****************************************");

    Product_Id = (uint32_t) * ((uint32_t*)(&bin_array[0]));
    Timestamp = (uint32_t) * ((uint32_t*)(&bin_array[4]));
    Firmware_Version = (uint32_t) * ((uint32_t*)(&bin_array[8]));
    Hardware_Version = (uint32_t) * ((uint32_t*)(&bin_array[12]));
    Compatability_Version = (uint32_t) * ((uint32_t*)(&bin_array[16]));
    Destination = (uint32_t) * ((uint32_t*)(&bin_array[20]));
    Length = (uint32_t) * ((uint32_t*)(&bin_array[24]));
    Fingerprint = (uint32_t) * ((uint32_t*)(&bin_array[92]));
    Header_Signature = (uint32_t) * ((uint32_t*)(&bin_array[192]));

    printf("\n---Milwaukee Tool Bootloader Structure");
    printf("\n------Product ID = 0x%08x", Product_Id);
    printf("\n------Timestamp = 0x%08x", Timestamp);
    printf("\n------Firmware Version = 0x%08x", Firmware_Version);
    printf("\n------Hardware Version = 0x%08x", Hardware_Version);
    printf("\n------Compatability Version = 0x%08x", Compatability_Version);
    printf("\n------Destination = 0x%08x", Destination);
    printf("\n------Length = 0x%08x", Length);
    printf("\n------Image Fingerprint = 0x%08x", Fingerprint);
    printf("\n------Header Fingerprint = 0x%08x", Header_Signature);
    printf("\n------Size of Header = 256");

    printf("\n\n****************************************");
    printf("\n*       Creating Signed BIN file       *");
    printf("\n****************************************");

#pragma warning(suppress : 4996)
    fSigned = fopen(signedfilename, "wb");
    if (fSigned == NULL)
    {
        /* Unable to open file hence exit */
        printf("\n\nUnable to open BIN file.\n");
        printf("Please check whether file exists and you have read/write privilege.\n");
        return 0;
    }

    printf("\n---Opened filename %s for writing", signedfilename);

    for (index = 0; index < file_length; index++)
    {
        file_value = bin_array[index];
        fwrite(&file_value, 1, 1, fSigned);
    }


    fclose(fSigned);
    printf("\n---Signed BIN file closed");

    printf("\n\nSigning Tool Finished");
    total_execution_time();
}

const uint32_t crc32_tab[] = {
    0x00000000U, 0x77073096U, 0xee0e612cU, 0x990951baU, 0x076dc419U, 0x706af48fU, 0xe963a535U, 0x9e6495a3U,
    0x0edb8832U, 0x79dcb8a4U, 0xe0d5e91eU, 0x97d2d988U, 0x09b64c2bU, 0x7eb17cbdU, 0xe7b82d07U, 0x90bf1d91U,
    0x1db71064U, 0x6ab020f2U, 0xf3b97148U, 0x84be41deU, 0x1adad47dU, 0x6ddde4ebU, 0xf4d4b551U, 0x83d385c7U,
    0x136c9856U, 0x646ba8c0U, 0xfd62f97aU, 0x8a65c9ecU, 0x14015c4fU, 0x63066cd9U, 0xfa0f3d63U, 0x8d080df5U,
    0x3b6e20c8U, 0x4c69105eU, 0xd56041e4U, 0xa2677172U, 0x3c03e4d1U, 0x4b04d447U, 0xd20d85fdU, 0xa50ab56bU,
    0x35b5a8faU, 0x42b2986cU, 0xdbbbc9d6U, 0xacbcf940U, 0x32d86ce3U, 0x45df5c75U, 0xdcd60dcfU, 0xabd13d59U,
    0x26d930acU, 0x51de003aU, 0xc8d75180U, 0xbfd06116U, 0x21b4f4b5U, 0x56b3c423U, 0xcfba9599U, 0xb8bda50fU,
    0x2802b89eU, 0x5f058808U, 0xc60cd9b2U, 0xb10be924U, 0x2f6f7c87U, 0x58684c11U, 0xc1611dabU, 0xb6662d3dU,
    0x76dc4190U, 0x01db7106U, 0x98d220bcU, 0xefd5102aU, 0x71b18589U, 0x06b6b51fU, 0x9fbfe4a5U, 0xe8b8d433U,
    0x7807c9a2U, 0x0f00f934U, 0x9609a88eU, 0xe10e9818U, 0x7f6a0dbbU, 0x086d3d2dU, 0x91646c97U, 0xe6635c01U,
    0x6b6b51f4U, 0x1c6c6162U, 0x856530d8U, 0xf262004eU, 0x6c0695edU, 0x1b01a57bU, 0x8208f4c1U, 0xf50fc457U,
    0x65b0d9c6U, 0x12b7e950U, 0x8bbeb8eaU, 0xfcb9887cU, 0x62dd1ddfU, 0x15da2d49U, 0x8cd37cf3U, 0xfbd44c65U,
    0x4db26158U, 0x3ab551ceU, 0xa3bc0074U, 0xd4bb30e2U, 0x4adfa541U, 0x3dd895d7U, 0xa4d1c46dU, 0xd3d6f4fbU,
    0x4369e96aU, 0x346ed9fcU, 0xad678846U, 0xda60b8d0U, 0x44042d73U, 0x33031de5U, 0xaa0a4c5fU, 0xdd0d7cc9U,
    0x5005713cU, 0x270241aaU, 0xbe0b1010U, 0xc90c2086U, 0x5768b525U, 0x206f85b3U, 0xb966d409U, 0xce61e49fU,
    0x5edef90eU, 0x29d9c998U, 0xb0d09822U, 0xc7d7a8b4U, 0x59b33d17U, 0x2eb40d81U, 0xb7bd5c3bU, 0xc0ba6cadU,
    0xedb88320U, 0x9abfb3b6U, 0x03b6e20cU, 0x74b1d29aU, 0xead54739U, 0x9dd277afU, 0x04db2615U, 0x73dc1683U,
    0xe3630b12U, 0x94643b84U, 0x0d6d6a3eU, 0x7a6a5aa8U, 0xe40ecf0bU, 0x9309ff9dU, 0x0a00ae27U, 0x7d079eb1U,
    0xf00f9344U, 0x8708a3d2U, 0x1e01f268U, 0x6906c2feU, 0xf762575dU, 0x806567cbU, 0x196c3671U, 0x6e6b06e7U,
    0xfed41b76U, 0x89d32be0U, 0x10da7a5aU, 0x67dd4accU, 0xf9b9df6fU, 0x8ebeeff9U, 0x17b7be43U, 0x60b08ed5U,
    0xd6d6a3e8U, 0xa1d1937eU, 0x38d8c2c4U, 0x4fdff252U, 0xd1bb67f1U, 0xa6bc5767U, 0x3fb506ddU, 0x48b2364bU,
    0xd80d2bdaU, 0xaf0a1b4cU, 0x36034af6U, 0x41047a60U, 0xdf60efc3U, 0xa867df55U, 0x316e8eefU, 0x4669be79U,
    0xcb61b38cU, 0xbc66831aU, 0x256fd2a0U, 0x5268e236U, 0xcc0c7795U, 0xbb0b4703U, 0x220216b9U, 0x5505262fU,
    0xc5ba3bbeU, 0xb2bd0b28U, 0x2bb45a92U, 0x5cb36a04U, 0xc2d7ffa7U, 0xb5d0cf31U, 0x2cd99e8bU, 0x5bdeae1dU,
    0x9b64c2b0U, 0xec63f226U, 0x756aa39cU, 0x026d930aU, 0x9c0906a9U, 0xeb0e363fU, 0x72076785U, 0x05005713U,
    0x95bf4a82U, 0xe2b87a14U, 0x7bb12baeU, 0x0cb61b38U, 0x92d28e9bU, 0xe5d5be0dU, 0x7cdcefb7U, 0x0bdbdf21U,
    0x86d3d2d4U, 0xf1d4e242U, 0x68ddb3f8U, 0x1fda836eU, 0x81be16cdU, 0xf6b9265bU, 0x6fb077e1U, 0x18b74777U,
    0x88085ae6U, 0xff0f6a70U, 0x66063bcaU, 0x11010b5cU, 0x8f659effU, 0xf862ae69U, 0x616bffd3U, 0x166ccf45U,
    0xa00ae278U, 0xd70dd2eeU, 0x4e048354U, 0x3903b3c2U, 0xa7672661U, 0xd06016f7U, 0x4969474dU, 0x3e6e77dbU,
    0xaed16a4aU, 0xd9d65adcU, 0x40df0b66U, 0x37d83bf0U, 0xa9bcae53U, 0xdebb9ec5U, 0x47b2cf7fU, 0x30b5ffe9U,
    0xbdbdf21cU, 0xcabac28aU, 0x53b39330U, 0x24b4a3a6U, 0xbad03605U, 0xcdd70693U, 0x54de5729U, 0x23d967bfU,
    0xb3667a2eU, 0xc4614ab8U, 0x5d681b02U, 0x2a6f2b94U, 0xb40bbe37U, 0xc30c8ea1U, 0x5a05df1bU, 0x2d02ef8dU
};


/*
 * Before each message CRC is generated, the CRC register must be
 * initialised by calling this function
 */
void CRC32_Init(CRC32_hT CRC_State)
{
    /* Initialise the CRC to 0xFFFF for the CCITT specification*/
    *CRC_State = 0xFFFFFFFFul;
}

/*
 * Process several bytes to update the current CRC Value
 */
void CRC32_ProcessMessage(CRC32_hT CRC_State, const uint8_t msg[], uint32_t len)
{
    uint32_t crc32_i = 0;

    for (; len > 0; len--)
    {
        *CRC_State = crc32_tab[(*CRC_State ^ msg[crc32_i]) & 0xFF] ^ (*CRC_State >> 8);
        crc32_i += 1;
    }
}


uint32_t CRC32_GetResult(CRC32_hT CRC_State)
{
    return (*CRC_State ^ 0xFFFFFFFFul);
}



/*********************************************************************************
 * Function Name: void print_help_page(void)
 * Arguments    : None.
 * Description  : It prints the Help page for Signing Utility on console
 *
 *********************************************************************************/

void print_help_page(void)
{
    printf("\r\nSigning Utility Version:");
    printf(MT_SIGNING_UTILITY_VERSION);
    printf("\r\nUsage: MT_Sign.exe     -i  Hex_File_Path     -k Signing_key_String     -o  Signed_File_Path \r\n");
    printf("\r\n[Arguments]:\n");
    printf("\r\n\t-i					: Input hex file flag");
    printf("\r\n\tHex_File_Path 	 			: Input Hex file path. example: \"d:\\folder1\\test.hex\" ");
    printf("\r\n\t-k					: Signing key flag");
    printf("\r\n\tSigning_key_String 			: Signing key String. example: \"01234ABCD\" ");
    printf("\r\n\t\t\t\t\t\t  Note:  if 'Signing_key_String' is absent, default value will be considered.");
    printf("\r\n\t\t\t\t\t\t  Only alphanumeric characters are allowed in Signing Key, Maximum Length = 20 Characters.\n");
    printf("\r\n\t-o					: Output hex file flag");
    printf("\r\n\tSigned_File_Path 			: Output Signed File Path. example: \"d:\\folder1\\test_signed.bin\" ");
}


/****************************************************************************************
 * Function Name:- int validate_arguments(int argc, char *argv[])
 * Arguments    :-
 * 				   int argc     : Argument Count, is int and stores number of
 * 				  				  command-line arguments passed by the user
 * 				  				  including the name of the program.
 *
 * 				   char *argv[] : Argument vector ,
 * 				    			  is array of character pointers listing all the arguments
 *
 * Description  : It validates the arguments given to the signing utility by User
 *
 ****************************************************************************************/
int validate_arguments(int argc, char* argv[])
{
    uint8_t input_cnt = 0;
    uint8_t i_found = 0, o_found = 0, k_found = 0;
    uint8_t signing_key_length = 0;
    char extension[8] = ".bin/0";

    get_start_clock();
    printf("\r\nRunning Signing Utility Version :  %s\r\n", MT_SIGNING_UTILITY_VERSION);

    if (argc == 1)
    {
        printf("\r\nMT_Sign: Fatal Error: no input files\r\nProcess terminated.\r\n");
        printf("For help type following command:\r\nMT_Sign.exe -h\r\n");
        total_execution_time();
        return 1;
    }
    else if ((strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "-H") == 0))
    {
        /*This is help Page for Signing Utility*/
        print_help_page();
        total_execution_time();
        return 0;
    }
    else if ((argc > 1) && (argc <= 7))
    {

        /*Check Input Argument sequence, Sort the arguments*/
        for (input_cnt = 0; input_cnt < argc; input_cnt++)
        {
            if ((strcmp(argv[input_cnt], "-i") == 0) || (strcmp(argv[input_cnt], "-I") == 0))
            {
                input_bin_file_path = argv[input_cnt + 1];
                if (check_file_extension(&extension[0], input_bin_file_path))
                {
                    if (check_file_exist(input_bin_file_path) == File_Absent)
                    {
                        total_execution_time();
                        return 1;
                    }
                    i_found = 1;
                }
                else
                {
                    printf("\nFatal Error: Input file is not .bin file\n");
                }
            }
            else if ((strcmp(argv[input_cnt], "-o") == 0) || (strcmp(argv[input_cnt], "-O") == 0))
            {
                output_bin_file_path = argv[input_cnt + 1];
                if (check_file_extension(&extension[0], output_bin_file_path))
                {
                    o_found = 1;
                }
                else
                {
                    printf("\nFatal Error: Input file is not .bin file\n");
                }

            }
            else if ((strcmp(argv[input_cnt], "-k") == 0) || (strcmp(argv[input_cnt], "-K") == 0))
            {
                signing_key = argv[input_cnt + 1];
                signing_key_length = strlen(signing_key);
                if ((signing_key_length <= 20) && (signing_key_length > 0))
                {                    for (int i = 0; i < signing_key_length; i++)
                        if (isalnum(signing_key[i]))
                            k_found = 1;
                        else
                        {
                            k_found = 0;
                            printf("\r\nError: Signing Key contains Special Characters: %s ,\r\nHence considering default Signing Key", signing_key);
                            signing_key = 0;
                            break;
                        }
                }
                else
                {
                    printf("\r\nError:Given Signing Key having %d number of characters, which is too long.\r\nMaximum allowable length is 16", signing_key_length);
                    signing_key = 0;
                }
            }
        }

        if (i_found)
        {

            if (o_found)
            {
                if (!k_found)
                {
                    printf("\r\nWarning: '-k' flag absent. \r\n");
                    printf("\r\nDefault value will be considered.");
                }
                go_for_signing = 1;
            }
            else
            {
                printf("\r\nMT_Sign: Fatal Error: '-o' flag absent. \r\n");
                printf("For help type following command:\r\nMT_Sign.exe -h\r\n");
                total_execution_time();
                return 1;
            }
        }
        else
        {
            printf("\r\nMT_Sign: Fatal Error:-i flag absent. \r\n");
            printf("For help type following command:\r\nMT_Sign.exe -h\r\n");
            total_execution_time();
            return 1;
            go_for_signing = 0;
        }

    }
    else if (argc > 7)
    {
        printf("\r\nMT_Sign: Fatal Error: Undefined command line option\r\nProcess terminated.\r\n");
        printf("For help type following command:\r\nMT_Sign.exe -h\r\n");
        total_execution_time();
        return 1;
    }

    return 0;
}


/****************************************************************************************
 * Function Name:- int check_file_exist(char *filename)
 * Arguments    :- char *filename: File name
 *
 * Description  : This function check that if the file is exist or Not.
 *
 ****************************************************************************************/
int check_file_exist(char* filename)
{
    /* try to open file to read */
    FILE* file;
#pragma warning(suppress : 4996)
    file = fopen(filename, "r");
    if (file)
    {
        fclose(file);
        return File_exist;
    }
    else
    {
        printf("\r\n\"%s\" file does not exist", filename);
        return File_Absent;
    }
}


int check_file_extension(char* ext, char* filename)
{

    if (strcmp(ext, filename))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/****************************************************************************************
 * Function Name:- void get_start_clock(void)
 * Arguments    :- None
 *
 * Description  : This function get the clock.
 * 				  this function is used at the start of the program
 *
 ****************************************************************************************/

void get_start_clock(void)
{
    begin = clock();
}

/****************************************************************************************
 * Function Name:- void total_execution_time(void)
 * Arguments    :- None
 *
 * Description  : This function get the clock, Calculate the required time in ms
 *                with respect to clock per sec.This function is used at the End of the program
 *
 ****************************************************************************************/
void total_execution_time(void)
{
    end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\nTotal execution time  = %0.3f (seconds)\n", time_spent);

}


