/*
 *  validation.c
 *
 *  Created on: May 27, 2020
 *  Author: Atul.Shukre
 *
 */
#include <stdint.h>
#include "validation.h"


clock_t begin,end;
/*sorted Argument data variables*/
char *input_hex_file_path = NULL;
char *input_map_file_path = NULL ;
char *output_hex_file_path = NULL;
const char *singning_key = NULL;
char *app_descriptor_name = NULL;

/*****************************/
volatile uint8_t view_signed_output_data = 0;
volatile uint8_t go_for_signing = 0;

/*********************************************************************************
 * Function Name: void print_help_page(void)
 * Arguments    : None.
 * Description  : It prints the Help page for Signing Utility on console
 *
 *********************************************************************************/

void print_help_page(void)
{
	printf("\r\nDescriptor Utility Version:");
	printf(MT_DESCRIPTOR_UTILITY_VERSION);
	printf("\r\nUsage: descriptor.exe     -i  Hex_File_Path     -m  Map_File_Path     -k Signing_key_String     -o  Signed_File_Path     -a  Applicatio_Descriptor_Name     [-v]   view Signed hex file Data on console \r\n");
	printf("\r\n[Arguments]:\n");
	printf("\r\n\t-i					: Input hex file flag");
	printf("\r\n\tHex_File_Path 	 			: Input Hex file path. example: \"d:\\folder1\\test.hex\" ");
	printf("\r\n\t-m					: Input map file flag");
	printf("\r\n\tMap_File_Path 	 			: Map file path. example: \"d:\\folder1\\test.map\" ");
	printf("\r\n\t-k					: Signing key flag");
	printf("\r\n\tSigning_key_String 			: Signing key String. example: \"01234ABCD\" ");
	printf("\r\n\t\t\t\t\t\t  Note:  if 'Signing_key_String' is absent, default value will be considered.");
	printf("\r\n\t\t\t\t\t\t  Only alphanumeric characters are allowed in Signing Key, Maximum Length = 16 Characters.\n");
	printf("\r\n\t-o					: Output hex file flag");
	printf("\r\n\tSigned_File_Path 			: Output Signed File Path. example: \"d:\\folder1\\test_signed.hex\" ");
	printf("\r\n\t-a					: Application_Descriptor_Name flag");
	printf("\r\n\tApplication_Descriptor_Name		: Name of the Application Descriptor block. example: app_desc ");
	printf("\r\n\t-v					: (Optional Input; Default: off) View the Signed Hex file data on Console.");
	printf("\r\n\t\t\t\t\t\t  Note: 'Application_Descriptor_Name' is absent, default value i.e. '.app_desc' will be considered.\n");



}


/****************************************************************************************
 * Function Name:- int validate_arguments(int argc, char *argv[])
 * Arguments    :-
 * 				   int argc     : Argument Count, is int and stores number of
 * 				  				  command-line arguments passed by the user
 * 				  				  including the name of the program.
 *
 * 				   char *argv[] : Argument vector ,
 * 				    			  is array of character pointers listing all the arguments
 *
 * Description  : It validates the arguments given to the signing utility by User
 *
 ****************************************************************************************/

int validate_arguments(int argc, char *argv[])
{
	uint8_t input_cnt=0;
	uint8_t i_found=0,m_found=0,o_found=0,a_found=0,k_found=0;
	uint8_t signing_key_length=0,app_desc_length=0;

	get_start_clock();
	printf("\r\nRunning Descriptor Utility Version :  %s\r\n",MT_DESCRIPTOR_UTILITY_VERSION);

	if(argc == 1)
		{
			printf("\r\ndescriptor: Fatal Error: no input files\r\nProcess terminated.\r\n");
			printf("For help type following command:\r\ndescriptor.exe -h\r\n");
			total_execution_time();
			return 1;
		}
	else if((strcmp(argv[1],"-h")==0) || (strcmp(argv[1],"-H")==0))
		{
			/*This is help Page for Descriptor Utility*/
			print_help_page();
			total_execution_time();
			return 0;
		}
	else if((argc > 1)&& (argc <= 12))
		{

			/*Check Input Argument sequence, Sort the arguments*/
			for(input_cnt=0;input_cnt<argc; input_cnt++)
			{
				if((strcmp(argv[input_cnt],"-i")==0) || (strcmp(argv[input_cnt],"-I")==0))
				{
					input_hex_file_path= argv[input_cnt+1];
					if(check_file_extension(".hex",input_hex_file_path))
					{
						if(check_file_exist(input_hex_file_path)== File_Absent)
						{
							total_execution_time();
							return 1;
						}
						i_found=1;
					}
					else
					{
						printf("\nFatal Error: Input file is not .hex file\n");
					}
				}
				else if((strcmp(argv[input_cnt],"-m")==0) || (strcmp(argv[input_cnt],"-M")==0))
				{
					input_map_file_path=argv[input_cnt+1];
					if(check_file_extension(".map",input_map_file_path))
					{
						if(check_file_exist(input_map_file_path)== File_Absent)
						{
							total_execution_time();
							return 1;
						}
						m_found=1;
					}
					else
					{
						printf("\nFatal Error: Input file is not .map file\n");
					}

				}
				else if((strcmp(argv[input_cnt],"-o")==0) || (strcmp(argv[input_cnt],"-O")==0))
				{
					output_hex_file_path=argv[input_cnt+1];
					if(check_file_extension(".hex",output_hex_file_path))
					{
						o_found=1;
					}
					else
					{
						printf("\nFatal Error: Input file is not .hex file\n");
					}

				}
				else if((strcmp(argv[input_cnt],"-k")==0) || (strcmp(argv[input_cnt],"-K")==0))
				{
					singning_key=argv[input_cnt+1];
					signing_key_length=strlen(singning_key);
					if((signing_key_length<=16) && (signing_key_length>0))
					{
						for(int i=0;i< signing_key_length; i++)
							if(isalnum(singning_key[i]))
								k_found=1;
							else
							{
								k_found=0;
								printf("\r\nError: Signing Key contains Special Characters: %s ,\r\nHence considering default Signing Key" ,singning_key);
								singning_key=0;
								break;
							}
					}
					else
					{
						printf("\r\nError:Given Signing Key having %d number of characters, which is too long.\r\nMaximum allowable length is 16",signing_key_length);
						singning_key=0;
					}



				}
				else if((strcmp(argv[input_cnt],"-a")==0) || (strcmp(argv[input_cnt],"-A")==0))
				{
					 app_descriptor_name= argv[input_cnt+1];

					 app_desc_length=strlen(app_descriptor_name);
					 if(app_desc_length)
						{
							for(int i=0;i< app_desc_length; i++)

								if(isalnum(app_descriptor_name[i]))
									a_found=1;
								else
									{
										if((app_descriptor_name[0] != '.') && (app_descriptor_name[i] != '_'))
										{
											a_found=0;
											printf("\r\nError: 'app descriptor name' contains Special Characters: %s ,\r\nHence considering default app descriptor" ,app_descriptor_name);
											app_descriptor_name=0;
											break;
										}
									}
						}


#ifdef Debug_Message
					 if(app_descriptor_name)
						 printf("\r\nApplication descriptor name:%s",app_descriptor_name);
#endif


				}
				else if((strcmp(argv[input_cnt],"-v")==0) || (strcmp(argv[input_cnt],"-V")==0))
						{

							view_signed_output_data = 1;

						}


			}

			if (i_found)
				{

					if( m_found )
						{
							if(o_found)
								{
									if(!a_found)
									{
										printf("\r\nWarning: '-a' flag absent. \r\n");
										printf("\r\nDefault value i.e. '.app_desc' will be considered.");
										app_descriptor_name= ".app_desc";
									}
									if(!k_found)
									{
										printf("\r\nWarning: '-k' flag absent. \r\n");
										printf("\r\nDefault value will be considered.");
									}
									go_for_signing=1;

								}
							else
								{
									printf("\r\ndescriptor: Fatal Error: '-o' flag absent. \r\n");
									printf("For help type following command:\r\ndescriptor.exe -h\r\n");
									total_execution_time();
									return 1;
								}
						}
					else
						{
							printf("\r\ndescriptor: Fatal Error: '-m' flag absent. \r\n");
							printf("For help type following command:\r\ndescriptor.exe -h\r\n");
							total_execution_time();
							return 1;
						}
				}
			else
				{
					printf("\r\ndescriptor: Fatal Error:-i flag absent. \r\n");
					printf("For help type following command:\r\ndescriptor.exe -h\r\n");
					total_execution_time();
					return 1;
					go_for_signing=0;
				}

		}
	else if(argc > 11)
		{
			printf("\r\ndescriptor: Fatal Error: Undefined command line option\r\nProcess terminated.\r\n");
			printf("For help type following command:\r\ndescriptor.exe -h\r\n");
			total_execution_time();
			return 1;
		}

	return 0;
}


/****************************************************************************************
 * Function Name:- int check_file_exist(char *filename)
 * Arguments    :- char *filename: File name
 *
 * Description  : This function check that if the file is exist or Not.
 *
 ****************************************************************************************/

int check_file_exist(char *filename)
{
   /* try to open file to read */
   FILE *file;
#pragma warning(suppress : 4996)
   file = fopen(filename, "r");
   if(file)
	{
		  fclose(file);
		  return File_exist;
	}
   else
	   {
		  printf("\r\n\"%s\" file does not exist",filename);
		  return File_Absent;
	   }
}


int check_file_extension(char* ext,char* filename)
{

//    char* temp_ext;
//    char* p;
//    char fullname []="file.txt";
//    temp_ext = strchr(filename,'.');
    if(strcmp(ext,filename))
     return 1;
    else
     return 0;
}

/****************************************************************************************
 * Function Name:- void get_start_clock(void)
 * Arguments    :- None
 *
 * Description  : This function get the clock.
 * 				  this function is used at the start of the program
 *
 ****************************************************************************************/

void get_start_clock(void)
{
	begin= clock();
}

/****************************************************************************************
 * Function Name:- void total_execution_time(void)
 * Arguments    :- None
 *
 * Description  : This function get the clock, Calculate the required time in ms
 *                with respect to clock per sec.This function is used at the End of the program
 *
 ****************************************************************************************/
void total_execution_time(void)
{
	end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("\nTotal execution time  = %0.3f (seconds)\n", time_spent );

}


