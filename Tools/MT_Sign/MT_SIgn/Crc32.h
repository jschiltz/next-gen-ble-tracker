/**
 * \file crc32.h
 *
 * \brief declares CRC32 functions
 *
 * Copyright Milwaukee Tool 2021
 *
 * $Author$
 * $Revision$
 * $Date$
 *
 */

#ifndef _CRC32_H
#define _CRC32_H

#include "stdint.h"

typedef uint32_t CRC32_T;
typedef CRC32_T * const CRC32_hT;

void CRC32_Init ( CRC32_hT CRC_State );
void CRC32_ProcessMessage ( CRC32_hT CRC_State, const uint8_t msg[], uint32_t len );
uint32_t CRC32_GetResult ( CRC32_hT CRC_State );

#endif /* _CRC32_H */
