#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "version.h"
#include "Std_types.h"


#define File_exist					1
#define File_Absent					0


void print_help_page(void);
int validate_arguments(int argc, char *argv[]);
int check_file_exist(char *filename);
int check_file_extension(char* ext,char* filename);
void get_start_clock(void);
void total_execution_time(void);
