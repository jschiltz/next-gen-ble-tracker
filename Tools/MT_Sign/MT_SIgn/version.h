
/**************************************************
 *
 * File : version.h
 * Description: This file describes the version
 * of signing utility
 *
 *************************************************/

#define MT_SIGNING_UTILITY_VERSION			"V 0.0.2"




/*  SIGNING UTILITY VERSION = "V 0.0.2"
 *
 * 1) Modification done in Command Syntax of signing.
 *
 * 	  Added '-v' flag into command Syntax which is used to view the Signed Output on console.
 * 	  Otherwise signing will not show the Signed Output on console, it will show only other message on Console.
 *
 *	  Usage: MT_Sign.exe -i Hex_File_Path -k Signing_key_String -o Signed_File_Path 
 *
 *	  [Arguments]:
 *
 *			-i                                                      : Input hex file flag
 *			Hex_File_Path                                           : Input Hex file path. example: "d:\folder1\test.hex"
 *			-k                                                      : Signing key flag
 *			Signing_key_String                                      : Signing key String. example: "01234ABCD"
 *			-o                                                      : Output hex file flag
 * 			Signed_File_Path                                        : Output Signed File Path. example: "d:\folder1\test_signed.hex"
 *
 *																	Note:  if 'Signing_key_String' is absent, default value will be considered.
 *
 */



/*  SIGNING UTILITY VERSION = "V 0.0.1"
 *
 *    Usage: MT_Sign.exe -i Bin_File_Path -k Signing_key_String -o Signed_File_Path 
 *
 *	  [Arguments]:
 *
 *			-i                                                      : Input hex file flag
 *			Bin_File_Path                                           : Input Hex file path. example: "d:\folder1\test.hex"
 *			-k                                                      : Signing key flag
 *			Signing_key_String                                      : Signing key String. example: "01234ABCD"
 *			-o                                                      : Output hex file flag
 * 			Signed_File_Path                                        : Output Signed File Path. example: "d:\folder1\test_signed.hex"
 *
 *																	Note:  if 'Signing_key_String' is absent, default value will be considered.
 *
 *
 *
 *
 *
 */
