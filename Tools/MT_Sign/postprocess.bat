echo off
echo **********************************************
echo *                                            *
echo *    Milwaukee Tool Packaging Script V1.0    *
echo *                                            *
echo **********************************************


MT_Sign.exe -i NextGen_Ticker.bin -o NextGen_Ticker_signed.bin

objcopy.exe -I binary -O ihex --change-addresses 0x010056800 NextGen_Ticker_signed.bin NextGen_Ticker_signed.hex

srec_cat.exe NextGen_Ticker_signed.hex -I BLE_OTA_ServiceManager.hex -I -o OTA_Manager_NextGen_Ticker_signed.hex -I 

echo Done!!!
