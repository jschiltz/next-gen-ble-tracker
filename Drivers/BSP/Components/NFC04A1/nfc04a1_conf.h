/**
 ******************************************************************************
 * @file    nfc04a1_conf.h
 * @author  SRA Application Team
 * @version V0.0.1
 * @date    26-Nov-2018
 * @brief   This file contains definitions for the NFC4 components bus interfaces
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2020 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NFC04A1_CONF_H__
#define __NFC04A1_CONF_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bluenrg_lp_evb_i2c.h"                         /* Added by SB */
//#include "bluenrg_lp_hal.h" //"stm32l0xx_hal.h"       /* Modified by SB */
//#include "stm32l0xx_nucleo_bus.h"                     /* Modified by SB */
#include "nfc04a1_errno.h" //"stm32l0xx_nucleo_errno.h" /* Modified by SB */

//#include "stm32l0xx_hal_exti.h"
#include "bluenrg_lp_hal.h"     /* Added by SB */

#define NFC04A1_I2C_Init         MX_I2Cx_Init //BSP_I2C_Init        //BSP_I2C1_Init /* Modified by SB */
#define NFC04A1_I2C_DeInit       MX_I2Cx_Deinit //BSP_I2C_DeInit      //BSP_I2C1_DeInit /* Modified by SB */
#define NFC04A1_I2C_ReadReg16    BSP_I2C_ReadReg16   //BSP_I2C1_ReadReg16 /* Modified by SB */
#define NFC04A1_I2C_WriteReg16   BSP_I2C_WriteReg16  //BSP_I2C1_WriteReg16 /* Modified by SB */
#define NFC04A1_I2C_Recv         BSP_I2C1_Recv
#define NFC04A1_I2C_IsReady      BSP_I2C_IsReady //BSP_I2C1_IsReady /* Modified by SB */

#define NFC04A1_GetTick          HAL_GetTick

/* Modified by SB */
#if defined(METCO_PCB)
  #define NFC04A1_LPD_PIN_PORT           GPIOB //GPIOA /* Modified by SB */
  #define NFC04A1_LPD_PIN                LL_GPIO_PIN_3 //GPIO_PIN_8 /* Modified by SB */
  #define NFC04A1_GPO_PIN_PORT           GPIOB //GPIOA /* Modified by SB */
  #define NFC04A1_GPO_PIN                LL_GPIO_PIN_2 //GPIO_PIN_6 /* Modified by SB */
  #define NFC04A1_NFCTAG_GPO_EXTI_LINE   LL_EXTI_LINE_PB2 //EXTI_LINE_6 /* Modified by SB */
  #define NFC04A1_GPO_EXTI               GPIOB_IRQn //EXTI4_15_IRQn /* Modified by SB */
//  extern EXTI_HandleTypeDef GPO_EXTI;
  #define H_EXTI_6 GPO_EXTI
#else
  #define NFC04A1_LPD_PIN_PORT           GPIOA //GPIOA /* Modified by SB */
  #define NFC04A1_LPD_PIN                LL_GPIO_PIN_4 //GPIO_PIN_8 /* Modified by SB */
  #define NFC04A1_GPO_PIN_PORT           GPIOA //GPIOA /* Modified by SB */
  #define NFC04A1_GPO_PIN                LL_GPIO_PIN_14 //GPIO_PIN_6 /* Modified by SB */
  #define NFC04A1_NFCTAG_GPO_EXTI_LINE   LL_EXTI_LINE_PA14 //EXTI_LINE_6 /* Modified by SB */
  #define NFC04A1_GPO_EXTI               GPIOA_IRQn //EXTI4_15_IRQn /* Modified by SB */
//  extern EXTI_HandleTypeDef GPO_EXTI;
  #define H_EXTI_6 GPO_EXTI
  #define NFC04A1_LED1_PIN_PORT          GPIOA //GPIOB  /* Modified by SB */
  #define NFC04A1_LED1_PIN               LL_GPIO_PIN_1 //GPIO_PIN_4 /* Modified by SB */
  #define NFC04A1_LED2_PIN_PORT          GPIOA //GPIOB /* Modified by SB */
  #define NFC04A1_LED2_PIN               LL_GPIO_PIN_6 //GPIO_PIN_5 /* Modified by SB */
  #define NFC04A1_LED3_PIN_PORT          GPIOB //GPIOA /* Modified by SB */
  #define NFC04A1_LED3_PIN               LL_GPIO_PIN_0 //GPIO_PIN_10 /* Modified by SB */
#endif
/* End of Modified by SB */
  
#define NFC04A1_NFCTAG_INSTANCE         (0)

#define NFC04A1_NFCTAG_GPO_PRIORITY     (0)

#ifdef __cplusplus
}
#endif

#endif /* __NFC04A1_CONF_H__*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

