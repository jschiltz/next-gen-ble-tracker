/**
  ******************************************************************************
  * @file    NG_Tick_FlashUtil.h
  * @author  Thomas W Liu
  * @brief   Header file of flash memory utilities.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
        BlueNRG-LP memory map
        +-------------------------+ 0x2000FFFF
        |  RAM (64K)              |
        +-------------------------+ 0x20000000
        |                         |
        |                         |
        +-------------------------+ 0x1007FFFF
        |  Stack nonvolatile data |
        |  Page 126/127 (2 page)  |
        |  FLASH (4K)             |
        +-------------------------+ 0x1007F000
        |  System Info Block      |
        |  Page 125 (1 page)      |
        |  FLASH (2K)             |
        +-------------------------+ 0x1007E800
        |  Page 0-124 (125 page)  |
        |  FLASH (250K)           |
        +-------------------------+ 0x10040000
        |                         |
        +-------------------------| 0x100017FF
        |   ROM (6K)              |
        +-------------------------+ 0x10000000
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_FLASH_UTIL_H
#define __NG_TICK_FLASH_UTIL_H

/* Defines -------------------------------------------------------------------*/
#define FLASH_TM_PIN			LL_GPIO_PIN_5
#define FLASH_TM_GPIO_PORT		GPIOB
#define FLASH_TM_GPIO_CLK_ENABLE()      LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOB)

/* Defines -------------------------------------------------------------------*/
/**
  * @brief  define LoL local memory command - attribute, flash
  *         LOL_LM_FLASH_ERASE = to erase flash memory
  *         LOL_LM_FLASH_READ = read flash memory n words into buffer
  *         LOL_LM_FLASH_WRITE = write a word to flash memory @ selected address
  *         LOL_LM_FLASH_NWRITE = write n word to flash memory @ selected address
  *         LOL_LM_FLASH_BURSTWRITE = burst write n word to flash memory @ selected
  *                                   address, in block of 16 bytes or 4 long words
  *         LOL_LM_FLASH_SYSINFOWR = write System Info Block to flash memory, required data only
  *         LOL_LM_FLASH_SYSINFORD = read System Info Block from flash memory, required data only
  *         LOL_LM_FLASH_SYSINFOINIT = init System Info Block in RAM with various value
  *         LOL_LM_FLASH_SYSINFOSM = init state machine to write  System Info Block from RAM
  *                                  to flash, required data only
  */
enum {
    LOL_LM_FLASH_ERASE = 1,
    LOL_LM_FLASH_READ,
    LOL_LM_FLASH_WRITE,
    LOL_LM_FLASH_NWRITE,
    LOL_LM_FLASH_BURSTWRITE,
    LOL_LM_FLASH_SYSINFOWR,
    LOL_LM_FLASH_SYSINFORD,
    LOL_LM_FLASH_SYSINFOINIT,
    LOL_LM_FLASH_SYSINFOSM
};

/* constant declaration ------------------------------------------------------*/
/**
  * @brief  define flash memory related
  *         FLASH_PAGE_TOTAL = total flash memory pages (2048 bytes per page)
  *         FLASH_PAGE_SYSINFO = flash page location for SystemIB
  *         FLASH_ADDR_SYSINFO = flash address where SystemIB is stored
  */
#define FLASH_PAGE_TOTAL	(((_MEMORY_FLASH_END_ + 1) - _MEMORY_FLASH_BEGIN_) / _MEMORY_BYTES_PER_PAGE_)
#define FLASH_PAGE_SYSINFO	(FLASH_PAGE_TOTAL - 3)
#define FLASH_ADDR_SYSINFO	(_MEMORY_FLASH_BEGIN_ + (FLASH_PAGE_SYSINFO * _MEMORY_BYTES_PER_PAGE_))

/* Defines -------------------------------------------------------------------*/
/* flash memory control block: action definition -----------------------------*/
/**
  * @brief  define action
  *         idle = no action required
  *         erase = to erase flash memory
  *         read = read flash memory into System Info Block
  *         erase write = erase flash then write System Info Block to flash memory
  *         write = write System Info Block to flash memory
  */
enum {
    FLASHCB_ACT_IDLE = 0,
    FLASHCB_ACT_ERASE,
    FLASHCB_ACT_READ,
    FLASHCB_ACT_ERASE_WRITE,
    FLASHCB_ACT_WRITE
};

/* flash memory control block: state for flash -------------------------------*/
/**
  * @brief  define state
  *         idle = no activity
  *         erase = erase flash memory
  *         read = read flash memory into System Info Block
  *         write = write System Info Block to flash memory
  *         verify = read back flash to compare
  *         update = if error, increment error count
  */
enum {
    FLASHCB_ST_IDLE = 0,
    FLASHCB_ST_ERASE,
    FLASHCB_ST_ERASE_WR,
    FLASHCB_ST_READ,
    FLASHCB_ST_WRITE,
    FLASHCB_ST_VERIFY,
    FLASHCB_ST_UPDATE
};

/**
  * @brief  define flash memory last error encountered
  *         none = no error
  *         erase = flash erase error
  *         write = flash write error
  *         read = flash memory read error
  *         verify = flash memory write read verify error
  */
#define FLASHCB_ERR_NONE        0x00
#define FLASHCB_ERR_ERASE       0x01
#define FLASHCB_ERR_WRITE       0x02
#define FLASHCB_ERR_READ        0x04
#define FLASHCB_ERR_VERIFY      0x08

    
/* structure declaration -----------------------------------------------------*/
/* flash memory control block structure --------------------------------------*/
/**
  * @brief  define structure
  *         uAction = to do
  *         uState = current flash control block state
  *         uStatus = status of current tasks
  *         uErrCode = last error code
  *         uwErrCnt = accumulative error count
  */
typedef struct 
{
    uint8_t  uAction;   /* action: idle, erase, read, write */
    uint8_t  uState;    /* state: idle, erase, read, write */
    uint8_t  uStatus;   /* status: TRUE = good, FALSE = error encountered */
    uint8_t  uErrCode;  /* last error code: 0 = no error, 1 = erase, 2 write */
    uint16_t uwErrCnt;  /* error count */
}  NGT_FLASHCB_TYPE;
    
/* extern variable declaration------------------------------------------------*/

/* extern function declaration------------------------------------------------*/
extern void CheckFlashMem(void);
extern uint8_t xLoLMT_FlashUtil( uint8_t *);
extern void Flash_ReadNWord( uint32_t *, uint32_t, uint16_t);

#endif  /* __NG_TICK_FLASH_UTIL_H */
