/**
  ******************************************************************************
  * @file    NG_Tick_Acc.h
  * @author  Thomas W Liu
  * @brief   header file of LIS2DW12 i2c accelerometer module.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_ACC_H
#define __NG_TICK_ACC_H

/* defines -------------------------------------------------------------------*/
/**
  * @brief Constants for movement detection
  */
#define SAMPLE_RATE             3       /* Rate of accelerometer being polled (in seconds) */

#define HISTOGRAM_SIZE_LIMIT    7       /* Fixed size of the histogram */
#define TIME_PERIOD_SIZE        7       /* Number of time periods for Tool_Last_Used */

#define MAX_MOVE_COUNTER        3       /* Number of consecutive moving threshold reads to enter moving_state  */
#define MAX_NOT_MOVE_COUNTER    13      /* Number of consecutive not_moving threshold reads to enter not_moving_state */

/**
  * @brief  accelerometer io port & interrupts definition
  */
/* PCBA revision 2 */

#define LIS2DW12_INT1_PIN                        LL_GPIO_PIN_7
#define LIS2DW12_INT1_GPIO_PORT                  GPIOB
#define LIS2DW12_INT1_GPIO_CLK_ENABLE()          LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOB) 
#define LIS2DW12_INT1_SYSCFG_CLK_ENABLE()        LL_APB0_EnableClock(LL_APB0_PERIPH_SYSCFG)   

#define MAXRETRY_FINDACC        3       /* msximum retry to find accelerometer device */
#define RETRYDLY_FINDACC        50      /* retry delay, in ms */

/* accelerometer control block: state for acc ------------------------------------------*/
/**
  * @brief  define state
  *         idle = no activity
  *         waitsysf = wait on system NFC operation, set/clear by system CB
  *         read = read NFC memory
  *         write = write data to NFC memory
  *         writeall = write all data to NFC memory (entire NDEF record & etc)
  *         writedyn = write dynamic data to NFC memory (begin @ advertisement & etc)
  *         update = if error, increment error count
  */
enum {
    ACCCB_ST_IDLE = 0,
    ACCCB_ST_WAITSYSF,
    ACCCB_ST_ERASE,
    ACCCB_ST_READ,
    ACCCB_ST_WRITE
};

/* accelerometer control block: action definition -----------------------------*/
/**
  * @brief  define action
  *         idle = no action required
  *         chk_int1 = check interrupt level
  *         waitsysf = wait on system NFC memory op to complete, follow by write
  *         read = read NFC memory into System Info Block
  *         write = write System Info Block to NFC memory
  */
enum {
    ACCCB_ACT_IDLE = 0,
    ACCCB_ACT_CHK_INT1,
    ACCCB_ACT_WAITSYSF,
    ACCCB_ACT_READ,
    ACCCB_ACT_WRITE
};

/* accelerometer movement detection -------------------------------------------*/
/**
  * @brief  define action
  *         ACCEL_STATE_NOT_MOVING = Default state in SM, set when no movement
  *         ACCEL_STATE_MOVING     = Set when movement is recognized
  *         ACCEL_STATE_IDLE       = Accelerometer is not reading values
  *         ACCEL_STATE_SHUTDOWN   = Accelerometer is completely shut down
  */
typedef enum
{
    ACCEL_STATE_NOT_MOVING = 0,
    ACCEL_STATE_MOVING,
    ACCEL_STATE_IDLE,
    ACCEL_STATE_SHUTDOWN
} ACCEL_STATE_ENUM;

/**
  * @brief  define action
  *         USAGE_LT_HOUR     = tool last used < 1 hour ago
  *         USAGE_LT_DAY      = tool last used < 1 day ago
  *         USAGE_LT_7_DAYS   = tool last used < 1 week ago
  *         USAGE_LT_30_DAYS  = tool last used < 30 days ago
  *         USAGE_LT_90_DAYS  = tool last used < 90 days ago
  *         USAGE_LT_180_DAYS = tool last used < 180 days ago
  *         USAGE_LT_365_DAYS = tool last used < 365 days ago
  *         USAGE_GT_365_DAYS = tool last used >= 365 days ago
  */
typedef enum
{
    USAGE_LT_HOUR = 0,
    USAGE_LT_DAY,
    USAGE_LT_7_DAYS,
    USAGE_LT_30_DAYS,
    USAGE_LT_90_DAYS,
    USAGE_LT_180_DAYS,
    USAGE_LT_365_DAYS,
    USAGE_GT_365_DAYS,
} TOOL_LAST_USED_ENUM;
/**
  * @brief  define flash memory last error encountered
  *         none = no error
  *         erase = erase error
  *         write = write error
  *         read = read error
  */
#define ACCCB_ERR_NONE        0
#define ACCCB_ERR_ERASE       1
#define ACCCB_ERR_WRITE       2
#define ACCCB_ERR_READ        4

/* structure declaration -----------------------------------------------------*/
/* accelerometer control block structure --------------------------------------*/
/**
  * @brief  define structure
  *         uFound = device found, T/F
  *         uMaxRetry = max retry
  *         uRetryCnt = current retry counter
  *         uAction = action to be performed
  *         uState = current flash control block state
  *         uStatus = status of current tasks
  *         uErrCode = last error encountered
  *         uwErrCnt = accumulative error count
  */
typedef struct 
{
    uint8_t     uFound;         /* is st25dv found? TRUE if found, FALSE if not found */
    uint8_t     iMaxRetry;      /* maximum retry to find device */
    uint8_t     iRetryCnt;      /* current retry counter to find device */
    uint8_t     uAction;        /* state: idle, search, wipe, read, write */
    uint8_t     uState;         /* state: idle, read, write */
    uint8_t     uStatus;        /* current status: TRUE = good, FALSE = error encountered */
    uint8_t     uErrCode;       /* last error code: 0 = no error, 1 = erase, 2 write */
    uint16_t    uwErrCnt;       /* error count */
}  NGT_ACCCB_TYPE;
    
/* accelerometer movement detection structure --------------------------------------*/
/**
  * @brief  define structure
  *         myState = current state of accelerometer movement detection state machine
  *         bMyThreshold = current accelerometer threshold value
  *         moving_counter = counter to determine if device has moved long enough to enter moving_state in SM
  *         not_moving_counter = counter to determine if device has stayed stationary long enough to enter not_moving_state in SM
  *         histogram_activity_counter = counter is incremented every time activity state is entered
  */
typedef struct
{
    ACCEL_STATE_ENUM      myState;
    bool                  bMyThreshold;
    uint8_t               moving_counter;
    uint8_t               not_moving_counter;
    uint32_t              histogram_activity_counter;
} ACCEL_SM_CTRL;

/**
  * @brief  define structure
  *         tool_last_used = value to set bit that correlates to how long tool has been inactive
  *         histogram = histogram that contains percentages usage duration
  */
typedef struct
{
#if 0
    TOOL_LAST_USED_ENUM *pToolLastUsed;
    uint8_t *pHistogram;
#endif
    TOOL_LAST_USED_ENUM tool_last_used;
    uint8_t histogram[7];
} NGT_ACCINFO_TYPE;

/* extern variable declaration -----------------------------------------------*/
extern uint32_t motion_detected;
extern uint16_t iAccInt1Cnt;    /* @#$% debug acc int1 counter */
extern uint16_t iAccSecCnt;     /* @#$% debug acc second counter */
extern ACCEL_SM_CTRL accel_sm;  /* Struct contains state machine variables */

/* extern function prototype -------------------------------------------------*/
extern void StartNextTransfers(void);

extern void Acc_Init_All(void);
extern void CheckAcc(void);
//extern void NormalCheckAcc(void);

extern void Accelerometer_Enter_Shutdown(void);
extern void Accelerometer_Exit_Shutdown(void);
extern void Accelerometer_Enter_Idle(void);
extern void Accelerometer_Exit_Idle(void);

#endif /* __NG_TICK_ACC_H */
