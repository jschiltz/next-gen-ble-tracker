/**
  ******************************************************************************
  * @file    NG_Tick_NFC.h
  * @author  Thomas W Liu
  * @brief   header file of NFC.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_NFC_H
#define __NG_TICK_NFC_H

/* Defines -------------------------------------------------------------------*/
/**
  * @brief  define LoL local memory command - attribute, flash
  *         LOL_LM_NFC_READ = read NFC memory
  *         LOL_LM_NFC_WRITE = write URL NDEF record to NFC memory
  *         LOL_LM_NFC_WRITEDYN = write dynamic data to NFC memory
  *         LOL_LM_NFC_ERASE = to erase NFC memory
  */
enum {
    LOL_LM_NFC_READ = 0,
    LOL_LM_NFC_WRITE,
    LOL_LM_NFC_WRITEDYN,
    LOL_LM_NFC_ERASE
};

/* Defines -------------------------------------------------------------------*/
/**
  * @brief  NFC io port & interrupts definition
  */
#define ST25DV_PIN                              LL_GPIO_PIN_2
#define ST25DV_GPIO_PORT                        GPIOB
#define ST25DV_GPIO_CLK_ENABLE()                LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOB) 
#define ST25DV_SYSCFG_CLK_ENABLE()              LL_APB0_EnableClock(LL_APB0_PERIPH_SYSCFG)   
#define ST25DV_EXTI_LINE                        LL_EXTI_LINE_PB2
#define ST25DV_EXTI_IRQn                        GPIOB_IRQn
#define ST25DV_EXTI_LINE_ENABLE()               LL_EXTI_EnableIT(ST25DV_EXTI_LINE)   
#define ST25DV_EXTI_RISING_TRIG_ENABLE()        LL_EXTI_SetTrigger(LL_EXTI_TRIGGER_RISING_EDGE, ST25DV_EXTI_LINE)
#define ST25DV_WAKEUP                           WAKEUP_PB2
#define NFC_WAKEUP                              ST25DV_WAKEUP

#define ST25DV_PWR_PIN                          LL_GPIO_PIN_3

/**
  * @brief  NFC GPO definition
  */
#define ST25DV_GPO_INT_ENABLE   (ST25DV_GPO_ENABLE_MASK | ST25DV_GPO_FIELDCHANGE_MASK)
#define ST25DV_GPO_INT_DISABLE  0

/**
  * @brief  NFC NDEF definition
  */
#define NDEF_HDR_TYPE_MB        0x80    /* message begin, first record in message */
#define NDEF_HDR_TYPE_ME        0x40    /* message end, last record in message */
#define NDEF_HDR_TYPE_CF        0x20    /* chunk flag, record is chunked */
#define NDEF_HDR_TYPE_SR        0x10    /* short record, used for payload length */
#define NDEF_HDR_TYPE_IL        0x10    /* id length, id length field is present */
#define NDEF_HDR_TYPE_TFT_EMPTY 0x00    /* TFT, empty, record doesnty contain any information */
#define NDEF_HDR_TYPE_TFT_WK    0x01    /* TFT, well-known, data is defined by record type definition */
#define NDEF_HDR_TYPE_TFT_MIME  0x02    /* TFT, MIME, MIME */
#define NDEF_HDR_TYPE_TFT_URI   0x03    /* TFT, URI, ptr to uniform resource identifier */
#define NDEF_HDR_TYPE_TFT_EXT   0x04    /* TFT, external, user defined data */
#define NDEF_HDR_TYPE_TFT_UKN   0x05    /* TFT, unknown, data is unknown */
#define NDEF_HDR_TYPE_TFT_UCHG  0x06    /* TFT, unchanged */
#define NDEF_HDR_TYPE_TFT_RSV   0x07    /* TFT, reserved */

/**
  * @brief  index to CCF record definition
  */
/* index to CCF */
#define CCF_INDEX_REC_HDR       6       /* index to record header in CCF*/

/**
  * @brief  index to NDEF record definition
  */
/* index to NDEF record - URL */
#define NDEF_INDEX_REC_HDR      0       /* index to record header */
#define NDEF_INDEX_TYPE_LEN     1       /* index to type length, 1 = 1 byte */
#define NDEF_INDEX_PL_LEN       2       /* index to payload length */
#define NDEF_INDEX_REC_TYPE     3       /* index to type for URI record type */
#define NDEF_INDEX_HDR_ID       4       /* index to URI header id */
#define NDEF_INDEX_URL_BODY     5       /* index to URI body */

/* index to NDEF record - text, index to URI applies */
#define NDEF_INDEX_STATUS       4       /* index to status - UTF-8 */
#define NDEF_INDEX_LANG_CODE_M  5       /* index to language code, MSB */
#define NDEF_INDEX_LANG_CODE_L  6       /* index to language code, LSB */

/* index to NFC NDEF constant string */
#define NFC_COS_NDEF     0       /* NDEF memory location */
#define NFC_COS_MPBID    13      /* MPBID location */
#define NFC_COS_BADV     26      /* BLE advertisement offset location */
#define NFC_COS_BUZZEV   47      /* buzzer event offset location */
#define NFC_COS_NFCEV    54      /* NFC event offset location */
#define NFC_COS_RSTEV    61      /* reset event offset location */

/* index to NFC NDEF RAM string, where all records are stored */
#define NFC_RMOS_NDEF     (NFC_COS_NDEF + NDEF_INDEX_URL_BODY)  /* NDEF memory location */
#define NFC_RMOS_BADV     (NFC_COS_BADV + NDEF_INDEX_URL_BODY)  /* BLE advertisement offset location */
#define NFC_RMOS_BUZZEV   (NFC_COS_BUZZEV + NDEF_INDEX_URL_BODY)  /* buzzer event offset location */
#define NFC_RMOS_NFCEV    (NFC_COS_NFCEV + NDEF_INDEX_URL_BODY)   /* NFC event offset location */
#define NFC_RMOS_RSTEV    (NFC_COS_RSTEV + NDEF_INDEX_URL_BODY)   /* reset event offset location */

/* index to NFC memory */
#define NFC_MEMOS_BADV  37      /* NFC memory offset for BLE advertisement location */
#define NFC_MEMLEN_DYNM 39      /* dynamic data length in NFC NDEF */

/* number of digit to display for each data field */
#define NFC_DIG_BUZZEV  4       /* 4 digit for NFC buzzer event, hex */
#define NFC_DIG_NFCEV   4       /* 4 digit for NFC NFC event, hex */
#define NFC_DIG_RSTEV   4       /* 4 digit for NFC reset event, hex */


#define SIZE_RECX_STR   24      // 8

/**
  * @brief  define user memory read buffer size
  */
#define MSG_UM_READ_SIZE 128

/**
  * @brief  define user memory write buffer size
  */
#define NFC_MEM_STR_SIZE    MSG_UM_READ_SIZE

/* Defines -------------------------------------------------------------------*/
/* NFC control block: state for NFC ------------------------------------------*/
/**
  * @brief  define state
  *         idle = no activity
  *         waitsysf = wait on system NFC operation, set/clear by system CB
  *         read = read NFC memory
  *         write = write data to NFC memory
  *         writeall = write all data to NFC memory (entire NDEF record & etc)
  *         writedyn = write dynamic data to NFC memory (begin @ advertisement & etc)
  *         update = if error, increment error count
  */
enum {
    NFCCB_ST_IDLE = 0,
    NFCCB_ST_WAITSYSF,
    NFCCB_ST_ERASE,
    NFCCB_ST_READ,
    NFCCB_ST_WRITE,
    NFCCB_ST_WRITEALL,
    NFCCB_ST_WRITEDYN,
    NFCCB_ST_UPDATE
};

/* NFC control block: action definition -----------------------------*/
/**
  * @brief  define action
  *         idle = no action required
  *         waitsysf = wait on system NFC memory op to complete, follow by write
  *         erase = to erase NFC memory
  *         read = read NFC memory into System Info Block
  *         write = write System Info Block to NFC memory
  *         writedyn = write dyanmic data in the NFC memory
  */
enum {
    NFCCB_ACT_IDLE = 0,
    NFCCB_ACT_WAITSYSF,
    NFCCB_ACT_ERASE,
    NFCCB_ACT_READ,
    NFCCB_ACT_WRITE,
    NFCCB_ACT_WRITEDYN
};

/**
  * @brief  define flash memory last error encountered
  *         none = no error
  *         erase = erase error
  *         write = write error
  *         writedyn = dynamic write error
  *         read = read error
  *         ship read = ship mode restore read error
  *         norm read = normal mode restore read error
  */
#define NFCCB_ERR_NONE        0
#define NFCCB_ERR_ERASE       1
#define NFCCB_ERR_WRITE       2
#define NFCCB_ERR_WRITEDYN    4
#define NFCCB_ERR_READ        8
#define NFCCB_ERR_SHIP_READ   0x10
#define NFCCB_ERR_NORM_READ   0x20

#define MAXRETRY_FINDNFC        3       /* msximum retry to find NFC device */
#define RETRYDLY_FINDNFC        50      /* retry delay, in ms */

/* structure declaration -----------------------------------------------------*/
/* NFC control block structure --------------------------------------*/
/**
  * @brief  define structure
  *         uFound = device found, T/F
  *         uMaxRetry = max retry
  *         uRetryCnt = current retry counter
  *         uAction = action to be performed
  *         uState = current flash control block state
  *         uStatus = status of current tasks
  *         uErrCode = last error encountered
  *         uwErrCnt = accumulative error count
  */
typedef struct 
{
    uint8_t     uFound;         /* is st25dv found? TRUE if found, FALSE if not found */
    uint8_t     iMaxRetry;      /* maximum retry to find device */
    uint8_t     iRetryCnt;      /* current retry counter to find device */
    uint8_t     uAction;        /* action: idle, search, wipe, read, write */
    uint8_t     uState;         /* state: idle, read, write */
    uint8_t     uStatus;        /* current status: TRUE = good, FALSE = error encountered */
    uint8_t     uErrCode;       /* last error code: 0 = no error, 1 = erase, 2 write */
    uint16_t    uwErrCnt;       /* error count */
}  NGT_NFCCB_TYPE;


/* define declaration --------------------------------------------------------*/
/**
  * @brief  define time delay in write and read routine
  */
#define NFC_WRITE_DELAY_MS      52      /* 49 ms + 3ms for error margin */
#define NFC_READ_DELAY_MS       18      /* 15 ms + 3ms for error margin */
#define NFC_PREWRITE_DELAY_MS   200     /* 200ms for power on */
#define NFC_AFTWRITE_DELAY_MS   100     /* 100ms before power off */

/* Includes ------------------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/

/* external variable declaration ---------------------------------------------*/
extern __IO uint8_t ubNFCInt_flag;     /* boolean NFC interrupt 2 flag */
extern uint8_t msg_NFT_Test_UM_Rd[];   /* read buffer */
extern uint8_t iNFCRecord0[];
extern __IO int16_t iNFC_DelayCnt;     /* from bluenrg_lp_it.c, NFC delay counter */        

/* external function prototype -----------------------------------------------*/
extern void NFC_Init_All(void);
extern int32_t SearchNFC(void);
extern void CheckNFC(void);
extern int16_t ReadNFCMemToDynData(void);
extern uint8_t xLoLMT_NFCRead( uint8_t *);
extern uint8_t xLoLMT_NFCWrite( uint8_t *);
extern void xNFC_TimeDelay( int16_t);

#endif  /* __NG_TICK_NFC_H */



