/**
  ******************************************************************************
  * @file    NG_Tick_sys.h
  * @author  Thomas W Liu
  * @brief   header file of system information block declaration.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  *
  * Note     NGT_SYSTEMINFO_TYPE structure is for system information block
  *          It consists of flash. RAM, NFC and accelerometer info block
  *
  * Note     NGT_SYSFLASHINFO_TYPE sturucture contain variables stored in flash memory
  *     ##$$ align this structure to 16 byte addressing.  must verify manually ##$$
  *     ##$$ blanks are declared to preserve 16 bytes boundr write and read ##$$
  *
  * Note     NGT_SYSRAMINFO_TYPE sturucture contain variables stored in RAM
  *
  * Note     NGT_SYSNFCINFO_TYPE sturucture contain variables stored in NFC
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_SYS_H
#define __NG_TICK_SYS_H

/* Defines constant ----------------------------------------------------------*/
#define DEF_A_UNDEFINED         "Undefined"
#define DEF_A_UNITSN            0x46
#define DEF_A_ASSETCODE         0x2222222222222222      /* Asset Code now matches ADMIN password */
#define DEF_A_ASSETCODE_PHONE   0xFFFFFFFFFFFFFFFF
#define DEF_A_UNIT_RST_NONE     0       /* unit reset mode: no */
#define DEF_A_ATT_STREAM_OFF    0       /* attribute straming OFF */
#define DEF_A_ATT_STREAM_ON     1       /* attribute streaming ON */
#define DEF_A_OWNER_CLAIMKEY    0x6666666666666666  /* 8 bytes */

#define DEF_A_BLE_SLEEP_TM      0      /* manufacturing test, BLE sleep current, default time */
#define DEF_A_BLE_SLEEP_TM_MAX  60000  /* manufacturing test, BLE sleep current, max time */

#define BLE_SHIP_MODE_DEF       0      /* BLE ship mode, default, no special mode enabled */
#define BLE_SHIP_MODE_NADS      1      /* BLE ship mode, no advertisement during sleep */
#define BLE_BLUENRG_LP          0x20   /* BLE BlueNRG LP */

#define BLE_ADVERTISING_NORMAL  0
#define BLE_ADVERTISING_IBEACON 1
    
#define PRODUCTION_MODE         0      /* current mode: production mode, not in ship mode, power on */
#define SHIP_MODE               1      /* current mode: ship mode, power down and wakeup */
#define NORMAL_MODE             2      /* current mode: normal operation */


/* Defines -------------------------------------------------------------------*/

/**
  * @brief  define structure
  */

#define SYS_LOL_MMAPVER_VAL    0x0001  /* memory map version */

#define SYS_LOL_SWPN_VAL     0x358700C1  /* part number */

#define FIRMWARE_VERSION_ID_ADVERTISING   0x01   

#define DEFAULT_COIN_CELL_ADVERTISING   0x7F    /* 0111 111, bits 6:0 are coin cell default */
#define DEFAULT_TOOL_LAST_USED          0xE0    /* 1110 000, bits 7:5 are  Tool Last Used default */
#define DEFAULT_HISTOGRAM               0xFF    
    
#define HISTOGRAM_MASK          0x7F
#define FWVER_L_MASK            0x80
#define FWVER_H_MASK            0x1F

#define COMPANY_ID_HIGH             0x01
#define COMPANY_ID_LOW              0x65

#define ONE_KEY_UUID_HIGH           0xFD
#define ONE_KEY_UUID_LOW            0xF5

#define DEFAULT_MPBID_0             0x01
#define DEFAULT_MPBID_1             0x00
#define DEFAULT_MPBID_2             0xFF
#define DEFAULT_MPBID_3             0xFF
#define DEFAULT_MPBID_4             0xFF

/* Product ID value */
#define SYS_LOL_PRODID_VAL   0x00000000

/* Hardware Version */
#define SYS_LOL_HWVER_VAL    0x00000001

/* Firmware Version */
#define FIRMWARE_VERSION_REV              0x01
#define FIRMWARE_VERSION_MAJOR            0x01
#define FIRMWARE_VERSION_MINOR            0x00
#define FIRMWARE_VERSION_SUB              0x00
#define SYS_LOL_FWVER_VAL    (FIRMWARE_VERSION_REV << 24) | (FIRMWARE_VERSION_MAJOR << 16) | (FIRMWARE_VERSION_MINOR << 8)  | FIRMWARE_VERSION_SUB/* version = 0xffff, revision 0xffff */
//#define SYS_LOL_FWVER_VAL    0xffffffff  /* version = 0xffff, revision 0xffff */
//#define SYS_LOL_FWVER_BYTE0  ((SYS_LOL_FWVER_VAL >> 24) & 0x000000ff)
//#define SYS_LOL_FWVER_BYTE1  ((SYS_LOL_FWVER_VAL >> 16) & 0x000000ff)
//#define SYS_LOL_FWVER_BYTE2  ((SYS_LOL_FWVER_VAL >> 8) & 0x000000ff)
//#define SYS_LOL_FWVER_BYTE3  (SYS_LOL_FWVER_VAL & 0x000000ff)

#define SYS_LOL_BOD_VAL       0x600B2E66  /* BOD, 1/22/2021, 1pm */
#define SYS_LOL_DFOS_VAL     0x600B2FA2  /* DFOS, activation date, 1/25/2021, 8am */
#define SYS_LOL_DLU_VAL      0x00000007  /* date last used (tool last used), > 365 days */

#define SYS_LOL_DTREF        0x601A2E87  /* date time reference, random */
#define SYS_LOL_PFID_NGT     0x0007      /* platform identifier, next generation tracker */

#define SYS_LOL_BLE_MAC      0x10E7C6805A21   /* BLE MAC address */
#define SYS_LOL_BLEMAC_BYTE0 (SYS_LOL_BLE_MAC & 0x000000ff)
#define SYS_LOL_BLEMAC_BYTE1 ((SYS_LOL_BLE_MAC >> 8) & 0x000000ff)
#define SYS_LOL_BLEMAC_BYTE2 ((SYS_LOL_BLE_MAC >> 16) & 0x000000ff)
#define SYS_LOL_BLEMAC_BYTE3 ((SYS_LOL_BLE_MAC >> 24) & 0x000000ff)
#define SYS_LOL_BLEMAC_BYTE4 ((SYS_LOL_BLE_MAC >> 32) & 0x000000ff)
#define SYS_LOL_BLEMAC_BYTE5 ((SYS_LOL_BLE_MAC >> 40) & 0x000000ff)

#define SYS_LOL_BGC_BUZZ_ACT_INDX       0       /* BLE generic command buzzer index, action (play/stop) */
#define SYS_LOL_BGC_BUZZ_TONE_INDX      1       /* BLE generic command buzzer index, tone id */
#define SYS_LOL_BGC_BUZZ_REPEAT_INDX    2       /* BLE generic command buzzer index, repeat */

/* structure declaration -----------------------------------------------------*/
#define SYS_LOL_MMVER_SIZE           2  /* memory map version size */
#define SYS_LOL_MPBID_SIZE           5  /* MPBID size */
#define SYS_LOL_SWPN_SIZE            4
#define SYS_LOL_FWVER_SIZE           4
#define SYS_LOL_UPN_SIZE             32 /* unique product name size */
#define SYS_LOL_DT_SIZE              4  /* date time size */
#define SYS_LOL_BLEMAC_SIZE          6  /* BLE MAC address size */
#define SYS_LOL_USN_SIZE             20 /* unit serial number size */
#define SYS_LOL_ASSETCODE_MIN_SIZE   5  /* asset code size, minimum length */
//#define SYS_LOL_ASSETCODE_SIZE       8  /* asset code size, maximum length */
//#define SYS_LOL_ASSETCODE_ALLOC      (SYS_LOL_ASSETCODE_SIZE + 1)  /* +1 for termintaor */
//#define SYS_LOL_OWNCLAIMKEY_SIZE     8  /* ownership claim key password size */
#define SYS_LOL_BGC_BUZZ_SIZE        3  /* BLE generic command buzzer size */
#define SYS_LOL_LUTC_SIZE            2  /* BLE generic command last used time constant size */
#define SYS_LOL_BBPARM_SIZE          7  /* BLE beacon paramter development size */
#define SYS_LOL_BLE_FWVER_SIZE       4  /* BLE firmware version size */
#define SYS_LOL_BLANK_SIZE           8  /*flash variable structure size */
#define NFC_BLE_ADV_SIZE             9  /* NFC BLE advertisement size */

/**
  * @brief  define BLE advertising array packet index
  *    ##$$ some are combined intoa byte, see specifiation ##$$
  */
#define NFC_BLE_ADV_INDEX_CCV           0       /* coin cell voltage, b6:0 */
#define NFC_BLE_ADV_INDEX_TOOLLASTUSED  1       /* tool last used, b7:5 */
#define NFC_BLE_ADV_INDEX_BLE_FWVER_H   1       /* BLE firmware version, high byte, b4:0 */
#define NFC_BLE_ADV_INDEX_BLE_FWVER_L   2       /* BLE firmware version, low byte, b7 */
#define NFC_BLE_ADV_INDEX_HIST_BIN7     2       /* histogram, bin 7 (>= 5hr), b6:0 */
#define NFC_BLE_ADV_INDEX_HIST_BIN6     3       /* histogram, bin 6 (3:00.00 - 4:59.59 hr) */
#define NFC_BLE_ADV_INDEX_HIST_BIN5     4       /* histogram, bin 5 (2:00.00 - 2:59.59 hr) */
#define NFC_BLE_ADV_INDEX_HIST_BIN4     5       /* histogram, bin 4 (1:00.00 - 1:59.59 hr) */
#define NFC_BLE_ADV_INDEX_HIST_BIN3     6       /* histogram, bin 3 (0:30.00 - 0:59.59 hr) */
#define NFC_BLE_ADV_INDEX_HIST_BIN2     7       /* histogram, bin 2 (0:01.00 - 0:29.59 hr) */
#define NFC_BLE_ADV_INDEX_HIST_BIN1     8       /* histogram, bin 1 (0:00.00 - 0:00.59 hr) */

/**
  * @brief  define i2c toekn, who has it currently
  */
#define I2C_TOKEN_DEF           0       /* default */
#define I2C_TOKEN_ACC           1       /* accelerometer */
#define I2C_TOKEN_NFC           2       /* NFC */

/**
  * @brief  define structure, variables stored to flash memory
  *    ##$$ align this structure to 16 byte addressing.  must verify manually ##$$
  *    ##$$ blanks are declared to preserve 16 bytes boundr write and read ##$$
  */
typedef struct 
{
    uint16_t uSysInitKey;                          /* system initliaztion key: first time = 0xff, unintialized = 0x00, initialized = 0x01 */
    uint16_t uiMMVer;                              /* 0x01, 0x00 LoL expect 2 bytes payload response */
    uint32_t ulDeviceSWPN;                         /* 0x01, 0x09 LoL expect 4 bytes payload response, saved to flash on demand */
    uint32_t ulDeviceFwVer;                        /* 0x01, 0x0d LoL expect 4 bytes payload response */
    uint32_t ulBornOnDate;                         /* 0x01, 0x11 LoL expect 4 bytes payload response, saved to flash on demand */
    uint32_t ulDateFirstServ;                      /* 0x01, 0x15 LoL expect 4 bytes payload response, saved to flash on demand */
    uint8_t  sUniqueProdName[SYS_LOL_UPN_SIZE];    /* 0x01, 0x23 LoL expect 0 as terminator */
    uint8_t  aUnitSN[SYS_LOL_USN_SIZE];            /* 0x01, 0xa024 LoL expect up to 20 bytes with terminator, saved to flash on demand */
    uint64_t aOwnerClaimKey;                       /* 0x01, 0xb000, LoL ownership claim key, saved to flash on demand */
    uint64_t uUserPassword;                        /* user password, saved to flash on demand */
    uint64_t uAdminPassword;                       /* admin password, saved to flash on demand */
    uint64_t uServicePassword;                     /* service password, saved to flash on demand */
    uint64_t uMETCOPassword;                       /* METCO password, saved to flash on demand */
    uint64_t aAssetIdCode;                         /* 0x01, 0xa038 LoL expect 8 bytes payload response, saved to flash on demand */
    uint64_t aAssetIdCodePhone;                    /* 0x01, 0xa040 LoL expect 8 bytes payload response, saved to flash on demand */
    uint8_t  aMPBID[SYS_LOL_MPBID_SIZE];           /* 0x01, 0x04 LoL expect 5 bytes payload response, saved to flash on demand */
    uint8_t  uBLEShipMode;                         /* 0x7c, LoL BLE ship mode, saved to flash on demand */
    uint8_t  uBornOnCoinCellV;                     /* 0x7c, read coin cell voltage when received above generic command */
                                                   /* updated when received BornOnDate LoL command */
    uint8_t  uShipModeStatus;                      /* ship mode status, updated when received BLEShipMode LoL command */
    uint8_t  uBlank[SYS_LOL_BLANK_SIZE];           /* preserved to structure aligned to 16 bytes */
}  NGT_SYSFLASHINFO_TYPE;

/**
  * @brief  define structure, variables stored in RAM
  */
typedef struct 
{
    uint8_t  aBLEMACAddr[SYS_LOL_BLEMAC_SIZE];     /* 0x01, 0x63 LoL */
    uint8_t  uBLEFwVer[SYS_LOL_BLE_FWVER_SIZE];    /* 0x7f, LoL BLE firmware version */
    uint32_t ulDateLastUsed;                       /* 0x01, 0x19 LoL expect 4 bytes payload response */
    uint32_t ulDateTimeRef;                        /* 0x01, 0x037 LoL */
    uint16_t uPlatformId;                          /* 0x7c LoL */
    uint16_t uUnitReset;                           /* 0x01, 0xa044, LoL uniet reset */
    uint8_t  uAttributeStream;                     /* 0x01, 0xa048, LoL streaming mode */
    uint16_t uMfgTst_BLESleepA;                    /* 0x71, 0x03 LoL manufaturing Test, BLE sleep current test */
    uint16_t uMfgTst_BuzzerTest;                   /* 0x71, 0x04 LoL manufaturing Test, buzzer test */
    uint8_t  uMfgTst_BLEReset;                     /* 0x71, 0x05 LoL manufaturing Test, BLE reset */
    uint16_t uLastUsedTimeCValue;                  /* 0x77, 0x01 LoL BLE generic command, set last used time constant value*/
    uint8_t  uLastUsedTimeC[SYS_LOL_LUTC_SIZE];    /* 0x77, 0x02 LoL BLE generic command, set last used time constant */
    uint8_t  uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_SIZE];/* 0x77, 0x03 LoL BLE generic command, pla buzzer */
    uint8_t  uCoinCellV;                           /* 0x77, 0x04 LoL BLE generic command, set last used time constant */
    uint8_t  uBLEBeaconParm[SYS_LOL_BBPARM_SIZE];  /* 0x78, 0x00 LoL BLE generic command, BLE beacon paramters */
    uint8_t  uBLEBeaconID;                         /* 0x78, 0x01 LoL BLE generic command, BLE beacon id */
    uint8_t  uBLEBeaconInterval;                   /* 0x78, 0x02 LoL BLE generic command, BLE beacon interval */
    uint8_t  uBLEDisconnect;                       /* 0x7b, LoL BLE disconnect */
    uint8_t  uBLESyncToolPhone;                    /* 0x7e, LoL BLE sync tool phone */
    uint8_t  uBLEAdv[NFC_BLE_ADV_SIZE];            /* BLE advertisement - used by BLE NFC*/
    uint16_t uNFCBuzzerEvents;                     /* buzzer events - used by NFC */
    uint16_t uNFCEvents;                           /* NFC events - used by NFC */
    uint16_t uNFCResetEvents;                      /* reset events - used by NFC */
    uint16_t uPORStatus;                           /* power on reset status */
    uint8_t  uI2CToken;                            /* i2c token */
}  NGT_SYSRAMINFO_TYPE;

/* structure contains flash, RAM and NFC info blocks */
typedef struct 
{
    NGT_SYSFLASHINFO_TYPE       SysFlashIB;     /* system flash info block struct */
    NGT_SYSRAMINFO_TYPE         SysRAMIB;       /* system ram info block struct */
}  NGT_SYSTEMINFO_TYPE;


/* structure declaration -----------------------------------------------------*/
#define SYS_CTRL_STATE_ERR      0xff
#define SYS_CTRL_STATE_IDLE     0
#define SYS_CTRL_STATE_ERASE    1
#define SYS_CTRL_STATE_WRITE    2
#define SYS_CTRL_STATE_READ     3
#define SYS_CTRL_STATE_WAIT     4

typedef struct
{
    uint32_t            uSysFlashIBByteLen;
    NGT_FLASHCB_TYPE    FlashCB;
    NGT_NFCCB_TYPE      NFCCB;
    NGT_ACCCB_TYPE      AccCB;
#if 0
    uint8_t  uSave2Flash;               /* save to flash flag: FALSE = no save, TRUE = save */
    int8_t   uSysState;                 /* system state, 0 = idle, 1 = erase, 2 = write, 3 = read, 4 = wait for reset */
    uint32_t uiSysTmTick;               /* time counter for reset, must wait */
#endif
}  NGT_SYSTEMCTRL_TYPE;

/* variable declaration ------------------------------------------------------*/

/* external variable declaration ---------------------------------------------*/
extern uint8_t SystemIBRdbk[];
extern NGT_SYSFLASHINFO_TYPE *pSystemIBRdbk;  /* debug, mapped to struct, easier to verify data */
extern NGT_SYSTEMINFO_TYPE SystemIB;
extern NGT_SYSTEMCTRL_TYPE SystemCB;

/* external function prototype -----------------------------------------------*/
extern void SystemVar_Init_All(void);

extern void NGT_SystemIB1_init();  /* testing only */
extern void NGT_SystemIB2_init();  /* testing only */

#endif  /* __NG_TICK_SYS_H */

