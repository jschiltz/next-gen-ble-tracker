/******************** (C) COPYRIGHT 2019 STMicroelectronics ********************
* File Name          : OTA_ServiceManager.c
* Author             : AMS - RF Application
* Version            : V1.0.0
* Date               : 04-April-2019
* Description        : BlueNRG-LP OTA Service Manager APIs.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include "bluenrg_lp_it.h"
#include "ble_const.h"
#include "bluenrg_lp_stack.h"
#include "clock.h" 
#include "OTA_ServiceManager.h"
#include "ble_const.h"

#include "OTA_btl.h" 
#include <string.h>
#include "gap_profile.h"
#include "NG_Tick_Acc.h"
#include "NG_Tick_NFC.h"
#include "NG_Tick_FlashUtil.h"
#include "NG_Tick_sys.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#ifdef DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile uint8_t set_connectable = 1;

static Advertising_Set_Parameters_t Advertising_Set_Parameters[1]; 

#define ADV_INTERVAL_MIN_MS  200
#define ADV_INTERVAL_MAX_MS  200

#define HIGH_NIBBLE   1
#define LOW_NIBBLE    2

#define FLASH_NVM_PARAM_BASE  ((const void*)0x1007E800)

/* Private function prototypes -----------------------------------------------*/
static uint8_t convert_byte_to_ASCII( uint8_t byte, uint8_t nibble);

/*******************************************************************************
* Function Name  : setConnectable
* Description    : Enter in connectable mode.
* Input          : None.
* Return         : None.
*******************************************************************************/
static void setConnectable(void)
{    
    Advertising_Set_Parameters[0].Advertising_Handle = 0;
    Advertising_Set_Parameters[0].Duration = 0;
    Advertising_Set_Parameters[0].Max_Extended_Advertising_Events = 0;

    aci_gap_set_advertising_enable(ENABLE, 1, Advertising_Set_Parameters); 
}


/*******************************************************************************
* Function Name  : OTA_ServiceManager_DeviceInit
* Description    : Init the OTA Service Manager device.
* Input          : none.
* Return         : Status.
*******************************************************************************/
uint8_t OTA_ServiceManager_DeviceInit(void)
{
    uint8_t ret;
    char temp_buff[15];
    NGT_SYSFLASHINFO_TYPE Flash_Pointer = {0};

    memcpy((void*)&Flash_Pointer, FLASH_NVM_PARAM_BASE, sizeof(Flash_Pointer));
#ifdef MT_OTA  
    static uint8_t adv_data[] = {0x02,
                                 AD_TYPE_FLAGS, 
                                 FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE | FLAG_BIT_BR_EDR_NOT_SUPPORTED,
                                 15, AD_TYPE_COMPLETE_LOCAL_NAME,
                                 '0','0','0','0','0','0','0','0','0','0','M','K','E',' ',
                                 0x03,  /* Length of field */
                                 AD_TYPE_16_BIT_SERV_UUID_CMPLT_LIST,
                                 ONE_KEY_UUID_LOW,
                                 ONE_KEY_UUID_HIGH,
    
                                 /* Advertising data: manufacturer specific data */
                                 0x03,  /* Length of field */
                                 AD_TYPE_MANUFACTURER_SPECIFIC_DATA,  //manufacturer type
                                 COMPANY_ID_LOW,  /* Company identifier code LOW */
                                 COMPANY_ID_HIGH, /* Company identifier code HIGH */
};

#else
    static uint8_t adv_data[] = {0x02,AD_TYPE_FLAGS, FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE|FLAG_BIT_BR_EDR_NOT_SUPPORTED,
                               14, AD_TYPE_COMPLETE_LOCAL_NAME,'O','T','A','S','e','r','v','i','c','e','M','g','r'};

#endif

    /* Convert MPBID to STRING */
    temp_buff[0] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[0], HIGH_NIBBLE);
    temp_buff[1] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[0], LOW_NIBBLE);
    temp_buff[2] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[1], HIGH_NIBBLE);
    temp_buff[3] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[1], LOW_NIBBLE);
    temp_buff[4] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[2], HIGH_NIBBLE);
    temp_buff[5] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[2], LOW_NIBBLE);
    temp_buff[6] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[3], HIGH_NIBBLE);
    temp_buff[7] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[3], LOW_NIBBLE);
    temp_buff[8] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[4], HIGH_NIBBLE);
    temp_buff[9] = convert_byte_to_ASCII(Flash_Pointer.aMPBID[4], LOW_NIBBLE);
    
    /* Put MPBID in for Name */
    memcpy(&adv_data[5], temp_buff, 10);
    adv_data[15] = 'M';
    adv_data[16] = 'K';
    adv_data[17] = 'E';
    adv_data[18] = 0x00;

    uint8_t bdaddr[] = {0};
    uint8_t length = 6;
    
    aci_hal_read_config_data(CONFIG_DATA_STORED_STATIC_RANDOM_ADDRESS, &length, bdaddr);
  
    ret = aci_gatt_srv_init();    
  
    uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
    
    ret  = aci_gap_init(GAP_PERIPHERAL_ROLE, 0,14, STATIC_RANDOM_ADDR, &service_handle, &dev_name_char_handle, &appearance_char_handle);
  
    /* Add OTA bootloader service */
    ret = OTA_Add_Btl_Service();

    if(ret == BLE_STATUS_SUCCESS)
    {
        PRINTF("\r\nOTA service added successfully.\n");
    }
    else
    {
        PRINTF("\r\nError while adding OTA service.\n");
    }

    /* 0 dBm output power */
    aci_hal_set_tx_power_level(0,25);
  
  
    ret = aci_gap_set_advertising_configuration(0, GAP_MODE_GENERAL_DISCOVERABLE,
                                                ADV_PROP_CONNECTABLE|ADV_PROP_SCANNABLE|ADV_PROP_LEGACY,
                                                (ADV_INTERVAL_MIN_MS*1000)/625, (ADV_INTERVAL_MAX_MS*1000)/625, //32, 32,
                                                ADV_CH_ALL,
                                                0,NULL,
                                                ADV_NO_WHITE_LIST_USE,
                                                0, /* 0 dBm */
                                                LE_1M_PHY, /* Primary advertising PHY */
                                                0, /* 0 skips */
                                                LE_1M_PHY, /* Secondary advertising PHY. Not used with legacy advertising. */
                                                0, /* SID */
                                                0 /* No scan request notifications */);
  
    ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(adv_data), adv_data);
  
    /* Add OTA service UUID to scan response */
    aci_gap_set_scan_response_data(0,18,BTLServiceUUID4Scan);
  
    return ret;
}


/*******************************************************************************
* Function Name  : APP_Tick.
* Description    : Tick to run the application state machine.
* Input          : none.
* Return         : none.
*******************************************************************************/
void APP_Tick(void)
{
    if(set_connectable) 
    {
        setConnectable();
        set_connectable = 0;
    }
}


/* ***************** BlueNRG-1 Stack Callbacks ********************************/

/* This function is called when there is a LE Connection Complete event.
*/
void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
{ 
  
}/* end hci_le_connection_complete_event() */

/*******************************************************************************
 * Function Name  : hci_le_enhanced_connection_complete_event.
 * Description    : This event indicates that a new connection has been created
 * Input          : See file bluenrg_lp_events.h
 * Output         : See file bluenrg_lp_events.h
 * Return         : See file bluenrg_lp_events.h
 *******************************************************************************/
void hci_le_enhanced_connection_complete_event(uint8_t Status,
                                               uint16_t Connection_Handle,
                                               uint8_t Role,
                                               uint8_t Peer_Address_Type,
                                               uint8_t Peer_Address[6],
                                               uint8_t Local_Resolvable_Private_Address[6],
                                               uint8_t Peer_Resolvable_Private_Address[6],
                                               uint16_t Conn_Interval,
                                               uint16_t Conn_Latency,
                                               uint16_t Supervision_Timeout,
                                               uint8_t Master_Clock_Accuracy)
{
  
  hci_le_connection_complete_event(Status,
                                   Connection_Handle,
                                   Role,
                                   Peer_Address_Type,
                                   Peer_Address,
                                   Conn_Interval,
                                   Conn_Latency,
                                   Supervision_Timeout,
                                   Master_Clock_Accuracy);
}

/* This function is called when the peer device get disconnected.
*/
void hci_disconnection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Reason)
{
    /* Make the device connectable again. */
    set_connectable = TRUE;
  
    OTA_terminate_connection();
  
}/* end hci_disconnection_complete_event() */

/*******************************************************************************
 * Function Name  : aci_gatt_srv_attribute_modified_event.
 * Description    : This event occurs when an attribute is modified.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_srv_attribute_modified_event(uint16_t Connection_Handle,
                                           uint16_t Attr_Handle,
                                           uint16_t Attr_Data_Length,
                                           uint8_t Attr_Data[])
{
    OTA_Write_Request_CB(Connection_Handle, Attr_Handle, Attr_Data_Length, Attr_Data);  //TBR???     
}


void aci_hal_end_of_radio_activity_event(uint8_t Last_State,
                                         uint8_t Next_State,
                                         uint32_t Next_State_SysTime)
{
    if (Next_State == 0x02) /* 0x02: Connection event slave */
    {
        OTA_Radio_Activity(Next_State_SysTime);  
    }
}

void aci_gatt_srv_read_event(uint16_t Connection_Handle, uint16_t Attribute_Handle, uint16_t Data_Offset)
{
    /* Handle Read operations on OTA characteristics with READ property */ 
    OTA_Read_Char(Connection_Handle,  Attribute_Handle, Data_Offset); //TBR???		
}
  
void aci_gatt_srv_write_event(uint16_t Connection_Handle,
                              uint8_t Resp_Needed,
                              uint16_t Attribute_Handle,
                              uint16_t Data_Length,
                              uint8_t Data[])
{
    uint8_t att_error = BLE_ATT_ERR_NONE;
    OTA_Write_Request_CB(Connection_Handle, Attribute_Handle, Data_Length, Data); //TBR???	

    if (Resp_Needed == 1U)
    {
        aci_gatt_srv_resp(Connection_Handle, Attribute_Handle, att_error, 0,  NULL);
    }
}

static uint8_t convert_byte_to_ASCII( uint8_t byte, uint8_t nibble)
{
    uint8_t data = 0;
    
    if(nibble == HIGH_NIBBLE)
    {
        data = byte >> 4;
    }
    else
    {
        data = byte & 0xF;
    }
    if(data > 9)
    {
        data += 0x37;
    }
    else
    {
        data += 0x30;    
    }
    return(data);
}

/******************* (C) COPYRIGHT 2015 STMicroelectronics *****END OF FILE****/
/** \endcond
*/
