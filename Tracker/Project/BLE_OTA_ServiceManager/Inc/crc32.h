/**
 * \file crc16.h
 *
 * \brief declares CRC32 functions
 *
 * Copyright Embed Limited Limited 2015
 *
 * $Author$
 * $Revision$
 * $Date$
 *
 */

#ifndef _CRC32_H
#define _CRC32_H

#include "stdint.h"

void CRC32_Init ( uint32_t *CRC_State );
void CRC32_ProcessMessage ( uint32_t *CRC_State, const uint8_t *msg, uint32_t len );
uint32_t CRC32_GetResult ( uint32_t *CRC_State );

#endif /* _CRC32_H */
