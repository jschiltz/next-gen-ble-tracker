/**
  ******************************************************************************
  * @file    NG_Tick_i2c.h
  * @author  Thomas W Liu
  * @brief   header file of next gen tick supporting i2c.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_I2C_H
#define __NG_TICK_I2C_H

/* defines -------------------------------------------------------------------*/
#define NGT_I2C                                   I2C1
#define NGT_I2C_CLK_ENABLE()                      LL_APB1_EnableClock(LL_APB1_PERIPH_I2C1);
#define NGT_I2C_CLK_DISABLE()                     LL_APB1_DisableClock(LL_APB1_PERIPH_I2C1);

#define NGT_I2C_DATA_PIN                          LL_GPIO_PIN_1
#define NGT_I2C_DATA_GPIO_PORT                    GPIOA
#define NGT_I2C_DATA_GPIO_PULL                    LL_GPIO_PULL_NO
#define NGT_I2C_DATA_GPIO_AF()                    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_1, LL_GPIO_AF_0)
#define NGT_I2C_DATA_GPIO_CLK_ENABLE()            LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOA)
#define NGT_I2C_DATA_GPIO_CLK_DISABLE()           LL_AHB_DisableClock(LL_AHB_PERIPH_GPIOA)

#define NGT_I2C_CLK_PIN                           LL_GPIO_PIN_0
#define NGT_I2C_CLK_GPIO_PORT                     GPIOA
#define NGT_I2C_CLK_GPIO_PULL                     LL_GPIO_PULL_NO
#define NGT_I2C_CLK_GPIO_AF()                     LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_0, LL_GPIO_AF_0)
#define NGT_I2C_CLK_GPIO_CLK_ENABLE()             LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOA)
#define NGT_I2C_CLK_GPIO_CLK_DISABLE()            LL_AHB_DisableClock(LL_AHB_PERIPH_GPIOA)

#define NGT_LED1_GPIO_PORT                        GPIOA
#define NGT_LED2_GPIO_PORT                        GPIOB


/** I2C Device Address 8 bit format  if SA0=0 -> 31 if SA0=1 -> 33 **/
#define LIS2DW12_I2C_ADD_L   0x31U
#define LIS2DW12_I2C_ADD_H   0x33U

/** Device Identification (Who am I) **/
#define LIS2DW12_ID            0x44U

/* extern function prototype -------------------------------------------------*/
extern void NGT_I2C_Init(void);
extern void NGT_I2C_DeInit(void);
extern int32_t NGT_I2C_Write(void *, uint8_t, uint8_t *, uint16_t);
extern int32_t NGT_I2C_Read(void *, uint8_t, uint8_t *, uint16_t);

#endif  /* __NG_TICK_I2C_H */