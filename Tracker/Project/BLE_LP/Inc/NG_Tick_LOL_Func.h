/**
  ******************************************************************************
  * @file    NG_Tick_LOL_Func.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link generic commands functions.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_FUNC_H
#define __NG_TICK_LOL_FUNC_H

/* Defines -------------------------------------------------------------------*/
#define DATA_FORMAT64_BC   8
#define DATA_FORMAT32_BC   4
#define DATA_FORMAT16_BC   2

union NGT_DATASWAP64_TYPE {
    uint64_t uiVal;
    uint8_t uiByte[DATA_FORMAT64_BC];
};

union NGT_DATASWAP32_TYPE {
    uint32_t uiVal;
    uint8_t uiByte[DATA_FORMAT32_BC];
};

union NGT_DATASWAP16_TYPE {
    uint16_t uiVal;
    uint8_t uiByte[DATA_FORMAT16_BC];
};

/* Defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/
    
/* extern variable declaration------------------------------------------------*/
extern union NGT_DATASWAP64_TYPE unDataSwap64;
extern union NGT_DATASWAP32_TYPE unDataSwap32;
extern union NGT_DATASWAP16_TYPE unDataSwap16;
extern uint16_t        uiESwapData16;
extern uint32_t        uiESwapData32;
extern uint64_t        uiESwapData64;

/* extern function declaration------------------------------------------------*/
extern void EndianSwapRead16( uint8_t *);
extern void EndianSwapRead32( uint8_t *, uint8_t);
extern void EndianSwapRead( uint8_t *, uint8_t);
extern void EndianSwap16( uint16_t *,  uint16_t);
extern void EndianSwap32( uint32_t *,  uint32_t);
extern void EndianSwap64( uint64_t *,  uint64_t);
extern void CheckLoL(void);
extern uint8_t xLoLGC_MfgTestAE( uint16_t);
extern uint8_t xLoLGC_BLEGCBuzzer( uint8_t, uint8_t, uint8_t);

#endif  /* __NG_TICK_LOL_FUNC_H */
