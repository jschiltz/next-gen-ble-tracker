/**
  ******************************************************************************
  * @file    NG_Tick_LOL_LMCmd_T.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link local memory commands, specific for NGT.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_LMCMD_T_H
#define __NG_TICK_LOL_LMCMD_T_H

/* Defines -------------------------------------------------------------------*/

/* Defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/
    
/* extern variable declaration------------------------------------------------*/

/* extern function declaration------------------------------------------------*/
extern void LOL_Init_All(void);
extern void fLMT_UnitSN( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AssetIdCode( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AssetIdCodePhone( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_UnitReset( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_BOCCV( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_OwnershipClaimKey( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AttributeStream( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AttributeFlashUtil( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AttributeSysStatus( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AttributeNFC( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AttributeProdStatus( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_AttributeMiscStatus( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);

#endif  /* __NG_TICK_LOL_LMCMD_T_H */
