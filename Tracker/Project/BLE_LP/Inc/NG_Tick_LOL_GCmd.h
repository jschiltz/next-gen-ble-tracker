/**
  ******************************************************************************
  * @file    NG_Tick_LOL_GCmd.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link generic commands.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_GCMD_H
#define __NG_TICK_LOL_GCMD_H

/* Defines -------------------------------------------------------------------*/

/* Defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/
    
/* extern variable declaration------------------------------------------------*/

/* extern function declaration------------------------------------------------*/
extern void fGCMfgTest( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fGCBLEGenericCmd( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fGCBLEBeaconParm( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fGCBLEDisconnet( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fGCBLEShipMode( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fGCBLESyncToolPhone( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fGCBLEFirmwareVer( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);


#endif  /* __NG_TICK_LOL_GCMD_H */
