/**
  ******************************************************************************
  * @file    CallBack.h
  * @author  Thomas W Liu
  * @brief   Header file of call backs functions
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2020 Milwaukee Tools
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CALLBACK_H
#define __CALLBACK_H


/* extern variable declaration -----------------------------------------------*/

/* extern function prototype -------------------------------------------------*/
extern void DMA1_TransmitComplete_Callback(void);
extern void DMA1_ReceiveComplete_Callback(void);
extern void USART_TransferError_Callback(void);
extern void USART_CharReception_Callback(void);
extern void AccInt1_Callback(void);
extern void NFCInt_Callback(void);
extern void Error_Callback(void);
extern void Error_Handler(void);
extern void assert_failed(uint8_t *, uint32_t);
extern void TimerCaptureCompare_Callback(void);

#endif  /* __CALLBACK_H */