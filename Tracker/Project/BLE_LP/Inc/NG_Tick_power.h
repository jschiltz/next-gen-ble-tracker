/**
  ******************************************************************************
  * @file    NG_Tick_power.h
  * @author  Thomas W Liu
  * @brief   header file of power saving.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_POWER_H
#define __NG_TICK_POWER_H

/* defines -------------------------------------------------------------------*/
#define ENABLE_PWR_DOWN_ACC             /* power down accelerometer to save power */
#define ENABLE_PWR_I2C_DEINIT           /* de-init i2c to save power */
#define ENABLE_PWR_DMA_DEINIT           /* de-init dma to save power */
#define ENABLE_PWR_UART_DEINIT          /* de-init uart to save power */
#define ENABLE_PWR_ADC_DEINIT           /* de-init adc to save power */
#define ENABLE_PWR_DOWN_IO              /* power down io to save power */

/**
  * @brief  vtime, 2 second interval definition
  */
#define NORMAL_2SEC_INTVAL_MS   2000

/* Includes ------------------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/

/* external variable declaration ---------------------------------------------*/
extern __IO uint32_t uiSysTick;     /* declared in bluenrg_lp_it.c */
extern uint16_t uiSysSleepMode;
extern WakeupSourceConfig_TypeDef sWakeupIO;
extern PowerSaveLevels iStopLevel;
extern uint8_t bNormalTime_Expired;
extern VTIMER_HandleType hNormalTimer;

/* external function prototype -----------------------------------------------*/
extern void PortAB_PullDown_Init(void);
extern void NGT_Power_Save(void);
extern void NGT_Power_Save2(void);
extern void NGT_Power_Save2_PD(void);
extern void Normal_VTimer_init(void);
extern void Normal_PowerSave_PD(void);
extern void Normal_PowerSave_PD_NoT(void);


#endif  /* __NG_TICK_POWER_H */



