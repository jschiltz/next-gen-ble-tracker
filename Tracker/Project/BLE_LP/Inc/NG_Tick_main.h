/**
  ******************************************************************************
  * @file    NG_Tick_main.h
  * @author  Thomas W Liu
  * @brief   Header file of main.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_dma.h"
#include "bluenrg_lp_ll_rcc.h"
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_system.h"
#include "bluenrg_lp_ll_exti.h"
#include "bluenrg_lp_ll_cortex.h"
#include "bluenrg_lp_ll_utils.h"
#include "bluenrg_lp_ll_pwr.h"
#include "bluenrg_lp_ll_usart.h"
#include "bluenrg_lp_ll_gpio.h"
#include "bluenrg_lp_ll_iwdg.h"
  
#if defined(USE_FULL_ASSERT)
#include "bluenrg_lp_assert.h"
#endif /* USE_FULL_ASSERT */

/* defines -------------------------------------------------------------------*/
//#define ENABLE_BLE    /* enable ble routines & sleep/wakeup, if disable, no sleep either */
//#define ENABLE_SLEEP  /* enable sleep feature */
#define ENABLE_LOL      /* enable LoL engine & parsing, if disabled, realterm */

/* Private includes ----------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/


/* Exported constants --------------------------------------------------------*/

    
/* Exported macro ------------------------------------------------------------*/

/* functions prototypes ------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/

/* Extern variable declaration -----------------------------------------------*/
extern __IO int16_t iSys_1SecF;

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
