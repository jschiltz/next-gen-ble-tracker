/**
  ******************************************************************************
  * @file    NG_Tick_uart.h
  * @author  Thomas W Liu
  * @brief   header file of next gen tick supporting uart.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_UART_H
#define __NG_TICK_UART_H

/* defines -------------------------------------------------------------------*/
#define RX_ST_IDLE      0       /* rx idle */
#define RX_ST_DATA      1       /* rx received data bytes, 2nd+ */
#define RX_ST_DATA_DONE 2       /* rx received complete */

/* defines -------------------------------------------------------------------*/
#define UART_RX_BUFF_SIZE  64      /* rx buffer size */
#define UART_TX_BUFF_SIZE  80      /* tx buffer size */
#define RX_MSG_TIMEOUT     200     /* rx message time out */

/* external variable declartion ----------------------------------------------*/
extern const char cNGT_Title[];
extern __IO uint16_t uiRxState;       /* rx state */
extern __IO uint8_t ubTxComplete;     /* tx complete flag */
extern __IO uint8_t ubRxComplete;     /* rx complete flag */

extern __IO uint8_t      bRxBufferReadyF;
extern __IO uint32_t     uwNbRxChars;   /* current bytes received */
extern uint8_t *pBufferReadyForUser;
extern uint8_t *pBufferReadyForRx;

/* extern variable declaration -----------------------------------------------*/
extern __IO int32_t iRxTimeout;
extern int16_t     iTxMsgLen;  /* expected tx data count */
extern int16_t     iRxMsgCnt;  /* expected Rx data count */
extern char    ucDestStr[];

/* extern function prototype -------------------------------------------------*/
extern void CheckRxMsg(void);
extern void UART_Init_All(void);
extern void UART_DeInit(void);
extern void pStartNextTransfers( uint8_t *, int16_t);
extern void StartTx_WaitTilEndOfTx(char *);
extern void cStartTx_WaitTilEndOfTx(const char *);
extern void uiStartTx_WaitTilEndOfTx(uint8_t *);
extern void CrEFUint8_StartTx_WaitTilEndOfTx_N( uint8_t *, int16_t);
extern void CrEFUint8_StartTx_WaitTilEndOfTx(uint8_t *);
extern void StartTransfers(void);
extern void StartNextTransfers(void);
extern void CheckEndOfTx(void);
extern void CheckEndOfRx(void);
extern void WaitTilEndOfTxTransfer(void);

#endif  /* __NG_TICK_UART_H */
