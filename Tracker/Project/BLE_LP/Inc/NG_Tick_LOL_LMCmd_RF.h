/**
  ******************************************************************************
  * @file    NG_Tick_LOL_LMCmd_RF.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link local memory commands, specific for RF.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_LMCMD_RF_H
#define __NG_TICK_LOL_LMCMD_RF_H

/* Defines -------------------------------------------------------------------*/
#define LOL_LM_RF_TX_LOW_1MBPS          0x00  /* RF tx, low 1 mbps */
#define LOL_LM_RF_TX_MID_1MBPS          0x01  /* RF tx, mid 1 mbps */
#define LOL_LM_RF_TX_HIGH_1MBPS         0x02  /* RF tx, high 1 mbps */
#define LOL_LM_RF_TX_LOW_2MBPS          0x03  /* RF tx, low 2 mbps */
#define LOL_LM_RF_TX_MID_2MBPS          0x04  /* RF tx, mid 2 mbps */
#define LOL_LM_RF_TX_HIGH_2MBPS         0x05  /* RF tx, high 2 mbps */
#define LOL_LM_RF_TX_LOW_CW             0x06  /* RF tx, cw */
#define LOL_LM_RF_TX_MID_CW             0x07  /* RF tx, cw */
#define LOL_LM_RF_TX_HIGH_CW            0x08  /* RF tx, cw */
#define LOL_LM_RF_TX_STOP               0xFF  /* Stop TX mode, DOES NOT RESUME ADVERTISING */

#define LOL_LM_RF_RX_LOW_1MBPS          0x00  /* RF rx, low 1 mbps */
#define LOL_LM_RF_RX_MID_1MBPS          0x01  /* RF rx, mid 1 mbps */
#define LOL_LM_RF_RX_HIGH_1MBPS         0x02  /* RF rx, high 1 mbps */
#define LOL_LM_RF_RX_STOP               0xFF  /* Stop RX mode, DOES NOT RESUME ADVERTISING */

#define LOL_LM_RF_OTA_OFF               0x00  /* RF OTA, off */
#define LOL_LM_RF_OTA_ON                0x01  /* RF OTA, on */

/* Defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/
    
/* extern variable declaration------------------------------------------------*/

/* extern function declaration------------------------------------------------*/
extern void fLMT_RF_TX( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_RF_RX( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMT_RF_OTA( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);

#endif  /* __NG_TICK_LOL_LMCMD_RF_H */
