/**
  ******************************************************************************
  * @file    BLE_NFC.h 
  * @author  RF Application Team - AME Region
  * @brief   Header for I2C_NFC_04A1_main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics. 
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the 
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLE_NFC_H
#define __BLE_NFC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_hal.h"

/* Private includes ----------------------------------------------------------*/
#include "bluenrg_lp_evb_config.h"

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);
void MX_NFC_Init(void);
void MX_NFC_Process(void);


/** @defgroup STM32L0XX_NUCLEO_BUS_Exported_Constants STM32L0XX_NUCLEO BUS Exported Constants
  * @{
  */
#if defined(METCO_PCB)
  #define I2Cx                                        I2C1
  #define I2Cx_SCL_PORT                               GPIOA
  #define I2Cx_SCL_PIN                                GPIO_PIN_0
  #define I2Cx_SCL_AF                                 GPIO_AF0_I2C1
  #define I2Cx_SDA_PORT                               GPIOA
  #define I2Cx_SDA_PIN                                GPIO_PIN_1
  #define I2Cx_SDA_AF                                 GPIO_AF0_I2C1
  #define __HAL_RCC_I2Cx_CLK_ENABLE                   __HAL_RCC_I2C1_CLK_ENABLE
  #define __HAL_RCC_I2Cx_CLK_DISABLE                  __HAL_RCC_I2C1_CLK_DISABLE
  #define __HAL_RCC_I2Cx_SCL_GPIO_CLK_ENABLE          __HAL_RCC_GPIOA_CLK_ENABLE
  #define __HAL_RCC_I2Cx_SDA_GPIO_CLK_ENABLE          __HAL_RCC_GPIOA_CLK_ENABLE
#else
  #define I2Cx                                        I2C2
  #define I2Cx_SCL_PORT                               GPIOA
  #define I2Cx_SCL_PIN                                GPIO_PIN_13
  #define I2Cx_SCL_AF                                 GPIO_AF0_I2C2
  #define I2Cx_SDA_PORT                               GPIOB //GPIOA
  #define I2Cx_SDA_PIN                                GPIO_PIN_14
  #define I2Cx_SDA_AF                                 GPIO_AF1_I2C2 //GPIO_AF0_I2C2
  #define __HAL_RCC_I2Cx_CLK_ENABLE                   __HAL_RCC_I2C2_CLK_ENABLE
  #define __HAL_RCC_I2Cx_CLK_DISABLE                  __HAL_RCC_I2C2_CLK_DISABLE
  #define __HAL_RCC_I2Cx_SCL_GPIO_CLK_ENABLE          __HAL_RCC_GPIOA_CLK_ENABLE
  #define __HAL_RCC_I2Cx_SDA_GPIO_CLK_ENABLE          __HAL_RCC_GPIOB_CLK_ENABLE
#endif

/*
#define BUS_I2C1_INSTANCE I2C1
#define BUS_I2C1_SCL_GPIO_PORT GPIOB
#define BUS_I2C1_SCL_GPIO_AF GPIO_AF4_I2C1
#define BUS_I2C1_SCL_GPIO_CLK_ENABLE() __HAL_RCC_GPIOB_CLK_ENABLE()
#define BUS_I2C1_SCL_GPIO_CLK_DISABLE() __HAL_RCC_GPIOB_CLK_DISABLE()
#define BUS_I2C1_SCL_GPIO_PIN GPIO_PIN_8
#define BUS_I2C1_SDA_GPIO_PIN GPIO_PIN_9
#define BUS_I2C1_SDA_GPIO_CLK_DISABLE() __HAL_RCC_GPIOB_CLK_DISABLE()
#define BUS_I2C1_SDA_GPIO_PORT GPIOB
#define BUS_I2C1_SDA_GPIO_AF GPIO_AF4_I2C1
#define BUS_I2C1_SDA_GPIO_CLK_ENABLE() __HAL_RCC_GPIOB_CLK_ENABLE()
*/

#ifndef BUS_I2C_POLL_TIMEOUT
   #define BUS_I2C_POLL_TIMEOUT                0x1000U
#endif
/* I2C1 Frequeny in Hz  */
#ifndef BUS_I2Cx_FREQUENCY
   #define BUS_I2Cx_FREQUENCY  1000000U /* Frequency of I2Cn = 100 KHz*/
#endif

/**
  * @}
  */

/** @defgroup STM32L0XX_NUCLEO_BUS_Private_Types STM32L0XX_NUCLEO BUS Private types
  * @{
  */
#if (USE_HAL_I2C_REGISTER_CALLBACKS == 1U)
typedef struct
{
  pI2C_CallbackTypeDef  pMspInitCb;
  pI2C_CallbackTypeDef  pMspDeInitCb;
}BSP_I2C_Cb_t;
#endif /* (USE_HAL_I2C_REGISTER_CALLBACKS == 1U) */
/**
  * @}
  */

/** @defgroup STM32L0XX_NUCLEO_LOW_LEVEL_Exported_Variables LOW LEVEL Exported Constants
  * @{
  */

extern I2C_HandleTypeDef hi2c1;

/**
  * @}
  */

/** @addtogroup STM32L0XX_NUCLEO_BUS_Exported_Functions
  * @{
  */

/* BUS IO driver over I2C Peripheral */
HAL_StatusTypeDef MX_I2C1_Init(I2C_HandleTypeDef* hi2c);
int32_t MX_I2Cx_Init(void);
//int32_t BSP_I2C1_Init(void);
int32_t MX_I2Cx_Deinit(void);
//int32_t BSP_I2C1_DeInit(void);
int32_t BSP_I2C_IsReady(uint16_t DevAddr, uint32_t Trials);
int32_t BSP_I2C_WriteReg(uint16_t Addr, uint16_t Reg, uint8_t *pData, uint16_t Length);
int32_t BSP_I2C_ReadReg(uint16_t Addr, uint16_t Reg, uint8_t *pData, uint16_t Length);
int32_t BSP_I2C_WriteReg16(uint16_t Addr, uint16_t Reg, uint8_t *pData, uint16_t Length);
int32_t BSP_I2C_ReadReg16(uint16_t Addr, uint16_t Reg, uint8_t *pData, uint16_t Length);
int32_t BSP_I2C_Send(uint16_t DevAddr, uint8_t *pData, uint16_t Length);
int32_t BSP_I2C_Recv(uint16_t DevAddr, uint8_t *pData, uint16_t Length);
int32_t BSP_I2C_SendRecv(uint16_t DevAddr, uint8_t *pTxdata, uint8_t *pRxdata, uint16_t Length);
#if (USE_HAL_I2C_REGISTER_CALLBACKS == 1U)
int32_t BSP_I2C_RegisterDefaultMspCallbacks (void);
int32_t BSP_I2C_RegisterMspCallbacks (BSP_I2C_Cb_t *Callbacks);
#endif /* (USE_HAL_I2C_REGISTER_CALLBACKS == 1U) */

int32_t BSP_GetTick(void);
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private defines -----------------------------------------------------------*/
#define I2C_ADDRESS 0x30F



/* Size of Transmission buffer */
#define TXBUFFERSIZE                      (COUNTOF(aTxBuffer) - 1)
/* Size of Reception buffer */
#define RXBUFFERSIZE                      TXBUFFERSIZE


#ifdef __cplusplus
}
#endif

#endif /* __BLE_NFC_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
