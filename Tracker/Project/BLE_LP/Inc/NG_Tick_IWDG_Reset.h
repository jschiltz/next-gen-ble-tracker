/**
  ******************************************************************************
  * @file    NG_Tick_IWDG_Reset.h
  * @author  Kurtis Alessi
  * @brief   Header file for watchdog reset.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_IWDG_RESET_H
#define __NG_TICK_IWDG_RESET_H

/* Defines -------------------------------------------------------------------*/
//#define IWDG_TIMER

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_iwdg.h"
#include "bluenrg_lp_ll_rcc.h"
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_system.h"
#include "bluenrg_lp_ll_exti.h"
#include "bluenrg_lp_ll_cortex.h"
#include "bluenrg_lp_ll_utils.h"
#include "bluenrg_lp_ll_pwr.h"
#include "bluenrg_lp_ll_dma.h"
#include "bluenrg_lp.h"
#include "bluenrg_lp_ll_gpio.h"

/* extern function prototype -------------------------------------------------*/
extern void MX_IWDG_Init(void);
extern int16_t ResetReasonByIWDGRST(void);



#endif  /* __NG_TICK_IWDG_RESET_H */
