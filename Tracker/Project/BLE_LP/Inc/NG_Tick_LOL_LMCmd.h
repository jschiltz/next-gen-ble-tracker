/**
  ******************************************************************************
  * @file    NG_Tick_LOL_LMCmd.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link local memory commands.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_LMCMD_H
#define __NG_TICK_LOL_LMCMD_H

/* Defines -------------------------------------------------------------------*/

/* Defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/
    
/* extern variable declaration------------------------------------------------*/

/* extern function declaration------------------------------------------------*/
extern void debug_assert(bool);
extern void begin_super_loop( void );
extern eCommsErr Tool_data_tx( uint8_t *, uint8_t, uint8_t);  /* !@#$ */
extern void sendDevNameChannel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMSWPN( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void slave5( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void simpleAck( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void sendMaxPktLn( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void MBCompleteCallback1( bool, uint8_t *, uint8_t, uint8_t);
extern void passwordEnable( uint8_t *, uint8_t, uint8_t , uint8_t, uint8_t );
extern eMM_Sec_Lev PasswordVerification( uint8_t *);
extern void setSecLevel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void readMemMapVersion( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void RWExtMemOffset( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMMPBID( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMFwVersion( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMDateOfMfg( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMDateOfService( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMDateLastUSed( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMDateTimeRef( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMPlatformId( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
extern void fLMBLEMACAddr( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);

#endif  /* __NG_TICK_LOL_LMCMD_H */
