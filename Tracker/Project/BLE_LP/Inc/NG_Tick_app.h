/**
  ******************************************************************************
  * @file    NG_Tick_app.h
  * @author  Thomas W Liu
  * @brief   header file of application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  *
  * Note     
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_APP_H
#define __NG_TICK_APP_H

/* Defines constant ----------------------------------------------------------*/
#define APP2SEC_CNT     2       /* 2 second counter, based on 1 second count */
//#define APP10SEC_CNT    10      /* 10 second counter, based on 1 second count */
//#define APP10MIN_CNT    (10*60) /* 10 minutes counter, based on 1 second count */

//#define NORM_APP2SEC_CNT        1       /* 2 second counter, based on 2 second count */
#define NORM_APP60MIN_CNT       (60*30) /* 60 minutes counter, based on 2 second count */

/* @#$% testing purpose */
#define APP60SEC_CNT    60      /* 60 seconds counter, based on 1 second count */
#define APP60MIN_CNT    (60*60) /* 60 minutes counter, based on 1 second count */

//#define NORM_APP60SEC_CNT    30 /* 60 seconds counter, based on 2 second count */
#define NORM_APP60SEC_CNT    20 /* 60 seconds counter, based on 3 second count */

/* Defines -------------------------------------------------------------------*/

/* Defines ----------------------------------------------------5---------------*/
/* application action block: action definition -----------------------------*/
/**
  * @brief  define action
  *         idle = no action required
  *         to ship mode = to ship mode
  *         ship mode = in ship mode
  *         activate = activation of NGT
  *         activate adc = get coin cell voltage, start adc conversion
  *         activate flash = write system info block to flash
  *         activate ACC = configure accelerometer for vibration
  *         activate NFC = write NFC dynmic data to memory
  *         activate last = last house keeping in activation
  *         normal = normal operation (after activation)
  */
enum {
    APPCB_ACT_IDLE = 0,
    APPCB_ACT_2SHIPMODE,
    APPCB_ACT_SHIPMODE,
    APPCB_ACT_ACTIVATE,
    APPCB_ACT_ACTIVATE_ADC,
    APPCB_ACT_ACTIVATE_FLASH,
    APPCB_ACT_ACTIVATE_ACC,
    APPCB_ACT_ACTIVATE_NFC,
    APPCB_ACT_ACTIVATE_LAST,
    APPCB_ACT_NORMAL
};

/* application control block: state for all ----------------------------------*/
/**
  * @brief  define state
  *         idle = no activity
  *         to ship mode = ship mode command received, send ack, save flash, init BLE
  *                        for advertising, then go to ship mode
  *         to ship mode pwr dn = to ship mode & wait to power down
  *         ship mode = in ship mode, advertise periodically
  *         activate = activation of NGT
  *         normal = normal operation (after activation)
  */
enum {
    APPCB_ST_IDLE = 0,
    APPCB_ST_2SHIPMODE,
    APPCB_ST_2SHIPMODE_PWRDN,
    APPCB_ST_SHIPMODE,
    APPCB_ST_ACTIVATE,
    APPCB_ST_NORMAL
};

/**
  * @brief  define structure, application info block
  */
typedef struct 
{
    uint8_t     uAction;        /* state: idle, to ship mode, in ship mode, activate, normal */
    uint8_t     uState;         /* state: idle, to ship mode, in ship mode, activate, normal */
}  NGT_APPINFO_TYPE;


/* structure declaration -----------------------------------------------------*/
#define APP_CTRL_STATE_ERR      0xff
#define APP_CTRL_STATE_IDLE     0
#define APP_CTRL_STATE_ERASE    1
#define APP_CTRL_STATE_WRITE    2
#define APP_CTRL_STATE_READ     3
#define APP_CTRL_STATE_WAIT     4

typedef struct 
{
    uint8_t     uAction;        /* state: idle, search, wipe, read, write */
    uint8_t     uState;         /* state: idle, read, write */
}  NGT_APPCTRL_TYPE;

/* structure declaration -----------------------------------------------------*/
/**
  * @brief  define structure, time control block
  */
typedef struct 
{
    uint16_t    uiApp_SecCnt;   /* 1 second counter */
    uint16_t    uiApp_2SecCnt;   /* 2 second counter */
    uint8_t     bApp_2SecF;     /* 2 second flag: T/F */
uint8_t     bApp_60SecF;    /* 60 second flag: T/F */  //test only $%^&
    uint8_t     bApp_60MinF;    /* 60 minute flag: T/F */
}  NGT_APPTMCTRL_TYPE;

/* variable declaration ------------------------------------------------------*/

/* external variable declaration ---------------------------------------------*/
extern __IO int16_t iSys_1SecF;         /* from bluenrg_lp_it.c */
extern NGT_APPCTRL_TYPE sAppCB;

/* external function prototype -----------------------------------------------*/
extern void CheckLastMode(void);
extern void CheckApp(void);

#endif  /* __NG_TICK_APP_H */

