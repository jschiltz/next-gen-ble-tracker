/**
  ******************************************************************************
  * @file    NG_Tick_PWM.h
  * @author  Thomas W Liu
  * @brief   Header file of next gen tick supporting pwm output generation.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_PWM_H
#define __NG_TICK_PWM_H


/* defines -------------------------------------------------------------------*/
/**
  * @brief  pwm output io port & interrupts definition
  */
#define TIM1_CH6_PIN            LL_GPIO_PIN_11
#define TIM1_CH6_GPIO_PORT      GPIOA
#define TIM1_CH6_AF             LL_GPIO_AF_4

#define BUZZER_PIN              LL_GPIO_PIN_11
#define BUZZZER_GPIO_PORT       GPIOA
#define BUZZER_AF               LL_GPIO_AF_4

/**
  * @brief  HELPER macro calculating the prescaler value to achieve the required counter clock frequency.
  * @note ex: @ref __LL_TIM_CALC_PSC (80000000, 1000000);
  * @param  __TIMCLK__ timer input clock frequency (in Hz)
  * @param  __CNTCLK__ counter clock frequency (in Hz)
  * @retval Prescaler value  (between Min_Data=0 and Max_Data=65535)
  * @note   modification of __LL_TIM_CALC_PSC macro to compensate round off issue from division
  */
#define _NGT__LL_TIM_CALC_PSC(__TIMCLK__, __CNTCLK__)   \
   (((__TIMCLK__) >= (__CNTCLK__)) ? (uint32_t)((((float)__TIMCLK__ + (((float)__CNTCLK__)/2))/((float)__CNTCLK__)) - 1U) : 0U)
//   (((__TIMCLK__) >= (__CNTCLK__)) ? (uint32_t)(((__TIMCLK__ + ((__CNTCLK__)/2))/(__CNTCLK__)) - 1U) : 0U)


/* extern variable declaration -----------------------------------------------*/
extern uint32_t timxPrescaler;     /* prescaler */
extern uint32_t timxPeriod;        /* time period */

/* extern function prototype -------------------------------------------------*/
extern void Buzzer_TIM1_Var_Init(void);
extern void Buzzer_TIM1_Init(void);
extern void Buzzer_TIM1_Enable(void);
extern void Buzzer_TIM1_Disable(void);
extern void Configure_DutyCycle6(uint32_t);
extern void Configure_DutyCycle6_50DC(void);

#endif /* __NG_TICK_PWM_H */
