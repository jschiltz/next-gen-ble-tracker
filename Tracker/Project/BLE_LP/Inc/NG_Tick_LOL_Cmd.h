/**
  ******************************************************************************
  * @file    NG_Tick_LOL_Cmd.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link commands.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_CMD_H
#define __NG_TICK_LOL_CMD_H

/* Defines -------------------------------------------------------------------*/
#define LOL_ACK_RESP    0x40    /**< ACK reponse, message accepted */
#define LOL_RD_RESP     0x41    /**< read response */

/* Defines -------------------------------------------------------------------*/
#define SLAVEARRAY0SIZE 42
#define SLAVEARRAY1SIZE 40

/* Defines -------------------------------------------------------------------*/
/**
  * @brief  define write value within msg, within SID byte
  */
#define LOL_SID_WR      0x01    /* LoL message write action in SID byte */
#define LOL_SID_RD      0x00    /* LoL message read action in SID byte */
#define LOL_LMADDR_SIZE 2       /* LoL local memory address size */
#define LOL_GSC_SIZE    1       /* LoL generic command sub-command size size */

/**
  * @brief  define segment 1, virtual memory address
  */
#define LOL_LM_MEMMAPVER        0x0000  /* memory map version */
#define LOL_LM_EXTMEMOS         0x0002  /* extended memory offset */
#define LOL_LM_MPBID            0x0004  /* MPBID */
#define LOL_LM_SWPN             0x0009  /* software part number */
#define LOL_LM_FMREV            0x000d  /* firmware revision */
#define LOL_LM_BOD              0x0011  /* born on date */
#define LOL_LM_DFO_ACT          0x0015  /* date of first operation, activation */
#define LOL_LM_DATE_LAST_USED   0x0019  /* data of last use = tool last used */
#define LOL_LM_DEV_NAME         0x0023  /* unique device name */
#define LOL_LM_DT_REF           0x0037  /* date time reference */
#define LOL_LM_PW_ENABLE        0x003b  /* password enable */
#if 0
#define LOL_LM_USER_PW          0x0043  /* user password */
#define LOL_LM_ADMIN_PW         0x004b  /* admin password */
#define LOL_LM_SERVICE_PW       0x0053  /* service password */
#define LOL_LM_METCO_PW         0x005b  /* metco password */
#endif
#define LOL_LM_BLE_MAC_ADDR     0x0063  /* BLE MAC address */
#define LOL_LM_OL_CORE_VER      0x0069  /* openlink core version */
#define LOL_LM_PLATFORM_ID      0x007c  /* platform identifier */

#define LOL_LM_UNIT_SN          0xa024  /* unit serial number */
#define LOL_LM_ASSET_CODE       0xa038  /* asset code */
#define LOL_LM_ASSET_CODE_PHONE 0xa040  /* asset code phone */
#define LOL_LM_BOCCV            0xa048  /* born on coin cell voltage */
#define LOL_LM_UNIT_RESET       0xa04a  /* unit reset */
#define LOL_LM_ATT_STREAM       0xa04c  /* special feature - streaming enable/disable */
#define LOL_LM_ATT_DEBUG        0xa04e  /* special feature - debug mode */
#define LOL_LM_ATT_STATUS       0xa050  /* special feature - system status */
#define LOL_LM_ATT_NFC          0xa052  /* special feature - NFC memory */
#define LOL_LM_ATT_PRODUCTION   0xa054  /* special feature - production status */
#define LOL_LM_ATT_MISC         0xa056  /* special feature - misc status */
#define LOL_LM_RF_TX            0xa057  /* special feature - RF tx */
#define LOL_LM_RF_RX            0xa058  /* special feature - RF rx */
#define LOL_LM_RF_OTA           0xa059  /* special feature - RF OTA */
#define LOL_LM_OWNER_CKPW       0xb000  /* owner claim key password */

#define LOL_LM_NONE             0xffff  /* none */

#define LOL_GSC_NA              0xffff  /* sub cmd - na */

//#define LOL_GC_MFG_TEST         0x0071  /* manufacturing test */
#define LOL_GSC_MFG_TEST_CUR    0x0003  /* sub cmd - BLE currest test */
#define LOL_GSC_MFG_TEST_BUZZ   0x0004  /* sub cmd - buzzer test */
#define LOL_GSC_MFG_TEST_RST    0x0005  /* sub cmd - reeet BLE */

//#define LOL_GC_BLE              0x0077  /* BLE generic command */
#define LOL_GSC_BLE_WLUT        0x0001  /* sub cmd - set last used time */
#define LOL_GSC_BLE_RLUT        0x0002  /* sub cmd - get last used time */
#define LOL_GSC_BLE_BUZZER      0x0003  /* sub cmd - play buzzer */
#define LOL_GSC_BLE_GCCV        0x0004  /* sub cmd - get coin cell voltage */

//#define LOL_GC_BLE_DISCONNECT   0x007b  /* disconnect BLE */

//#define LOL_GC_BLE_SHIP         0x007c  /* BLE ship mode */
#define LOL_GSC_BLE_SHIP_DEF    0x0000  /* sub cmd - default, no special mode */
#define LOL_GSC_BLE_SHIP_NOADV  0x0001  /* sub cmd - no advertising during sleep */

//#define LOL_GC_BLE_FWVER        0x007f  /* BLE firmware version */
#define LOL_GC_SYNC_TOOL_PH     0x007e  /* BLE sync tool phone */

//#define LOL_GC_BEACON_PARM      0x0078  /* set BLE beacon parameter */
#define LOL_GSC_BEACON_DEVEL    0x0000  /* sub cmd - developmental veacon paramter */
#define LOL_GSC_BEACON_SET_INTV 0x0001  /* sub cmd - set beacon intervals */
#define LOL_GSC_BEACON_GET_INTV 0x0002  /* sub cmd - get beacon intervals */

#define LOL_GC_OTA_UPDATE       0x0074  /* BLE OTA */


/* structure declaration -----------------------------------------------------*/
#define RSP_DATA_SIZE   128
struct ResponseData
{
    uint8_t     testNumber;             // number of the test we are running, 0-127
    uint8_t     acknack;                // 0x80 - 0x83
    uint8_t     length;                 // Length of response data
    uint8_t     data[RSP_DATA_SIZE];    // expected data
};

/* used for 0x02 master test */
#define MASTERTESTNOTRUN    0x10    // Test has not been run
#define MASTERTESTPASSED    0x20    // Test Passed
#define MASTERTESTFAILED    0x30    // Test Failed
#define MAXIMUNMASTERTEST   128     // 128 is the number of allowed tests - for NOW!
#define RETURNMASTERRESULTS 0xFF    // Test that will return the results of the master test
#define RESETMASTERRESULTS  0xFE    // Reset results array

#define CHANNELZERO 0
#define CHANNELONE  1

#define LOL_CHAN_UART   CHANNELZERO
#define LOL_CHAN_BLE    CHANNELONE

/**
  * @brief  define tx & rx buffer size for each channel
  */
#define CHAN0_TX_BUFF_SIZE      96
#define CHAN0_RX_BUFF_SIZE      96
#define CHAN1_TX_BUFF_SIZE      32
#define CHAN1_RX_BUFF_SIZE      32
    
#define LOCAL_RETPL_LEN   2     /* local variable return payload length */

/* constant declaration ------------------------------------------------------*/
#define LOL_MMVER_PAYLOAD_LEN       2  /* memory map version payload length */
#define LOL_XMMOS_PAYLOAD_LEN       2  /* memory map offset payload length */
#define LOL_MPBID_WR_PAYLOAD_LEN    7  /* MPBID write payload length */
#define LOL_SWPN_PAYLOAD_LEN        4  /* SWPN payload length */
#define LOL_BOD_PAYLOAD_LEN         4  /* BOD payload length */
#define LOL_DT_PAYLOAD_LEN          4  /* time date payload length */
#define LOL_FWVER_PAYLOAD_LEN       4  /* firmware version payload length */
#define LOL_NACK_PAYLOAD_LEN        2  /* NACK payload length */
#define LOL_ACK_PAYLOAD_LEN         0  /* ACK payload length */
#define LOL_PFID_PAYLOAD_LEN        2  /* platform identifier payload length */
#define LOL_BLEMAC_PAYLOAD_LEN      6  /* BLE MAC address payload length */
#define LOL_URST_PAYLOAD_LEN        1  /* unit reset payload length */
#define LOL_URSTPW_PAYLOAD_LEN      4  /* unit reset password payload length */
#define LOL_BOCCV_PAYLOAD_LEN       1  /* born on coin cell voltage payload length */
#define LOL_OWN_CLAIM_K_PAYLOAD_LEN 8  /* owner claim key payload length */
#define LOL_ATT_STREAM_PAYLOAD_LEN  1  /* attribute streaming payload length */
#define LOL_ATT_PRODSTAT_PAYLOAD_LEN    1  /* attribute prouection status payload length */

#define LOL_MFGTST_BLEZ_PAYLOAD_LEN 2  /* manufacturing test, BLE sleep current test */
#define LOL_MFGTST_BUZZ_PAYLOAD_LEN 2  /* manufacturing test, buzzer test */
#define LOL_MFGTST_BRST_PAYLOAD_LEN 0  /* manufacturing test, reset BLE module */

#define LOL_BGC_WLUTC_PAYLOAD_LEN   2  /* BLE generic command, set last used time constant */
#define LOL_BGC_RLUTC_PAYLOAD_LEN   2  /* BLE generic command, read last used time constant */
#define LOL_BGC_PBUZZ_PAYLOAD_LEN   3  /* BLE generic command, play buzzer */
#define LOL_BGC_GCCV_PAYLOAD_LEN    1  /* BLE generic command, get coin cell voltage */

#define LOL_BBP_DEVL_PAYLOAD_LEN    8  /* BLE beacon parameter command, developmental */
#define LOL_BBP_INTV_WR_PAYLOAD_LEN 3  /* BLE beacon parameter command, interval, write */
#define LOL_BBP_INTV_RD_PAYLOAD_LEN 1  /* BLE beacon parameter command, interval, read */

#define LOL_BLE_SHIP_PAYLOAD_LEN    1  /* BLE ship mode */
#define LOL_BLE_SYNC_TP_PAYLOAD_LEN 1  /* BLE sync tool phone */

#define LOL_BLE_FW_VER_PAYLOAD_LEN  4  /* BLE firmware version, big endian */

/* constant declaration ------------------------------------------------------*/

/* extern variable declaration------------------------------------------------*/

/* extern function declaration------------------------------------------------*/

#endif  /* __NG_TICK_LOL_CMD_H */

