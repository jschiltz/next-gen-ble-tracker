/**
  ******************************************************************************
  * @file    NG_Tick_ADC.h
  * @author  Johnny Lienau
  * @brief   Header file of adc
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_Tick_ADC_H
#define __NG_Tick_ADC_H

/* Private define ------------------------------------------------------------*/
#define USER_SAMPLERATE       (LL_ADC_SAMPLE_RATE_28)
#define USER_DATAWIDTH        (LL_ADC_DS_DATA_WIDTH_12_BIT)
#define USER_RATIO            (LL_ADC_DS_RATIO_128)

/* ADC control block: action definition -----------------------------*/
/**
  * @brief  define action
  *         idle = no action required
  *         convert = wait on end of conversion
  *         process = process adc data
  *         update = update adc data in BLE advertisement field
  */
enum
{
    ADCCB_ST_IDLE = 0,
    ADCCB_ST_CONVERT,
    ADCCB_ST_PROCESSING,
    ADCCB_ST_UPDATE
};

/* ADC control block: action definition -----------------------------*/
/**
  * @brief  define action
  *         idle = no action required
  *         convert = convert adc (start adc conversion)
  */
enum {
    ADCCB_ACT_IDLE = 0,
    ADCCB_ACT_CONVERT
};

/* ADC control block: error definition -----------------------------*/
/**
  * @brief  define adc error code
  *         none = no error
  *         def = default error, not converted
  *         overrun = adc overrun error
  *         sm = state machine error
  */
#define ADCCB_ERR_NONE        0
#define ADCCB_ERR_DEF         0x7f
#define ADCCB_ERR_OVERRUN     0x7e
#define ADCCB_ERR_SM          0x7d

/* structure declaration -----------------------------------------------------*/
/* ADC control block structure --------------------------------------*/
/**
  * @brief  define structure
  *         uAction = action to be performed
  *         uState = current adc control block state
  *         *uBattVoltage = ptr to adc value
  */
typedef struct
{
    uint8_t     uAction;        /* action:  idle, convert */
    uint8_t     uState;         /* state: idle, convert, process */
    uint8_t     *pBattVoltage;  /* ptr to voltage value */
}  NGT_ADCCB_TYPE;

/* constant declaration ------------------------------------------------------*/
#define CONVERSION_COMPLETE     1
#define OVERRUN_ERROR           1

/**
  * @brief  define boundry for adc count and value
  *         BATTERY_UPPER_V = battery upper bound voltage value
  *         BATTERY_LOWER_V = battery lower bound voltage value
  *         ADC_UPPER_CNT = adc upper bound count value
  *         ADC_LOWER_VCNT= adc lower bound count value
  */
#define BATTERY_UPPER_V 120
#define BATTERY_LOWER_V 0
#define ADC_UPPER_CNT   3000
#define ADC_LOWER_CNT   1800

/* extern variable declaration -----------------------------------------------*/
extern NGT_ADCCB_TYPE  AdcCB;

/* extern function prototype -------------------------------------------------*/
extern void ADC_Init_All(void);
extern void CheckADC(void);
extern void ADC_DeInit(void);

#endif  /* NG_Tick_ADC_H */