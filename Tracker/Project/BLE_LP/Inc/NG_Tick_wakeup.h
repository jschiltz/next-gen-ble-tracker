/**
  ******************************************************************************
  * @file    NG_Tick_wakeup.h
  * @author  Thomas W Liu
  * @brief   header file of wakeup.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_WAKEUP_H
#define __NG_TICK_WAKEUP_H

/* Defines -------------------------------------------------------------------*/
/**
  * @brief wakeup timeout for RTC definition, in second (+1)
  */
/* the WUTF flag is set every (WUT[15:0] + 1) ck_wut cycles */
//#define WAKEUP_TIMEOUT 4        /* 5 seconds */
#define WAKEUP_TIMEOUT 1        /* 2 seconds */

/**
  * @brief  system sleep mode definition
  */
#define SYS_SLEEP_IDLE          0       /* idle */
#define SYS_SLEEP_SHIP          1       /* ship mode */
#define SYS_SLEEP_OP_PENDING    2       /* normal operation sleep pending */
#define SYS_SLEEP_OP            3       /* normal operation *sleep */
#define SYS_SLEEP_RTC           4       /* RTC */
#define SYS_WAKEUP_RTC          5       /* RTC wakeup */
#define SYS_WAKEUP_ACC          6       /* accelerometer wakeup mode */
#define SYS_WAKEUP_NFC          7       /* NFC wakeup mode */
#define SYS_WAKEUP_INIT         8       /* initialize wakeup mode */
#define SYS_WAKEUP_WAIT         9       /* wait until audio engine finish playing tones */


/* Includes ------------------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/

/* external variable declaration ---------------------------------------------*/
extern __IO uint32_t uiSysTick;     /* declared in bluenrg_lp_it.c */
extern uint16_t uiSysSleepMode;

/* external function prototype -----------------------------------------------*/
extern void WakeUp_Normal_Init(void);

#endif  /* __NG_TICK_WAKEUP_H */



