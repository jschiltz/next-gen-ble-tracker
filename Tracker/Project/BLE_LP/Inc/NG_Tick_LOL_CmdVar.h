/**
  ******************************************************************************
  * @file    NG_Tick_LOL_CmdVar.h
  * @author  Thomas W Liu
  * @brief   Header file of Lite Open Link command variables.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_LOL_CMDVAR_H
#define __NG_TICK_LOL_CMDVAR_H

/* Defines -------------------------------------------------------------------*/
#define XMEM_OFFSET             0x1234  /* extended memory offset value */
#define NAK_DATA_ARRAY_SIZE     2       /* nak data array size */

#define SIZE_DEVICE_NAME_BUFF    23

/**
  * @brief  define system status busy bits
  *         non-idle (busy) in flash, NFC, accelerometer
  */
#define SYSSTAT_FLASH_BUSY      (0x01 << 0)
#define SYSSTAT_NFC_BUSY        (0x01 << 1)
#define SYSSTAT_ACC_BUSY        (0x01 << 2)
#define SYSSTAT_ADC_BUSY        (0x01 << 3)
#define SYSSTAT_NFC_NOTFOUND    (0x01 << 6)
#define SYSSTAT_ACC_NOTFOUND    (0x01 << 7)

/**
  * @brief  define structure
  */
typedef struct 
{
    uint8_t     uSysStatus;     /* system general status */
    uint8_t     uFlashErrCode;  /* flash last error code */
    uint8_t     uFlashErrCnt[2];/* flash error counter */
    uint8_t     uNFCErrCode;    /* NFC last error code */
    uint8_t     uNFCErrCnt[2];  /* NFC error counter */
    uint8_t     uAccErrCode;    /* accelerometer last error code */
    uint8_t     uAccErrCnt[2];  /* accelerometer error counter */
}  NGT_SYSSTATUS_TYPE;

/* constant declaration ------------------------------------------------------*/
    
/* extern variable declaration------------------------------------------------*/
extern char deviceName[];
extern uint16_t extendedMemoryOffset;

extern uint8_t maxPacketLength;
extern uint8_t nakData[];

extern __IO BOOL bLOL_1msF;     /* LOL 1ms flag, set by 1ms interrupt, reset in main.c */
extern bool ms_time;            /* Not time to kick out the 1 mS tick yet */
extern sendVals sendPacket;
extern ackTransactionVals ackVals;
extern ackTransactionVals slaveAckVals;

extern uint8_t LOLRXBuff0[];
extern uint8_t LOLTXBuff0[];
extern uint8_t LOLRXBuff1[];
extern uint8_t LOLTXBuff1[];

extern const slaveVals slaveArray0[];
extern const slaveVals slaveArray1[];

/* extern function declaration------------------------------------------------*/

#endif  /* __NG_TICK_LOL_CMDVAR_H */
