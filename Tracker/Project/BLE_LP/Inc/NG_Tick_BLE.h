/**
  ******************************************************************************
  * @file    NG_Tick_BLE.h
  * @author  Johnny L
  * @brief   Header file of BLE.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _NG_TICK_BLE_H_
#define _NG_TICK_BLE_H_

#define  IBEACON_1_RATE      1 /* 60sec / Advertising rate (2sec) */
#define  IBEACON_2_RATE      8 /*  8 minutes = 480sec / Advertising rate (2sec) */
#define  IBEACON_3_RATE     60 /* 60 minutes = 3600sec / Advertising rate (2sec) */

#define TICKS_PER_MINUTE    20 /* Number of ticks per minute */

#define  CONNECTION_TIMEOUT_RATE     (5 * TICKS_PER_MINUTE) /*  5 minutes = 300sec / Advertising rate (3sec) */

#define IBEACON_2_ID         1
#define IBEACON_3_ID         2

#define  ADVERTISING_RATE    9 /* 900ms / Advertising rate (100msec) */

#define  IBEACON_ADVERTISING_RATE          160   /* 160 = 100mS / 625uS */    
#define  SHIP_MODE_ADVERTISEMENT_LENGTH      8
#define  NORMAL_MODE_ADVERTISEMENT_LENGTH   17

/* extern function declaration------------------------------------------------*/
extern void BLE_Init_All(void);
extern void BLE_ModulesTick(void);
extern void pBLEStartNextTransfers( uint8_t *uiPtr, int16_t iLen);
extern void add_MPBID_to_advertising_data( uint8_t* mpbid);
extern uint8_t* get_ble_MAC_data_pointer(void);
extern void add_coin_voltage_to_advertising_data( uint8_t coin);
extern void add_histogram_to_advertising_data( uint8_t* histogram);
extern void add_last_used_to_advertising_data( uint8_t last_used);
extern void Update_Advertising_Service(void);
extern void set_ibeacon_rate( uint8_t beacon, uint8_t rate);
extern void BLE_Disconnect( void );

#endif  /* _NG_TICK_BLE_H_ */
