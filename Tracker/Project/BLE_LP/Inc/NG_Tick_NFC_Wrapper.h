/**
  ******************************************************************************
  * @file    NG_Tick_NFC_Wrapper.h
  * @author  Thomas W Liu
  * @brief   header file of NFC.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2020 Milwaukee Tools
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_NFC_WRAPPER_H
#define __NG_TICK_NFC_WRAPPER_H


/* external function prototype -----------------------------------------------*/
extern uint16_t NfcType5_TT5Init_L3(void);
extern uint16_t NfcType5_AppendData(uint8_t, uint16_t, uint8_t *);
extern uint16_t NfcType5_AppendData_L3(uint8_t, uint16_t, uint8_t *);


#endif  /* __NG_TICK_NFC_WRAPPER_H */
