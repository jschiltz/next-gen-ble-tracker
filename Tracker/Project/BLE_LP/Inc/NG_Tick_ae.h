/**
  ******************************************************************************
  * @file    NG_Tick_ae.h
  * @author  Thomas W Liu
  * @brief   Header file of audio engine.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NG_TICK_AE_H
#define __NG_TICK_AE_H

/* Includes ------------------------------------------------------------------*/

/* define declaration ------------------------------------------------------*/
/**
  * @brief  define gpio for volume control
  */
#define AUDIOAMP_PWR_PIN                LL_GPIO_PIN_4
#define AUDIOAMP_PWR_GPIO_PORT          GPIOB
#define AUDIOAMP_EN2_PIN                LL_GPIO_PIN_0
#define AUDIOAMP_EN2_GPIO_PORT          GPIOB
#define AUDIOAMP_EN1_PIN                LL_GPIO_PIN_1
#define AUDIOAMP_EN1_GPIO_PORT          GPIOB
#define AUDIOAMP_EN12_GPIO_CLK_ENABLE() LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOB)

/* define declaration ------------------------------------------------------*/
/**
  * @brief  define counter frequency & auto reload
  */
#define COUNTER_FREQ_I          100
#define COUNTER_FREQ_F          (float)COUNTER_FREQ_I
#define COUNTER_AUTORELOAD      (COUNTER_FREQ_I - 1)

/* define declaration ------------------------------------------------------*/
#define NOTE_TERM       NULL
#define NOTE_NO_NOTE    0

#define NOTE_C2         65	/* measured 130 */
#define NOTE_D2         73	/* measured 147 */
#define NOTE_E2         82	/* measured 165 */
#define NOTE_F2         87	/* measured 177 */
#define NOTE_G2         98	/* measured 196 */
#define NOTE_A2         110	/* measured 220 */
#define NOTE_B2         123	/* measured 247 */

#define NOTE_C3         130	/* measured 130 */
#define NOTE_D3         147	/* measured 147 */
#define NOTE_E3         165	/* measured 165 */
#define NOTE_F3         177	/* measured 177 */
#define NOTE_G3         196	/* measured 196 */
#define NOTE_A3         220	/* measured 220 */
#define NOTE_B3         247	/* measured 247 */

#define NOTE_C4         262	/* measured 262 */
#define NOTE_D4         294	/* measured 293 */
#define NOTE_E4         330	/* measured 330 */
#define NOTE_F4         349	/* measured 349 */
#define NOTE_G4         391	/* measured 391 */
#define NOTE_A4         440	/* measured 440 */
#define NOTE_B4         494	/* measured 494 */

#define NOTE_C5         523	/* measured 523 */
#define NOTE_D5         587	/* measured 587 */
#define NOTE_E5         659	/* measured 658 */
#define NOTE_F5         698	/* measured 698 */
#define NOTE_G5         784	/* measured 783 */
#define NOTE_A5         880	/* measured 879 */
#define NOTE_B5         988	/* measured 988 */

#define NOTE_C6         1047	/* measured 1046 */
#define NOTE_D6         1175	/* measured 1174 */
#define NOTE_E6         1319	/* measured 1317 */
#define NOTE_F6         1397	/* measured 1394 */
#define NOTE_G6         1567	/* measured 1565 */
#define NOTE_A6         1760	/* measured 1758 */
#define NOTE_B6         1976	/* measured 1976 */

#define NOTE_C7         2093	/* measured 2092 */
#define NOTE_D7         2349	/* measured 2344 */
#define NOTE_E7         2637	/* measured 2634 */
#define NOTE_F7         2794	/* measured 2782, 0.43% error */  
#define NOTE_G7         3136	/* measured 3123, 0.41% error */  
#define NOTE_A7         3520	/* measured 3516 */
#define NOTE_B7         3951	/* measured 3951 */

#define NOTE_C8         4186	/* measured 4182 */
#define NOTE_D8         4699	/* measured 4671, 0.60% error */  
#define NOTE_E8         5274	/* measured 5247, 0.51% error */  
#define NOTE_F8         5588	/* measured 5565, 0.41% error */
#define NOTE_G8         6272	/* measured 6215, 0.91% error */  
#define NOTE_A8         7040	/* measured 7032 */
#define NOTE_B8         7902	/* measured 7899 */

#define NOTE_C9         8372	/* measured 8311, 0.73% error */  
#define NOTE_D9         9397	/* measured 9275, 1.30% error */  
#define NOTE_E9         10548	/* measured 10493,0.53% error */  
#define NOTE_F9         11175	/* measured 11035,1.25% error */  
#define NOTE_G9         12543	/* measured 12306,1.89% error */  

/*The following 'Notes' are for the Final Tones*/
#define NOTE_D3_SHARP   157


#define NOTE_DURATION_TERM_CNT          NULL
#define NOTE_DURATION_20MSEC_CNT        20
#define NOTE_DURATION_26MSEC_CNT        26
#define NOTE_DURATION_64MSEC_CNT        64
#define NOTE_DURATION_82MSEC_CNT        82
#define NOTE_DURATION_58MSEC_CNT        58
#define NOTE_DURATION_95MSEC_CNT        95
#define NOTE_DURATION_100MSEC_CNT       100
#define NOTE_DURATION_125MSEC_CNT       125
#define NOTE_DURATION_150MSEC_CNT       150
#define NOTE_DURATION_200MSEC_CNT       200
#define NOTE_DURATION_255MSEC_CNT       255
#define NOTE_DURATION_300MSEC_CNT       300
#define NOTE_DURATION_400MSEC_CNT       400
#define NOTE_DURATION_500MSEC_CNT       500
#define NOTE_DURATION_750MSEC_CNT       750
#define NOTE_DURATION_776MSEC_CNT       776
#define NOTE_DURATION_808MSEC_CNT       808
#define NOTE_DURATION_MINMSEC_CNT       20

/* type declaration ----------------------------------------------------------*/
/*  song contains mulitple chords or chord progression, chords can be repeated,
 *  chords are made up of notes */
/**
  * @brief  define chord control block structure
  */
typedef struct 
{
    int16_t  iNoteFreqCnt;      /* note: frequency count */
    int16_t  iNoteFreqDuration; /* note: frequncy duration, in msec */
}  STRUCT_CHORD_CB_TYPE;

/**
  * @brief  define progression (song) control block structure
  */
typedef struct
{
    int32_t iChordRepeat;               /* chord: repeat */
    STRUCT_CHORD_CB_TYPE *sChordCBPtr;  /* chord:  ptr to chord */
}  STRUCT_PROGRESS_CB_TYPE;

/**
  * @brief  define progression (song) control block structure
  */
typedef struct
{
    uint16_t uiSongId;                  /* progression: song id */
    uint16_t uiSongVolume;              /* progression: volume level */
    int32_t  iProgressRepeat;           /* progression: repeat */
    STRUCT_PROGRESS_CB_TYPE *sProgressCBPtr;    /* progression: ptr */
}  STRUCT_SONG_CB_TYPE;

/**
  * @brief  define song index 
  */
enum {
    SONG_INDEX_NONE = 0,
    SONG_INDEX_FIND1,           /* 1 */
    SONG_INDEX_FIND2,           /* 2 */
    SONG_INDEX_FIND3,           /* 3 */
    SONG_INDEX_SUCCESS,         /* 4 */
    SONG_INDEX_FAIL,            /* 5 */
    SONG_INDEX_ACK1,            /* 6 */
    SONG_INDEX_ACK2,            /* 7 */
    SONG_INDEX_4K,              /* 8 */ 
    SONG_INDEX_BEACON,          /* 9 */
    SONG_INDEX_BEACON_ALT,      /* 10 */
    SONG_INDEX_PAIR,            /* 11 */
    SONG_INDEX_FUN,             /* 12 */
    SONG_INDEX_TT0,             /* 13 */
    SONG_INDEX_TT1,             /* 14 */
    SONG_INDEX_TT2,             /* 15 */
    SONG_INDEX_B2,              /* 16 */
    SONG_INDEX_P2,              /* 17 */
    SONG_INDEX_TERM             /* 18 */  /* must be the last one in enum */
};
/* last song index, must update, checked in LoL */
#define LAST_SONG_INDEX       (SONG_INDEX_TERM - 1)

/**
  * @brief  define audio level
  */
enum {
    AUDIO_LEVEL_0 = 0,
    AUDIO_LEVEL_1,
    AUDIO_LEVEL_2,
    AUDIO_LEVEL_3
};

/**
  * @brief  define audio engine default volume override
  *         AUDIO_LEVEL_3 for max volume, AUDIO_LEVEL_1 for lowest volume
  */
#define DEF_AE_VOL_OVERRIDE     AUDIO_LEVEL_0

/**
  * @brief  define state of audio engine
  */
enum {
    AUDIOENG_ST_IDLE = 0,
    AUDIOENG_ST_NEW,
    AUDIOENG_ST_START,
    AUDIOENG_ST_SONG,
    AUDIOENG_ST_PROGRESS,
    AUDIOENG_ST_NEXT_PROGRESS,
    AUDIOENG_ST_CHECK_PROGRESS,
    AUDIOENG_ST_WAIT_PROGRESS,
    AUDIOENG_ST_CHORD,
    AUDIOENG_ST_NEXT_CHORD,
    AUDIOENG_ST_CHECK_CHORD,
    AUDIOENG_ST_WAIT_CHORD,
    AUDIOENG_ST_CHECK_CHORD_REPEAT,
    AUDIOENG_ST_NEXT_NOTE,
    AUDIOENG_ST_CHECK_NOTE,
    AUDIOENG_ST_WAIT_NOTE,
    AUDIOENG_ST_END
};


/**
  * @brief  structure definition of the audio engine state machine
  */
typedef struct 
{
    uint16_t uiAE_State;                        /* state */
    uint16_t uiAE_NewCmd;                       /* new command:  true/false */
    uint16_t uiAE_NewSongId;                    /* new song id */
    uint16_t uiAE_Abort;                        /* abort */
    uint16_t uiAE_SongVolume;                   /* song volume */
    uint16_t uiAE_SongVolumeOverride;           /* song volume override*/
    uint32_t iAE_ProgressRepeat;                /* progression repeat count */
    uint16_t iAE_ProgressRepeatOverride;        /* progression repeat overridecount */
    STRUCT_PROGRESS_CB_TYPE *sAE_ProgressCBPtr; /* progression control block ptr */
    uint32_t iAE_ChordRepeat;                   /* chord repeat count */
    STRUCT_CHORD_CB_TYPE *sAE_ChordCBPtr;       /* chord control block ptr */
    uint16_t iAE_NoteFreqCnt;                   /* note frequency count */
    uint32_t iAE_NoteFreqDuration;              /* note duration */
}  STRUCT_AUDIOENGINE_SM;


/* extern variable declaration -----------------------------------------------*/
extern int32_t iAudioEngineSysCounter;         /* audio engine system counter, 1ms, dec in system timer */

extern const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Pair[];
extern const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Beacon[];
extern const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_BeaconAlt[];
extern STRUCT_SONG_CB_TYPE      BuzzerSongTable[];
extern const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_TickTock[];

extern STRUCT_AUDIOENGINE_SM    sAudioEngine;   /* audio machine state machine */


/* extern function prototype -------------------------------------------------*/
extern void AudioEngine_Init_All(void);
extern void ReadPrintAllAudioTable(void);
extern void CheckAudioEngine(void);


#endif  /* __NG_TICK_AE_H */
