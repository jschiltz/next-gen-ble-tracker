/**
  ******************************************************************************
  * @file    NG_Tick_LOL_GCmd.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link generic commands.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_CmdVar.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_FlashUtil.h"    /* must come before NG_Tick_sys.h */
#include "NG_Tick_NFC.h"          /* must come before NG_Tick_sys.h */
#include "NG_Tick_Acc.h"          /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_app.h"
#include "NG_Tick_LoL_Func.h"
#include "NG_Tick_BLE.h"


/* constant variable declaration ---------------------------------------------*/

/* function prototype --------------------------------------------------------*/
void fGCMfgTest( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fGCBLEGenericCmd( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fGCBLEBeaconParm( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fGCBLEDisconnet( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fGCBLEShipMode( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fGCBLESyncToolPhone( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fGCBLEFirmwareVer( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);


/**
  ******************************************************************************
  * @func   fGCMfgTest()
  * @brief  generic command (write) - manufacturing test: buzzer test, reset BLE module
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK(), xLoLGC_MfgTestAE(), xLoLGC_MfgTestBLEReset()
  * @retval None
  ******************************************************************************
  */
void fGCMfgTest(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    /* check for read/write, if read, response nack, else response ack with data */
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {
        switch (payloadPtr[0])  /* check subcommand */
        {

            case LOL_GSC_MFG_TEST_BUZZ:  /* buzzer test */
                if ((LOL_MFGTST_BUZZ_PAYLOAD_LEN + LOL_GSC_SIZE) == payloadLength)  /* 3 */
                {
                    /* duration time saved - big endian, but ignored by the AE, play expected song */
                    SystemIB.SysRAMIB.uMfgTst_BuzzerTest = (payloadPtr[1] << 8) | payloadPtr[2];
//!@#$ insert code here
                    uStatus = xLoLGC_MfgTestAE( SystemIB.SysRAMIB.uMfgTst_BuzzerTest);
                }  /* if */
                else
                {
                    uStatus = NAKMALFORMEDPACKET;
                }  /* if else */
            break;  /* LOL_GSC_MFG_TEST_BUZZ */
        
            case LOL_GSC_MFG_TEST_RST:  /* BLE reset */
                if ((LOL_MFGTST_BRST_PAYLOAD_LEN + LOL_GSC_SIZE) == payloadLength)  /* 1 */
                {
                    SystemIB.SysRAMIB.uMfgTst_BLEReset = TRUE;
//!@#$ insert code here, BLE_Reset() - search it
//                  uStatus = xLoLGC_MfgTestBLEReset( SystemIB.uMfgTst_BLEReset);
                    uStatus = LOL_ACK_RESP;  /* to be deleted when function is implemented */
                }  /* if */
                else
                {
                    uStatus = NAKMALFORMEDPACKET;
                }  /* if else */
            break;  /* LOL_GSC_MFG_TEST_RST */
              
            default:
                uStatus = NAKILLEGALVALUE;
            break;
        }  /* switch */
    }  /* if */
    
    switch (uStatus)
    {
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCMfgTest() */


/**
  ******************************************************************************
  * @func   fGCBLEGenericCmd()
  * @brief  generic command - BLE buzzer, BLE coin cell voltage
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK(), xLoLGC_BLEGCBuzzer()
  * @retval None
  ******************************************************************************
  */
void fGCBLEGenericCmd(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    switch (payloadPtr[0])  /* check subcommand */
    {
        case LOL_GSC_BLE_BUZZER:  /* write buzzer */
            if ((LOL_BGC_PBUZZ_PAYLOAD_LEN + LOL_GSC_SIZE) == payloadLength)  /* 4 */
            {
                /* big endian */
                SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_ACT_INDX] = payloadPtr[1];     /* BLE generic command - buzzer, play/stop (write only) */
                SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_TONE_INDX] = payloadPtr[2];    /* BLE generic command - buzzer, tone id (write only) */
                SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_REPEAT_INDX] = payloadPtr[3];  /* BLE generic command - buzzer, repeat (write only) */
                uStatus = xLoLGC_BLEGCBuzzer( payloadPtr[1], payloadPtr[2], payloadPtr[3]);
            }  /* if */
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }  /* if else */
        break;  /* LOL_GSC_BLE_BUZZER */

        case LOL_GSC_BLE_GCCV:  /* read coin cell voltage */
//!@#$ insert code here, 0 - 100%, 0V - 3.3V
//                  slaveAckVals.ackNack = xLoLGC_MfgTestNLEReset( SystemIB.uMfgTst_BLEReset);
            /* read last used time constant, prep ptr to send internal value*/
            uStatus = LOL_RD_RESP;
        break;  /* LOL_GSC_BLE_GCCV */

        default:  /* other cases */
            uStatus = NAKILLEGALVALUE;
        break;  /* default */

    }  /* switch */

    switch (uStatus)
    {
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&SystemIB.SysRAMIB.uCoinCellV;
            slaveAckVals.payloadLength = LOL_BGC_GCCV_PAYLOAD_LEN;  /* 1 */
        break;  /* LOL_RD_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCBLEGenericCmd() */


/**
  ******************************************************************************
  * @func   fGCBLEBeaconParm()
  * @brief  generic memory - BLE beacon paramters
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fGCBLEBeaconParm(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    switch (payloadPtr[0])  /* check subcommand */
    {
#if 0
        case LOL_GSC_BEACON_DEVEL:  /* write beacon development */
            if ((LOL_BBP_DEVL_PAYLOAD_LEN) == payloadLength)  /* 8 */
            {
                /* write, prep ptr to update internal value */
                uint8_t *destPtr = SystemIB.uBLEBeaconParm;
                uint8_t *srcPtr = &payloadPtr[1];
                int16_t iCnt = SYS_LOL_BBPARM_SIZE;
                
                while (iCnt-- > 0)
                {
                    *destPtr++ = *srcPtr++;
                }  /* while */
                uStatus = LOL_ACK_RESP;
            }  /* if */
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }  /* if else */
        break;  /* LOL_GSC_BEACON_DEVEL */
#endif
        
        case LOL_GSC_BEACON_SET_INTV:  /* set beacon interval */
            if ((LOL_BBP_INTV_WR_PAYLOAD_LEN) == payloadLength)  /* 3 */
            {
                /* big endian */
                SystemIB.SysRAMIB.uBLEBeaconID = payloadPtr[1];        /* BLE ibeacon interval cmd - id (write only) */
                SystemIB.SysRAMIB.uBLEBeaconInterval = payloadPtr[2];  /* BLE ibeacon cmd - interval (write only) */
                set_ibeacon_rate( SystemIB.SysRAMIB.uBLEBeaconID, SystemIB.SysRAMIB.uBLEBeaconInterval );
                uStatus = LOL_ACK_RESP;
            }  /* if */
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }  /* if else */
        break;  /* LOL_GSC_BEACON_SET_INTV */
        
        case LOL_GSC_BEACON_GET_INTV:  /* read beacon interval */
            uStatus = LOL_RD_RESP;
        break;  /* LOL_GSC_BEACON_GET_INTV */
        
        default:  /* other cases */
            uStatus = NAKILLEGALVALUE;
        break;  /* default */
    }  /* switch */

    switch (uStatus)
    {
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&SystemIB.SysRAMIB.uBLEBeaconID;
            slaveAckVals.payloadLength = LOL_BBP_INTV_RD_PAYLOAD_LEN;  /* 1 */
        break;  /* LOL_RD_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCBLEBeaconParm() */


/**
  ******************************************************************************
  * @func   fGCBLEDisconnet()
  * @brief  generic memory - BLE disconnect
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK(), xLoLGC_BLEDisconnect()
  * @retval None
  ******************************************************************************
  */
void fGCBLEDisconnet(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    SystemIB.SysRAMIB.uBLEDisconnect = TRUE;        /* BLE disconnect */
//!@#$ insert code here - BLE_Disconnect() - search it
//!@#$ BLE only
//          uStatus = xLoLGC_BLEDisconnect( SystemIB.uBLEDisconnect);
    BLE_Disconnect();
    uStatus = LOL_ACK_RESP;  /* delete when BLE function is implemented */

    switch (uStatus)
    {
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCBLEDisconnet() */


/**
  ******************************************************************************
  * @func   fGCBLEShipMode()
  * @brief  generic memory - BLE beacon paramters
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK(), xLoLGC_BLEShipMode_Default(), xLoLGC_BLEShipMode_NoAdv()
  * @retval None
  ******************************************************************************
  */
void fGCBLEShipMode(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;

    if ((LOL_BLE_SHIP_PAYLOAD_LEN) == payloadLength)  /* 1 */
    {
        /* write, prep ptr to update internal value */
        switch (payloadPtr[0])  /* check subcommand */
        {
            /* does not change mode, reply with ack */
            case LOL_GSC_BLE_SHIP_DEF:  /* default ship - no special mode enabled */
                if (PRODUCTION_MODE == SystemIB.SysFlashIB.uShipModeStatus)
                {
                    SystemIB.SysFlashIB.uBLEShipMode = payloadPtr[0];
                    SystemIB.SysFlashIB.uShipModeStatus = SHIP_MODE;
                    sAppCB.uAction = APPCB_ACT_2SHIPMODE;  /* action - goto ship mode */
                }  /* if */
                uStatus = LOL_ACK_RESP;  /* delete when function is implemented */
            break;  /* LOL_GSC_BLE_SHIP_DEF */
              
            case LOL_GSC_BLE_SHIP_NOADV:  /* no advertisement during sleep */
                /* write, prep ptr to update internal value */
                if (PRODUCTION_MODE == SystemIB.SysFlashIB.uShipModeStatus)
                {
                    SystemIB.SysFlashIB.uBLEShipMode = payloadPtr[0];
                    SystemIB.SysFlashIB.uShipModeStatus = SHIP_MODE;
                    sAppCB.uAction = APPCB_ACT_2SHIPMODE;  /* action - goto ship mode */
                }  /* if */
                uStatus = LOL_ACK_RESP;  /* delete when function is implemented */
            break;  /* LOL_GSC_BLE_SHIP_NOADV */
            
            default:  /* other cases */
                uStatus = NAKILLEGALVALUE;
            break;  /* default */
        }  /* switch */
    }  /* if payload */

    switch (uStatus)
    {
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */

        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCBLEShipMode() */


/**
  ******************************************************************************
  * @func   fGCBLESyncToolPhone()
  * @brief  generic memory - BLE sync tool phone
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - copied, not parsed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fGCBLESyncToolPhone(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (0 == payloadLength)
    {
        uStatus = LOL_RD_RESP;
    }  /* if */

    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&SystemIB.SysRAMIB.uBLESyncToolPhone;
            slaveAckVals.payloadLength = LOL_BLE_SYNC_TP_PAYLOAD_LEN;  /* 1 */
        break;  /* LOL_RD_RESP */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCBLESyncToolPhone() */


/**
  ******************************************************************************
  * @func   fGCBLEFirmwareVer()
  * @brief  generic memory - BLE sync tool  phone
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - copied, not parsed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fGCBLEFirmwareVer(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
              uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (0 == payloadLength)
    {
        uStatus = LOL_RD_RESP;
    }  /* if */

    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&SystemIB.SysRAMIB.uBLEFwVer;
            slaveAckVals.payloadLength = LOL_BLE_FW_VER_PAYLOAD_LEN;  /* 4 */
        break;  /* LOL_RD_RESP */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fGCBLEFirmwareVer() */



