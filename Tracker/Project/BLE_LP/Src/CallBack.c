/**
  ******************************************************************************
  * @file    CallBack.c
  * @author  Thomas W Liu
  * @brief   Source file of call backs from interrupts.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* include files -------------------------------------------------------------*/
#include "bluenrg_lp_ll_tim.h"

#include "NG_Tick_main.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_Acc.h"
#include "NG_Tick_NFC.h"

/* variable declaration ------------------------------------------------------*/
__IO uint32_t uwMeasuredDutyCycle = 0;

/* extern declaration --------------------------------------------------------*/

/* function prototype---------------------------------------------------------*/
void DMA1_TransmitComplete_Callback(void);
void DMA1_ReceiveComplete_Callback(void);
void USART_TransferError_Callback(void);
void USART_CharReception_Callback(void);
void AccInt1_Callback(void);
void NFCInt_Callback(void);
void Error_Callback(void);
void Error_Handler(void);
void assert_failed(uint8_t *, uint32_t);
void TimerCaptureCompare_Callback(void);


/******************************************************************************/
/*   USER IRQ HANDLER TREATMENT Functions                                     */
/******************************************************************************/
/**
  ******************************************************************************
  * @func   DMA1_TransmitComplete_Callback()
  * @brief  called from DMA1 IRQ Handler when Tx transfer is completed
  * @param  None
  * @gvar   ubTxComplete
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void DMA1_TransmitComplete_Callback(void)
{
    ubTxComplete = TRUE;  /* DMA Tx transfer completed */
}       /* DMA1_TransmitComplete_Callback() */

/**
  ******************************************************************************
  * @func   DMA1_ReceiveComplete_Callback()
  * @brief  called from DMA1 IRQ Handler when Rx transfer is completed
  * @param  None
  * @gvar   ubRxComplete
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void DMA1_ReceiveComplete_Callback(void)
{
    ubRxComplete = TRUE;  /* DMA Rx transfer completed */
}       /* DMA1_ReceiveComplete_Callback() */

/**
  ******************************************************************************
  * @func   USART_TransferError_Callback()
  * @brief  detect error in USART IT Handler, disable DMA 6 & 7
  * @param  None
  * @gvar   None
  * @gfunc  LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void USART_TransferError_Callback(void)
{
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);    /* Disable DMA1 Tx Channel */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_6);    /* Disable DMA1 Rx Channel */
}       /* USART_TransferError_Callback() */

/**
  ******************************************************************************
  * @func   USART_CharReception_Callback()
  * @brief  from USART IRQ Handler when RXNE flag is set
  *         Function is in charge of reading character received on USART RX line.
  * @param  None
  * @gvar   uwNbRxChars, pBufferReadyForRx, iRxMsgCnt, ubRxBufferReadyF
  * @gfunc  LL_USART_ReceiveData8()
  * @retval None
  ******************************************************************************
  */
void USART_CharReception_Callback(void)
{
//  uint8_t *ptemp;
#if 1
    iRxTimeout = RX_MSG_TIMEOUT;    /* just received char, reset timeout counter */
    switch (uiRxState)
    {
        case RX_ST_IDLE:
            if (0 == uwNbRxChars)
            {
                uiRxState = RX_ST_DATA;
                pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);
//                iRxMsgCnt = pBufferReadyForRx[0] - 0x30;
                iRxMsgCnt = pBufferReadyForRx[0];
            }
        break;  /* RX_ST_IDLE */
        
        case RX_ST_DATA:  /* received char after byte count */
            if (uwNbRxChars < iRxMsgCnt)
            {
                pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);
            }
            else if (uwNbRxChars >= iRxMsgCnt)
            {
                pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);
                bRxBufferReadyF = TRUE;        /* Set Buffer swap indication */
//                uwNbRxChars = 0;      /* init char count */
                uiRxState = RX_ST_DATA_DONE;  /* received full message */
            }
        break;  /* RX_ST_DATA */
        
        case RX_ST_DATA_DONE:  /* received full message, await processing */
        break;  /* RX_ST_DATA_DONE */
        
        default:
        break;
    }  /* switch() */
#endif

#if 0
    iRxTimeout = RX_MSG_TIMEOUT;    /* just received char, reset timeout counter */
    if (0 == uwNbRxChars)
    {
        pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);
        iRxMsgCnt = pBufferReadyForRx[0] - 0x30;
    }
    else if (uwNbRxChars < iRxMsgCnt)
    {
        pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);
    }
    else if (uwNbRxChars >= iRxMsgCnt)
    {
        pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);
        bRxBufferReadyF = TRUE;        /* Set Buffer swap indication */
//    uwNbRxChars = 0;      /* init char count */
    }
#endif
#if 0
    /* Read Received character. RXNE flag is cleared by reading of RDR register */
    pBufferReadyForRx[uwNbRxChars++] = LL_USART_ReceiveData8(USART1);

    /* Checks if Buffer full indication has been set */
    if (uwNbRxChars >= RX_BUFFER_SIZE)
    {
        /* Set Buffer swap indication */
        bRxBufferReadyF = TRUE;
        
        /* Swap buffers for next bytes to be received */
        ptemp = pBufferReadyForUser;
        pBufferReadyForUser = pBufferReadyForRx;
        pBufferReadyForRx = ptemp;
        uwNbRxChars = 0;
    }
#endif
}       /* USART_CharReception_Callback() */

/**
  ******************************************************************************
  * @func   NFCInt_Callback()
  * @brief  Set NFC flag to acknowledge received interrupt for main loop
  * @param  None
  * @gvar   ubNFCInt_flag
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void NFCInt_Callback(void)
{
    ubNFCInt_flag = TRUE;
}       /* NFCInt_Callback() */


/**
  ******************************************************************************
  * @func   Error_Callback()
  * @brief  Function called in case of error detected in USART IT Handler
  * @param  None
  * @gvar   ubButtonPress
  * @gfunc  NVIC_DisableIRQ(), LL_USART_ReadReg(), Error_Handler()
  * @retval None
  ******************************************************************************
  */
void Error_Callback(void)
{
    __IO uint32_t isr_reg;

    NVIC_DisableIRQ(USART1_IRQn);  /* Disable USARTx_IRQn */

    /* Error handling example :
    - Read USART ISR register to identify flag that leads to IT raising
    - Perform corresponding error handling treatment according to flag
    */
    isr_reg = LL_USART_ReadReg(USART1, ISR);
    if (isr_reg & LL_USART_ISR_NE)
    {
        LL_USART_ClearFlag_NE(USART1);    /* case Noise Error flag is raised : Clear NF Flag */
    }
    else
    {
        Error_Handler();    /* Unexpected IT source */
    }
}       /* Error_Callback() */


/**
  ******************************************************************************
  * @func   Error_Handler()
  * @brief  executed in case of error occurrence
  * @param  None
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void Error_Handler(void)
{
    /* User can add his own implementation to report the HAL error return state */
}       /* Error_Handler() */

/**
  ******************************************************************************
  * @func   assert_failed()
  * @brief  exports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t *file, uint32_t line)
{ 
    /* User can add his own implementation to report the file name and line number,
    tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}       /* assert_failed() */
#endif /* USE_FULL_ASSERT */


/**
  ******************************************************************************
  * @func   TimerCaptureCompare_Callback()
  * @brief  exports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  None
  * @param  None
  * @gvar   uwMeasuredDutyCycle
  * @gfunc  LL_TIM_GetCounter(), LL_TIM_GetAutoReload(), LL_TIM_OC_GetCompareCH6()
  * @retval None
  ******************************************************************************
  */
void TimerCaptureCompare_Callback(void)
{
    uint32_t CNT, ARR;
    
    CNT = LL_TIM_GetCounter(TIM1);
    ARR = LL_TIM_GetAutoReload(TIM1);
    if (LL_TIM_OC_GetCompareCH6(TIM1) > ARR)
    {
        /* If capture/compare setting is greater than autoreload, there is a
           counter overflow and counter restarts from 0.
           Need to add full period to counter value (ARR+1)  */
        CNT = CNT + ARR + 1;
    }
    uwMeasuredDutyCycle = (CNT * 100) / ( ARR + 1 );
}  /* TimerCaptureCompare_Callback() */

