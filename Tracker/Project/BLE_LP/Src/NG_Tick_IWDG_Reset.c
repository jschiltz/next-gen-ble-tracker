/**
  ******************************************************************************
  * @file    NG_Tick_IWDG_Reset.c
  * @author  Kurtis Alessi
  * @brief   Source file for watchdog reset.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "NG_Tick_IWDG_Reset.h"
#include "NG_Tick_FlashUtil.h"  /* must come before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_uart.h"
#include "NG_Tick_sys.h"

/* Defines -------------------------------------------------------------------*/
#define IWDG_NO_WINDOW  IWDG_WINR_WIN

/* 32768 ms timeout */
#define IWDG_PRESCALER  LL_IWDG_PRESCALER_256
#define IWDG_RELOAD     0xFFF 

/* Variable Declaration ------------------------------------------------------*/

/* External Function Prototypes ----------------------------------------------*/
void MX_IWDG_Init(void);
int16_t ResetReasonByIWDGRST(void);

/* Internal Function Prototypes ----------------------------------------------*/

/**
  ******************************************************************************
  * @func   MX_IWDG_Init()
  * @brief  IWDG Initialization Function, IWDG
  * @param  None
  * @gvar   LL_APB0_PERIPH_WDG, IWDG
  * @gfunc  LL_APB0_EnableClock, LL_APB0_ForceReset, LL_APB0_ReleaseReset,
            LL_RCC_IsActiveFlag_WDGRSTREL, LL_IWDG_Enable, LL_IWDG_EnableWriteAccess,
            LL_IWDG_SetPrescaler, LL_IWDG_SetWindow, LL_IWDG_SetReloadCounter,
            LL_IWDG_IsReady, LL_IWDG_ReloadCounter
  * @retval None
  ******************************************************************************
  */
void MX_IWDG_Init(void)
{
    /* Enable APB0 indipendent Watchdog peripherals clock */
    LL_APB0_EnableClock(LL_APB0_PERIPH_WDG);
    
    /* Force WDG peripheral reset */
    LL_APB0_ForceReset(LL_APB0_PERIPH_WDG);
    LL_APB0_ReleaseReset(LL_APB0_PERIPH_WDG);
    
    /* Check if WDG Reset Release flag interrupt occurred or not */
    while(0 == LL_RCC_IsActiveFlag_WDGRSTREL())
    {
    }
      
    LL_IWDG_Enable(IWDG);
    LL_IWDG_EnableWriteAccess(IWDG);
    LL_IWDG_SetPrescaler(IWDG, IWDG_PRESCALER);
    LL_IWDG_SetWindow(IWDG, IWDG_NO_WINDOW);
    LL_IWDG_SetReloadCounter(IWDG, IWDG_RELOAD);
    while (1 != LL_IWDG_IsReady(IWDG))
    {
    }   
    LL_IWDG_ReloadCounter(IWDG);
} /* MX_IWDG_Init() */

/**
  ******************************************************************************
  * @func   ResetReasonByIWDGRST()
  * @brief  Check if IWDG reset happened
  * @param  None
  * @gvar   RAM_VR, RCC_CSR_WDGRSTF
  * @gfunc  None
  * @retval TRUE   = IWDG reset occured
            FALSE  = No IWDG reset
  ******************************************************************************
  */
int16_t ResetReasonByIWDGRST(void)
{
    if((RAM_VR.ResetReason & RCC_CSR_WDGRSTF) == RCC_CSR_WDGRSTF)
    {
        return TRUE; /* IWDGRST flag is set -- Error occured */
    }
    else
    {
        return FALSE; /* No reset from watchdog occured */
    }
} /* ResetReasonByIWDGRST() */
