/**
  ******************************************************************************
  * @file    NG_Tick_FlashUtil.c
  * @author  Thomas W Liu
  * @brief   Source file of flash memory utilities.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "system_BlueNRG_LP.h"
//#include "bluenrg_lp.h"
//#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_gpio.h"  /* !@#$ */
#include "bluenrg_lp_ll_bus.h"  /* !@#$ */
#include "LOLConfig.h"
#include "NG_Tick_LOL_Cmd.h"
#include "bluenrg_lp_ll_flash.h"
#include "NG_Tick_FlashUtil.h"    /* must come before NG_Tick_sys.h */
#include "NG_Tick_NFC.h"          /* must come before NG_Tick_sys.h */
#include "NG_Tick_Acc.h"          /* must comes before sys.h */
#include "NG_Tick_sys.h"

#include "NG_Tick_ae.h"

/* constant variable declaration ---------------------------------------------*/
#define FLASH_BUFF_SIZE 16

/* variable declaration ------------------------------------------------------*/
uint32_t uFlashBuffer[FLASH_BUFF_SIZE];
uint32_t uFlashDataBuffer[FLASH_BUFF_SIZE];

/* function prototype --------------------------------------------------------*/
void CheckFlashMem(void);
uint8_t xLoLMT_FlashUtil( uint8_t *);
void Flash_ReadNWord( uint32_t *, uint32_t, uint16_t);
uint8_t Flash_EraseNPage( uint32_t, uint16_t);
uint8_t Flash_WriteWord( uint32_t, uint32_t);
uint8_t Flash_WriteNWord( uint32_t, uint32_t *, uint32_t);
uint8_t Flash_WriteBurstNWord( uint32_t, uint32_t *, uint32_t);

/**
  ******************************************************************************
  * @func   CheckFlashMem()
  * @brief  check current flash modes:  idle, erase, read, write
  *         idle: check either read or write.  if write, erase then write
  *         erase: begin erase page
  *         erase/wr: begin erase page then write data
  *         read: read flash memory into SystemInfoBlock
  *         write: write SystemInfoBlock into flash memory
  *         update: if error, increment error counter
  * @param  None
  * @gvar   SystemCB, SystemIB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void CheckFlashMem()
{
    switch (SystemCB.FlashCB.uState)
    {
        case FLASHCB_ST_IDLE:  /* idle */
            switch (SystemCB.FlashCB.uAction)
            {
                case FLASHCB_ACT_ERASE:
                    SystemCB.FlashCB.uState = FLASHCB_ST_ERASE;
                break;  /* FLASHCB_ACT_READ */
                
                case FLASHCB_ACT_READ:
                    SystemCB.FlashCB.uState = FLASHCB_ST_READ;
                break;  /* FLASHCB_ACT_READ */
                
                case FLASHCB_ACT_ERASE_WRITE:
                    SystemCB.FlashCB.uState = FLASHCB_ST_ERASE_WR;
                break;  /*  */

                case FLASHCB_ACT_WRITE:
                    SystemCB.FlashCB.uState = FLASHCB_ST_WRITE;
                break;  /* FLASHCB_ACT_WRITE */
            }  /* switch */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_IDLE;  /* reset action back to idle */
        break;  /* FLASH_ST_IDLE */
          
        case FLASHCB_ST_READ:  /* read */
            Flash_ReadNWord( (uint32_t *)&SystemIB, FLASH_ADDR_SYSINFO, SystemCB.uSysFlashIBByteLen >> 2);
            SystemCB.FlashCB.uStatus = TRUE;    /* always */
            SystemCB.FlashCB.uState = FLASHCB_ST_UPDATE;
        break;  /*FLASHCB_ST_READ */
          
        case FLASHCB_ST_ERASE:  /* erase */
            if (!Flash_EraseNPage( FLASH_PAGE_SYSINFO, 1))  /* erase page */
            {
                SystemCB.FlashCB.uErrCode = FLASHCB_ERR_ERASE;
            }
            SystemCB.FlashCB.uState = FLASHCB_ST_UPDATE;  /* error */
        break;  /*FLASH_ST_ERASE */
          
        case FLASHCB_ST_ERASE_WR:  /* erase then write */
            if (Flash_EraseNPage( FLASH_PAGE_SYSINFO, 1))  /* erase page */
            {
                SystemCB.FlashCB.uState = FLASHCB_ST_WRITE;  /* go to write */
            }
            else
            {   /* error in erase, iStatus still FALSE, no continuation to write */
                SystemCB.FlashCB.uErrCode = FLASHCB_ERR_ERASE;
                SystemCB.FlashCB.uState = FLASHCB_ST_UPDATE;  /* error */
            }
        break;  /*FLASH_ST_ERASE */
          
        case FLASHCB_ST_WRITE:  /* write SystemInfoBlock to flash memory */
            if (Flash_WriteBurstNWord( FLASH_ADDR_SYSINFO, (uint32_t *)&SystemIB, SystemCB.uSysFlashIBByteLen >> 2))
            {
                SystemCB.FlashCB.uState = FLASHCB_ST_VERIFY;  /* verify */
            }  /* if */
            else
            {   /* error in write, iStatus still FALSE, no continuation to write */
                SystemCB.FlashCB.uErrCode = FLASHCB_ERR_WRITE;
                SystemCB.FlashCB.uState = FLASHCB_ST_UPDATE;  /* error */
            }
        break;  /* FLASH_ST_WRITE */
          
        case FLASHCB_ST_VERIFY:  /* read back what was wrote in flash */
            pSystemIBRdbk = (NGT_SYSFLASHINFO_TYPE *)SystemIBRdbk;
            memset( (void *)SystemIBRdbk, 0, sizeof(NGT_SYSFLASHINFO_TYPE));
            Flash_ReadNWord( (uint32_t *)SystemIBRdbk, FLASH_ADDR_SYSINFO, SystemCB.uSysFlashIBByteLen >> 2);
            if (0 == memcmp( (void *)&SystemIBRdbk, (void *)&SystemIB, SystemCB.uSysFlashIBByteLen))
            {
                SystemCB.FlashCB.uStatus =  TRUE;
            }  /* if */
            else
            {   /* error in verify, iStatus still FALSE */
                SystemCB.FlashCB.uErrCode = FLASHCB_ERR_VERIFY;
            }
            SystemCB.FlashCB.uState = FLASHCB_ST_UPDATE;
        break;  /* FLASHCB_ST_VERIFY */
          
        case FLASHCB_ST_UPDATE:  /* update status */
            if (!SystemCB.FlashCB.uStatus)
            {
                SystemCB.FlashCB.uwErrCnt++;  /* inc error counter if error */
            }
            SystemCB.FlashCB.uStatus =  FALSE;  /* reset status */
            SystemCB.FlashCB.uState = FLASHCB_ST_IDLE;  /* reset state to idle */
        break;  /* FLASH_ST_UPDATE */
        
        default:
        break;
    }   /* switch() */
}  /* CheckFlashMem() */


/**
  ******************************************************************************
  * @func   xLoLMT_FlashUtil()
  * @brief  Parse flash commands: erase, read, write, write multiple, write burst
  *         write 2 sets of system infor block with different values
  * @param  source ptr of commands & parameters
  * @gvar   
  * @gfunc  Flash_EraseNPage(), memset(), Flash_ReadNWord(), Flash_WriteWord(),
  *         Flash_WriteNWord(), Flash_WriteBurstNWord()
  * @retval LOL_ACK_RESP, NAKMALFORMEDPACKET, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_FlashUtil( uint8_t *uPtr)
{
    uint32_t uSrcAddr;
    uint32_t uData;
    uint8_t uStatus;
    uint8_t uCnt;
    uint8_t uOffset;

    switch (uPtr[2])
    {
        case LOL_LM_FLASH_ERASE:
            if (Flash_EraseNPage( uPtr[3], uPtr[4]))
            {
                uStatus = LOL_ACK_RESP;
            }
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }
        break;  /* LOL_LM_FLASH_ERASE */
      
        case LOL_LM_FLASH_READ:
            uSrcAddr = (uPtr[3] << 24) | (uPtr[4] << 16) | (uPtr[5] << 8) | (uPtr[6]);
            memset( uFlashBuffer, FLASH_BUFF_SIZE, 0);
            Flash_ReadNWord( uFlashBuffer, uSrcAddr, uPtr[7] % FLASH_BUFF_SIZE);
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_FLASH_READ */
      
        case LOL_LM_FLASH_WRITE:  /* word count to write is [7] */
            uSrcAddr = (uPtr[3] << 24) | (uPtr[4] << 16) | (uPtr[5] << 8) | (uPtr[6]);
            uData = (uPtr[8] << 24) | (uPtr[9] << 16) | (uPtr[10] << 8) | (uPtr[11]);
            uFlashDataBuffer[0] = uData;
            if (Flash_WriteWord( uSrcAddr, uData))
            {
                uStatus = LOL_ACK_RESP;
            }
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }
        break;  /* LOL_LM_FLASH_WRITE */
      
        case LOL_LM_FLASH_NWRITE:  /* word count to write is [7] */
            uCnt = 0;
            uSrcAddr = (uPtr[3] << 24) | (uPtr[4] << 16) | (uPtr[5] << 8) | (uPtr[6]);
            memset( uFlashDataBuffer, FLASH_BUFF_SIZE, 0);
            uOffset = 8;
            for (uCnt = 0; uCnt < uPtr[7]; uCnt++)
            {
                uData = (uPtr[uOffset] << 24) | (uPtr[uOffset+1] << 16) | (uPtr[uOffset+2] << 8) | (uPtr[uOffset+3]);
                uOffset += 4;
                uFlashDataBuffer[uCnt] = uData;
            }  /* for */
            if (Flash_WriteNWord( uSrcAddr, uFlashDataBuffer, uPtr[7]))
            {
                uStatus = LOL_ACK_RESP;
            }
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }
        break;  /* LOL_LM_FLASH_NWRITE */
      
        case LOL_LM_FLASH_BURSTWRITE:  /* word count to write is [7] */
            uCnt = 0;
            uSrcAddr = (uPtr[3] << 24) | (uPtr[4] << 16) | (uPtr[5] << 8) | (uPtr[6]);
            memset( uFlashDataBuffer, FLASH_BUFF_SIZE, 0);
            uOffset = 8;
            for (uCnt = 0; uCnt < uPtr[7]; uCnt++)
            {
                uData = (uPtr[uOffset] << 24) | (uPtr[uOffset+1] << 16) | (uPtr[uOffset+2] << 8) | (uPtr[uOffset+3]);
                uOffset += 4;
                uFlashDataBuffer[uCnt] = uData;
            }  /* for */
            if (Flash_WriteBurstNWord( uSrcAddr, uFlashDataBuffer, uPtr[7]))
            {
                uStatus = LOL_ACK_RESP;
            }
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }
        break;  /* LOL_LM_FLASH_NWRITE */
      
        case LOL_LM_FLASH_SYSINFOWR:  /* write system info block to flash */
            if (Flash_WriteBurstNWord( FLASH_ADDR_SYSINFO, (uint32_t *)&SystemIB, SystemCB.uSysFlashIBByteLen >> 2))
            {
                uStatus = LOL_ACK_RESP;
            }
            else
            {
                uStatus = NAKMALFORMEDPACKET;
            }
        break;  /* LOL_LM_FLASH_SYSINFOWR */
      
        case LOL_LM_FLASH_SYSINFORD:  /* read system info block from flash */
            Flash_ReadNWord( (uint32_t *)&SystemIB, FLASH_ADDR_SYSINFO, SystemCB.uSysFlashIBByteLen >> 2);
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_FLASH_SYSINFORD */
      
        case LOL_LM_FLASH_SYSINFOINIT:  /* int system info block in RAM */
            switch (uPtr[3])
            {
                case 0:  /* init system info block to zero */
                    memset( (void *)&SystemIB, 0, sizeof(SystemIB));
                    uStatus = LOL_ACK_RESP;
                break;
                
                case 1:  /* init system infor block to alternate 1 */
                    NGT_SystemIB1_init();
                    uStatus = LOL_ACK_RESP;
                break;
                
                case 2:  /* init system infor block to alternate 2 */
                    NGT_SystemIB2_init();
                    uStatus = LOL_ACK_RESP;
                break;
                
                default:
                    uStatus = NAKILLEGALVALUE;
                break;
            }  /* switch */
        break;  /* LOL_LM_FLASH_SYSINFOINIT */

        /* set flag for system info block save to flash, begin state machine */
        case LOL_LM_FLASH_SYSINFOSM:
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE;
            uStatus = LOL_ACK_RESP;
       break;  /* LOL_LM_FLASH_SYSINFOSM */
      
        default:
            uStatus = NAKILLEGALVALUE;
        break;
    }  /* switch */
    return(uStatus);
}  /* xLoLMT_FlashUtil() */

/**
  ******************************************************************************
  * @func   Flash_ReadNWord()
  * @brief  Read n word from flash memory in word format
  * @param  destination of flash memory data to stored to
  * @param  beginning flash memory address to read from
  * @param  number of word to read
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void Flash_ReadNWord( uint32_t *iDestPtr, uint32_t iSrcAdd, uint16_t uNWord)
{
    uint32_t iCnt;
    uint32_t *srcPtr;

    iCnt = uNWord;
    srcPtr = (uint32_t *)iSrcAdd;
    while (iCnt-- > 0)
    {
        *iDestPtr++ = *srcPtr++;
    }  /* while */
}  /*Flash_ReadNWord() */


/**
  ******************************************************************************
  * @func   Flash_EraseNPage()
  * @brief  erase n page in flash memory
  * @param  page number in flash memory
  * @param  number of flash memory pages to erase
  * @gvar   None
  * @gfunc  LL_FLASH_GetFlag(), LL_FLASH_Erase()
  * @retval TRUE = erased success, FALSE = not erased
  ******************************************************************************
  */
uint8_t Flash_EraseNPage( uint32_t uPageN, uint16_t uNPage)
{
    uint8_t bStatus = FALSE;
  
    if (SET == LL_FLASH_GetFlag( FLASH, LL_FLASH_FLAG_CMDDONE))
    {
        LL_FLASH_Erase( FLASH, LL_FLASH_TYPE_ERASE_PAGES, uPageN, uNPage);  /* p3 = page #, p4 = # page to erase */
        bStatus = TRUE;
    }
    return(bStatus);
}  /*Flash_EraseNPage() */



/**
  ******************************************************************************
  * @func   Flash_WriteWord()
  * @brief  write a word in flash memory in word format (4 bytes)
  * @param  destination of flash memory address
  * @param  data word to write
  * @gvar   None
  * @gfunc  LL_FLASH_GetFlag(), LL_FLASH_Program()
  * @retval TRUE = write success, FALSE = not written
  ******************************************************************************
  */
uint8_t Flash_WriteWord( uint32_t iDestPtr, uint32_t uDataWord)
{
    uint8_t bStatus = FALSE;
  
    if (SET == LL_FLASH_GetFlag( FLASH, LL_FLASH_FLAG_CMDDONE))
    {
        LL_FLASH_Program(FLASH, iDestPtr, uDataWord);
        bStatus = TRUE;
    }
    return(bStatus);
}  /*Flash_WriteWord() */

/**
  ******************************************************************************
  * @func   Flash_WriteNWord()
  * @brief  write N word (one word at a time) in flash memory in word format (4 bytes)
  * @param  destination of flash memory address
  * @param  data word ptr to write
  * @param  number of words to write
  * @gvar   None
  * @gfunc  LL_FLASH_GetFlag(), LL_FLASH_Program()
  * @retval TRUE = write success, FALSE = not written
  ******************************************************************************
  */
uint8_t Flash_WriteNWord( uint32_t iDestPtr, uint32_t *ulwDataPtr, uint32_t uDataWordCnt)
{
    uint8_t bStatus = FALSE;
    uint8_t iCnt = uDataWordCnt;
    uint8_t iStatusOkCnt = 0;
 
    while (iCnt-- > 0)
    {
        if (SET == LL_FLASH_GetFlag( FLASH, LL_FLASH_FLAG_CMDDONE))
        {
            LL_FLASH_Program(FLASH, iDestPtr, *ulwDataPtr);
            iDestPtr += 4;     /* long word aligned address inc */
            ulwDataPtr++;
            iStatusOkCnt++;
        } /* if */
    }  /* while */
    
    if (iStatusOkCnt == uDataWordCnt)
    {
        bStatus = TRUE;
    }
    return(bStatus);
}  /*Flash_WriteNWord() */


/**
  ******************************************************************************
  * @func   Flash_WriteBurstNWord()
  * @brief  write N word (block of 4 words) in flash memory, aligned by 4 words boundry (no check)
  * @param  destination of flash memory address
  * @param  data word ptr to write
  * @param  data byte count to write
  * @gvar   None
  * @gfunc  LL_FLASH_GetFlag(), LL_FLASH_ProgramBurst()
  * @retval TRUE = write success, FALSE = not written
  ******************************************************************************
  */
uint8_t Flash_WriteBurstNWord( uint32_t iDestPtr, uint32_t *ulwDataPtr, uint32_t uDataWordCnt)
{
    uint8_t bStatus = FALSE;
    int16_t iCnt = (int16_t)uDataWordCnt;
    int32_t iWrOkWordCnt = 0;
    uint32_t *ulSrcPtr = ulwDataPtr;
    
    while (iCnt > 0)
    {
        if (SET == LL_FLASH_GetFlag( FLASH, LL_FLASH_FLAG_CMDDONE))
        {
            LL_FLASH_ProgramBurst(FLASH, iDestPtr, ulSrcPtr);
            iDestPtr += 16;     /* long word aligned address increment for 4 words */
            ulSrcPtr++;         /* increment src address for 4 words */
            ulSrcPtr++;
            ulSrcPtr++;
            ulSrcPtr++;
            iWrOkWordCnt += 4;
        } /* if */
        iCnt -= 4;
    }  /* while */
    
    if (iWrOkWordCnt >= uDataWordCnt)
    {
        bStatus = TRUE;
    }
    return(bStatus);
}  /*Flash_WriteBurstNWord() */


