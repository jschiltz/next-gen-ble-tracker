/**
  ******************************************************************************
  * @file    NG_Tick_app2.c
  * @author  Thomas W Liu
  * @brief   Source file of application
  ******************************************************************************
  * @attention
  * @note    This module contains test code for the main app.This module is not
  *          in the project, so no code is included in the hex file.  This is
  *          to use as a reference.
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp_stack.h"
#include "bluenrg_lp_ll_dma.h"
#include "bluenrg_lp_ll_adc.h"
#include "bluenrg_lp_hal_vtimer.h"
#include "NG_Tick_main.h"
#include "bluenrg_lp.h"
#include "NG_Tick_IWDG_Reset.h"
#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"

#include "ble_status.h"
#include "NG_Tick_BLE.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_adc.h"
#include "NG_Tick_app.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_ble.h"

#include "bluenrg_lp_hal_power_manager.h"
#include "NG_Tick_power.h"
#include "NG_Tick_i2c.h"

#include "bluenrg_lp_hal_vtimer.h"

/* defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
/* function prototype --------------------------------------------------------*/
void xTask_60Sec(void);  /* $%^& */
void xApp_Normal(void);

void xDebugAdv(void);

void Normal_App_Tick(void);


/* external function prototype -----------------------------------------------*/


/**
  ******************************************************************************
  * @func   xTask_60Sec()
  * @brief  excute 60 second tasks
  * @param  None
  * @gvar   bApp_60SecF
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xTask_60Sec()
{
    if (sAppTimeCB.bApp_60SecF)
    {
        sAppTimeCB.bApp_60SecF = FALSE;
        switch (sAppCB.uState)
        {
            case APPCB_ST_IDLE:
                AdcCB.uAction = ADCCB_ACT_CONVERT;      /* start convert $%^& */
            break;  /* APPCB_ST_IDLE */
          
            case APPCB_ST_NORMAL:
                AdcCB.uAction = ADCCB_ACT_CONVERT;      /* start convert */
                SystemIB.SysRAMIB.uNFCBuzzerEvents += 1;
                SystemIB.SysRAMIB.uNFCEvents += 2;
                SystemIB.SysRAMIB.uNFCResetEvents += 3;
                SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN;    /* write dynamic to NFC memory */
            break;  /* APPCB_ST_NORMAL */
          
        }  /* switch */
    }  /* if */
}  /* xTask_60Sec */

/**
  ******************************************************************************
  * @func   xDebugAdv()
  * @brief  excute 10 minutes tasks: NFC dynamic write, set flag to begin ADC convert
  * @param  None
  * @gvar   bApp_10MinF, AdcCB, SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xDebugAdv()
{
    adv_data[13] = uAppCnt;
    adv_data[14] = sAppCB.uAction;  //uAppCnt2;
    adv_data[15] = sAppCB.uState;  //uAppCnt1;
    adv_data[16] = (uint8_t)iAccSecCnt;         // acc second counter
//    adv_data[17] = (uint8_t)iAccInt1Cnt;        // acc interrupt 1 counter
    adv_data[18] = (uint8_t)SystemIB.SysRAMIB.uNFCBuzzerEvents;
    adv_data[19] = (uint8_t)SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_CCV];
    adv_data[20] = AdcCB.uState;
    adv_data[21] = uAppCnt1;  // 2 sec loop counter
    adv_data[22] = uAppCnt2;
    adv_data[23] = uAppCnt3;
//    adv_data[24] = uAppCnt4;
    
    adv_data[14] = (uint8_t)(((SystemCB.NFCCB.uState << 4) & 0x00f0) | (SystemCB.NFCCB.uAction & 0x0f));
//    adv_data[17] = uAppCnt3;
//    adv_data[18] = uAppCnt4;
//    adv_data[17] = (uint8_t)((uiSysTick >> 16) & 0x00ff);
//    adv_data[18] = (uint8_t)(uiSysTick >> 24);
    adv_data[22] = (uint8_t)(((AdcCB.uState << 4) & 0x00f0) | (AdcCB.uAction & 0x0f));
    adv_data[23] = (uint8_t)(((accel_sm.myState << 4) & 0x00f0) | (SystemCB.AccCB.uAction & 0x0f));
    adv_data[24] = (uint8_t)iAccInt1Cnt;        // acc interrupt 1 counter
//    adv_data[24] = (uint8_t)(((sAudioEngine.uiAE_State << 4) & 0x00f0) | (sAudioEngine.uiAE_NewCmd & 0x0f));
}  /* xDebugAdv */


/**
  ******************************************************************************
  * @func   CheckApp()
  * @brief  check application, states are idle, to ship mode, ship mode, activate,
  *         normal
  * @param  None
  * @gvar   sAppCB
  * @gfunc  xApp_Idle(), xApp_2ShipMode(), xApp_2ShipMode_PowerDn(), xApp_ShipMode(),
  *         xApp_Activate(), CheckAudioEngine(), CheckEndOfRx(), CheckEndOfTx(), ,
  *         CheckRxMsg(), CheckLoL(), CheckFlashMem(), CheckNFC(), CheckADC(),
  *         CheckAcc(), TrackTime(), BLE_ModulesTick(), xTask_2Sec(), xTask_60Sec()
  * @retval None
  ******************************************************************************
  */
void CheckApp()
{
    switch (sAppCB.uState)
    {
        /*----------------------------------------------------------------------
         *  Idle Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_IDLE:
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckFlashMem();    /* check for flash memory operation */
            CheckNFC();         /* check NFC tasks & interrupt */
            CheckADC();         /* check adc */
#ifdef ENABLE_ACC_MFG
            CheckAcc();         /* check accelerometer */
#endif
            TrackTime();
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            xTask_2Sec();
xTask_60Sec();
            xApp_Idle();
uAppCnt--;
//xDebugAdv();
            /* @#$% init time counter & flags, before going to APPCB_ST_2SHIPMODE */
        break;  /* APPCB_ST_IDLE */
        
        /*----------------------------------------------------------------------
         *  To Ship Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_2SHIPMODE:
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL?? */
            CheckFlashMem();    /* check for flash memory operation */
            CheckNFC();         /* check NFC tasks & interrupt */
            CheckADC();         /* check adc */
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            xApp_2ShipMode();
        break;  /* APPCB_ST_2SHIPMODE */
        
        /*----------------------------------------------------------------------
         *  To Ship Mode Power Down
         *--------------------------------------------------------------------*/
        case APPCB_ST_2SHIPMODE_PWRDN:
            TrackTime();
            xTask_2Sec();
            xApp_2ShipMode_PowerDn();
        break;  /* APPCB_ST_2SHIPMODE_PWRDN */
        
        /*----------------------------------------------------------------------
         *  Ship Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_SHIPMODE:
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckNFC();         /* check NFC tasks & interrupt */
//            CheckADC();         /* check adc */
            TrackTime();
//xTask_10Sec();
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            xApp_ShipMode();
uAppCnt++;
//xDebugAdv();
        break;  /* APPCB_ST_SHIPMODE */
            
        /*----------------------------------------------------------------------
         *  Activate Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_ACTIVATE:
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckFlashMem();    /* check for flash memory operation */
            CheckNFC();         /* check NFC tasks & interrupt */
            CheckADC();         /* check adc */
            TrackTime();
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
//            xTask_2Sec();
            xApp_Activate();
uAppCnt++;
//xDebugAdv();
        break;  /* APPCB_ST_ACTIVATE */

        /*----------------------------------------------------------------------
         *  Normal Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_NORMAL:
#if 1
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckNFC();         /* needed to check NFC interrupt */
#ifdef ENABLE_BLE
            BLE_ModulesTick();
            BLE_AppTick();
#endif
  if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
  {
            Normal_PowerSave_PD_NoT();                
  }  /* if ae */
//            Normal_PowerSave_PD_NoT();                
            
#else
            NormalTrackTime();
            xTask_2Sec();
xTask_60Sec();
            xTask_60Min();
            Normal_App_Tick();

#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckFlashMem();    /* check for flash memory operation */
            CheckADC();         /* check adc */
            CheckNFC();         /* check NFC tasks & interrupt */
#ifdef ENABLE_ACC_NORMAL
//            CheckAcc();         /* check accelerometer */
            NormalCheckAcc();
#endif
            xApp_Normal();
//uAppCnt++;
uAppCnt--;
//xDebugAdv();
#endif
        break;  /* APPCB_ST_NORMAL */

        default:
        break;
    }  /* switch() */
}  /* CheckApp() */


/**
  ******************************************************************************
  * @func   xApp_Normal()
  * @brief  app check in normal operation.
  * @param  None
  * @gvar   AdcCB, SystemCB, accel_sm
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xApp_Normal()
{
    int8_t bNoNFCEvents = FALSE;
    int8_t bNoADCEvents = FALSE;
    int8_t bNoAccEvents = FALSE;
    int8_t bNoAEEvents = FALSE;

    if ((NFCCB_ST_IDLE == SystemCB.NFCCB.uState) && (FLASHCB_ACT_IDLE == SystemCB.NFCCB.uAction))
    {
        bNoNFCEvents = TRUE;
    }
    
    if ((ADCCB_ST_IDLE == AdcCB.uState) && (ADCCB_ACT_IDLE == AdcCB.uAction))
    {
        bNoADCEvents = TRUE;
    }
        
//        if ((ACCEL_STATE_IDLE == accel_sm.myState) && (ACCCB_ACT_IDLE == SystemCB.AccCB.uAction))
    if (ACCCB_ACT_IDLE == SystemCB.AccCB.uAction)
    {
        bNoAccEvents = TRUE;
    }
    
    if ((AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State) && (FALSE == sAudioEngine.uiAE_NewCmd))
    {
        bNoAEEvents = TRUE;
    }
    
    bNoEvents = FALSE;
    if (bNoNFCEvents && bNoADCEvents && bNoAccEvents && bNoAEEvents)
    {
        bNoEvents = TRUE;
    }
	
   uAppCnt3++;

#ifdef ENABLE_PWR_DOWN_IO
    /* if audio engine is not on, from NFC hit, then enable power on, otherwise, skip */
    if (TRUE == bNoEvents)
    {
        /*--------------------------------------------------------------
         * all peripherals are already shutdown or de-initalized, so
         * init gpio for pull down and call power manager only
         *------------------------------------------------------------*/
          Normal_PowerSave_PD();
uAppCnt4++;
    }  /* if */
#endif

}  /* xApp_Normal */


/**
  ******************************************************************************
  * @func   NGT_PowerSave_PD_VTimer()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  None
  * @gvar   
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void Normal_App_Tick()
{
    /*  Update sensor value */
    if (bNormalTime_Expired)
    {
        bNormalTime_Expired = FALSE;
        bSys_2SecF = TRUE;
        if (HAL_VTIMER_StartTimerMs( &hNormalTimer, NORMAL_2SEC_INTVAL_MS) != BLE_STATUS_SUCCESS)
        {
            bNormalTime_Expired = TRUE;
        }  /* if */
    } /* if */
}

