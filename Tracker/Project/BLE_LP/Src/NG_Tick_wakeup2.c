/**
  ******************************************************************************
  * @file    NG_Tick_wakeup.c
  * @author  Thomas W Liu
  * @brief   Source file of wakeup.
  ******************************************************************************
  * @attention
  * @note    This module contains test code for the power saving.  This module is not
  *          in the project, so no code is included in the hex file.  This is
  *          to use as a reference.
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp.h"
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_rcc.h"
#include "bluenrg_lp_ll_rtc.h"
#include "bluenrg_lp_hal_power_manager.h"
#include "NG_Tick_wakeup.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_NFC.h"

#include "NG_Tick_Acc.h"        /* !@#$ wakeup, delete later */
#include "NG_Tick_ae.h"
#include "NG_Tick_ble.h"


#define EN_RD_WAKEUP_MSG

/* constant variable declaration ------------------------------------------------------*/
#ifdef EN_RD_NFC_MSG   /* !@#$ */
//const char ccNFCStr_RstMBEnDyn_OK1[] = "NFC_ResetMBEn_Dyn:  NFCTAG OK\r\n";
#endif

/* constant variable declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
const char uccNGTStr_BLE_Init[] = "BLE Initialized\r\n";

/* variable declaration ------------------------------------------------------*/
uint16_t uiSysSleepMode = SYS_SLEEP_IDLE;

/* External variables --------------------------------------------------------*/


/* function prototype --------------------------------------------------------*/
void CheckSysSleep(void);
void SleepWakeUp_Init_All(void);
void WakeUp_Normal_Init(void);
void StartTxWU_WaitTilEndOfTx( uint8_t *);
uint32_t Test_WakeUp_RTC(void);
uint32_t GoToSleep_Normal(void);
void PrintNegitatedLevel(uint8_t);
void PrintWakeupSource(uint32_t);
void RTC_WakeupInit(void);
void SetRTC_WakeupTimeout(uint32_t);
void DisableRTC_WakeupTimeout(void);

/* external variable declaration ---------------------------------------------*/

/* external function prototype -----------------------------------------------*/


/**
  ******************************************************************************
  * @func   CheckSysSleep()
  * @brief  CheckSysSleep
  * @param  None
  * @gvar   SystemCB, SystemIB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void CheckSysSleep()
{
    switch (uiSysSleepMode)
    {
        case SYS_SLEEP_OP_PENDING:
//                uiSysSleepMode = SYS_SLEEP_OP;
            WakeUp_Normal_Init();
            uiSysSleepMode = SYS_SLEEP_RTC;
        break;  /* SYS_SLEEP_OP_PENDING */
         
        case SYS_SLEEP_OP:
            switch (Test_WakeUp_RTC())
            {
                case NFC_WAKEUP:
                    uiSysSleepMode = SYS_WAKEUP_NFC;
                break;  /* NFC_WAKEUP */
                
                case WAKEUP_PB7:  /* acc int 1 */
                    uiSysSleepMode = SYS_WAKEUP_ACC;
                break;  /* WAKEUP_PB7 */

                case WAKEUP_PB6:  /* acc int 2 */
                    uiSysSleepMode = SYS_WAKEUP_ACC;
                break;  /* WAKEUP_PB6 */

                case WAKEUP_RTC:
                    uiSysSleepMode = SYS_WAKEUP_RTC;
                break;  /* WAKEUP_RTC */
                
                default:
                break;  /* default */
            }  /* switch() */
        break;  /* SYS_SLEEP_OP() */

        case SYS_SLEEP_RTC:
            switch (GoToSleep_Normal())
            {
                case NFC_WAKEUP:
                    if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
                    {
                        sAudioEngine.uiAE_NewSongId = SONG_INDEX_P2;
                        sAudioEngine.uiAE_NewCmd = TRUE;
                        ubNFCInt_flag = FALSE;
                    }
//                        uiSysSleepMode = SYS_WAKEUP_NFC;
                    uiSysSleepMode = SYS_WAKEUP_WAIT;
                break;  /* NFC_WAKEUP */
                
                case WAKEUP_PB7:  /* acc int 1 */
                    if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
                    {
                        sAudioEngine.uiAE_NewSongId = SONG_INDEX_TT1;
                        sAudioEngine.uiAE_NewCmd = TRUE;
                        
                    }
//                        uiSysSleepMode = SYS_WAKEUP_ACC;
                    uiSysSleepMode = SYS_WAKEUP_WAIT;
                break;  /* WAKEUP_PB7 */

                case WAKEUP_PB6:  /* acc int 2 */
                    if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
                    {
                        sAudioEngine.uiAE_NewSongId = SONG_INDEX_TT2;
                        sAudioEngine.uiAE_NewCmd = TRUE;
                        
                    }
//                        uiSysSleepMode = SYS_WAKEUP_ACC;
                    uiSysSleepMode = SYS_WAKEUP_WAIT;
                break;  /* WAKEUP_PB6 */

                case WAKEUP_RTC:
                    uiSysSleepMode = SYS_WAKEUP_RTC;
                break;  /* WAKEUP_RTC */
                
                default:
                    uiSysSleepMode = SYS_SLEEP_RTC;
                break;  /* default */
            }  /* switch() */
        break;  /* SYS_SLEEP_RTC */
        
        case SYS_WAKEUP_WAIT:
            if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
            {
                uiSysSleepMode = SYS_WAKEUP_RTC;
            }
        break;  /* SYS_WAKEUP_WAIT */
            
        case SYS_WAKEUP_RTC:
/* !@#$ ** add code here */
          uiSysSleepMode = SYS_SLEEP_RTC;
        break;  /* SYS_WAKEUP_RTC */
        
        case SYS_WAKEUP_ACC:
        case SYS_WAKEUP_NFC:
            if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
            {
                uiSysSleepMode = SYS_SLEEP_RTC;
            }
        break;  /* SYS_WAKEUP_ACC, SYS_WAKEUP_NFC */
        
        case SYS_SLEEP_IDLE:
        break;  /* SYS_SLEEP_IDLE */
        
        case SYS_WAKEUP_INIT:
            WakeUp_Normal_Init();
            uiSysSleepMode = SYS_SLEEP_IDLE;
        break;
    }  /* switch */
#ifdef ENABLE_BLE
    BLE_ModulesTick();
#endif
}  /* CheckSysSleep() */


/**
  ******************************************************************************
  * @func   WakeUp_Init_All()
  * @brief  init sleep/wakeup
  * @param  None
  * @gvar   None
  * @gfunc  RTC_WakeupInit(), LL_PWR_EnablePDA(), LL_PWR_EnablePDB()
  * @retval None
  ******************************************************************************
  */
void SleepWakeUp_Init_All()
{
#ifdef ENABLE_BLE
//    uiSysSleepMode = SYS_SLEEP_IDLE;
    uiSysSleepMode = SYS_SLEEP_OP_PENDING;
    BLE_Init_All();
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx(uccNGTStr_BLE_Init);
#endif
#else
    uiSysSleepMode = SYS_SLEEP_IDLE;
#endif
}  /* WakeUp_Init_All */

/**
  ******************************************************************************
  * @func   WakeUp_Normal_Init()
  * @brief  init RTC for wakeup.  init pull down to reduce leakage
  * @param  None
  * @gvar   None
  * @gfunc  RTC_WakeupInit(), LL_PWR_EnablePDA(), LL_PWR_EnablePDB()
  * @retval None
  ******************************************************************************
  */
void WakeUp_Normal_Init()
{
#if 0   /* not suing RTC */
    RTC_WakeupInit();    /* RTC Wakeup Peripheral Init */
#endif

#if 1
//    LL_PWR_EnablePDA( LL_PWR_PUPD_IO8 };
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
                      LL_PWR_PUPD_IO8 |
                      LL_PWR_PUPD_IO9 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
#endif
    

#if 0
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
                      LL_PWR_PUPD_IO9 |
                      LL_PWR_PUPD_IO10 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
#endif
    
#if 0
    /* Pull UP/DOWN configuration to reduce the current leakage */
    /* PA0 = i2c clk, PA1 = i2c sda, PA8 = uart rx, PA9 = uart tx */
    /* PA11 = DIN */
//    LL_PWR_EnablePDA( LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO4|
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
#endif
    
#if 1
    // RP2.5
    /* PB2 = NFC interrupt, PB3 = NFC power */
    /* PB4 = buzzer power */
    /* PB6 = Acc interrupt 2, PB7 = Acc interrupt 1 */
    /* PB3 = NFC low power1, PB4 = buzzer PWR */
    LL_PWR_EnablePDB( 
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 | 
//                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO8 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO4 | 
                      LL_PWR_PUPD_IO3 | 
                      LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |  /* testing */
                      LL_PWR_PUPD_IO9 | LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 |
                      LL_PWR_PUPD_IO15);
#else
    /* PB2 = NFC interrupt, PB6 = Acc interrupt 2, PB7 = Acc interrupt 1 */
    /* PB3 = NFC low power1, PB4 = buzzer PWR, PB14 = NFC power */
    LL_PWR_EnablePDB( LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 | LL_PWR_PUPD_IO3 | 
                      LL_PWR_PUPD_IO4 | LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO8 |
                      LL_PWR_PUPD_IO9 | LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 |
                      LL_PWR_PUPD_IO15);
#endif
}  /* WakeUp_RTC_Init() */

#include <string.h>
#include "bluenrg_lp_ll_dma.h"
extern char    ucDestStr[];
extern int16_t iTxMsgLen;               /* expected tx data count */
extern __IO uint8_t ubTxComplete;       /* tx complete flag */
/**
  ******************************************************************************
  * @func   StartTxWU_WaitTilEndOfTx()
  * @brief  tx array of uint8 via uart
  * @param  None
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void StartTxWU_WaitTilEndOfTx( uint8_t *uiStr)
{
    iTxMsgLen = (int)strlen((char *)uiStr);   /* calculate str length */
    pStartNextTransfers( uiStr, iTxMsgLen);    /* Initiate next DMA transfers */
//    while (ubTxComplete != TRUE);  /* 1 - Wait end of transmission */
    while (!ubTxComplete);      /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;       /* reset tx complete flag */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}  /* StartTxWU_WaitTilEndOfTx() */

const char cWakeUp_Msg[] = { "Pwr Save Req: STOP_W_TIMER (RTC)\r\n" };
const char cWakeUpN_Msg[] = { "Pwr Save Req: STOP_WO_TIMER (RTC)\r\n" };
const char cWakeUpM_Msg[] = { "Pwr Save Req: CPU_HALT\r\n" };
const char cWakeUpS_Msg[] = { "Pwr Save Req: Shutdown\r\n" };
uint8_t  uiStr0[] = { "*0-" };
uint8_t  uiStr1[] = { "*1-" };
uint8_t  uiStr2[] = { "*2-" };
uint8_t  uiStr3[] = { "*3-" };
uint8_t  uiStr4[] = { "*4-" };
uint8_t  uiStr5[] = { "*5-" };
uint8_t  uiStr6[] = { "*6-" };
uint8_t  uiStr7[] = { "*7-" };
uint8_t  uiStr8[] = { "*8-" };
uint8_t  uiStr9[] = { "*9-" };
char cWakeUp_Msg_Err[128];
/**
  ******************************************************************************
  * @func   Test_WakeUp_RTC()
  * @brief  test wakeup options, RTC wakeup & GPIO wakeup
  * @param  None
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
uint32_t Test_WakeUp_RTC()
{
    uint8_t ret_val;
    WakeupSourceConfig_TypeDef wakeupIO;
    uint32_t wakeupSources;
    PowerSaveLevels stopLevel;
    char *cPtr;
//    while (bWakeUpFlag)
//    {
        /* POWER_SAVE_LEVEL_STOP_WITH_TIMER : wake on UART (PA8)/timeout 5 sec, (RTC)/button PUSH1 (PA10) */
#ifdef  EN_RD_WAKEUP_MSG
//        printf("Enable Power Save Request : STOP_WITH_TIMER (RTC)\r\n");
//        cStartTx_WaitTilEndOfTx(cWakeUp_Msg);
        cStartTx_WaitTilEndOfTx(cWakeUpN_Msg);
//        cStartTx_WaitTilEndOfTx(cWakeUpM_Msg);
#endif
//        while(BSP_COM_UARTBusy());          
        wakeupIO.IO_Mask_High_polarity = WAKEUP_PB2 | WAKEUP_PB6 | WAKEUP_PB7;
//        wakeupIO.IO_Mask_Low_polarity = WAKEUP_PA8;
StartTxWU_WaitTilEndOfTx(uiStr9);
        wakeupIO.RTC_enable = 1;          
StartTxWU_WaitTilEndOfTx(uiStr0);
        SetRTC_WakeupTimeout(WAKEUP_TIMEOUT);
StartTxWU_WaitTilEndOfTx(uiStr1);
//        ret_val = HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_WITH_TIMER, wakeupIO, &stopLevel);
        ret_val = HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, wakeupIO, &stopLevel);
//        ret_val = HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_CPU_HALT, wakeupIO, &stopLevel);
StartTxWU_WaitTilEndOfTx(uiStr2);
        if (ret_val != SUCCESS)
        {
#ifdef  EN_RD_WAKEUP_MSG
//            printf("Error during clock config 0x%2x\r\n", ret_val);
            sprintf( cWakeUp_Msg_Err, "Error during clock config 0x%2x\r\n", ret_val);
            cPtr = &cWakeUp_Msg_Err[0];
            cStartTx_WaitTilEndOfTx(cPtr);
#endif
        }  /* if */
StartTxWU_WaitTilEndOfTx(uiStr3);
#ifdef  EN_RD_WAKEUP_MSG
        PrintNegitatedLevel(stopLevel);
#endif
        if (stopLevel >= POWER_SAVE_LEVEL_STOP_WITH_TIMER)
        {
StartTxWU_WaitTilEndOfTx(uiStr4);
            wakeupSources = HAL_PWR_MNGR_WakeupSource();
StartTxWU_WaitTilEndOfTx(uiStr5);
#ifdef  EN_RD_WAKEUP_MSG
            PrintWakeupSource(wakeupSources);
#endif
        }  /* if */
StartTxWU_WaitTilEndOfTx(uiStr6);
        DisableRTC_WakeupTimeout();
//StartTxWU_WaitTilEndOfTx(uiStr7);
//        LL_PWR_ClearWakeupSource(LL_PWE_EWS_ALL);  /* Clear previous wakeup sources */
//        LL_RTC_ClearFlag_WUT(RTC);  /* Clear RTC Wake Up timer Flag */
StartTxWU_WaitTilEndOfTx(uiStr8);
        wakeupIO.RTC_enable = 0;          
#ifdef  EN_RD_WAKEUP_MSG
        sprintf( cWakeUp_Msg_Err, "SysTick %04d\r\n", uiSysTick);
        cPtr = &cWakeUp_Msg_Err[0];
        cStartTx_WaitTilEndOfTx(cPtr);
#endif
//    }
    return(wakeupSources);
}  /* Test_WakeUp_RTC() */


/**
  ******************************************************************************
  * @func   GoToSleep_Normal()
  * @brief  test wakeup options, RTC wakeup & GPIO wakeup
  * @param  None
  * @gvar   None
  * @gfunc  cStartTx_WaitTilEndOfTx(), SetRTC_WakeupTimeout(), HAL_PWR_MNGR_Request(),
  *         PrintNegitatedLevel(), HAL_PWR_MNGR_WakeupSource(), PrintWakeupSource(),
  *         DisableRTC_WakeupTimeout()
  * @retval wakeup source
  ******************************************************************************
  */
uint32_t GoToSleep_Normal()
{
    WakeupSourceConfig_TypeDef wakeupIO;
    PowerSaveLevels stopLevel;
    uint32_t uiWakeupSources;
    uint8_t ret_val;
    char *cPtr;
    
    /* POWER_SAVE_LEVEL_STOP_WITH_TIMER : wake on UART (PA8)/timeout 5 sec, (RTC)/button PUSH1 (PA10) */
#ifdef  EN_RD_WAKEUP_MSG
StartTxWU_WaitTilEndOfTx(uiStr0);
//    cStartTx_WaitTilEndOfTx(cWakeUp_Msg);
//    cStartTx_WaitTilEndOfTx(cWakeUpN_Msg);
//    cStartTx_WaitTilEndOfTx(cWakeUpM_Msg);
//    cStartTx_WaitTilEndOfTx(cWakeUpS_Msg);
#endif
    
//      while(BSP_COM_UARTBusy());          
    wakeupIO.IO_Mask_High_polarity = WAKEUP_PB2 | WAKEUP_PB6 | WAKEUP_PB7;
    wakeupIO.RTC_enable = 1;          
    SetRTC_WakeupTimeout(WAKEUP_TIMEOUT);
    ret_val = HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_WITH_TIMER, wakeupIO, &stopLevel);
//    ret_val = HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, wakeupIO, &stopLevel);
//    ret_val = HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_CPU_HALT, wakeupIO, &stopLevel);
    if (ret_val != SUCCESS)
    {
#ifdef  EN_RD_WAKEUP_MSG
        sprintf( cWakeUp_Msg_Err, "Error in clock config 0x%2x\r\n", ret_val);
        cPtr = &cWakeUp_Msg_Err[0];
        cStartTx_WaitTilEndOfTx(cPtr);
#endif
    }  // if
    PrintNegitatedLevel(stopLevel);
    if (stopLevel >= POWER_SAVE_LEVEL_STOP_WITH_TIMER)
    {
        uiWakeupSources = HAL_PWR_MNGR_WakeupSource();
        PrintWakeupSource(uiWakeupSources);
    }  // if
    DisableRTC_WakeupTimeout();
StartTxWU_WaitTilEndOfTx(uiStr9);
    return(uiWakeupSources);
}  /* GoToSleep_Normal() */



/**
  ******************************************************************************
  * @func   PrintNegitatedLevel()
  * @brief  Display the Stop Level negotiated.
  * @param  stopLevel negotiated Stop level
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void PrintNegitatedLevel(uint8_t stopLevel)
{
//    char cPowerLevel_Msg[] = { "\r\nPower save level negotiated: " };
    char cPowerLevel_Msg[] = { "\r\nPwr level: " };
    char cPowerLevel_Msg_Run[] = { "Running\r\n" };
    char cPowerLevel_Msg_CPUW[] = { "CPU_Halt\r\n" };
    char cPowerLevel_Msg_STOPT[] = { "STOP_W_Timer\r\n" };
    char cPowerLevel_Msg_STOPNoT[] = { "STOP_NoTimer\r\n" };
    char *cPtr;

    cPtr = &cPowerLevel_Msg[0];
    cStartTx_WaitTilEndOfTx(cPtr);
    switch (stopLevel)
    { 
        case POWER_SAVE_LEVEL_RUNNING:
            cPtr = &cPowerLevel_Msg_Run[0];
        break;
        
        case POWER_SAVE_LEVEL_CPU_HALT:
            cPtr = &cPowerLevel_Msg_CPUW[0];
        break;
        
        case POWER_SAVE_LEVEL_STOP_WITH_TIMER:
            cPtr = &cPowerLevel_Msg_STOPT[0];
        break;
        
        case POWER_SAVE_LEVEL_STOP_NOTIMER:
            cPtr = &cPowerLevel_Msg_STOPNoT[0];
        break;
    }  /* switch() */
    cStartTx_WaitTilEndOfTx(cPtr);
}  /* PrintNegitatedLevel() */

char cWakeUpSrc_MSG_Misc[64];
/**
  ******************************************************************************
  * @func   PrintWakeupSource()
  * @brief  display the wakeup souce
  * @param  wakeupSource Wakeup Sources
  * @gvar   cWakeUpSrc_MSG_Misc[]
  * @gfunc  uiStartTx_WaitTilEndOfTx(), cStartTx_WaitTilEndOfTx()
  * @retval None
  ******************************************************************************
  */
void PrintWakeupSource(uint32_t wakeupSources)
{
//    uint8_t uiWakeUpSrc_MSG[] = { "Wakeup Src: " };
#if 0
    uint8_t uiWakeUpSrc_MSG_RTC[] = { "WU_RTC\r\n" };
    uint8_t uiWakeUpSrc_MSG_PB7_AccInt1[] = { "WU_PB7-Acc Int1\r\n" };
    uint8_t uiWakeUpSrc_MSG_PB6_AccInt2[] = { "WU_PB6-Acc Int2\r\n" };
    uint8_t uiWakeUpSrc_MSG_NFC[] = { "WU_PB2-NFC\r\n" };
#else    
    uint8_t uiWakeUpSrc_MSG_RTC[] = { "WU_RTC " };
    uint8_t uiWakeUpSrc_MSG_PB7_AccInt1[] = { "WU_ACC1 " };
    uint8_t uiWakeUpSrc_MSG_PB6_AccInt2[] = { "WU_ACC2 " };
    uint8_t uiWakeUpSrc_MSG_NFC[] = { "WU_NFC " };
#endif
    uint8_t *uiPtr;
    char *cPtr;
    
//    uiPtr = &uiWakeUpSrc_MSG[0];/* Wakeup source */
//    uiStartTx_WaitTilEndOfTx(uiPtr);
    switch (wakeupSources)
    {
        case WAKEUP_RTC:        /* wake up RTC */
            uiPtr = &uiWakeUpSrc_MSG_RTC[0];
            uiStartTx_WaitTilEndOfTx(uiPtr);
        break;
        
        case WAKEUP_PB7:        /* wake PB7 - accelerometer interrupt 1 */
            uiPtr = &uiWakeUpSrc_MSG_PB7_AccInt1[0];
            uiStartTx_WaitTilEndOfTx(uiPtr);
        break;
        
        case WAKEUP_PB6:        /* wake PB6 - accelerometer interrupt 2 */
            uiPtr = &uiWakeUpSrc_MSG_PB6_AccInt2[0];
            uiStartTx_WaitTilEndOfTx(uiPtr);
        break;
        
        case WAKEUP_PB2:        /* wake PB2 - NFC interrupt */
            uiPtr = &uiWakeUpSrc_MSG_NFC[0];
            uiStartTx_WaitTilEndOfTx(uiPtr);
        break;
        
        default:
            sprintf( cWakeUpSrc_MSG_Misc, "(default) WAKEUP src 0x%08x\r\n", wakeupSources);
            cPtr = &cWakeUpSrc_MSG_Misc[0];
            cStartTx_WaitTilEndOfTx(cPtr);
        break;
    }  /* switch() */
}  /* PrintWakeupSource() */

/**
  ******************************************************************************
  * @func   RTC_WakeupInit()
  * @brief  initialize RTC wakeup
  * @param  None
  * @gvar   None
  * @gfunc  LL_APB0_EnableClock(), LL_APB0_ForceReset(), LL_APB0_ReleaseReset(),
  *         LL_RCC_IsActiveFlag_RTCRSTREL(), LL_RTC_DisableWriteProtection(),
  *         LL_RTC_EnableInitMode(), LL_RTC_IsActiveFlag_INIT(), LL_RTC_SetHourFormat(),
  *         LL_RTC_SetAlarmOutEvent(), LL_RTC_SetOutputPolarity(), LL_RTC_SetAsynchPrescaler(),
  *         LL_RTC_SetSynchPrescaler(), LL_RTC_DisableInitMode(), LL_RTC_EnableWriteProtection()
  * @retval None
  ******************************************************************************
  */
void RTC_WakeupInit(void)
{  
    /* Enable RTC peripheral Clocks */  /* Enable RTC Clock */
    LL_APB0_EnableClock(LL_APB0_PERIPH_RTC);
    
    /* Force RTC peripheral reset */
    LL_APB0_ForceReset(LL_APB0_PERIPH_RTC);
    LL_APB0_ReleaseReset(LL_APB0_PERIPH_RTC);
    
    /* Check if RTC Reset Release flag interrupt occurred or not */
    while (0 == LL_RCC_IsActiveFlag_RTCRSTREL());
    
    LL_RTC_DisableWriteProtection(RTC);   /* Disable the write protection for RTC registers */
    LL_RTC_EnableInitMode(RTC);           /* Init mode setup */
    
    while (RESET == LL_RTC_IsActiveFlag_INIT(RTC));  /* Wait till the Init mode is active */
    
    LL_RTC_SetHourFormat( RTC, LL_RTC_HOURFORMAT_24HOUR);  /* Configure Hour Format */
    LL_RTC_SetAlarmOutEvent( RTC, LL_RTC_ALARMOUT_DISABLE);/* Output disabled */
    LL_RTC_SetOutputPolarity( RTC, LL_RTC_OUTPUTPOLARITY_PIN_HIGH);  /* Output polarity */
    LL_RTC_SetAsynchPrescaler( RTC, 0x7F); /* Set Asynchronous prescaler factor */
    LL_RTC_SetSynchPrescaler( RTC, 0x00FF);/* Set Synchronous prescaler factor */
    LL_RTC_DisableInitMode(RTC);           /* Exit Initialization mode */
    LL_RTC_EnableWriteProtection(RTC);     /* Enable write protection */
}  /* RTC_WakeupInit() */

/**
  ******************************************************************************
  * @func   SetRTC_WakeupTimeout()
  * @brief  set the RTC sleep duration to wakeup
  * @param  sleep time in seconds - 1, ie 4 = 5 seconds
  * @gvar   None
  * @gfunc  LL_RTC_DisableWriteProtection(), LL_RTC_WAKEUP_Disable(), LL_RTC_DisableIT_WUT(),
  *         LL_RTC_IsActiveFlag_WUTW(), LL_PWR_ClearWakeupSource(), LL_RTC_ClearFlag_WUT(),
  *         LL_RTC_WAKEUP_SetAutoReload(), LL_RTC_WAKEUP_SetClock(),
  *         LL_RTC_EnableIT_WUT(), LL_RTC_WAKEUP_Enable(), LL_RTC_EnableWriteProtection(),
  *         NVIC_SetPriority(), NVIC_EnableIRQ(), LL_RTC_ClearFlag_WUT()
  * @retval None
  ******************************************************************************
  */
void SetRTC_WakeupTimeout(uint32_t time)
{
    LL_RTC_DisableWriteProtection(RTC); /* Disable write protection */
    LL_RTC_WAKEUP_Disable(RTC);         /* Disable Wake-up Timer */

    /* In case of interrupt mode is used, the interrupt source must disabled */
    LL_RTC_DisableIT_WUT(RTC);
    
    while (LL_RTC_IsActiveFlag_WUTW(RTC) == 0); /* Wait till RTC WUTWF flag is set  */
    
    LL_PWR_ClearWakeupSource(LL_PWR_EWS_INT);   /* Clear PWR wake up Flag */
    LL_RTC_ClearFlag_WUT(RTC);                  /* Clear RTC Wake Up timer Flag */
    LL_RTC_WAKEUP_SetAutoReload(RTC, time);     /* Configure the Wake-up Timer counter */
    LL_RTC_WAKEUP_SetClock(RTC, LL_RTC_WAKEUPCLOCK_CKSPRE);    /* Configure the clock source */
    LL_RTC_EnableIT_WUT(RTC);           /* Configure the Interrupt in the RTC_CR register */
    LL_RTC_WAKEUP_Enable(RTC);          /* Enable the Wake-up Timer */
    LL_RTC_EnableWriteProtection(RTC);  /* Enable write protection */
    
    /* Configure NVIC for RTC */
    NVIC_SetPriority(RTC_IRQn, IRQ_LOW_PRIORITY);
    NVIC_EnableIRQ(RTC_IRQn);    
  
    LL_RTC_ClearFlag_WUT(RTC);    /* Clear RTC Wake Up timer Flag */
}  /* SetRTC_WakeupTimeout() */

/**
  ******************************************************************************
  * @func   DisableRTC_WakeupTimeout()
  * @brief  disable RTC wakeup timeout
  * @param  None
  * @gvar   None
  * @gfunc  LL_RTC_DisableWriteProtection(), LL_RTC_WAKEUP_Disable(),
  *         LL_RTC_EnableWriteProtection()
  * @retval None
  ******************************************************************************
  */
void DisableRTC_WakeupTimeout(void)
{
    LL_RTC_DisableWriteProtection(RTC); /* Disable write protection */
    LL_RTC_WAKEUP_Disable(RTC);         /* disable the Wake-up Timer */
    LL_RTC_EnableWriteProtection(RTC);  /* Enable write protection */
}  /* DisableRTC_WakeupTimeout() */

    
#if 0
/**
  ******************************************************************************
  * @func   NGT_Power_SaveRTC()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_SaveRTC()
{
    RTC_WakeupInit();    /* RTC Wakeup Peripheral Init */
#ifdef ENABLE_PWR_I2C_DEINIT           /* de-init i2c to save power */
    NGT_I2C_DeInit();
#endif

#ifdef ENABLE_PWR_ADC_DEINIT           /* de-init adc to save power */
    ADC_DeInit();
#endif
           
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
//    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PA8 | WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 1;
    SetRTC_WakeupTimeout(WAKEUP_TIMEOUT);

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_WITH_TIMER, sWakeupIO, &iStopLevel);
#endif
    DisableRTC_WakeupTimeout();
    sWakeupIO.RTC_enable = 0;
}  /* NGT_Power_SaveRTC */


/**
  ******************************************************************************
  * @func   NGT_Power_SaveRTC_PD()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_SaveRTC_PD()
{
    RTC_WakeupInit();    /* RTC Wakeup Peripheral Init */
           
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
//    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PA8 | WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 1;
    SetRTC_WakeupTimeout(WAKEUP_TIMEOUT);

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_WITH_TIMER, sWakeupIO, &iStopLevel);
#endif
    
//    LL_RTC_ClearFlag_WUT(RTC);  /* Clear RTC Wake Up timer Flag */
    DisableRTC_WakeupTimeout();
    sWakeupIO.RTC_enable = 0;
}  /* NGT_Power_SaveRTC */

#endif

