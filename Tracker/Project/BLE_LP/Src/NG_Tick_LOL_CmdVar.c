/**
  ******************************************************************************
  * @file    NG_Tick_LOL_CmdVar.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link command variables.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_LMCmd.h"
#include "NG_Tick_LOL_LMCmd_T.h"
#include "NG_Tick_LOL_LMCmd_RF.h"
#include "NG_Tick_LOL_GCmd.h"
#include "NG_Tick_LOL_CmdVar.h"

/* constant variable declaration ---------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
/*                                                   10        20        30 */
/*                                          123456789012345678901234567890 */
char deviceName[SIZE_DEVICE_NAME_BUFF] = { "Next Generation Tracker" };

//uint64_t userPassword = USERPW;
//uint64_t adminPassword = ADMINPW;
//uint64_t servicePassword = SERVICEPW;
//uint64_t METCOPassword = METCOPW;
uint16_t extendedMemoryOffset = XMEM_OFFSET;

uint8_t maxPacketLength = CHAN1_TX_BUFF_SIZE - 2;  /* 20 */
uint8_t nakData[NAK_DATA_ARRAY_SIZE] = {NAKMAXMESSAGE, 0xFF};


__IO BOOL bLOL_1msF = FALSE;    /* LOL 1ms flag, set by 1ms interrupt, reset in main.c */

bool ms_time = FALSE;           /* Not time to kick out the 1 mS tick yet */
sendVals sendPacket;
ackTransactionVals ackVals;
ackTransactionVals slaveAckVals;

uint8_t LOLRXBuff0[CHAN0_RX_BUFF_SIZE];
uint8_t LOLTXBuff0[CHAN0_TX_BUFF_SIZE];
uint8_t LOLRXBuff1[CHAN1_RX_BUFF_SIZE];
uint8_t LOLTXBuff1[CHAN1_TX_BUFF_SIZE];

/* function prototype --------------------------------------------------------*/

/* constant variable declaration ------------------------------------------------------*/
/* LM = local memory */
/* p0 = function ptr, p1 = read security, p2 = write security, p3 = msg id, p4 = LM address*/
const slaveVals slaveArray0[SLAVEARRAY0SIZE] =
{
    {*readMemMapVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, LOL_LM_MEMMAPVER},              /* LM - read Memory Map Version */
    {*RWExtMemOffset, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_EXTMEMOS},                   /* LM - read/write Extended Memory Offset */
    {*fLMMPBID, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_MPBID},                  /* LM - read/write MPBID */
    {*sendDevNameChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DEV_NAME},     /* LM - unique device name */
    {*fLMFwVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_FMREV},              /* LM - firmware version */
    {*fLMSWPN, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_SWPN},                    /* LM - SW Part Number */
    {*fLMDateOfMfg, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BOD},                /* LM - read/write BOD */
    {*fLMDateOfService, eMM_Sec_Lev__USER, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DFO_ACT},       /* LM - read/write DOS/DOA, activation */
    {*fLMDateLastUSed, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DATE_LAST_USED}, /* LM - read data last used, tool last used */
    {*fLMDateTimeRef, eMM_Sec_Lev__ALL, eMM_Sec_Lev__USER, eLOCAL_MEM_ACCESS, LOL_LM_DT_REF},                    /* LM - read/write data time reference */
    {*fLMPlatformId, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, LOL_LM_PLATFORM_ID},                /* LM - read platform identifier */
    {*fLMBLEMACAddr, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BLE_MAC_ADDR},      /* LM - read/write BLE MAC address */
    {*fLMT_UnitSN, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_UNIT_SN},   /* LM - read/write unit serial number */
    {*fLMT_AssetIdCode, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_ASSET_CODE},   /* LM - read/write asset id code */
    {*fLMT_AssetIdCodePhone, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ASSET_CODE_PHONE},  /* LM - read/write asset id code phone */
    {*fLMT_BOCCV, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BOCCV},                /* LM - read/write born on coin cell voltage */
    {*fLMT_UnitReset, eMM_Sec_Lev__NONE, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_UNIT_RESET},      /* LM - write unit reset */
    {*fLMT_AttributeStream, eMM_Sec_Lev__NONE, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_ATT_STREAM},/* LM - write attribute streaming */
    {*fLMT_AttributeFlashUtil, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_DEBUG},         /* LM - read/write attribute debug */
    {*fLMT_AttributeSysStatus, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_STATUS},         /* LM - read/write attribute system status */
    {*fLMT_AttributeNFC, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_NFC},                  /* LM - read/write attribute NFC */
    {*fLMT_AttributeProdStatus, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_PRODUCTION},    /* LM - read/write attribute production status */
    {*fLMT_AttributeMiscStatus, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_MISC},          /* LM - read/write attribute misc status */
    {*fLMT_RF_TX, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_RF_TX},       /* LM - RF, tx */
    {*fLMT_RF_RX, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_RF_RX},       /* LM - RF, tx */
    {*fLMT_RF_OTA, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_RF_OTA},     /* LM - RF, ota */
    {*fLMT_OwnershipClaimKey, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_OWNER_CKPW},   /* LM - read/write ownership claim key */
    {*passwordEnable, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_PW_ENABLE},                 /* LM - Write Password Enable */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eUSER_PASSWORD},   /* LM - Read/Write USER password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eADMIN_PASSWORD},  /* LM - Read/Write ADMIN password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eSERVICE_PASSWORD},/* LM - Read/Write SERVICE password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eMETCO_PASSWORD},  /* LM - Read/Write METCO password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BLE_MAC_ADDR},  /* LM - Read/Write BLE MAC address */
    {*sendMaxPktLn, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eGET_MAX_PKT_LEN, LOL_GSC_NA},                  /* GET_MAX_PKT_LENGTH */
    {*fGCMfgTest, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, eMANUFACTURING_TEST, LOL_GSC_NA},                /* generic command - manufacturing tests */
    {*fGCBLEGenericCmd, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_GENERIC_CMD, LOL_GSC_NA},              /* generic command - BLE generic command */
    {*fGCBLEBeaconParm, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_SET_BEACON_PARAMETERS, LOL_GSC_NA},    /* generic command - BLE beacon parameter command */
//    {*fGCBLEDisconnet, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_DISCONNECT, LOL_GSC_NA},                /* generic command - BLE disconnect */
    {*fGCBLEShipMode, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_SET_SHIPPING_ADV_MODE, LOL_GSC_NA},      /* generic command - BLE ship mode */
//    {*fGCBLESyncToolPhone, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_SYNC_TOOL_PHONE, LOL_GSC_NA},       /* generic command - BLE sync tool phone */
    {*fGCBLEFirmwareVer, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_GET_FW_VERSION, LOL_GSC_NA},          /* generic command - BLE firmware version */
    {*simpleAck, eMM_Sec_Lev__USER, eMM_Sec_Lev__USER, eADAPTER_PACKET_TARGET, LOL_GSC_NA},             /* Simple way to test security level access */
//    {*simpleAck, eMM_Sec_Lev__USER_ADMIN, eMM_Sec_Lev__SERVICE, eMANUFACTURING_TEST, LOL_GSC_NA},     /* Simple way to test security level access */
    {*simpleAck, eMM_Sec_Lev__SERVICE, eMM_Sec_Lev__SERVICE, eMANUFACTURING_TEST_RESP, LOL_GSC_NA},     /* Simple way to test security level access */
    {*simpleAck, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__SERVICE, eRESERVED7, LOL_GSC_NA},             /* Simple way to test security level access */
//    {*slave5, eMM_Sec_Lev__ALL, eMM_Sec_Lev__USER_ADMIN, eBLE_SET_SHIPPING_ADV_MODE, LOL_GSC_NA}        /* Simple test */
};  /* slaveArray0[] */

const slaveVals slaveArray1[SLAVEARRAY1SIZE] =
{
    {*readMemMapVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, LOL_LM_MEMMAPVER},              /* LM - read Memory Map Version */
    {*RWExtMemOffset, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_EXTMEMOS},                   /* LM - read/write Extended Memory Offset */
    {*fLMMPBID, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_MPBID},                  /* LM - read/write MPBID */
    {*sendDevNameChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DEV_NAME},     /* LM - unique device name */
    {*fLMFwVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_FMREV},              /* LM - firmware version */
    {*fLMSWPN, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_SWPN},                    /* LM - SW Part Number */
    {*fLMDateOfMfg, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BOD},                /* LM - read/write BOD */
    {*fLMDateOfService, eMM_Sec_Lev__USER, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DFO_ACT},       /* LM - read/write DOS/DOA, activation */
    {*fLMDateLastUSed, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DATE_LAST_USED}, /* LM - read data last used, tool last used */
    {*fLMDateTimeRef, eMM_Sec_Lev__ALL, eMM_Sec_Lev__USER, eLOCAL_MEM_ACCESS, LOL_LM_DT_REF},                    /* LM - read/write data time reference */
    {*fLMPlatformId, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, LOL_LM_PLATFORM_ID},                /* LM - read platform identifier */
    {*fLMBLEMACAddr, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BLE_MAC_ADDR},      /* LM - read/write BLE MAC address */
    {*fLMT_UnitSN, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_UNIT_SN},   /* LM - read/write unit serial number */
    {*fLMT_AssetIdCode, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_ASSET_CODE},   /* LM - read/write asset id code */
    {*fLMT_AssetIdCodePhone, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ASSET_CODE_PHONE},  /* LM - read/write asset id code phone */
    {*fLMT_BOCCV, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BOCCV},                /* LM - read/write born on coin cell voltage */
    {*fLMT_UnitReset, eMM_Sec_Lev__NONE, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_UNIT_RESET},      /* LM - write unit reset */
    {*fLMT_AttributeStream, eMM_Sec_Lev__NONE, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_ATT_STREAM},/* LM - write attribute streaming */
    {*fLMT_AttributeSysStatus, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_STATUS},        /* LM - read/write attribute system status */
  {*fLMT_AttributeMiscStatus, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_ATT_MISC},          /* LM - read/write attribute misc status */
    {*fLMT_RF_OTA, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_RF_OTA},    /* LM - RF, ota */
    {*fLMT_OwnershipClaimKey, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_OWNER_CKPW},   /* LM - read/write ownership claim key */
    {*passwordEnable, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_PW_ENABLE},                 /* test for Password Enable */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eUSER_PASSWORD},   /* Read/Write USER password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eADMIN_PASSWORD},  /* Read/Write ADMIN password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eSERVICE_PASSWORD},/* Read/Write SERVICE password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, eMETCO_PASSWORD},  /* Read/Write MANUFACTURING password */
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BLE_MAC_ADDR},  /* LM - Read/Write BLE MAC address */
    {*sendMaxPktLn, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eGET_MAX_PKT_LEN, LOL_GSC_NA},                  /* GET_MAX_PKT_LENGTH */
    {*fGCMfgTest, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, eMANUFACTURING_TEST, LOL_GSC_NA},                /* generic command - manufacturing tests */
    {*fGCBLEGenericCmd, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_GENERIC_CMD, LOL_GSC_NA},              /* generic command - BLE generic command */
    {*fGCBLEBeaconParm, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_SET_BEACON_PARAMETERS, LOL_GSC_NA},    /* generic command - BLE beacon parameter command */
    {*fGCBLEDisconnet, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_DISCONNECT, LOL_GSC_NA},                /* generic command - BLE disconnect */
    {*fGCBLEShipMode, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_SET_SHIPPING_ADV_MODE, LOL_GSC_NA},      /* generic command - BLE ship mode */
    {*fGCBLESyncToolPhone, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_SYNC_TOOL_PHONE, LOL_GSC_NA},       /* generic command - BLE sync tool phone */
    {*fGCBLEFirmwareVer, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eBLE_GET_FW_VERSION, LOL_GSC_NA},          /* generic command - BLE firmware version */
    {*simpleAck, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__SERVICE, eADAPTER_PACKET_TARGET, LOL_GSC_NA}, /* Simple way to test security level access */
//    {*simpleAck, eMM_Sec_Lev__SERVICE, eMM_Sec_Lev__SERVICE, eMANUFACTURING_TEST, LOL_GSC_NA},          /* Simple way to test security level access */
    {*simpleAck, eMM_Sec_Lev__USER_ADMIN, eMM_Sec_Lev__SERVICE, eMANUFACTURING_TEST_RESP, LOL_GSC_NA},  /* Simple way to test security level access */
    {*simpleAck, eMM_Sec_Lev__USER, eMM_Sec_Lev__SERVICE, eRESERVED7, LOL_GSC_NA}                       /* Simple way to test security level access */
};  /* slaveArray1[] */

