/**
  ******************************************************************************
  * @file    NG_Tick_uart.c
  * @author  Thomas W Liu
  * @brief   Source file of uart.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
#include "NG_Tick_main.h"
#include "NG_Tick_uart.h"

#include "NG_Tick_LOL_Cmd.h"

/* variable declaration ------------------------------------------------------*/
__IO int32_t iRxTimeout = RX_MSG_TIMEOUT;


/* variable declaration ------------------------------------------------------*/
//#ifdef ENABLE_RND_MSG
/* Buffer used for transmission */
/* Note : the buffer must be into the embedded SRAM because DMA cannot read/write from/to the Flash memory */
/*                                   10        20        30        40        50        60        70        80        90        100       110        121*/
/*                         01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901*/
const char cNGT_Title[] = "NG Tracker V0.01\r\n";
const uint8_t aTxBuffer[] = "NGT: 115200, 8, 1, none...\r\nAccelerometer double tap\r\nNFC st25dv\r\n";
uint8_t ubNbDataToTx = sizeof(aTxBuffer) - 1;
//#endif

/* Buffer used for reception */
/* Note : the buffer must be into the embedded SRAM because DMA cannot read/write from/to the Flash memory */

uint8_t aRXBufferA[UART_RX_BUFF_SIZE];
uint8_t aRXBufferB[UART_RX_BUFF_SIZE];

uint8_t *pBufferReadyForUser;
uint8_t *pBufferReadyForRx;

__IO uint16_t   uiRxState = RX_ST_IDLE;   /* rx state */
__IO uint8_t    ubTxComplete = FALSE;   /* tx complete flag */
__IO uint8_t    ubRxComplete = FALSE;   /* rx complete flag */
__IO uint32_t   uwNbRxChars;            /* current bytes received */
__IO uint8_t    bRxBufferReadyF = FALSE;       /* rx buffer ready flag */

BOOL bRxMsgReady = FALSE;

int16_t iRxMsgCnt = 0;  /* Rx data byte count */
int16_t iTxMsgLen = 0;  /* Tx data byte count */
char    ucDestStr[UART_TX_BUFF_SIZE] = { 0 };  /* message to be transmitted */


/* function prototype --------------------------------------------------------*/
void CheckRxMsg(void);
void UART_Init_All(void);
void MX_USART1_UART_Init(void);
void MX_DMA_Init(void);
void UART_Vars_Init(void);
void UART_DeInit(void);
void pStartNextTransfers( uint8_t *, int16_t);
void StartTx_WaitTilEndOfTx(char *);
void cStartTx_WaitTilEndOfTx(const char *);
void uiStartTx_WaitTilEndOfTx(uint8_t *);
void CrEFUint8_StartTx_WaitTilEndOfTx_N( uint8_t *, int16_t);
void CrEFUint8_StartTx_WaitTilEndOfTx( uint8_t *);
void StartTransfers(void);
void StartNextTransfers(void);
void CheckEndOfTx(void);
void CheckEndOfRx(void);
void WaitTilEndOfTxTransfer(void);
uint8_t Buffercmp8(uint8_t *, uint8_t *, uint8_t);
void ParseRxMsg(void);

/* extern function prototype --------------------------------------------------*/
extern void uartRxCallback(uint8_t *, uint8_t, uint8_t);  /* LOLPacketReceive.c */

/**
  ******************************************************************************
  * @func   CheckRxMsg()
  * @brief  check rx message
  * @param  None
  * @gvar   bRxMsgReady
  * @gfunc  uartRxCallback(), ParseRxMsg()
  * @retval None
  ******************************************************************************
  */
void CheckRxMsg()
{
    
    if (bRxMsgReady)
    {
        bRxMsgReady = FALSE;
#ifdef ENABLE_LOL
        /* !@#$ insert LOL code here */
        /* LOL:  rx msg 1st byte = pBufferReadyForUser + 1, rx msg cnt = iRxMsgCnt -1 */
        uartRxCallback( (uint8_t *)pBufferReadyForUser + 1, LOL_CHAN_UART, iRxMsgCnt);
//        uartRxCallback( (uint8_t *)pBufferReadyForRx + 1, LOL_CHAN_UART, iRxMsgCnt);
#else
        ParseRxMsg();
#endif
    }
}  /* CheckRxMsg() */


/**
  ******************************************************************************
  * @func   MX_USART1_UART_Init()
  * @brief  USART1 Initialization
  * @param  None
  * @gvar   UART_Vars_Init[], MX_DMA_Init(), MX_USART1_UART_Init()
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void UART_Init_All()
{
    UART_Vars_Init();             /* init UART variables */
    MX_DMA_Init();                /* init UART TX DMA */
    MX_USART1_UART_Init();        /* init UART port */
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx(cNGT_Title);
#endif
}  /* UART_Init_All */

/**
  ******************************************************************************
  * @func   MX_USART1_UART_Init()
  * @brief  USART1 Initialization
  * @param  None
  * @gvar   aTxBuffer[], 
  * @gfunc  LL_APB1_EnableClock(), LL_GPIO_Init(), LL_DMA_SetPeriphRequest(),
  *         LL_DMA_SetDataTransferDirection(), LL_DMA_SetChannelPriorityLevel(), LL_DMA_SetMode(),
  *         LL_DMA_SetPeriphIncMode(), LL_DMA_SetMemoryIncMode(), LL_DMA_SetPeriphSize(),
  *         LL_DMA_SetMemorySize(), NVIC_SetPriority(), NVIC_EnableIRQ(),
  *         LL_DMA_ConfigAddresses(), LL_DMA_SetDataLength(), LL_DMA_EnableIT_TC(),
  *         LL_DMA_EnableIT_TE(), LL_USART_Init(), LL_USART_EnableFIFO(), 
  *         LL_USART_ConfigAsyncMode(), LL_USART_Enable(), LL_USART_IsActiveFlag_TEACK(),
  *         LL_USART_IsActiveFlag_REACK(), LL_USART_ClearFlag_ORE(), LL_USART_EnableIT_RXNE(),
  *         LL_USART_EnableIT_ERROR()
  * @retval None
  ******************************************************************************
  */
void MX_USART1_UART_Init(void)
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* Peripheral clock enable */
    LL_APB1_EnableClock(LL_APB1_PERIPH_USART);
    
    /**USART1 GPIO Configuration  
    PA9  AF0   ------> USART1_TX    
    PA8  AF0   ------> USART1_RX   
    */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_8|LL_GPIO_PIN_9;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
    /* USART1 DMA Init */
    
    /* USART1_TX Init */
    LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_7, LL_DMAMUX_REQ_USART1_TX);
    LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
    LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_7, LL_DMA_PRIORITY_HIGH);
    LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_7, LL_DMA_MODE_NORMAL);
    LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_7, LL_DMA_PERIPH_NOINCREMENT);
    LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_7, LL_DMA_MEMORY_INCREMENT);
    LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_7, LL_DMA_PDATAALIGN_BYTE);
    LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_7, LL_DMA_MDATAALIGN_BYTE);

    /* USART1 interrupt Init */
    NVIC_SetPriority(USART1_IRQn, 0);
    NVIC_EnableIRQ(USART1_IRQn);
    
    /* Configure the DMA functional parameters for transmission */
    LL_DMA_ConfigAddresses(DMA1, 
                           LL_DMA_CHANNEL_7,
                           (uint32_t)aTxBuffer,
                           LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_TRANSMIT),
                           LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_7, ubNbDataToTx);

    /* Enable DMA transfer complete/error interrupts  */
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_7);
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_7);
    USART_InitStruct.PrescalerValue = LL_USART_PRESCALER_DIV1;
    USART_InitStruct.BaudRate = 115200;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    LL_USART_Init(USART1, &USART_InitStruct);
    LL_USART_SetTXFIFOThreshold(USART1, LL_USART_FIFOTHRESHOLD_1_8);
    LL_USART_SetRXFIFOThreshold(USART1, LL_USART_FIFOTHRESHOLD_1_8);
  //  LL_USART_DisableFIFO(USART1);       // disable FIFO
    LL_USART_EnableFIFO(USART1);          // enable FIFO
    LL_USART_ConfigAsyncMode(USART1);
     
    LL_USART_Enable(USART1);
    
    /* Polling USART1 initialisation */
    while ((!(LL_USART_IsActiveFlag_TEACK(USART1))) || (!(LL_USART_IsActiveFlag_REACK(USART1))))
    {
    }
  
    // support RX interrupt
    /* Clear Overrun flag, in case characters have already been sent to USART */
    LL_USART_ClearFlag_ORE(USART1);
    
    /* Enable RXNE and Error interrupts */
    LL_USART_EnableIT_RXNE(USART1);
    LL_USART_EnableIT_ERROR(USART1);
}       /* MX_USART1_UART_Init */

/**
  ******************************************************************************
  * @func   MX_DMA_Init()
  * @brief  init for DMA controller clock
  * @param  None
  * @gvar   None
  * @gfunc  LL_AHB_EnableClock(), NVIC_SetPriority(), NVIC_EnableIRQ()
  * @retval None
  ******************************************************************************
  */
void MX_DMA_Init(void) 
{
    /* Init with LL driver */
    /* DMA controller clock enable */
    LL_AHB_EnableClock(LL_AHB_PERIPH_DMA);
    
    /* DMA interrupt init */
    /* DMA_IRQn interrupt configuration */
    NVIC_SetPriority(DMA_IRQn, 0);
    NVIC_EnableIRQ(DMA_IRQn);  
}       /* MX_DMA_Init() */

/**
  ******************************************************************************
  * @func   UART_Vars_Init()
  * @brief  init uart related variables
  * @param  None
  * @gvar   pBufferReadyForRx, pBufferReadyForUser, uwNbRxChars, 
  *         bRxBufferReadyF, aRXBufferA[], aRXBufferB[]
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void UART_Vars_Init(void) 
{
    /* init Rx variables */
    pBufferReadyForRx =   aRXBufferA;
    pBufferReadyForUser = aRXBufferB;
    uwNbRxChars = 0;
    bRxBufferReadyF = FALSE;
}       /* UART_Vars_Init() */

/**
  ******************************************************************************
  * @func   UART_DeInit()
  * @brief  Deinitialize the UART interface
  * @param  None
  * @gvar   None
  * @gfunc  LL_USART_DisableIT_RXNE(), NVIC_DisableIRQ(), LL_USART_Disable(),
  *         LL_GPIO_SetPinMode(), LL_GPIO_SetPinSpeed(), LL_GPIO_SetPinOutputType(),
  *         LL_GPIO_SetPinPull(),
  * @retval None
  ******************************************************************************
  */
void UART_DeInit(void)
{
    /* Disable the UART interrupts */
    LL_USART_DisableIT_RXNE(USART1);    // BSP_UART
    NVIC_DisableIRQ(USART1_IRQn);       // BSP_UART_IRQn

    /* Disable the UART peripheral */
    LL_USART_Disable(USART1);           // BSP_UART

    /* Disable the UART clock */
//    BSP_UART_CLK_DISABLE();
    LL_APB1_DisableClock(LL_APB1_PERIPH_USART);

    /* Reset the UART pins */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_8, LL_GPIO_MODE_ANALOG);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_8, LL_GPIO_SPEED_FREQ_LOW);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_8, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_8, LL_GPIO_PULL_NO);
#if 0
    LL_GPIO_SetPinMode(BSP_UART_RX_GPIO_PORT, BSP_UART_RX_PIN, LL_GPIO_MODE_ANALOG);
    LL_GPIO_SetPinSpeed(BSP_UART_RX_GPIO_PORT, BSP_UART_RX_PIN, LL_GPIO_SPEED_FREQ_LOW);
    LL_GPIO_SetPinOutputType(BSP_UART_RX_GPIO_PORT, BSP_UART_RX_PIN, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(BSP_UART_RX_GPIO_PORT, BSP_UART_RX_PIN, LL_GPIO_PULL_NO);
#endif
    
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ANALOG);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_LOW);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_NO);
#if 0    
    LL_GPIO_SetPinMode(BSP_UART_TX_GPIO_PORT, BSP_UART_TX_PIN, LL_GPIO_MODE_ANALOG);
    LL_GPIO_SetPinSpeed(BSP_UART_TX_GPIO_PORT, BSP_UART_TX_PIN, LL_GPIO_SPEED_FREQ_LOW);
    LL_GPIO_SetPinOutputType(BSP_UART_TX_GPIO_PORT, BSP_UART_TX_PIN, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(BSP_UART_TX_GPIO_PORT, BSP_UART_TX_PIN, LL_GPIO_PULL_NO);
#endif
}  /* UART_DeInit */


/**
  ******************************************************************************
  * @func   StartTx_WaitTilEndOfTx()
  * @brief  copy message, transmit message via DMA, wait until message is sent, disable tx DMA
  * @param  char *str:  pointer to string
  * @gvar   ucDestStr[], iTxMsgLen, ubTxComplete
  * @gfunc  strcpy(), strlen(), StartNextTransfers(), LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void StartTx_WaitTilEndOfTx(char *str)
{
    strcpy( ucDestStr, str);              /* copy message */
    iTxMsgLen = (int)strlen(ucDestStr);   /* calculate str length */
    StartNextTransfers();                 /* Initiate next DMA transfers */
    while (!ubTxComplete);      /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;       /* reset tx complete flag */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}  /* StartTx_WaitTilEndOfTx () */

/**
  ******************************************************************************
  * @func   StartTx_WaitTilEndOfTx()
  * @brief  copy message, transmit message via DMA, wait until message is sent, disable tx DMA
  * @param  char *str:  pointer to string
  * @gvar   ucDestStr[], iTxMsgLen, ubTxComplete
  * @gfunc  strcpy(), strlen(), StartNextTransfers(), LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void cStartTx_WaitTilEndOfTx(const char *str)
{
    strcpy( ucDestStr, str);              /* copy message */
    iTxMsgLen = (int)strlen(ucDestStr);   /* calculate str length */
    StartNextTransfers();                 /* Initiate next DMA transfers */
    while (!ubTxComplete);      /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;       /* reset tx complete flag */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}  /* StartTx_WaitTilEndOfTx () */

/**
  ******************************************************************************
  * @func   uiStartTx_WaitTilEndOfTx()
  * @brief  copy message, transmit message via DMA, wait until message is sent, disable tx DMA
  * @param  char *str:  pointer to string
  * @gvar   ucDestStr[]
  * @gfunc  strcpy(), strlen(), StartNextTransfers(), LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void uiStartTx_WaitTilEndOfTx(uint8_t *str)
{
    strcpy( ucDestStr, (char *)str);      /* copy message */
    iTxMsgLen = (int)strlen(ucDestStr);   /* calculate str length */
    StartNextTransfers();                 /* Initiate next DMA transfers */
    while (!ubTxComplete);      /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;       /* reset tx complete flag */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}  /* uiStartTx_WaitTilEndOfTx () */

/**
  ******************************************************************************
  * @func   CrEFUint8_StartTx_WaitTilEndOfTx_N()
  * @brief  copy message, transmit message via DMA, wait until message is sent, disable tx DMA
  * @param  char *str:  pointer to string
  * @gvar   iTxMsgLen, ucDestStr[], ubTxComplete
  * @gfunc  strcpy(), strlen(), StartNextTransfers(), LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void CrEFUint8_StartTx_WaitTilEndOfTx_N( uint8_t *str, int16_t icnt)
{
    sprintf( ucDestStr, "%s %d\r\n", (char *)str, icnt);
    iTxMsgLen = (int)strlen(ucDestStr);   /* calculate str length */
    StartNextTransfers();                 /* Initiate next DMA transfers */
    while (!ubTxComplete);      /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;       /* reset tx complete flag */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}  /* CrEFUint8_StartTx_WaitTilEndOfTx_N() */

/**
  ******************************************************************************
  * @func   CrEFUint8_StartTx_WaitTilEndOfTx()
  * @brief  copy message, transmit message via DMA, wait until message is sent, disable tx DMA
  * @param  char *str:  pointer to string
  * @gvar   ucDestStr[], iTxMsgLen, ubTxComplete
  * @gfunc  strcpy(), strlen(), StartNextTransfers(), LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void CrEFUint8_StartTx_WaitTilEndOfTx(uint8_t *str)
{
    strcpy( ucDestStr, (char *)str);              /* copy message */
    strcat( ucDestStr, "\r\n");           /* append cr & line feed */
    iTxMsgLen = (int)strlen(ucDestStr);   /* calculate str length */
    StartNextTransfers();                 /* Initiate next DMA transfers */
    while (!ubTxComplete);      /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;       /* reset tx complete flag */
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}  /* CrEFUint8_StartTx_WaitTilEndOfTx () */

/**
  ******************************************************************************
  * @func   StartTransfers()
  * @brief  initiates TX and RX DMA transfers by enabling DMA channels
  * @param  None
  * @gvar   None
  * @gfunc  LL_USART_EnableDMAReq_TX(), LL_DMA_EnableChannel()
  * @retval None
  ******************************************************************************
  */
void StartTransfers(void)
{
//      LL_USART_EnableDMAReq_RX(USART1);  /* Enable DMA RX Interrupt */
    LL_USART_EnableDMAReq_TX(USART1);  /* Enable DMA TX Interrupt */
//      LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_6);  /* Enable DMA Channel Rx */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Enable DMA Channel Tx */
}       /* StartTransfers() */

/**
  ******************************************************************************
  * @func   pStartNextTransfers()
  * @brief  initiates TX DMA transfers by enabling DMA channels
  * @param  uiPtr = msg ptr (to be tx)
  * @param  iLen = lenght to be tx
  * @gvar   None
  * @gfunc  LL_DMA_ConfigAddresses(), LL_DMA_SetDataLength(), LL_USART_EnableDMAReq_TX(),
  *         LL_DMA_EnableChannel()
  * @retval None
  ******************************************************************************
  */
void pStartNextTransfers( uint8_t *uiPtr, int16_t iLen)
{
    /* Configure the DMA functional parameters for transmission */
    LL_DMA_ConfigAddresses(DMA1, 
                           LL_DMA_CHANNEL_7,
                           (uint32_t)uiPtr,
                           LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_TRANSMIT),
                           LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_7, iLen);
    LL_USART_EnableDMAReq_TX(USART1);  /* Enable DMA TX Interrupt */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Enable DMA Channel Tx */
}       /* pStartNextTransfers() */

/**
  ******************************************************************************
  * @func   StartNextTransfers()
  * @brief  initiates TX DMA transfers by enabling DMA channels
  * @param  None
  * @gvar   ucDestStr(), iTxMsgLen
  * @gfunc  LL_DMA_ConfigAddresses(), LL_DMA_SetDataLength(), LL_USART_EnableDMAReq_TX(),
  *         LL_DMA_EnableChannel()
  * @retval None
  ******************************************************************************
  */
void StartNextTransfers(void)
{
    /* Configure the DMA functional parameters for transmission */
    LL_DMA_ConfigAddresses(DMA1, 
                           LL_DMA_CHANNEL_7,
                           (uint32_t)ucDestStr,
                           LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_TRANSMIT),
                           LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7));
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_7, iTxMsgLen);
    LL_USART_EnableDMAReq_TX(USART1);  /* Enable DMA TX Interrupt */
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Enable DMA Channel Tx */
}       /* StartNextTransfers() */

/**
  ******************************************************************************
  * @func   CheckEndOfTx()
  * @brief  Check for end of transmit.  If ned, disable TX DMA
  * @param  None
  * @gvar   ubTxComplete
  * @gfunc  LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void CheckEndOfTx(void)
{
    if (ubTxComplete)
    {
        ubTxComplete = FALSE;
        LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
    }
}       /* CheckEndOfTx() */

/**
  ******************************************************************************
  * @func   CheckEndOfRx()
  * @brief  Check for end of receive.  If received entire message, swap rx buffer.
  *         If in process of receiving message, check for timeout.  
  *         If time out not expired, exit.
  *         If time out expired, terminate receive, set to rx idle, swap rx buffer.
  * @param  None
  * @gvar   bRxBufferReadyF, pBufferReadyForUser, pBufferReadyForRx,
  *         uwNbRxChars, bRxMsgReady, uiRxState, iRxTimeout
  * @gfunc  LL_DMA_ConfigAddresses(), LL_DMA_SetDataLength(), LL_USART_EnableDMAReq_TX(),
  *         LL_DMA_EnableChannel()
  * @retval None
  ******************************************************************************
  */
void CheckEndOfRx(void)
{
    uint8_t *ptemp;

#if 1
    /* if rx msg is complete, ready for processing */
    if (bRxBufferReadyF)
    {
#if 1
        /* Swap buffers for next bytes to be received */
        ptemp = pBufferReadyForUser;
        pBufferReadyForUser = pBufferReadyForRx;
        pBufferReadyForRx = ptemp;
#endif
        
        bRxBufferReadyF = FALSE;
        uwNbRxChars = 0;      // reset rx char count
        bRxMsgReady = TRUE;
        uiRxState = RX_ST_IDLE;
    }
    else
    {
        /* if rx msg awaits complete (only in RX_ST_DATA), check for rx timeout */
        switch (uiRxState)
        {
            case RX_ST_DATA:
                /* incomplete msg, if reached timeout, terminate rx msg, process msg */
                if (iRxTimeout <= 0)
                {
                    /* Swap buffers for next bytes to be received */
                    ptemp = pBufferReadyForUser;
                    pBufferReadyForUser = pBufferReadyForRx;
                    pBufferReadyForRx = ptemp;
                    uwNbRxChars = 0;      // reset rx char count
                    bRxMsgReady = TRUE;
                    uiRxState = RX_ST_IDLE;
                }  /* if */
            break;  /* RX_ST_DATA */
        }  /* switch */
    }
#endif
    
#if 0
//    if (TRUE == bRxBufferReadyF)
    if (bRxBufferReadyF)
    {
        bRxBufferReadyF = FALSE;
        strncpy( ucDestStr, (char *)pBufferReadyForRx, uwNbRxChars);
        cptr = (uint8_t *)&ucDestStr[uwNbRxChars];
        *cptr++ = (uint8_t)'\r';
        *cptr++ = (uint8_t)'\n';
        *cptr = (uint8_t)'\0';
        iTxMsgLen = uwNbRxChars+2;
        LL_DMA_ConfigAddresses(DMA1, 
                               LL_DMA_CHANNEL_7,
                               (uint32_t)ucDestStr,
                               LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_TRANSMIT),
                               LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7));
        LL_DMA_SetDataLength( DMA1, LL_DMA_CHANNEL_7, iTxMsgLen);
        LL_USART_EnableDMAReq_TX(USART1);     /* Enable DMA TX Interrupt */
        LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_7); /* Enable DMA Channel Tx */
        uwNbRxChars = 0;      // reset rx char count
        
        bRxMsgReady = TRUE;
    }
#endif
#if 0
#define CH_RXTERM    0x0d
    if (uwNbRxChars != 0)
    {
        if (pBufferReadyForRx[uwNbRxChars-1] == CH_RXTERM)
        {
            strncpy( ucDestStr, pBufferReadyForRx, uwNbRxChars-1);
            cptr = &ucDestStr[uwNbRxChars-1];
            *cptr++ = (uint8_t)'\r';
            *cptr++ = (uint8_t)'\n';
            *cptr = (uint8_t)'\0';
            iTxMsgLen = uwNbRxChars+1;
            LL_DMA_ConfigAddresses(DMA1, 
                                   LL_DMA_CHANNEL_7,
                                   (uint32_t)ucDestStr,
                                   LL_USART_DMA_GetRegAddr(USART1, LL_USART_DMA_REG_DATA_TRANSMIT),
                                   LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_7));
            LL_DMA_SetDataLength( DMA1, LL_DMA_CHANNEL_7, iTxMsgLen);
            LL_USART_EnableDMAReq_TX(USART1);     /* Enable DMA TX Interrupt */
            LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_7); /* Enable DMA Channel Tx */
            uwNbRxChars = 0;      // reset rx char count
        }
    }
#endif
}


/**
  ******************************************************************************
  * @func   WaitTilEndOfTxTransfer()
  * @brief  Wait until end of tx dma, check transmit complete flag
  * @param  None
  * @gvar   ubTxComplete
  * @gfunc  LL_DMA_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void WaitTilEndOfTxTransfer(void)
{
    while (!ubTxComplete);  /* 1 - Wait end of transmission */
    ubTxComplete = FALSE;
    LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
}       /* WaitTilEndOfTxTransfer() */


/**
  ******************************************************************************
  * @func   Buffercmp8()
  * @brief  Check for end of receive.  If need, disable TX DMA
  * @param  pBuffer1: pointer to the source buffer to be compared to.
  * @param  pBuffer2: pointer to the second source buffer to be compared to the first.
  * @param  BufferLength: buffer's length.
  * @gvar   
  * @gfunc  
  * @retval 0: Comparison is OK (the two Buffers are identical)
  *         Value different from 0: Comparison is NOK (Buffers are different)
  ******************************************************************************
  */
uint8_t Buffercmp8(uint8_t *pBuffer1, uint8_t *pBuffer2, uint8_t BufferLength)
{
    while (BufferLength--)
    {
        if (*pBuffer1 != *pBuffer2)
        {
            return(1);
        }
        pBuffer1++;
        pBuffer2++;
    }
   
    return(0);
}       /* Buffercmp8() */

