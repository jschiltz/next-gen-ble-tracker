/**
  ******************************************************************************
  * @file    NG_Tick_ae2.c
  * @author  Thomas W Liu
  * @brief   Source file of audio engine.
  ******************************************************************************
  * @attention
  * @note    This module contains test code for the audio engine.  It is the
  *          predecessor to the audio engine state machine.  This module is not
  *          in the project, so no code is included in the hex file.  This is
  *          to use as a reference.
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "bluenrg_lp_ll_tim.h"
#include "NG_Tick_main.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_pwm.h"


/* variable declaration ------------------------------------------------------*/
int32_t iAudioEngineSysCounter;         /* audio engine system counter, 1ms, dec in system timer */

STRUCT_AUDIOENGINE_SM sAudioEngine;   /* audio machine state machine */

//#define EN_RD_AUDIO_MSG
//#ifdef  EN_RD_AUDIO_AE_MSG      /* audio engine message */
#ifdef  EN_RD_AUDIO_MSG

char    ucAudioStr[80] = { 0 };   /* message to be transmitted */

const char uccAudioStr_SongRepeat[] = "Song Repeat: ";
const char uccAudioStr_ProgressRecCount[] = "Progression Records: ";
const char uccAudioStr_ProgressRepeat[] = "Progression Repeat:";
const char uccAudioStr_ChordRecCount[] = "Chord Records: ";
const char uccAudioStr_ChordRepeat[] = "Chord Repeat: ";
const char uccAudioStr_Freq[] = "Note: ";
const char uccAudioStr_Duration[] = "Duration: ";
#endif


/* function prototype --------------------------------------------------------*/
void ReadPrintAllAudioTable(void);
static void ReadAudioTable( STRUCT_PROGRESS_CB_TYPE *);


/**
  ******************************************************************************
  * @func   ReadPrintAllAudioTable()
  * @brief  Initialize tone related variables
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void ReadPrintAllAudioTable(void)
{
#ifdef  EN_RD_AUDIO_MSG
    sprintf( ucAudioStr, "\r\n<Song>---Pairing------\r\n");
    StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
    ReadAudioTable((STRUCT_PROGRESS_CB_TYPE *)BuzzerSong_Pair);

#ifdef  EN_RD_AUDIO_MSG
    sprintf( ucAudioStr, "\r\n<Song>---Beacon-------\r\n");
    StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
    ReadAudioTable((STRUCT_PROGRESS_CB_TYPE *)BuzzerSong_Beacon);

#ifdef  EN_RD_AUDIO_MSG
    sprintf( ucAudioStr, "\r\n<Song>---Beacon Alt---\r\n");
    StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
    ReadAudioTable((STRUCT_PROGRESS_CB_TYPE *)BuzzerSong_BeaconAlt);

#ifdef  EN_RD_AUDIO_MSG
    sprintf( ucAudioStr, "\r\n<Song>---Tick Tock----\r\n");
    StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
    ReadAudioTable((STRUCT_PROGRESS_CB_TYPE *)BuzzerSong_TickTock);
}  


/**
  ******************************************************************************
  * @func   ReadAudioTable()
  * @brief  Read sound related variables
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
static void ReadAudioTable( STRUCT_PROGRESS_CB_TYPE *SongPtr)
{
    STRUCT_PROGRESS_CB_TYPE *ssProgressPtr;
    STRUCT_CHORD_CB_TYPE *ssChordCBPtr;
    uint32_t uiChordRepeat;
    uint16_t uiNoteFreqCnt;
    uint32_t uiNoteFreqDuration;
    int16_t iNoteRecCnt = 0;
    int16_t iChordRecCnt = 0;
    
      /* beacon control block, get # of tone records */
    iChordRecCnt = 0;                   /* init chord record count */
    ssProgressPtr = SongPtr;            /* ptr to song beacon */
    uiChordRepeat = ssProgressPtr->iChordRepeat;    /* get chord repeat */
    /* while not at end of song (chord repeat must be 0), read tables */
    while (uiChordRepeat != 0)   /* is repeat chord == 0?, end of song? */
    {
        ssProgressPtr++; /* ptr = inc to next chord */
        iChordRecCnt++;  /* inc chord record count */
        uiChordRepeat = ssProgressPtr->iChordRepeat;  /* get chord repeat count */
    }
#ifdef EN_RD_AUDIO_MSG
    sprintf( ucAudioStr, "%s%u\r\n", uccAudioStr_ProgressRecCount, iChordRecCnt);
    StartTx_WaitTilEndOfTx(ucAudioStr);
#endif

    ssProgressPtr = SongPtr;                    /* init ptr to song beacon */
    uiChordRepeat = ssProgressPtr->iChordRepeat;/* get chord repeat */
    /* while not at end of song table (chord repeat must be 0), read tables */
    while (uiChordRepeat != 0)   /* is repeat chord == 0?, end of song? */
    {
#ifdef EN_RD_AUDIO_MSG
        sprintf( ucAudioStr, "%s%u\r\n", uccAudioStr_ChordRepeat, uiChordRepeat);
        StartTx_WaitTilEndOfTx(ucAudioStr);
#endif

        /* ---- step tru each note in this chord record ---------------------- */
        iNoteRecCnt = 0;                        /* init note record count */
        ssChordCBPtr = ssProgressPtr->sChordCBPtr;  /* init for first tone record */
        uiNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;      /* get note frequency count from table */
        uiNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;    /* get note duration count from table */
        /* while not at end of table (both note & duraction must be 0), read tables */
//        while ((uiNoteFreqCnt != NOTE_NO_NOTE) || (uiNoteFreqDuration != NOTE_DURATION_TERM_CNT))
        while ((uiNoteFreqCnt != NOTE_TERM) || (uiNoteFreqDuration != NOTE_DURATION_TERM_CNT))
        {
#ifdef EN_RD_AUDIO_MSG
            sprintf( ucAudioStr, "%s%4u, %s%3u\r\n", uccAudioStr_Freq, uiNoteFreqCnt, uccAudioStr_Duration, uiNoteFreqDuration);
            StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
            ssChordCBPtr++; /* inc ptr for next tone control block */
            iNoteRecCnt++;  /* inc tone record count */
            uiNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;              /* get note frequency count from table */
            uiNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;    /* get note duration count from table */
        }
#ifdef EN_RD_AUDIO_MSG
        sprintf( ucAudioStr, "%s%u\r\n", uccAudioStr_ChordRecCount, iNoteRecCnt);
        StartTx_WaitTilEndOfTx(ucAudioStr);
#endif

        /* ---- step tru next tone record in this song ---------------------- */
        ssProgressPtr++;    /* inc, go to next chord record */
        uiChordRepeat = ssProgressPtr->iChordRepeat;  /* get chord repeat count */
    }
#ifdef EN_RD_AUDIO_MSG
    sprintf( ucAudioStr, "\r\n");
    StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
}  /* ReadAudioTable() */

