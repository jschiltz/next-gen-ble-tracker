/**
  ******************************************************************************
  * @file    NG_Tick_ADC.c
  * @author  Johnny Lienau
  * @brief   Initialize ADC, read coin cell, send value to SysInfo.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_adc.h"
#include "bluenrg_lp_ll_bus.h"
#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_ADC.h"
#include "NG_Tick_ble.h"

/* variables declaration -----------------------------------------------------*/
NGT_ADCCB_TYPE  AdcCB;
uint32_t        ADC_Raw_Value;
uint32_t        ADC_Converted_Value;

/* Function prototypes -----------------------------------------------*/
void CheckADC(void);
void ADC_Init_All(void);
void MX_ADC_Init(void);
void ADC_DeInit(void);

/* Private user code ---------------------------------------------------------*/


/**
  ******************************************************************************
  * @func   CheckADC()
  * @brief  check adc state: idle, convert, process
  * @param  none
  * @gvar   AdcCB
  * @gfunc  LL_ADC_StartConversion(), LL_ADC_IsActiveFlag_EODS(),
  *         LL_ADC_IsActiveFlag_OVRDS(), LL_ADC_ClearFlag_OVRDS(),
  *         LL_ADC_DSGetOutputData(), LL_ADC_GetADCConvertedValueBatt(),
  *         LL_ADC_ClearFlag_EODS()
  * @retval none
  ******************************************************************************
  */
void CheckADC()
{
    uint8_t uTemp;
    
    switch (AdcCB.uState)
    {
        case ADCCB_ST_IDLE:  /* 0 */
            if (ADCCB_ACT_CONVERT == AdcCB.uAction)
            {
                LL_ADC_StartConversion(ADC);     /* Start ADC conversion */
                AdcCB.uAction = ADCCB_ACT_IDLE; 
                AdcCB.uState = ADCCB_ST_CONVERT; /* Set state to convert/busy*/
            }
        break;  /* ADCCB_ST_IDLE */
  
        case ADCCB_ST_CONVERT:  /* 1 */
            /*check to see if ADC conversion is finished*/     
            if (CONVERSION_COMPLETE == LL_ADC_IsActiveFlag_EODS(ADC)) 
            {
                AdcCB.uState = ADCCB_ST_PROCESSING; /* Set state to processing*/
            }  /* if */
        break;  /* ADCCB_ST_CONVERT */
  
        case ADCCB_ST_PROCESSING:  /* 2 */
            /* Check the ADC flag for overrun of Down Sampler */
            if (OVERRUN_ERROR == LL_ADC_IsActiveFlag_OVRDS(ADC)) 
            {
                /* Clear the ADC flag overrun of Down Sampler */
                LL_ADC_ClearFlag_OVRDS(ADC);
                *AdcCB.pBattVoltage = ADCCB_ERR_OVERRUN;  /* 7E is ADC overrun error flag */
            } /* if */
            else
            {
                /* Get the battery raw value from the Down Sampler */
                ADC_Raw_Value = LL_ADC_DSGetOutputData(ADC);

                /* Convert to battery voltage based on 3.6V reference*/
                ADC_Converted_Value = LL_ADC_GetADCConvertedValueBatt(ADC, ADC_Raw_Value, USER_DATAWIDTH);

                *AdcCB.pBattVoltage = BATTERY_UPPER_V;  /* defult: ADC_Converted_Value > 3000 */
                if (ADC_Converted_Value < ADC_LOWER_CNT)  // 1800
                {
                    *AdcCB.pBattVoltage = BATTERY_LOWER_V;  // 0
                }
                else
                {
                   /*Check if Battery voltage is between 1.800V and 3.000V */     
                   if (ADC_Converted_Value <= ADC_UPPER_CNT)  // 3000
                    {
                        /* Convert to decimal value between 0 and 120 */
                        *AdcCB.pBattVoltage = (ADC_Converted_Value - 1800) / 10;
                    }  /* if */
                }  /* if else */
            }  /* if else */
            LL_ADC_ClearFlag_EODS(ADC);         /* Clear ADC flag End Of Down Sampler conversion */
            AdcCB.uState = ADCCB_ST_UPDATE;     /* update data in BLE fileds */
        break;  /* ADCCB_ST_PROCESSING */
    
        case ADCCB_ST_UPDATE:  /* 3 */  /* update battery voltage in BLE advertisment field*/
            /* if ship mode status is default, then update adc value to born on coin cell voltage */
            if (PRODUCTION_MODE == SystemIB.SysFlashIB.uShipModeStatus)
            {
                SystemIB.SysFlashIB.uBornOnCoinCellV = *AdcCB.pBattVoltage;
            }  /* if */
            uTemp = SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_CCV];
            uTemp &= 0x80;
            SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_CCV] = uTemp | *AdcCB.pBattVoltage;
            
            //Update advertising field in BLE data array
//            adv_data[17] = uTemp;
            add_coin_voltage_to_advertising_data(*AdcCB.pBattVoltage);
            AdcCB.uState = ADCCB_ST_IDLE;       /* Restart State Machine conversion */
        break;  /* ADCCB_ST_UPDATE */
        
        default:
            *AdcCB.pBattVoltage = ADCCB_ERR_SM; /* 0x7D is State Machine Error Flag */
            AdcCB.uState = ADCCB_ST_IDLE;
        break;
    }  /* switch */
}

/**
  ******************************************************************************
  * @func   ADC_Init_All()
  * @brief  Initialize ADC io
  * @param  none
  * @gvar   none
  * @gfunc  MX_ADC_Init()
  * @retval none
  ******************************************************************************
  */
void ADC_Init_All(void)
{
    MX_ADC_Init();      /* init adc io */
}  /* ADC_Init_All() */

/**
  ******************************************************************************
  * @func   MX_ADC_Init()
  * @brief  Initialize ADC io & hardware related
  * @param  none
  * @gvar   none
  * @gfunc  LL_APB1_EnableClock(), LL_ADC_Enable(), LL_ADC_SetSampleRate(),
  *         LL_ADC_ConfigureDSDataOutput(), LL_ADC_SetADCMode(), LL_ADC_SetChannelSeq0(),
  *         LL_ADC_SetSequenceLength(), LL_ADC_SetCalibPoint1Gain(),
  *         LL_ADC_SetVoltageRangeSingleVinp0()
  * @retval none
  ******************************************************************************
  */
void MX_ADC_Init(void)
{
    /* Peripheral clock enable */
    LL_APB1_EnableClock(LL_APB1_PERIPH_ADCDIG | LL_APB1_PERIPH_ADCANA);

    /* Enable the ADC */
    LL_ADC_Enable(ADC);

    /* Configure the sample rate */
    LL_ADC_SetSampleRate(ADC, USER_SAMPLERATE);

    /* Configure the Down Sampler data width and ratio */
    LL_ADC_ConfigureDSDataOutput(ADC, USER_DATAWIDTH, USER_RATIO);

    /* Configure the operation mode as ADC mode (static/low frequency signal) */
    LL_ADC_SetADCMode(ADC, LL_ADC_OP_MODE_ADC);

    /* Set the first (and only in this example) input of the conversion sequence as battery level detector */
    LL_ADC_SetChannelSeq0(ADC, LL_ADC_CH_BATTERY_LEVEL_DETECTOR);

    /* Set the length of the conversion sequence as 1 */
    LL_ADC_SetSequenceLength(ADC, LL_ADC_SEQ_LEN_01);
    
    /* Set the GAIN */
    LL_ADC_SetCalibPoint1Gain(ADC, LL_ADC_DEFAULT_RANGE_VALUE_3V6);
    LL_ADC_SetVoltageRangeSingleVinp0(ADC, LL_ADC_VIN_RANGE_3V6);
}  /* MX_ADC_Init() */

/**
  ******************************************************************************
  * @func   ADC_DeInit()
  * @brief  de-init ADC io & hardware related
  * @param  none
  * @gvar   none
  * @gfunc  LL_APB1_DisableClock(), LL_ADC_Disable()
  * @retval none
  ******************************************************************************
  */
void ADC_DeInit()
{
    /* Force reset of ADC clock (core clock) */
    LL_APB1_DisableClock(LL_APB1_PERIPH_ADCDIG | LL_APB1_PERIPH_ADCANA);
    LL_ADC_Disable(ADC);        /* disable the ADC */
}  /* ADC_DeInit */
