/**
  ******************************************************************************
  * @file    NG_Tick_app.c
  * @author  Thomas W Liu
  * @brief   Source file of application
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp_stack.h"
#include "bluenrg_lp_ll_dma.h"
#include "bluenrg_lp_ll_adc.h"
#include "bluenrg_lp_hal_vtimer.h"
#include "NG_Tick_main.h"
#include "bluenrg_lp.h"
#include "NG_Tick_IWDG_Reset.h"
#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"

#include "ble_status.h"
#include "NG_Tick_BLE.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_adc.h"
#include "NG_Tick_app.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_ble.h"

#include "bluenrg_lp_hal_power_manager.h"
#include "NG_Tick_power.h"
#include "NG_Tick_i2c.h"
#include "app_state.h"

#include "bluenrg_lp_hal_vtimer.h"

/* defines -------------------------------------------------------------------*/

/* constant declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
NGT_APPCTRL_TYPE        sAppCB;         /* application control block */
NGT_APPTMCTRL_TYPE      sAppTimeCB;     /* application time control block */
uint16_t uiApp_SecCnt = 0;      /* 1 second counter, based on 1ms */
//uint16_t uiApp_2SecCnt = 0;     /* 2 second counter, based on 2s */

uint8_t bNoEvents = FALSE;      /* events running t/f */
uint8_t bSys_2SecF = FALSE;     /* 2 second counter flag, t/f */

/* debug variables */
//uint8_t uAppCnt = 0;
//uint8_t uAppCnt1 = 0;
//uint8_t uAppCnt2 = 0;
//uint8_t uAppCnt3 = 0;
//uint8_t uAppCnt4 = 0;


/* function prototype --------------------------------------------------------*/
void CheckLastMode(void);
void AppTime_Init_All(void);
void TrackTime(void);
void NormalTrackTime(void);
void xTask_2Sec(void);
void xTask_60Sec(void);  /* $%^& */
void xTask_60Min(void);
void CheckApp(void);
void xApp_Idle();
void xApp_2ShipMode(void);
void xApp_2ShipMode_PowerDn(void);
void xApp_ShipMode(void);
void xApp_Activate(void);
void xApp_Normal(void);
void App_Init_All(void);
void AppCB_init(void);
void AppIB_init(void);

void xDebugAdv(void);


/* external variable prototype -----------------------------------------------*/
extern uint8_t advertising_mode;

/* external function prototype -----------------------------------------------*/
extern void CheckPOR(void);         /* from main.c */
extern void CheckAudioEngine(void); /* check audio engine */
extern void CheckEndOfRx(void);     /* check for end of rx */
extern void CheckEndOfTx(void);     /* wait til end of tx */
extern void CheckRxMsg(void);       /* check receive msg */
extern void CheckLoL(void);         /* check LoL */
extern void CheckFlashMem(void);    /* check for flash memory operation */
extern void CheckNFC(void);         /* check NFC tasks & interrupt */
extern void BLE_AppTick(void);      /* BLE app tick */

/**
  ******************************************************************************
  * @func   CheckLastMode()
  * @brief  check previous ship mode status.
  *         if in normal mode, then restore related variablesg
  *         if in ship mode, then restore related variables & go back to power saving
  *         variable & event, and resume ship mode
  * @param  None
  * @gvar   SystemCB, SystemIB, adv_data, sAppCB
  * @gfunc  CheckFlashMem(), SearchNFC(), ReadNFCMemToDynData(), NGT_Power_Save(),
  *         AppTime_Init_All()
  * @retval None
  ******************************************************************************
  */
void CheckLastMode()
{
    /*--------------------------------------------------------------------------
    *  if required, wait until SystemIB.SysFlashIB is retrieved from flash memory.
    *-------------------------------------------------------------------------*/
    while (!((FLASHCB_ST_IDLE == SystemCB.FlashCB.uState) && (FLASHCB_ACT_IDLE == SystemCB.FlashCB.uAction)))
    {
        CheckFlashMem();
    }
  
    /*--------------------------------------------------------------------------
    *  check for last mode
    *-------------------------------------------------------------------------*/
    switch (SystemIB.SysFlashIB.uShipModeStatus)
    {
        /*----------------------------------------------------------------------
        *  last mode was ship:
        *      restore MPBID in advertisement.  check for NFC IC.
        *      Read back dynamic data.  go to power saving mode
        *---------------------------------------------------------------------*/
        case SHIP_MODE:
            /* #$%^ quick fix to restore MPBID back to BLE advertising data arrary */
            add_MPBID_to_advertising_data(SystemIB.SysFlashIB.aMPBID);            
            /* check for i2c token */
            if (SystemIB.SysRAMIB.uI2CToken != I2C_TOKEN_NFC)
            {
                SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_NFC;
                SearchNFC();
            }  /* if */
            
            if (FALSE == ReadNFCMemToDynData())
            {
                SystemCB.NFCCB.uErrCode = NFCCB_ERR_SHIP_READ;
                SystemCB.NFCCB.uwErrCnt++;
            }

            sAppCB.uState = APPCB_ST_SHIPMODE;  /* restore back to last mode */
            AppTime_Init_All();
            NGT_Power_Save();  /* power save mode */
            break;  /* SHIP_MODE */
            
        /*----------------------------------------------------------------------
        *  last mode was normal:
        *      restore MPBID in advertisement.  check for NFC IC.
        *      Read back dynamic data.  get coin cell voltage.
        *      increment reset counter.  Write all dynamic data to NFC memory
        *---------------------------------------------------------------------*/
        case NORMAL_MODE:
            /* #$%^ quick fix to restore MPBID back to BLE advertising data arrary */
            add_MPBID_to_advertising_data(SystemIB.SysFlashIB.aMPBID);            
            
            /* check for i2c token */
            if (SystemIB.SysRAMIB.uI2CToken != I2C_TOKEN_NFC)
            {
                SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_NFC;
                SearchNFC();
            }  /* if */
          
            if (FALSE == ReadNFCMemToDynData())
            {
                SystemCB.NFCCB.uErrCode = NFCCB_ERR_NORM_READ;
                SystemCB.NFCCB.uwErrCnt++;
            }

            /*------------------------------------------------------------------
            *  set convert flag to read coin cell battery voltage
            *-----------------------------------------------------------------*/
            AdcCB.uAction = ADCCB_ACT_CONVERT;
            while ((ADCCB_ST_IDLE != AdcCB.uState) || (ADCCB_ACT_IDLE != AdcCB.uAction))
            {
                CheckADC();
            }  /* while */
            
            /*------------------------------------------------------------------
            *  increment reset counter.  write all dynamic data to NFC memory
            *-----------------------------------------------------------------*/
            SystemIB.SysRAMIB.uNFCResetEvents++;
            SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN;    /* write dynamic to NFC memory */
            while ((NFCCB_ST_IDLE != SystemCB.NFCCB.uState) || (NFCCB_ACT_IDLE != SystemCB.NFCCB.uAction))
            {
                CheckNFC();
            }  /* while */
            sAppCB.uState = APPCB_ST_NORMAL;  /* restore back to last mode */
            AppTime_Init_All();

            break;  /* NORMAL_MODE */
    }  /* switch */
}  /* CheckLastMode() */

/**
  ******************************************************************************
  * @func   AppTime_Init_All()
  * @brief  application time init 1 secound counter and various flag.
  * @param  None
  * @gvar   sAppTimeCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void AppTime_Init_All()
{
    sAppTimeCB.uiApp_SecCnt = 0;
    sAppTimeCB.uiApp_2SecCnt = 0;
    sAppTimeCB.bApp_2SecF = FALSE;
sAppTimeCB.bApp_60SecF = FALSE;
    sAppTimeCB.bApp_60MinF = FALSE;
}  /* AppTime_Init_All() */

/**
  ******************************************************************************
  * @func   TrackTime()
  * @brief  track app time, check 2/10 seconds and 10 minutes counter.
  *         reset 1sec count after 10 minutes is reached
  * @param  None
  * @gvar   iSys_1SecF, uiApp_SecCnt, bApp_2SecF, bApp_10SecF, bApp_10MinF
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void TrackTime()
{
    if (iSys_1SecF)
    {
        iSys_1SecF = FALSE;
        sAppTimeCB.uiApp_SecCnt++;
        
        if (0 == (sAppTimeCB.uiApp_SecCnt % APP2SEC_CNT))
        {
            sAppTimeCB.bApp_2SecF = TRUE;
        }  /* if 2 sec */

/* $%^& */
if (0 == (sAppTimeCB.uiApp_SecCnt % APP60SEC_CNT))
{
    sAppTimeCB.bApp_60SecF = TRUE;
}  /* if 60 sec */
        
        if (0 == (sAppTimeCB.uiApp_SecCnt % APP60MIN_CNT))
        {
            sAppTimeCB.bApp_60MinF = TRUE;
            sAppTimeCB.uiApp_SecCnt = 0;
        }  /* if 10 min*/
    }
}  /* TrackTime */


/**
  ******************************************************************************
  * @func   NormalTrackTime()
  * @brief  track app time during normal mode, check 2 seconds and610 minutes counter.
  *         reset 2sec count after 60 minutes is reached
  * @param  None
  * @gvar   iSys_2SecF, sAppTimeCB
  * @gfunc  None
  * @retval None
  * @note   Called from aci_hal_end_of_radio_activity_event(), every 2 seconds
  ******************************************************************************
  */
void NormalTrackTime()
{
    if (bSys_2SecF)
    {
        bSys_2SecF = FALSE;
        sAppTimeCB.uiApp_2SecCnt++;
        
        sAppTimeCB.bApp_2SecF = TRUE;
        
/* $%^& */
if (0 == (sAppTimeCB.uiApp_2SecCnt % NORM_APP60SEC_CNT))
{
    sAppTimeCB.bApp_60SecF = TRUE;
}  /* if 60 sec */
        
        if (0 == (sAppTimeCB.uiApp_2SecCnt % NORM_APP60MIN_CNT))
        {
            sAppTimeCB.bApp_60MinF = TRUE;
            sAppTimeCB.uiApp_2SecCnt = 0;
        }  /* if 10 min*/
    }
}  /* NormalTrackTime */

/**
  ******************************************************************************
  * @func   xTask_2Sec()
  * @brief  excute 2 second tasks.  do nothing
  * @param  None
  * @gvar   sAppTimeCB, sAppCB
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void xTask_2Sec()
{
    if (sAppTimeCB.bApp_2SecF)
    {
        sAppTimeCB.bApp_2SecF = FALSE;

        switch (sAppCB.uState)
        {
            case APPCB_ST_IDLE:
            case APPCB_ST_NORMAL:
//                SystemCB.AccCB.uAction = ACCCB_ACT_CHK_INT1;
            break;  /* APPCB_ST_IDLE, APPCB_ST_NORMAL */
        }  /* switch */
    }  /* if */
}  /* xTask_2Sec */


/**
  ******************************************************************************
  * @func   xTask_60Sec()
  * @brief  excute 60 second tasks: NFC dynamic write, set flag to begin ADC convert
  * @param  None
  * @gvar   sAppTimeCB, sAppCB, AdcCB, SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xTask_60Sec()
{
    if (sAppTimeCB.bApp_60SecF)
    {
        sAppTimeCB.bApp_60SecF = FALSE;
        switch (sAppCB.uState)
        {
            case APPCB_ST_IDLE:
                AdcCB.uAction = ADCCB_ACT_CONVERT;      /* start convert $%^& */
            break;  /* APPCB_ST_IDLE */
          
            case APPCB_ST_NORMAL:
                AdcCB.uAction = ADCCB_ACT_CONVERT;      /* start convert */
                SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN;    /* write dynamic to NFC memory */
            break;  /* APPCB_ST_NORMAL */
          
        }  /* switch */
    }  /* if */
}  /* xTask_60Sec */

/**
  ******************************************************************************
  * @func   xTask_60Min()
  * @brief  excute 60 minutes tasks: NFC dynamic write, set flag to begin ADC convert
  * @param  None
  * @gvar   sAppTimeCB, sAppCB, AdcCB, SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xTask_60Min()
{
    if (sAppTimeCB.bApp_60MinF)
    {
        sAppTimeCB.bApp_60MinF = FALSE;
        if (APPCB_ST_NORMAL == sAppCB.uState)
        {
            AdcCB.uAction = ADCCB_ACT_CONVERT;              /* start convert */
            SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN;    /* write dynamic to NFC memory */
        }
    }
}  /* xTask_60Min */

#if 0
/**
  ******************************************************************************
  * @func   xDebugAdv()
  * @brief  excute 10 minutes tasks: NFC dynamic write, set flag to begin ADC convert
  * @param  None
  * @gvar   bApp_10MinF, AdcCB, SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xDebugAdv()
{
    adv_data[13] = uAppCnt;
//    adv_data[14] = sAppCB.uAction;  //uAppCnt2;
//    adv_data[15] = sAppCB.uState;  //uAppCnt1;
//    adv_data[16] = (uint8_t)iAccSecCnt;         // acc second counter
//    adv_data[17] = (uint8_t)iAccInt1Cnt;        // acc interrupt 1 counter
    adv_data[14] = (uint8_t)iAccSecCnt;         // acc second counter
    adv_data[15] = (uint8_t)iAccInt1Cnt;        // acc interrupt 1 counter
    adv_data[16] = (uint8_t)SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_CCV];
    adv_data[17] = (uint8_t)(((accel_sm.myState << 4) & 0x00f0) | (SystemCB.AccCB.uAction & 0x0f));
    adv_data[18] = (uint8_t)SystemIB.SysRAMIB.uNFCBuzzerEvents;
    adv_data[19] = (uint8_t)SystemIB.SysRAMIB.uNFCEvents;
//    adv_data[19] = (uint8_t)SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_CCV];
//    adv_data[20] = AdcCB.uState;
//    adv_data[21] = uAppCnt1;  // 2 sec loop counter
//    adv_data[22] = uAppCnt2;
//    adv_data[23] = uAppCnt3;
//    adv_data[24] = uAppCnt4;
    
//    adv_data[14] = (uint8_t)(((SystemCB.NFCCB.uState << 4) & 0x00f0) | (SystemCB.NFCCB.uAction & 0x0f));
//    adv_data[17] = uAppCnt3;
//    adv_data[18] = uAppCnt4;
//    adv_data[17] = (uint8_t)((uiSysTick >> 16) & 0x00ff);
//    adv_data[18] = (uint8_t)(uiSysTick >> 24);
//    adv_data[22] = (uint8_t)(((AdcCB.uState << 4) & 0x00f0) | (AdcCB.uAction & 0x0f));
//    adv_data[23] = (uint8_t)(((accel_sm.myState << 4) & 0x00f0) | (SystemCB.AccCB.uAction & 0x0f));
//    adv_data[24] = (uint8_t)iAccInt1Cnt;        // acc interrupt 1 counter
//    adv_data[24] = (uint8_t)(((sAudioEngine.uiAE_State << 4) & 0x00f0) | (sAudioEngine.uiAE_NewCmd & 0x0f));
}  /* xDebugAdv */
#endif

/**
  ******************************************************************************
  * @func   CheckApp()
  * @brief  check application, states are idle, to ship mode, ship mode, activate,
  *         normal
  * @param  None
  * @gvar   sAppCB
  * @gfunc  xApp_Idle(), xApp_2ShipMode(), xApp_2ShipMode_PowerDn(), xApp_ShipMode(),
  *         xApp_Activate(), CheckAudioEngine(), CheckEndOfRx(), CheckEndOfTx(), ,
  *         CheckRxMsg(), CheckLoL(), CheckFlashMem(), CheckNFC(), CheckADC(),
  *         CheckAcc(), TrackTime(), BLE_ModulesTick(), xTask_2Sec(), xTask_60Sec()
  * @retval None
  ******************************************************************************
  */
void CheckApp()
{
    switch (sAppCB.uState)
    {
        /*----------------------------------------------------------------------
         *  Idle Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_IDLE:
            TrackTime();
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckFlashMem();    /* check for flash memory operation */
            CheckNFC();         /* check NFC tasks & interrupt */
            CheckADC();         /* check adc */
            CheckAcc();         /* check accelerometer */
            xTask_2Sec();
            xApp_Idle();
//xDebugAdv();
            /* @#$% init time counter & flags, before going to APPCB_ST_2SHIPMODE */
        break;  /* APPCB_ST_IDLE */
        
        /*----------------------------------------------------------------------
         *  To Ship Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_2SHIPMODE:
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL?? */
            CheckFlashMem();    /* check for flash memory operation */
            CheckNFC();         /* check NFC tasks & interrupt */
            CheckADC();         /* check adc */
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            xApp_2ShipMode();
        break;  /* APPCB_ST_2SHIPMODE */
        
        /*----------------------------------------------------------------------
         *  To Ship Mode Power Down
         *--------------------------------------------------------------------*/
        case APPCB_ST_2SHIPMODE_PWRDN:
            TrackTime();
            xTask_2Sec();
            xApp_2ShipMode_PowerDn();
        break;  /* APPCB_ST_2SHIPMODE_PWRDN */
        
        /*----------------------------------------------------------------------
         *  Ship Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_SHIPMODE:
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckNFC();         /* check NFC tasks & interrupt */
            TrackTime();
            xApp_ShipMode();
//uAppCnt++;
//xDebugAdv();
        break;  /* APPCB_ST_SHIPMODE */
            
        /*----------------------------------------------------------------------
         *  Activate Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_ACTIVATE:
#ifdef ENABLE_BLE
            BLE_ModulesTick();
#endif
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            CheckFlashMem();    /* check for flash memory operation */
            CheckNFC();         /* check NFC tasks & interrupt */
            CheckADC();         /* check adc */
            TrackTime();
            xApp_Activate();
//uAppCnt++;
//xDebugAdv();
        break;  /* APPCB_ST_ACTIVATE */

        /*----------------------------------------------------------------------
         *  Normal Mode
         *--------------------------------------------------------------------*/
        case APPCB_ST_NORMAL:
#ifdef ENABLE_BLE
            BLE_ModulesTick();
//            BLE_AppTick();
#endif
            CheckAudioEngine(); /* check audio engine */
            CheckEndOfRx();     /* check for end of rx */
            CheckEndOfTx();     /* wait til end of tx */
            CheckRxMsg();       /* check receive msg */
            CheckLoL();         /* check LoL */
            
            /*------------------------------------------------------------------
             *  must check for NFC.  A read will cause a wakeup and buzz.
             *----------------------------------------------------------------*/
            CheckNFC();         /* needed to check NFC interrupt */
//uAppCnt++;
//xDebugAdv();
            
            if ((AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State) && (sAudioEngine.uiAE_NewCmd != TRUE))
//            if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
            {
                Normal_PowerSave_PD_NoT();                
            }  /* if ae */
        break;  /* APPCB_ST_NORMAL */

        default: 
        break;
    }  /* switch() */
}  /* CheckApp() */

/**
  ******************************************************************************
  * @func   xApp_Idle()
  * @brief  app check in idle mode, if go to ship mode, flash erase/write of
  *         SystemIB.Flash, set NFC for wait system flash op, then to ship mode.
  * @param  None
  * @gvar   sAppCB, SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xApp_Idle()
{
    switch (sAppCB.uAction)
    {
        case APPCB_ACT_2SHIPMODE:
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* just write, no erase required */
            SystemCB.NFCCB.uAction = NFCCB_ACT_WAITSYSF;        /* write deafult to NFC memory, wait for system finish writing to flash */
            sAppCB.uState = APPCB_ST_2SHIPMODE;
        break;  /* APPCB_ACT_2SHIPMODE */
        
        default:
        break;
    }  /* switch */
}  /* xApp_Idle() */


/**
  ******************************************************************************
  * @func   xApp_2ShipMode()
  * @brief  app into ship mode, wait until flash memory erase/write and NFC
  *         write finished, then go to delay, wait for power down (NFC).
  * @param  None
  * @gvar   SystemCB, sAppCB
  * @gfunc  AppTime_Init_All()
  * @retval None
  ******************************************************************************
  */
void xApp_2ShipMode()
{
    switch (SystemCB.NFCCB.uAction)
    {
        case NFCCB_ACT_IDLE:
            if ((NFCCB_ST_IDLE == SystemCB.NFCCB.uState) && (FLASHCB_ST_IDLE == SystemCB.FlashCB.uState))
            {
                AppTime_Init_All();
                sAppCB.uState = APPCB_ST_2SHIPMODE_PWRDN;       /* wait for power down */
            }
        break;  /* NFCCB_ACT_IDLE */
        
        default:
        break;
    }  /* switch */
}  /* xApp_2ShipMode() */


/**
  ******************************************************************************
  * @func   xApp_2ShipMode_PowerDn()
  * @brief  app delay for certain time (idle state), then power down NFC.  Then go to ship mode.
  * @param  None
  * @gvar   sAppCB
  * @gfunc  NGT_Power_Save()
  * @retval None
  ******************************************************************************
  */
void xApp_2ShipMode_PowerDn()
{
    NGT_Power_Save();  /* power save mode */
        
    sAppCB.uState = APPCB_ST_SHIPMODE;
}  /* xApp_2ShipMode_PowerDn() */


/**
  ******************************************************************************
  * @func   xApp_ShipMode()
  * @brief  app check in ship mode, wait until activate action from matching Asset
  *         ID Code Phone.
  * @param  None
  * @gvar   sAppCB, AdcCB, sWakeupIO, sWakeupIO
  * @gfunc  NGT_Power_Save2(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void xApp_ShipMode()
{
#ifdef ENABLE_PWR_DOWN_IO
    /* if audio engine is not on, from NFC hit, then enable power on, otherwise, skip */
    if ( (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
         && (sAudioEngine.uiAE_NewCmd != TRUE)
         && (TRUE == HAL_VTIMER_SleepCheck()) )
    {
        /*--------------------------------------------------------------
         * all peripherals are already shutdown or de-initalized, so
         * init gpio for pull down and call power manager only
         *------------------------------------------------------------*/
        NGT_Power_Save2();
    }  /* if */
#endif
    
    switch (sAppCB.uAction)
    {
        case APPCB_ACT_ACTIVATE:
            if ((ADCCB_ST_IDLE == AdcCB.uState) && (ADCCB_ACT_IDLE == AdcCB.uAction))
            {
                sAppCB.uState = APPCB_ST_ACTIVATE;
            }  /* if */
        break;  /* APPCB_ACT_ACTIVATE */
        
        default:
        break;
    }  /* switch */
}  /* xApp_ShipMode() */


/**
  ******************************************************************************
  * @func   xApp_Activate()
  * @brief  app check in activation.
  *         1) begin ADC conversion.
  *         2) wait until ADC finished conversion, then flash erase/write SystemIB
  *         3) wait until flash erase/write op finished, then NFC op
  *         4) go to accelerometer configure
  *         5) configure accelerometer for vibration, go to normal mode
  * @param  None
  * @gvar   sAppCB, AdcCB, SystemCB, SystemIB, sAudioEngine
  * @gfunc  ADC_Init_All(), AppTime_Init_All(), Acc_Init_All(), Normal_PowerSave_PD_NoT()
  * @retval None
  ******************************************************************************
  */
void xApp_Activate()
{
    switch (sAppCB.uAction)
    {
        case APPCB_ACT_ACTIVATE:
            ADC_Init_All();     /* init adc */
            AdcCB.uAction = ADCCB_ACT_CONVERT;
            sAppCB.uAction = APPCB_ACT_ACTIVATE_ADC;
        break;  /* APPCB_ACT_ACTIVATE */

        case APPCB_ACT_ACTIVATE_ADC:
            if ((ADCCB_ST_IDLE == AdcCB.uState) && (ADCCB_ACT_IDLE == AdcCB.uAction))
            {
                /* update current mode before writing to flash memory */
                SystemIB.SysFlashIB.uShipModeStatus = NORMAL_MODE;    /* store new mode value */
                SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* just write, no erase required */
                sAppCB.uAction = APPCB_ACT_ACTIVATE_FLASH;
            }
        break;  /* APPCB_ACT_ACTIVATE_ADC */

        case APPCB_ACT_ACTIVATE_FLASH:
            if ((FLASHCB_ST_IDLE == SystemCB.FlashCB.uState) && (FLASHCB_ACT_IDLE == SystemCB.FlashCB.uAction))
            {
                sAppCB.uAction = APPCB_ACT_ACTIVATE_ACC;
            }
        break;  /* APPCB_ACT_ACTIVATE_FLASH */

        case APPCB_ACT_ACTIVATE_ACC:
#ifdef ENABLE_PWR_DOWN_ACC
            Acc_Init_All();     /* init i2c & accelerometer */ //^&* works
#endif

#ifdef ENABLE_PWR_TONE
            if ((AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State) && (sAudioEngine.uiAE_NewCmd != TRUE))
            {
                sAudioEngine.uiAE_NewSongId = SONG_INDEX_SUCCESS;
                sAudioEngine.uiAE_NewCmd = TRUE;
                SystemIB.SysRAMIB.uNFCBuzzerEvents++;  //^&*(
            }  /* if ae */
#endif
            SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN; /* write dynamic data */
            sAppCB.uAction = APPCB_ACT_ACTIVATE_NFC;
        break;  /* APPCB_ACT_ACTIVATE_ACC */

        case APPCB_ACT_ACTIVATE_NFC:
            if ((NFCCB_ST_IDLE == SystemCB.NFCCB.uState) && (NFCCB_ACT_IDLE == SystemCB.NFCCB.uAction))
            {
                sAppCB.uAction = APPCB_ACT_ACTIVATE_LAST;
            }
        break;  /* APPCB_ACT_ACTIVATE_NFC */
        
        case APPCB_ACT_ACTIVATE_LAST:
            AppTime_Init_All();
            sAppCB.uAction = APPCB_ACT_IDLE;
            sAppCB.uState = APPCB_ST_NORMAL;
        break;  /* APPCB_ACT_ACTIVATE_LAST */
    }  /* switch */
}  /* xApp_Activate() */

/**
  ******************************************************************************
  * @func   App_Init_All()
  * @brief  application information & control variable initialization
  * @param  None
  * @gvar   None
  * @gfunc  AppIB_init(), AppCB_init()
  * @retval None
  ******************************************************************************
  */
void App_Init_All()
{
    AppIB_init();  /* must init info block before control block */
    AppCB_init();
}  /* App_Init_All() */


/**
  ******************************************************************************
  * @func   AppCB_init()
  * @brief  application control block variable initialization
  * @param  None
  * @gvar   sAppCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void AppCB_init()
{
    sAppCB.uAction = APPCB_ACT_IDLE;    /* action: idle */
    sAppCB.uState = APPCB_ST_IDLE;      /* state: idle */
}  /* AppCB_init() */


/**
  ******************************************************************************
  * @func   AppIB_init()
  * @brief  application infomation block variable initialization
  * @param  None
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void AppIB_init()
{
}  /* AppIB_init() */


/**
  ******************************************************************************
  * @func   xBleTask_2Sec()
  * @brief  BLE excute 2 second tasks.  if in normal mode, set flag to check accelerometer.
  * @param  None
  * @gvar   sAppTimeCB, SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xBleTask_2Sec()
{
    if (sAppTimeCB.bApp_2SecF)
    {
        sAppTimeCB.bApp_2SecF = FALSE;

        switch (sAppCB.uState)
        {
            case APPCB_ST_NORMAL:
                SystemCB.AccCB.uAction = ACCCB_ACT_CHK_INT1;
            break;  /* APPCB_ST_NORMAL */
            
            case APPCB_ST_IDLE:
                SystemCB.AccCB.uAction = ACCCB_ACT_CHK_INT1;
            break;  /* APPCB_ST_IDLE */
        }  /* switch */
    }  /* if */
}  /* xBleTask_2Sec */


/**
  ******************************************************************************
  * @func   xBleTask_60Sec()
  * @brief  excute 60 second tasks
  * @param  None
  * @gvar   bApp_60SecF
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xBleTask_60Sec()
{
    if (sAppTimeCB.bApp_60SecF)
    {
        sAppTimeCB.bApp_60SecF = FALSE;
        switch (sAppCB.uState)
        {
            case APPCB_ST_IDLE:
                AdcCB.uAction = ADCCB_ACT_CONVERT;      /* start convert $%^& */
            break;  /* APPCB_ST_IDLE */
          
            case APPCB_ST_NORMAL:
                AdcCB.uAction = ADCCB_ACT_CONVERT;              /* start convert */
#if 0
                SystemIB.SysRAMIB.uNFCBuzzerEvents += 1;
                SystemIB.SysRAMIB.uNFCEvents += 2;
                SystemIB.SysRAMIB.uNFCResetEvents += 3;
#endif
                SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN;    /* write dynamic to NFC memory */
            break;  /* APPCB_ST_NORMAL */
          
        }  /* switch */
    }  /* if */
}  /* xBleTask_60Sec */


/**
  ******************************************************************************
  * @func   xBleTask_60Min()
  * @brief  BLE excute 60 minutes tasks: NFC dynamic write, set flag to begin ADC convert
  * @param  None
  * @gvar   sAppTimeCB, AdcCB, SystemIB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xBleTask_60Min()
{
    if (sAppTimeCB.bApp_60MinF)
    {
        sAppTimeCB.bApp_60MinF = FALSE;
        switch (sAppCB.uState)
        {
            case APPCB_ST_IDLE:
                AdcCB.uAction = ADCCB_ACT_CONVERT;      /* start convert $%^& */
            break;  /* APPCB_ST_IDLE */

            case APPCB_ST_NORMAL:
                AdcCB.uAction = ADCCB_ACT_CONVERT;              /* start convert */
                SystemCB.NFCCB.uAction = NFCCB_ACT_WRITEDYN;    /* write dynamic to NFC memory */
            break;  /* APPCB_ST_NORMAL */
          
        }  /* switch */
    }  /* if */
}  /* xBleTask_60Min */


/**
  ******************************************************************************
  * @func   BleNormalTrackTime()
  * @brief  BLE track app time during normal mode, check 2 seconds and 60 minutes
  *          counter. reset 2 sec count after 60 minutes is reached
  * @param  None
  * @gvar   sAppTimeCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void BleNormalTrackTime()
{
    sAppTimeCB.uiApp_2SecCnt++;
    sAppTimeCB.bApp_2SecF = TRUE;

    /* $%^& delete after testing */
    if (0 == (sAppTimeCB.uiApp_2SecCnt % NORM_APP60SEC_CNT))
    {
        sAppTimeCB.bApp_60SecF = TRUE;
    }  /* if 60 sec */
        
    if (0 == (sAppTimeCB.uiApp_2SecCnt % NORM_APP60MIN_CNT))
    {
        sAppTimeCB.bApp_60MinF = TRUE;
        sAppTimeCB.uiApp_2SecCnt = 0;
    }  /* if 60 min*/
}  /* BleNormalTrackTime */


/**
  ******************************************************************************
  * @func   aci_hal_end_of_radio_activity_event()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  last state, next state, next state system time
  * @gvar   sAppCB, SystemCB, AdcCB, 
  * @gfunc  BleNormalTrackTime(), xBleTask_2Sec(), xBleTask_60Sec(), xBleTask_60Min(),
  *         CheckADC(), CheckNFC(), CheckAcc()
  * @retval None
  ******************************************************************************
  */
void aci_hal_end_of_radio_activity_event( uint8_t Last_State, uint8_t Next_State,
                                         uint32_t Next_State_SysTime)
{
    /* next state:  advertising */
    if ((0x01 == Next_State) && (advertising_mode == BLE_ADVERTISING_NORMAL))
    {
        switch (sAppCB.uState)
        {
            case APPCB_ST_NORMAL:
                BleNormalTrackTime();
                xBleTask_2Sec();
//  xBleTask_60Sec();       /* $%^& delete after testing */
                xBleTask_60Min();  // restore for release
                /*--------------------------------------------------------------
                 * execute ADC until converting (return to no action & idle state)
                 * execute NFC write until finished (return to no action & idle state)
                 *------------------------------------------------------------*/
                while (((SystemCB.NFCCB.uState != NFCCB_ST_IDLE)
                       || (SystemCB.NFCCB.uAction != NFCCB_ACT_IDLE))
                       || ((AdcCB.uState != ADCCB_ST_IDLE)
                       || (AdcCB.uAction != ADCCB_ACT_IDLE)))
                {
                    CheckADC();         /* check adc */
                    CheckNFC();         /* check NFC tasks */
                }  /* while */
                
                /*--------------------------------------------------------------
                 * execute accelerometer until finished
                 * (return to no action, state may not be in idle**)
                 *------------------------------------------------------------*/
                while (ACCCB_ACT_IDLE != SystemCB.AccCB.uAction)
                {
                    CheckAcc();
                }  /* while */
            break;  /* APPCB_ST_NORMAL */
        }  /* switch */
        
    }
    
    /* next state:  advertising */
    if (0x01 == Next_State)
    {
        /* Check to see if we switch advertiing modes to iBeacon */
        Update_Advertising_Service();
    }
}  /* aci_hal_end_of_radio_activity_event */


