/**
  ******************************************************************************
  * @file    NG_Tick_ae.c
  * @author  Thomas W Liu
  * @brief   Source file of audio engine.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "bluenrg_lp_ll_tim.h"
#include "NG_Tick_main.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_pwm.h"


/* variable declaration ------------------------------------------------------*/
int32_t iAudioEngineSysCounter;         /* audio engine system counter, 1ms, dec in system timer */

STRUCT_AUDIOENGINE_SM sAudioEngine;   /* audio machine state machine */

//#define EN_RD_AUDIO_MSG
//#ifdef  EN_RD_AUDIO_AE_MSG      /* audio engine message */
#ifdef  EN_RD_AUDIO_MSG

char    ucAudioStr[80] = { 0 };   /* message to be transmitted */

const char uccAudioStr_SongRepeat[] = "Song Repeat: ";
const char uccAudioStr_ProgressRecCount[] = "Progression Records: ";
const char uccAudioStr_ProgressRepeat[] = "Progression Repeat:";
const char uccAudioStr_ChordRecCount[] = "Chord Records: ";
const char uccAudioStr_ChordRepeat[] = "Chord Repeat: ";
const char uccAudioStr_Freq[] = "Note: ";
const char uccAudioStr_Duration[] = "Duration: ";
#endif


/* function prototype --------------------------------------------------------*/
void AudioEngine_Init_All(void);
void AudioVolume_GPIO_Init(void);
void AudioEngineSM_Init(void);
void CheckAudioEngine(void);
static void SetTM_FreqDutyCycle(uint16_t);
void SetTM_Volume(uint16_t);

/**
  ******************************************************************************
  * @func   AudioEngine_Init_All()
  * @brief  init audio engine related
  * @param  none
  * @gvar   none
  * @gfunc  AudioVolume_GPIO_Init(), AudioEngineSM_Init(), SetTM_Volume(),
  *         Buzzer_TIM1_Var_Init(), Buzzer_TIM1_Init()
  * @retval None
  ******************************************************************************
  */
void AudioEngine_Init_All(void)
{    
    AudioVolume_GPIO_Init();
    AudioEngineSM_Init();       /* init audio engine state machine */
    SetTM_Volume(0);
//    ReadPrintAllAudioTable();   /* read and print all audio tables */
    Buzzer_TIM1_Var_Init();     /* init TIM1 related variables */
    Buzzer_TIM1_Init();         /* init TIM1 */
}  /* AudioEngine_Init_All() */

/**
  ******************************************************************************
  * @func   AudioVolume_GPIO_Init()
  * @brief  init gpio pins to the audio amplifer (MAS6240C2), PB0, PB1 & PB4
  * @param  none
  * @gvar   none
  * @gfunc  none
  * @retval None
  ******************************************************************************
  */
void AudioVolume_GPIO_Init(void)
{
    AUDIOAMP_EN12_GPIO_CLK_ENABLE();
    LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
    LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
    LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);

    LL_GPIO_SetPinMode( AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinSpeed( AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType( AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull( AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN, LL_GPIO_PULL_DOWN);
    
    LL_GPIO_SetPinMode( AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinSpeed( AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType( AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull( AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN, LL_GPIO_PULL_DOWN);

    LL_GPIO_SetPinMode( AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinSpeed( AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType( AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull( AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN, LL_GPIO_PULL_DOWN);

    LL_GPIO_ResetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
    LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
#if 1
    LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
#else
    LL_GPIO_ResetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
#endif
}  /* AudioVolume_GPIO_Init() */


/**
  ******************************************************************************
  * @func   AudioEngineSM_Init()
  * @brief  initialize audio engine state machine
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void AudioEngineSM_Init()
{
    sAudioEngine.uiAE_State = AUDIOENG_ST_IDLE;
    sAudioEngine.uiAE_NewCmd = FALSE;
    sAudioEngine.uiAE_NewSongId = 0;
    sAudioEngine.uiAE_Abort = FALSE;
    sAudioEngine.uiAE_SongVolume = 0;
    sAudioEngine.uiAE_SongVolumeOverride = DEF_AE_VOL_OVERRIDE;
    sAudioEngine.iAE_ProgressRepeat = 0;
    sAudioEngine.iAE_ProgressRepeatOverride = 0;
    sAudioEngine.sAE_ProgressCBPtr = NULL;
    sAudioEngine.iAE_ChordRepeat = 0;
    sAudioEngine.sAE_ChordCBPtr = NULL;
    sAudioEngine.iAE_NoteFreqCnt = 0;
    sAudioEngine.iAE_NoteFreqDuration = 0;
}  /* AudioEngineSM_Init */


/**
  ******************************************************************************
  * @func   CheckAudioEngine()
  * @brief  check audio engine state machine
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void CheckAudioEngine()
{
    static STRUCT_SONG_CB_TYPE  *sSongTblPtr;
    static STRUCT_PROGRESS_CB_TYPE *ssProgressCBPtr;
    static STRUCT_CHORD_CB_TYPE *ssChordCBPtr;
    static int32_t iProgressRepeat;
    static int32_t iChordRepeat;
    static int16_t iNoteFreqCnt;
    static int16_t iNoteFreqDuration;
    uint8_t bFound;
    uint8_t bExit;
    
    switch (sAudioEngine.uiAE_State)
    {
        /*----------------------------------------------------------------------
         *  audio engine: idle state
         *    checks for new song command
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_IDLE:
            if (sAudioEngine.uiAE_NewCmd)
            {
                sAudioEngine.uiAE_State = AUDIOENG_ST_SONG;
            }   /*if */
        break;  /* AUDIOENG_ST_IDLE */

        /*----------------------------------------------------------------------
         *  audio engine: song state, received new command
         *    must have new song id:  none zero
         *    if song id = 0 or not found (invalid or reset), exit to idle state
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_SONG:
            sSongTblPtr = &BuzzerSongTable[0];
            bFound = bExit = FALSE;     /* song not found, exit (end of table) */
            
            /* check for new song id.  if new song id == 0, then exit */
            if (NULL == sAudioEngine.uiAE_NewSongId)
            {
                bExit = TRUE;
            }
            
            /* NOTE ------------------------------------------------------------
             * while loop compares new song id to song id in table, until end of table
             * it does not check for new song id = 0, which is invalid
             * therefore, this check must come before this while loop
             *----------------------------------------------------------------*/
            while (!bFound && !bExit)
            {
                if (sAudioEngine.uiAE_NewSongId == sSongTblPtr->uiSongId)
                {
                    bFound = TRUE;
                }
                else
                {
                    if (NULL == sSongTblPtr->uiSongId)
                    {
                        bExit = TRUE;
                    }
                    else
                    {
                        sSongTblPtr++;
                    }  /* if else */
                }  /* if else */
            }  /* while */
            
            /*------------------------------------------------------------------
             * found song id in song table
             * fill information in audio engine structure
             *----------------------------------------------------------------*/
            if (bFound)
            {
                sAudioEngine.uiAE_State = AUDIOENG_ST_PROGRESS;
                sAudioEngine.uiAE_NewCmd = FALSE;
                sAudioEngine.sAE_ProgressCBPtr = sSongTblPtr->sProgressCBPtr;
                sAudioEngine.uiAE_SongVolume = sSongTblPtr->uiSongVolume;

                /*------------------------------------------------------------------
                 * check for progress repeat override
                 *  if 0, load from table.  if > 0, repeat = override, override = 0
                 *----------------------------------------------------------------*/
                if (0 == sAudioEngine.iAE_ProgressRepeatOverride)
                {
                    sAudioEngine.iAE_ProgressRepeat = sSongTblPtr->iProgressRepeat;
                }
                else
                {
                    sAudioEngine.iAE_ProgressRepeat = sAudioEngine.iAE_ProgressRepeatOverride;
                    sAudioEngine.iAE_ProgressRepeatOverride = 0;
                }
                
#ifdef  EN_RD_AUDIO_AE_MSG
                sprintf( ucAudioStr, "%s%u\r\n", uccAudioStr_ProgressRepeat, sAudioEngine.iAE_ProgressRepeat);
                StartTx_WaitTilEndOfTx(ucAudioStr);
#endif
            }
            
            /*------------------------------------------------------------------
             * song id is NOT found in song table
             * exit state machine, back to idle
             *----------------------------------------------------------------*/
            if (bExit)
            {
                sAudioEngine.uiAE_State = AUDIOENG_ST_IDLE;
                sAudioEngine.uiAE_NewCmd = FALSE;
            }
        break;  /* AUDIOENG_ST_SONG */

        /*----------------------------------------------------------------------
         *  audio engine: progress state
         *    must have new song id:  none zero
         *    follow thru from progress to chord and notes.  Find first note in
         *     first chord, and start playing ntoe.
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_PROGRESS:
            ssProgressCBPtr = sAudioEngine.sAE_ProgressCBPtr;  /* ptr to first chord */
            iProgressRepeat = sAudioEngine.iAE_ProgressRepeat; /* progress repeat */
            iChordRepeat = ssProgressCBPtr->iChordRepeat;      /* get first chord repeat */
            sAudioEngine.iAE_ChordRepeat = sAudioEngine.sAE_ProgressCBPtr->iChordRepeat;  /* chord repeat */
            sAudioEngine.sAE_ChordCBPtr = sAudioEngine.sAE_ProgressCBPtr->sChordCBPtr;

            /*------------------------------------------------------------------
             * if first chord repeat is valid (non-zero), get first note & duration.
             *   if chord not end of table, init note & duration, then start pwm
             *----------------------------------------------------------------*/
            if (iChordRepeat != NULL)
            {
                ssChordCBPtr = ssProgressCBPtr->sChordCBPtr;  /* init for first tone record */
                iNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;    /* get note frequency count from chord */
                iNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;    /* get note duration count from chord */

                /* if not at end of table (both note & duraction must be 0), read tables */
                if ((iNoteFreqCnt != NOTE_TERM) || (iNoteFreqDuration != NOTE_DURATION_TERM_CNT))
                {
#if 0
                    SetTM_FreqDutyCycle(iNoteFreqCnt);          /* init frequncy & duration */
                    Buzzer_TIM1_Enable();                       /* enable TIM1, pwm */
                    SetTM_Volume(sAudioEngine.uiAE_SongVolume); /* set volume */
#else
                    SetTM_Volume(sAudioEngine.uiAE_SongVolume); /* set volume */
                    SetTM_FreqDutyCycle(iNoteFreqCnt);          /* init frequncy & duration */
                    Buzzer_TIM1_Enable();                       /* enable TIM1, pwm */
#endif                    
                    /* init wait time count down counter */
                    iAudioEngineSysCounter = iNoteFreqDuration - 1;
                    sAudioEngine.uiAE_State = AUDIOENG_ST_WAIT_NOTE;
                    
                }  /* if */
                else
                {
                    /* end of chord, premature end of song, exit state machine, idle */
                    sAudioEngine.uiAE_State = AUDIOENG_ST_IDLE;
                    sAudioEngine.uiAE_NewCmd = FALSE;
                }  /* if else */
            }  /* if */
        break;  /* AUDIOENG_ST_PROGRESS */

        /*----------------------------------------------------------------------
         *  audio engine: wait during note state
         *    wait until current note is played (duration time is up)
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_WAIT_NOTE:
            if (iAudioEngineSysCounter < 0)
            {
                sAudioEngine.uiAE_State = AUDIOENG_ST_CHECK_NOTE;
            }  /* if */
        break;  /* AUDIOENG_ST_WAIT_NOTE */
        
        /*----------------------------------------------------------------------
         *  audio engine: check note state
         *    last note finished playing, check for next note in note table
         *    if more note in table, play note and wait
         *        if note is dead tone, disable timer
         *            else init frequency, duration & austion engine system counter
         *    if next note & duration are end of table, goto check chord repeat state
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_CHECK_NOTE:
             /* if last note is dead tone, re-enable timer counter */
            if (NOTE_TERM == iNoteFreqCnt)
            {
                LL_TIM_CC_EnableChannel(TIM1, LL_TIM_CHANNEL_CH6);  /* Enable output channel 6 */           
            }  /* if */
            ssChordCBPtr++;     /* inc ptr to next chord */
            iNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;             /* get note frequency count */
            iNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;  /* get note duration count */

            /* if end of note table (duraction must be 0), check for next chord */
            if (NOTE_DURATION_TERM_CNT == iNoteFreqDuration)
            {
                /* end of note, terminate ??!@ */
                sAudioEngine.uiAE_State = AUDIOENG_ST_CHECK_CHORD_REPEAT;
            }
            else
            {
              /* if note is a dead tone, disable timer (pwm) */
                if (NOTE_TERM == iNoteFreqCnt)
                {
                    LL_TIM_CC_DisableChannel(TIM1, LL_TIM_CHANNEL_CH6);  /* disable output channel 6 */           
                }
                else
                {
                    /* note is not a dead tone, init frequency & duty cycle */
                    SetTM_FreqDutyCycle(iNoteFreqCnt);  /* init frequency & duty cycle */
                }  /* if else */
                /* init wait time count down counter */
                iAudioEngineSysCounter = iNoteFreqDuration - 1;
                sAudioEngine.uiAE_State = AUDIOENG_ST_WAIT_NOTE;
            }  /* if else */
        break;  /* AUDIOENG_ST_CHECK_NOTE */
        
        /*----------------------------------------------------------------------
         *  audio engine: check chord repeat state
         *    a complete chord has been played.  check for repeat of current chord
         *    if repeat count > 0, then repeat current chord
         *        else go to nex chord
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_CHECK_CHORD_REPEAT:
            if (sAudioEngine.uiAE_Abort)
            {
                sAudioEngine.uiAE_Abort = FALSE;
                sAudioEngine.uiAE_State = AUDIOENG_ST_END;
            }
            else
            {
                iChordRepeat--;    /* decrement repeat count */
                /* check for end of repeat count, if not, continue */
                if (iChordRepeat > 0)
                {
                    ssChordCBPtr = ssProgressCBPtr->sChordCBPtr;  /* init for first tone record */
                    iNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;      /* get note frequency count from table */
                    iNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;    /* get note duration count from table */
                    SetTM_FreqDutyCycle(iNoteFreqCnt);
                    iAudioEngineSysCounter = iNoteFreqDuration - 1;
                    sAudioEngine.uiAE_State = AUDIOENG_ST_WAIT_NOTE;
                }  /* */
                else
                {
                    sAudioEngine.uiAE_State = AUDIOENG_ST_NEXT_CHORD;
                }
            }  /* if else */
        break;  /* AUDIOENG_ST_CHECK_CHORD_REPEAT */

        /*----------------------------------------------------------------------
         *  audio engine: check for next chord state
         *    last chord has reached repeat count.
         *    check for chord in next progression in table
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_NEXT_CHORD:
            /*------------------------------------------------------------------
             * next progression, get first chord in next progression
             *----------------------------------------------------------------*/
            ssProgressCBPtr++;
            iChordRepeat = ssProgressCBPtr->iChordRepeat;      /* get chord repeat */
            sAudioEngine.iAE_ChordRepeat = sAudioEngine.sAE_ProgressCBPtr->iChordRepeat;      /* chord repeat */
            sAudioEngine.sAE_ChordCBPtr = sAudioEngine.sAE_ProgressCBPtr->sChordCBPtr;

            /*------------------------------------------------------------------
             * if first chord repeat is valid (non-zero), get first note & duration.
             *   if chord not end of table, init note & duration, then start pwm
             *----------------------------------------------------------------*/
            if (iChordRepeat != NULL)
            {
                ssChordCBPtr = ssProgressCBPtr->sChordCBPtr;  /* init for first tone record */
                iNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;      /* get note frequency count from table */
                iNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;    /* get note duration count from table */

                SetTM_FreqDutyCycle(iNoteFreqCnt);  /* init frequency & duty cycle */
                
                /* init wait time count down counter */
                iAudioEngineSysCounter = iNoteFreqDuration - 1;
                sAudioEngine.uiAE_State = AUDIOENG_ST_WAIT_NOTE;
            }  /* if */
            else
            {
                /* end of chord, go to next progression */
                sAudioEngine.uiAE_State = AUDIOENG_ST_NEXT_PROGRESS;
            }  /* if else */
        break;  /* AUDIOENG_ST_NEXT_CHORD */

        /*----------------------------------------------------------------------
         *  audio engine: check progression state
         *    checking for progression repeat here
         *    if repeat, reload progression & chord control block ptr
         *        else end of sound, go to next state (clean up & idle)
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_NEXT_PROGRESS:
           iProgressRepeat--;    /* decrement repeat count */
            /* check for end of repeat count, if not, continue */
            if (iProgressRepeat > 0)
            {
                ssProgressCBPtr = sAudioEngine.sAE_ProgressCBPtr;  /* ptr to first chord */
                iChordRepeat = ssProgressCBPtr->iChordRepeat;      /* get chord repeat */
                sAudioEngine.iAE_ChordRepeat = sAudioEngine.sAE_ProgressCBPtr->iChordRepeat;   /* chord repeat */
                sAudioEngine.sAE_ChordCBPtr = sAudioEngine.sAE_ProgressCBPtr->sChordCBPtr;

                ssChordCBPtr = ssProgressCBPtr->sChordCBPtr;  /* init for first tone record */
                iNoteFreqCnt = ssChordCBPtr->iNoteFreqCnt;    /* get note frequency count from table */
                iNoteFreqDuration = ssChordCBPtr->iNoteFreqDuration;    /* get note duration count from table */

                SetTM_FreqDutyCycle(iNoteFreqCnt);

                /* init wait time count down counter */
                iAudioEngineSysCounter = iNoteFreqDuration - 1;
                sAudioEngine.uiAE_State = AUDIOENG_ST_WAIT_NOTE;
            }  /* if */
            else
            {
                /* end of chord, terminate ??!@ */
                sAudioEngine.uiAE_State = AUDIOENG_ST_END;
            }  /* if else */
        break;  /* AUDIOENG_ST_NEXT_PROGRESS */

        /*----------------------------------------------------------------------
         *  audio engine: end state
         *    the song finished playing, disable time (pw) and to to idle
         *--------------------------------------------------------------------*/
        case AUDIOENG_ST_END:
            SetTM_Volume(0);
            Buzzer_TIM1_Disable();
            sAudioEngine.uiAE_State = AUDIOENG_ST_IDLE;
        break;  /* AUDIOENG_ST_END */

        /*----------------------------------------------------------------------
         *  audio engine: default state
         *    not found, set to idle state
         *--------------------------------------------------------------------*/
        default:
            sAudioEngine.uiAE_State = AUDIOENG_ST_IDLE;
        break;
    }  /* switch() */
}  /* CheckAudioEngine */


/**
  ******************************************************************************
  * @func   SetTM_FreqDutyCycle()
  * @brief  set frequency & duty cycle (fixed at 50%)
  * @param  frequency
  * @gvar   timxPrescaler, timxPeriod
  * @gfunc  LL_TIM_SetPrescaler(), LL_TIM_SetAutoReload(), Configure_DutyCycle6_50DC()
  * @retval None
  * @note   prescaler = (((tmclk + ((hz * COUNTER_FREQ_F) / 2.0)) / (hz * COUNTER_FREQ_F)) - 1.0)
  *         to compensate round off error:  add 0.5 before truncate to uint32
  *         auto reload register: defaulted to COUNTER_FREQ_I or COUNTER_FREQ_F - 1
  ******************************************************************************
  */
static void SetTM_FreqDutyCycle(uint16_t uiFreq)
{
    float ftimxPrescaler;

    // to compensat division round off
    ftimxPrescaler = ((((float)TIM_PERIPHCLK + (((float)uiFreq * COUNTER_FREQ_F) / 2.0)) / ((float)uiFreq * COUNTER_FREQ_F)) - 1.0);
    timxPrescaler = (uint32_t)(ftimxPrescaler + 0.5);         /* to compensat division round off */
    LL_TIM_SetPrescaler( TIM1, timxPrescaler);  /* init PSC register */
    timxPeriod = COUNTER_AUTORELOAD;
    LL_TIM_SetAutoReload(TIM1, timxPeriod);  /* Change PWM signal frequency */
    Configure_DutyCycle6_50DC(); /* Change PWM signal duty cycle */
}  /* SetTM_FreqDutyCycle() */


/**
  ******************************************************************************
  * @func   SetTM_Volume()
  * @brief  set volume to the audio amplifer (MAS6240C2), if volume is not 0,
  *         then volume override takes precedence over passing parameter
  * @param  volume, value: 0, 1, 2
  * @gvar   sAudioEngine
  * @gfunc  none
  * @retval None
  * @note   volume  PB1(EN1)  PB0(EN2)
  *            0       0         0
  *            1       0         1
  *            2       1         0
  *            3       1         1
  * @note   if SongVolumeOverride == 0, then default volume (from table) is used.
  ******************************************************************************
  */
void SetTM_Volume(uint16_t volume)
{
    if ((volume != 0) && (sAudioEngine.uiAE_SongVolumeOverride != volume) &&
        (sAudioEngine.uiAE_SongVolumeOverride != 0))
    {
        volume = sAudioEngine.uiAE_SongVolumeOverride;
    }

    switch (volume)
    {
        case 0:
            LL_GPIO_ResetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
        break;  /* volume = 0 */
        
        case 1:
#if 1
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            iAudioEngineSysCounter = 2;
            while (iAudioEngineSysCounter > 0);
#else
            LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
#endif
        break;  /* volume = 0 */
        
        case 2:
#if 1
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            iAudioEngineSysCounter = 2;
            while (iAudioEngineSysCounter > 0);
#else
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
#endif
        break;  /* volume = 0 */
        
        case 3:
#if 1
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            iAudioEngineSysCounter = 2;
            while (iAudioEngineSysCounter > 0);
#else
            LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
#endif
        break;  /* volume = 0 */
        
        default:
#if 1
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            iAudioEngineSysCounter = 2;
            while (iAudioEngineSysCounter > 0);
#else
            LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
            LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
            LL_GPIO_SetOutputPin(AUDIOAMP_PWR_GPIO_PORT, AUDIOAMP_PWR_PIN);
#endif
        break;  /* default */
    }  /* switch */
}  /* SetTM_Volume() */

