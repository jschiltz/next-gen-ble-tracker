/**
  ******************************************************************************
  * @file    NG_Tick_wakeup.c
  * @author  Thomas W Liu
  * @brief   Source file of wakeup.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp.h"
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_rcc.h"
#include "bluenrg_lp_ll_rtc.h"
#include "bluenrg_lp_hal_power_manager.h"
#include "NG_Tick_wakeup.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_NFC.h"

#include "NG_Tick_Acc.h"        /* !@#$ wakeup, delete later */
#include "NG_Tick_ae.h"
#include "NG_Tick_ble.h"


//#define EN_RD_WAKEUP_MSG

/* constant variable declaration ------------------------------------------------------*/

/* constant variable declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
uint16_t uiSysSleepMode = SYS_SLEEP_IDLE;

/* function prototype --------------------------------------------------------*/
void WakeUp_Normal_Init(void);



/**
  ******************************************************************************
  * @func   WakeUp_Normal_Init()
  * @brief  init RTC for wakeup.  init pull down to reduce leakage
  * @param  None
  * @gvar   None
  * @gfunc  RTC_WakeupInit(), LL_PWR_EnablePDA(), LL_PWR_EnablePDB()
  * @retval None
  ******************************************************************************
  */
void WakeUp_Normal_Init()
{
#if 0   /* not suing RTC */
    RTC_WakeupInit();    /* RTC Wakeup Peripheral Init */
#endif

#if 1
//    LL_PWR_EnablePDA( LL_PWR_PUPD_IO8 };
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
                      LL_PWR_PUPD_IO8 |
                      LL_PWR_PUPD_IO9 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
#endif
    

#if 0
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
                      LL_PWR_PUPD_IO9 |
                      LL_PWR_PUPD_IO10 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
#endif
    
#if 0
    /* Pull UP/DOWN configuration to reduce the current leakage */
    /* PA0 = i2c clk, PA1 = i2c sda, PA8 = uart rx, PA9 = uart tx */
    /* PA11 = DIN */
//    LL_PWR_EnablePDA( LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO4|
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
#endif
    
#if 1
    // RP2.5
    /* PB2 = NFC interrupt, PB3 = NFC power */
    /* PB4 = buzzer power */
    /* PB6 = Acc interrupt 2, PB7 = Acc interrupt 1 */
    /* PB3 = NFC low power1, PB4 = buzzer PWR */
    LL_PWR_EnablePDB( 
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 | 
//                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO8 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO4 | 
                      LL_PWR_PUPD_IO3 | 
                      LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |  /* testing */
                      LL_PWR_PUPD_IO9 | LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 |
                      LL_PWR_PUPD_IO15);
#else
    /* PB2 = NFC interrupt, PB6 = Acc interrupt 2, PB7 = Acc interrupt 1 */
    /* PB3 = NFC low power1, PB4 = buzzer PWR, PB14 = NFC power */
    LL_PWR_EnablePDB( LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 | LL_PWR_PUPD_IO3 | 
                      LL_PWR_PUPD_IO4 | LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO8 |
                      LL_PWR_PUPD_IO9 | LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 |
                      LL_PWR_PUPD_IO15);
#endif
}  /* WakeUp_RTC_Init() */


/**
  ******************************************************************************
  * @func   NGT_Power_Save()
  * @brief  power saving mode: shutdown accelerometer, de-init i2c, de-init DMA,
  *         de-init UART, de-init ADC, init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  NGT_I2C_DeInit(), Accelerometer_Enter_Shutdown(), LL_DMA_DeInit(), 
  *         UART_DeInit(), ADC_DeInit(), WakeUp_Normal_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_Save()
{
#ifdef ENABLE_PWR_DOWN_ACC
    Accelerometer_Enter_Shutdown(); /* power down accelerometer */
#endif
        
#ifdef ENABLE_PWR_I2C_DEINIT           /* de-init i2c to save power */
    NGT_I2C_DeInit();
#endif

#ifdef ENABLE_PWR_DMA_DEINIT
    LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
#endif
            
#ifdef ENABLE_PWR_UART_DEINIT
    UART_DeInit();  /* de-initialize UART to save power */
#endif
        
#ifdef ENABLE_PWR_ADC_DEINIT           /* de-init adc to save power */
    ADC_DeInit();
#endif
           
#ifdef ENABLE_PWR_DOWN_IO
    WakeUp_Normal_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, sWakeupIO, &iStopLevel);
#endif
}  /* NGT_Power_Save */


/**
  ******************************************************************************
  * @func   NGT_Power_Save2()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  WakeUp_Normal_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_Save2()
{
#ifdef ENABLE_PWR_DOWN_IO
    WakeUp_Normal_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, sWakeupIO, &iStopLevel);
#endif
}  /* NGT_Power_Save2 */


