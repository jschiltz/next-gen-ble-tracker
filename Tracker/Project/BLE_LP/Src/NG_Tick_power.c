/**
  ******************************************************************************
  * @file    NG_Tick_wakeup.c
  * @author  Thomas W Liu
  * @brief   Source file of wakeup.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp.h"
#include "bluenrg_lp_ll_dma.h"
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_rcc.h"
#include "bluenrg_lp_ll_rtc.h"
#include "bluenrg_lp_ll_utils.h"
#include "bluenrg_lp_hal_power_manager.h"

#include "bluenrg_lp_hal_vtimer.h"
#include "NG_Tick_power.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_NFC.h"
#include "ble_status.h"

#include "NG_Tick_Acc.h"
#include "NG_Tick_Adc.h"
#include "NG_Tick_i2c.h"

//#define EN_RD_WAKEUP_MSG

/* constant variable declaration ------------------------------------------------------*/
#ifdef EN_RD_NFC_MSG   /* !@#$ */
//const char ccNFCStr_RstMBEnDyn_OK1[] = "NFC_ResetMBEn_Dyn:  NFCTAG OK\r\n";
#endif

/* constant variable declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
static WakeupSourceConfig_TypeDef sWakeupIO;
static PowerSaveLevels iStopLevel;
//uint16_t uiSysSleepMode = SYS_SLEEP_IDLE;

VTIMER_HandleType hNormalTimer;         /* handler for timer in normal mode, 2 second */
uint8_t bNormalTime_Expired = FALSE;    /* timer expired flag, in normal mode, t/f */

/* variable declaration ------------------------------------------------------*/

/* External variables --------------------------------------------------------*/


/* function prototype --------------------------------------------------------*/
void PortAB_PullDown_Init(void);
void NGT_Power_Save(void);
void NGT_Power_Save2(void);
void NGT_Power_Save2_PD(void);

void VTimerTimeoutCB(void *);
void Normal_VTimer_init(void);
void Normal_PowerSave_PD(void);
void Normal_PowerSave_PD_NoT(void);


/* external variable declaration ---------------------------------------------*/

/* external function prototype -----------------------------------------------*/



/**
  ******************************************************************************
  * @func   PortAB_PullDown_Init()
  * @brief  init pull down to reduce leakage
  * @param  None
  * @gvar   None
  * @gfunc  LL_PWR_EnablePDA(), LL_PWR_EnablePDB()
  * @retval None
  ******************************************************************************
  */
void PortAB_PullDown_Init()
{
//    LL_PWR_EnablePDA( LL_PWR_PUPD_IO8 };
    LL_PWR_EnablePDA( LL_PWR_PUPD_IO4|
                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |
                      LL_PWR_PUPD_IO8 |
                      LL_PWR_PUPD_IO9 |
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO8 | LL_PWR_PUPD_IO9 |  /* testing */
                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 |  /* testing */
//                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 | LL_PWR_PUPD_IO12 |
                      LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 | LL_PWR_PUPD_IO15);
     
    // RP2.5
    /* PB2 = NFC interrupt, PB3 = NFC power */
    /* PB4 = buzzer power */
    /* PB6 = Acc interrupt 2, PB7 = Acc interrupt 1 */
    /* PB3 = NFC low power1, PB4 = buzzer PWR */
    LL_PWR_EnablePDB( 
//                      LL_PWR_PUPD_IO0 | LL_PWR_PUPD_IO1 | 
//                      LL_PWR_PUPD_IO5 | LL_PWR_PUPD_IO8 |
//                      LL_PWR_PUPD_IO2 | LL_PWR_PUPD_IO3 | LL_PWR_PUPD_IO4 | 
                      LL_PWR_PUPD_IO3 | 
                      LL_PWR_PUPD_IO6 | LL_PWR_PUPD_IO7 |  /* testing */
                      LL_PWR_PUPD_IO6 |   /* testing */
                      LL_PWR_PUPD_IO9 | LL_PWR_PUPD_IO10 | LL_PWR_PUPD_IO11 |
                      LL_PWR_PUPD_IO12 | LL_PWR_PUPD_IO13 | LL_PWR_PUPD_IO14 |
                      LL_PWR_PUPD_IO15);
    
    LL_PWR_SetNoPullB(LL_PWR_PUPD_IO7); /* float acc int1 pin */
}  /* PortAB_PullDown_Init() */

/**
  ******************************************************************************
  * @func   NGT_Power_Save()
  * @brief  power saving mode: shutdown accelerometer, de-init i2c, de-init DMA,
  *         de-init UART, de-init ADC, init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  NGT_I2C_DeInit(), Accelerometer_Enter_Shutdown(), LL_DMA_DeInit(), 
  *         UART_DeInit(), ADC_DeInit(), PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_Save()
{
#ifdef ENABLE_PWR_DOWN_ACC
    Accelerometer_Enter_Shutdown();     /* power down accelerometer */
#endif
        
#ifdef ENABLE_PWR_I2C_DEINIT           /* de-init i2c to save power */
    NGT_I2C_DeInit();
#endif

#ifdef ENABLE_PWR_DMA_DEINIT
    LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_7);  /* Disable DMA1 Tx Channel */
#endif
            
#ifdef ENABLE_PWR_UART_DEINIT
    UART_DeInit();  /* de-initialize UART to save power */
#endif
        
#ifdef ENABLE_PWR_ADC_DEINIT           /* de-init adc to save power */
    ADC_DeInit();
#endif
           
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, sWakeupIO, &iStopLevel);
#endif
}  /* NGT_Power_Save */


/**
  ******************************************************************************
  * @func   NGT_Power_Save2()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_Save2()
{
#ifdef ENABLE_PWR_I2C_DEINIT           /* de-init i2c to save power */
//    NGT_I2C_DeInit();
#endif

#ifdef ENABLE_PWR_ADC_DEINIT           /* de-init adc to save power */
//    ADC_DeInit();
#endif
           
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, sWakeupIO, &iStopLevel);
#endif
}  /* NGT_Power_Save2 */


/**
  ******************************************************************************
  * @func   NGT_Power_Save2_PD()
  * @brief  power saving mode: init gpio for pull down and call power manager
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void NGT_Power_Save2_PD()
{
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, sWakeupIO, &iStopLevel);
#endif
}  /* NGT_Power_Save2_PD */

/**
  ******************************************************************************
  * @func   VTimerTimeoutCB()
  * @brief  call back for vtimer, during normal for 2 second
  * @param  None
  * @gvar   bNormalTime_Expired
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void VTimerTimeoutCB(void *param)
{
    bNormalTime_Expired = TRUE;
}  /* VTimerTimeoutCB */ 


/**
  ******************************************************************************
  * @func   Normal_VTimer_init()
  * @brief  in activation mode: init vtimer and advance to normal mode
  * @param  None
  * @gvar   hNormalTimer, bNormalTime_Expired
  * @gfunc  HAL_VTIMER_StartTimerMs(), VTimerTimeoutCB()
  * @retval None
  ******************************************************************************
  */
void Normal_VTimer_init()
{
    int iStatus = FALSE;
    
    /* Start the Sensor Timer */
    hNormalTimer.callback = VTimerTimeoutCB;  
    iStatus = HAL_VTIMER_StartTimerMs( &hNormalTimer, NORMAL_2SEC_INTVAL_MS);
    if (iStatus == BLE_STATUS_SUCCESS)
    {
        bNormalTime_Expired = FALSE;
    } else {
//        error;
    }
}


/**
  ******************************************************************************
  * @func   Normal_PowerSave_PD()
  * @brief  In normal mode, power save with pull down, stop with timer
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void Normal_PowerSave_PD()
{
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = 0;
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_WITH_TIMER, sWakeupIO, &iStopLevel);
#endif
}  /* Normal_PowerSave_PD */

/**
  ******************************************************************************
  * @func   Normal_PowerSave_PD_NoT()
  * @brief  In normal mode, power save with pull down, stop with timer, NFC wakeup
  * @param  None
  * @gvar   sWakeupIO, iStopLevel
  * @gfunc  PortAB_PullDown_Init(), HAL_PWR_MNGR_Request()
  * @retval None
  ******************************************************************************
  */
void Normal_PowerSave_PD_NoT()
{
#ifdef ENABLE_PWR_DOWN_IO
    PortAB_PullDown_Init();
#endif
    
#ifdef ENABLE_PWR_DOWN_IO
    sWakeupIO.IO_Mask_High_polarity = WAKEUP_PB2;   /* NFC interrupt wake up */
//    sWakeupIO.IO_Mask_High_polarity = 0;
    sWakeupIO.IO_Mask_Low_polarity = 0;
    sWakeupIO.RTC_enable = 0;

    /* Power Save Request */
    HAL_PWR_MNGR_Request(POWER_SAVE_LEVEL_STOP_NOTIMER, sWakeupIO, &iStopLevel);
#endif
}  /* Normal_PowerSave_PD_NoT */

