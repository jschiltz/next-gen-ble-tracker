/**
  ******************************************************************************
  * @file    NG_Tick_LOL_CmdTbl.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link command tables.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
//#include "main.h"
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
//#include "bluenrg_lp.h"
//#include "bluenrg_lp_ll_bus.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_LMCmd.h"
#include "NG_Tick_LOL_GCmd.h"
#include "NG_Tick_LOL_CmdTbl.h"

/* constant variable declaration ---------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
__IO BOOL bLOL_1msF = FALSE;            /* LOL 1ms flag, set by 1ms interrupt, reset in main.c */

bool ms_time = FALSE;        // Not time to kick out the 1 mS tick yet
sendVals sendPacket;
ackTransactionVals ackVals;
ackTransactionVals slaveAckVals;

uint8_t LOLRXBuff0[CHAN0_RX_BUFF_SIZE];
uint8_t LOLTXBuff0[CHAN0_TX_BUFF_SIZE];
uint8_t LOLRXBuff1[CHAN1_RX_BUFF_SIZE];
uint8_t LOLTXBuff1[CHAN1_TX_BUFF_SIZE];

/* function prototype --------------------------------------------------------*/

/* constant variable declaration ------------------------------------------------------*/
const slaveVals slaveArray0[SLAVEARRAY0SIZE] =
{
    {*readMemMapVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, LOL_LM_MEMMAPVER},              // local memory - read Memory Map Version
    {*RWExtMemOffset, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_EXTMEMOS},                   // local memory - read/write Extended Memory Offset
    {*sendMPBIDChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_MPBID},          // local memory - read/write MPBID
    {*sendDevNameChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DEV_NAME},     // local memory - unique device name
    {*sendFwVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_FMREV},             // local memory - firmware version
    {*sendSWPNChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_SWPN},            // local memory - SW Part Number
    {*fLMDateOfMfg, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BOD},                // local memory - read/write BOD
    {*fLMDateOfService, eMM_Sec_Lev__USER, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DFO_ACT},       // local memory - read/write DOS, activation
    {*fLMDateLastUSed, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_DATE_LAST_USED}, // local memory - read data last used, tool last used
    {*fLMDateTimeRef, eMM_Sec_Lev__ALL, eMM_Sec_Lev__USER, eLOCAL_MEM_ACCESS, LOL_LM_DT_REF},                    // local memory - read/write data time reference
    {*fLMPlatformId, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, LOL_LM_PLATFORM_ID},                // local memory - read platform identifier
    {*fLMBLEMACAddr, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BLE_MAC_ADDR},      // local memory - read/write BLE MAC address
//    {*mailbox, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, 0xa02a},                                 // test for Mailbox
    {*passwordEnable, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, LOL_LM_PW_ENABLE},                 // local memory - Write Password Enable
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_USER_PW},   // Rlocal memory - Read/Write USER password
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_ADMIN_PW},  // local memory - Read/Write ADMIN password
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_SERVICE_PW},// local memory - Read/Write SERVICE password
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, LOL_LM_BLE_MAC_ADDR},  // local memory - Read/Write BLE MAC address
//    {*startMasterTest, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, 0x02, 0xFFFF},                          // Start the master testing, !@#$
    {*sendMaxPktLn, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eGET_MAX_PKT_LEN, 0xFFFF},                  // GET_MAX_PKT_LENGTH
    {*simpleAck, eMM_Sec_Lev__USER, eMM_Sec_Lev__USER, 0x70, 0xFFFF},                               // Simple way to test security level access
//    {*simpleAck, eMM_Sec_Lev__USER_ADMIN, eMM_Sec_Lev__SERVICE, 0x71, 0xFFFF},                      // Simple way to test security level access
    {*fLMMfgTestBuzz, eMM_Sec_Lev__NONE, eMM_Sec_Lev__MANUFACTURING, LOL_GC_MFG_TEST, LOL_GSC_MFG_TEST_BUZZ},  // generic command - manufacturing test
    {*simpleAck, eMM_Sec_Lev__SERVICE, eMM_Sec_Lev__SERVICE, 0x72, 0xFFFF},                         // Simple way to test security level access
    {*simpleAck, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__SERVICE, 0x73, 0xFFFF},                   // Simple way to test security level access
    {*slave5, eMM_Sec_Lev__ALL, eMM_Sec_Lev__USER_ADMIN, 0x7c, 0xFFFF}                              // Simple test
};

const slaveVals slaveArray1[SLAVEARRAY1SIZE] =
{
    {*readMemMapVersion, eMM_Sec_Lev__ALL, eMM_Sec_Lev__NONE, eLOCAL_MEM_ACCESS, 0x0000},           // test for returning Memory Map Version
    {*RWExtMemOffset, eMM_Sec_Lev__ALL, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, 0x0002},               // test for Extended Memory Offset
    {*sendMPBIDChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, 0x0004},   // test for reading/writing MPBID
    {*sendSWPNChannel, eMM_Sec_Lev__ALL, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, 0x0009},    // test for returning SW Part Number
    {*passwordEnable, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, eLOCAL_MEM_ACCESS, 0x03B},               // test for Password Enable
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, 0x0043},// Read/Write USER password
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, 0x004B},// Read/Write ADMIN password
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, 0x0053},// Read/Write SERVICE password
    {*setSecLevel, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__MANUFACTURING, eLOCAL_MEM_ACCESS, 0x005B},// Read/Write MANUFACTURING password
//    {*startMasterTest, eMM_Sec_Lev__NONE, eMM_Sec_Lev__ALL, 0x02, 0xFFFF},                          // Start the master testing, !@#$
    {*simpleAck, eMM_Sec_Lev__MANUFACTURING, eMM_Sec_Lev__SERVICE, 0x70, 0xFFFF},                   // Simple way to test security level access
    {*simpleAck, eMM_Sec_Lev__SERVICE, eMM_Sec_Lev__SERVICE, 0x71, 0xFFFF},                         // Simple way to test security level access
    {*simpleAck, eMM_Sec_Lev__USER_ADMIN, eMM_Sec_Lev__SERVICE, 0x72, 0xFFFF},                      // Simple way to test security level access
    {*simpleAck, eMM_Sec_Lev__USER, eMM_Sec_Lev__SERVICE, 0x73, 0xFFFF}                             // Simple way to test security level access
};

