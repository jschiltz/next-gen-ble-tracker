/**
  ******************************************************************************
  * @file    NG_Tick_LOL_LMCmd_T.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link local memory command - NGT specific.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_gpio.h"
#include "bluenrg_lp.h"
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_CmdVar.h"
#include "NG_Tick_FlashUtil.h"    /* must come before NG_Tick_sys.h */
#include "NG_Tick_NFC.h"          /* must come before NG_Tick_sys.h */
#include "NG_Tick_Acc.h"          /* must comes before sys.h */
#include "NG_Tick_sys.h"

#include "NG_Tick_app.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_LOL_Func.h"

/* constant variable declaration ---------------------------------------------*/

/* constant variable declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
NGT_SYSSTATUS_TYPE sSysStatus;
uint8_t aMiscStatus[32];

/* function prototype --------------------------------------------------------*/
void LOL_NGT_Init(void);
void xLoLMT_SysStatus(void);
void xLoLMT_ProductionStatus(void);
void fLMT_UnitSN( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_AssetIdCode( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_AssetIdCodePhone( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_BOCCV( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_UnitReset( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_OwnershipClaimKey( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_AttributeStream( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_AttributeFlashUtil( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_AttributeSysStatus( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_AttributeProdStatus( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);

/**
  ******************************************************************************
  * @func   LOL_Init_All()
  * @brief  initliaze LOL channel 0 & 1
  * @param  None
  * @gvar   uwNbRxChars, pBufferReadyForRx, iRxMsgCnt, ubRxBufferReadyF
  * @gfunc  LOLInitChannel()
  * @retval None
  ******************************************************************************
  */
void LOL_Init_All()
{
    LOLInitChannel( LOL_CHAN_UART, LOLTXBuff0, CHAN0_TX_BUFF_SIZE, LOLRXBuff0,
                   CHAN0_RX_BUFF_SIZE, slaveArray0, SLAVEARRAY0SIZE);
    LOLInitChannel( LOL_CHAN_BLE, LOLTXBuff1, CHAN1_TX_BUFF_SIZE, LOLRXBuff1,
                   CHAN1_RX_BUFF_SIZE, slaveArray1, SLAVEARRAY1SIZE);
    xLoLMT_SysStatus();
    bLOL_1msF = FALSE;
}  /* LOL_Init_All() */


/**
  ******************************************************************************
  * @func   xLoLMT_SysStatus()
  * @brief  update system status into the new LoL structure.
  * @param  None
  * @gvar   sSysStatus, SystemCB
  * @gfunc  None
  * @retval None
  * @note   Big Endian is used for 16 bit numbers (error counters)
  ******************************************************************************
  */
void xLoLMT_SysStatus()
{
#if 0
    sSysStatus.uSysStatus = 0x85;
    sSysStatus.uFlashErrCode = 0x01;
    sSysStatus.uFlashErrCnt[0] = 0x32;
    sSysStatus.uFlashErrCnt[1] = 0x10;
    sSysStatus.uNFCErrCode = 0x03;
    sSysStatus.uNFCErrCnt[0] = 0x54;
    sSysStatus.uNFCErrCnt[1] = 0x76;
    sSysStatus.uAccErrCode = 0x50;
    sSysStatus.uAccErrCnt[0] = 0xcd;
    sSysStatus.uAccErrCnt[1] = 0xab;
#endif
#if 1
    sSysStatus.uSysStatus = 0;
    if (SystemCB.FlashCB.uState != FLASHCB_ST_IDLE)
    {
        sSysStatus.uSysStatus |= SYSSTAT_FLASH_BUSY;
    }  /* if */
    if (SystemCB.NFCCB.uState != NFCCB_ST_IDLE)
    {
        sSysStatus.uSysStatus |= SYSSTAT_NFC_BUSY;
    }  /* if */
    if (SystemCB.AccCB.uState != ACCCB_ST_IDLE)
    {
        sSysStatus.uSysStatus |= SYSSTAT_ACC_BUSY;
    }  /* if */
    if (!SystemCB.NFCCB.uFound)
    {
        sSysStatus.uSysStatus |= SYSSTAT_NFC_NOTFOUND;
    }  /* if */
    if (!SystemCB.AccCB.uFound)
    {
        sSysStatus.uSysStatus |= SYSSTAT_ACC_NOTFOUND;
    }  /* if */
#endif
    sSysStatus.uFlashErrCode = SystemCB.FlashCB.uErrCode;
    sSysStatus.uFlashErrCnt[0] = (SystemCB.FlashCB.uwErrCnt & 0xff00) >> 8;  /* MSB */
    sSysStatus.uFlashErrCnt[1] = SystemCB.FlashCB.uwErrCnt & 0x00ff;         /* LSB */
    sSysStatus.uNFCErrCode = SystemCB.NFCCB.uErrCode;
    sSysStatus.uNFCErrCnt[0] = (SystemCB.NFCCB.uwErrCnt & 0xff00) >> 8;      /* MSB */
    sSysStatus.uNFCErrCnt[1] = SystemCB.NFCCB.uwErrCnt & 0x00ff;             /* LSB */
    sSysStatus.uAccErrCode = SystemCB.AccCB.uErrCode;
    sSysStatus.uAccErrCnt[0] = (SystemCB.AccCB.uwErrCnt & 0xff00) >> 8;      /* MSB */
    sSysStatus.uAccErrCnt[1] = SystemCB.AccCB.uwErrCnt & 0x00ff;             /* LSB */
}  /* xLoLMT_SysStatus() */


/**
  ******************************************************************************
  * @func   xLoLMT_ProductionStatus()
  * @brief  update production status into the new LoL structure.
  * @param  None
  * @gvar   sSysStatus, SystemCB
  * @gfunc  None
  * @retval None
  * @note   
  ******************************************************************************
  */
void xLoLMT_ProductionStatus()
{
    sSysStatus.uSysStatus = 0;
    if (SystemCB.FlashCB.uState != FLASHCB_ST_IDLE)
    {
        sSysStatus.uSysStatus |= SYSSTAT_FLASH_BUSY;
    }  /* if */
    if (SystemCB.NFCCB.uState != NFCCB_ST_IDLE)
    {
        sSysStatus.uSysStatus |= SYSSTAT_NFC_BUSY;
    }  /* if */
    if (SystemCB.AccCB.uState != ACCCB_ST_IDLE)
    {
        sSysStatus.uSysStatus |= SYSSTAT_ACC_BUSY;
    }  /* if */
    if (!SystemCB.NFCCB.uFound)
    {
        sSysStatus.uSysStatus |= SYSSTAT_NFC_NOTFOUND;
    }  /* if */
    if (!SystemCB.AccCB.uFound)
    {
        sSysStatus.uSysStatus |= SYSSTAT_ACC_NOTFOUND;
    }  /* if */
}  /* xLoLMT_ProductionStatus() */


extern __IO int32_t iSys_1SecCnt;  /* 1 second counter */
extern uint16_t uiApp_SecCnt;      /* 1 second counter, based on 1ms */
extern uint8_t iArrayNFCMemory[];  /* write buffer */
/**
  ******************************************************************************
  * @func   xLoLMT_MiscStatus()
  * @brief  update misc status into the new LoL structure.
  * @param  None
  * @gvar   aMiscStatus[]
  * @gfunc  None
  * @retval None
  * @note   
  ******************************************************************************
  */
void xLoLMT_MiscStatus()
{
    aMiscStatus[0] = (iAccInt1Cnt & 0xff00) >> 8;       /* MSB */
    aMiscStatus[1] = iAccInt1Cnt & 0x00ff;              /* LSB */
//    aMiscStatus[2] = (iAccSecCnt & 0xff00) >> 8;        /* MSB */
//    aMiscStatus[3] = iAccSecCnt & 0x00ff;               /* LSB */
    aMiscStatus[2] = SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_CCV];
    aMiscStatus[3] = SystemIB.SysRAMIB.uCoinCellV;
    
    aMiscStatus[4] = (uint8_t)SystemIB.SysRAMIB.uNFCBuzzerEvents;
    aMiscStatus[5] = (uint8_t)SystemIB.SysRAMIB.uNFCEvents;
    aMiscStatus[6] = (uint8_t)SystemIB.SysRAMIB.uNFCResetEvents;
    aMiscStatus[7] = SystemIB.SysFlashIB.uShipModeStatus;   /* ship mode status */
//    aMiscStatus[8] = (uint8_t)((uiApp_SecCnt & 0xff00) >> 8);   /* MSB */
//    aMiscStatus[9] = (uint8_t)(uiApp_SecCnt & 0x00ff);          /* LSB */
//    aMiscStatus[10] = (uint8_t)((iSys_1SecCnt & 0x0000ff00) >> 8);      /* b15:b8 */
//    aMiscStatus[11] = (uint8_t)(iSys_1SecCnt & 0x000000ff);             /* b7:b0 */
    aMiscStatus[8] = (uint8_t)((SystemIB.SysRAMIB.uPORStatus & 0x0ff00) >> 8);   /* MSB */
    aMiscStatus[9] = (uint8_t)(SystemIB.SysRAMIB.uPORStatus & 0x000ff);          /* LSB */
    aMiscStatus[10] = (uint8_t)((SystemIB.SysRAMIB.uNFCResetEvents & 0x0000ff00) >> 8);      /* b15:b8 */
    aMiscStatus[11] = (uint8_t)(SystemIB.SysRAMIB.uNFCResetEvents & 0x000000ff);             /* b7:b0 */
    memcpy( &aMiscStatus[12], &iArrayNFCMemory[NFC_RMOS_BUZZEV], NFC_DIG_BUZZEV);
    memcpy( &aMiscStatus[16], &iArrayNFCMemory[NFC_RMOS_NFCEV], NFC_DIG_NFCEV);
    memcpy( &aMiscStatus[20], &iArrayNFCMemory[NFC_RMOS_RSTEV], NFC_DIG_RSTEV);
}  /* xLoLMT_MiscStatus() */


/**
  ******************************************************************************
  * @func   fLMT_UnitSN()
  * @brief  Read/Write local memory - unit serial number
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_UnitSN(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write, prep ptr to update internal value */
        if ((SYS_LOL_USN_SIZE + LOL_LMADDR_SIZE) == payloadLength)
        {
            uint8_t *destPtr = SystemIB.SysFlashIB.aUnitSN;
            uint8_t *srcPtr = &payloadPtr[2];
            int16_t iCnt = SYS_LOL_USN_SIZE;
            
            while (iCnt-- > 0)
            {
                *destPtr++ = *srcPtr++;
            }  /* while */
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */
         }
        else
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read unit SN, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.SysFlashIB.aUnitSN;
        slaveAckVals.payloadLength = SYS_LOL_USN_SIZE;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_UnitSN() */


/**
  ******************************************************************************
  * @func   fLMT_AssetIdCode()
  * @brief  Read/Write local memory - asset code, 5 - 8 bytes
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AssetIdCode(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write, prep ptr to update internal value */
        if ((sizeof(uint64_t) + 2) == payloadLength)
        {
            uint8_t *srcPtr = &payloadPtr[2];
            int8_t iCnt = payloadLength - 2;
            
            EndianSwapRead( srcPtr, iCnt);
            SystemIB.SysFlashIB.aAssetIdCode = unDataSwap64.uiVal;
            
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */
         }
        else
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read asset code password, dynamic length, prep ptr to send internal value*/
        EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.aAssetIdCode);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData64;  /* outgoing variable address */
        slaveAckVals.payloadLength = (sizeof(uint64_t));
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AssetIdCode() */


/**
  ******************************************************************************
  * @func   fLMT_AssetIdCodePhone()
  * @brief  Read/Write local memory - asset id code phone, 5 - 8 bytes
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AssetIdCodePhone(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write, prep ptr to update internal value */
        if ((sizeof(uint64_t) + 2) == payloadLength)
        {
            uint8_t *srcPtr = &payloadPtr[2];
            int8_t iCnt = payloadLength - 2;
            
            EndianSwapRead( srcPtr, iCnt);
            SystemIB.SysFlashIB.aAssetIdCodePhone = unDataSwap64.uiVal;

            if (SystemIB.SysFlashIB.aAssetIdCodePhone == SystemIB.SysFlashIB.aAssetIdCode)
            {
#if 1
                if ((AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State) && (sAudioEngine.uiAE_NewCmd != TRUE))
                {
                    sAudioEngine.uiAE_NewSongId = SONG_INDEX_SUCCESS;
//                    sAudioEngine.uiAE_NewSongId = SONG_INDEX_B2;
                    sAudioEngine.uiAE_NewCmd = TRUE;
                    SystemIB.SysRAMIB.uNFCBuzzerEvents++;  //^&*(
                }  /* if ae */
#endif
                /* if in ship mode, then activate.  Otherwise, do nothing */
                if (APPCB_ST_SHIPMODE == sAppCB.uState)
                {
                    sAppCB.uAction = APPCB_ACT_ACTIVATE;  /* go to activation mode */
                }  /* if */
                
                /* read owership claim key, prep ptr to send internal value*/
                EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.aOwnerClaimKey);  /* convert to big endian */
                slaveAckVals.data = (uint8_t *)&uiESwapData64;  /* outgoing variable address */
                slaveAckVals.payloadLength = LOL_OWN_CLAIM_K_PAYLOAD_LEN;
            }  /* if */
            else
            {
                // NAK it!!
                returnPayload[0] = NAKMAXMESSAGE;
                returnPayload[1] = NAKMALFORMEDPACKET;
                slaveAckVals.ackNack = FALSE;
                slaveAckVals.data = returnPayload;
                slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
            }  /* if else */
         }
        else
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read asset code password, dynamic length, prep ptr to send internal value*/
        EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.aAssetIdCodePhone);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData64;  /* outgoing variable address */
        slaveAckVals.payloadLength = (sizeof(uint64_t));
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AssetIdCodePhone() */


/**
  ******************************************************************************
  * @func   fLMT_BOCCV()
  * @brief  Read/Write local memory - born on coin cell voltage
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_BOCCV(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write hardware PCBA version, prep ptr to update internal value */
        if ((LOL_BOCCV_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)  /* 3 */
        {
            SystemIB.SysRAMIB.uCoinCellV = payloadPtr[2];
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }
        else
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read born on coin cell voltage, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.SysRAMIB.uCoinCellV;
        slaveAckVals.payloadLength = LOL_BOCCV_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_BOCCV() */

/**
  ******************************************************************************
  * @func   fLMT_UnitReset()
  * @brief  Write local memory - unit reset, 1 byte
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_UnitReset(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write, prep ptr to update internal value */
        if ((LOL_URST_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)
        {
            SystemIB.SysRAMIB.uUnitReset =  payloadPtr[2];
//!@#$ insert code here to compare reset password, ack if compare =, nack if !=
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
         }
    }  /* if */
    
    if (FALSE == slaveAckVals.ackNack)
    {   
        // NAK it!!
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.ackNack = FALSE;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_UnitReset() */



/**
  ******************************************************************************
  * @func   fLMT_OwnershipClaimKey()
  * @brief  Write local memory - ownership claim key
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_OwnershipClaimKey(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write hardware PCBA version, prep ptr to update internal value */
        if ((sizeof(uint64_t) + 2) == payloadLength)
        {
            uint8_t *srcPtr = &payloadPtr[2];
            int8_t iCnt = payloadLength - 2;
            
            EndianSwapRead( srcPtr, iCnt);
            SystemIB.SysFlashIB.aOwnerClaimKey = unDataSwap64.uiVal;

            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */
        }
        else
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read owership claim key, prep ptr to send internal value*/
        EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.aOwnerClaimKey);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData64;  /* outgoing variable address */
        slaveAckVals.payloadLength = LOL_OWN_CLAIM_K_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_OwnershipClaimKey() */


/**
  ******************************************************************************
  * @func   fLMT_AttributeStream()
  * @brief  Write local memory - attribute stream - 0: disable, 1: enable
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AttributeStream(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write attribute stream, prep ptr to update internal value */
        if ((LOL_ATT_STREAM_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)
        {
            SystemIB.SysRAMIB.uAttributeStream = payloadPtr[2];
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }
    }  /* if SID */
    
    if (FALSE == slaveAckVals.ackNack)
    {   
        /* write only, NAK it */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AttributeStream() */


/**
  ******************************************************************************
  * @func   fLMT_AttributeFlashUtil()
  * @brief  Write local memory - attribute flash erase, read, write
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AttributeFlashUtil(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        uStatus = xLoLMT_FlashUtil(payloadPtr);
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AttributeFlashUtil() */

/**
  ******************************************************************************
  * @func   fLMT_AttributeSysStatus()
  * @brief  Read local memory - attribute system status read
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AttributeSysStatus(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        xLoLMT_SysStatus();
        uStatus = LOL_RD_RESP;
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&sSysStatus;  /* status */
            slaveAckVals.payloadLength = sizeof(NGT_SYSSTATUS_TYPE);  /* ? */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AttributeSysStatus() */


/**
  ******************************************************************************
  * @func   fLMT_AttributeNFC()
  * @brief  NFC memory operations: read, write NDEF, write dynamic and wipe
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, msg_NFT_Test_UM_Rd
  * @gfunc  LOLSendACK(), xLoLMT_NFCRead(), xLoLMT_NFCWrite()
  * @retval None
  ******************************************************************************
  */
void fLMT_AttributeNFC(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    /* check for i2c token */
    if (SystemIB.SysRAMIB.uI2CToken != I2C_TOKEN_NFC)
    {
        SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_NFC;
        
        if (SystemIB.SysFlashIB.uShipModeStatus != PRODUCTION_MODE)
        {
            /* turn on NFC power, add delay before dynamic write */
            LL_GPIO_SetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
            xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
        }  /* if */

        SearchNFC();
    }  /* if */
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        uStatus = xLoLMT_NFCRead(payloadPtr);
    }
    else
    {
        uStatus = xLoLMT_NFCWrite(payloadPtr);
    }  /* if SID */

    if (SystemIB.SysFlashIB.uShipModeStatus != PRODUCTION_MODE)
    {
        /* turn on NFC power, add delay before dynamic write */
        LL_GPIO_ResetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
#ifdef ENABLE_NFC_DELAY
        xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    }
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)msg_NFT_Test_UM_Rd;  /* status */
            slaveAckVals.payloadLength = payloadPtr[3];  /* ? */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AttributeNFC() */


/**
  ******************************************************************************
  * @func   fLMT_AttributeProdStatus()
  * @brief  Read local memory - attribute production status read
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AttributeProdStatus(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        xLoLMT_ProductionStatus();
        uStatus = LOL_RD_RESP;
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&sSysStatus;  /* status */
            slaveAckVals.payloadLength = LOL_ATT_PRODSTAT_PAYLOAD_LEN;  /* 1 */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AttributeProdStatus() */




/**
  ******************************************************************************
  * @func   fLMT_AttributeMiscStatus()
  * @brief  Read local memory - attribute misc status read
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMT_AttributeMiscStatus(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        xLoLMT_MiscStatus();
        uStatus = LOL_RD_RESP;
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)&aMiscStatus;  /* status */
//            slaveAckVals.payloadLength = 24;  //LOL_ATT_PRODSTAT_PAYLOAD_LEN;  /* 1 */
            slaveAckVals.payloadLength = 12;  //LOL_ATT_PRODSTAT_PAYLOAD_LEN;  /* 1 */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_AttributeMiscStatus() */



