
/******************** (C) COPYRIGHT 2019 STMicroelectronics ********************
* File Name          : BLE_Beacon_main.c
* Author             : RF Application Team + METCO: Johnny Lienau
* Version            : 1.0.0
* Date               : 10-January-2019
* Description        : Code demonstrating the Bluetooth LE Beacon application
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/


/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "bluenrg_lp_it.h"
#include "ble_const.h"
#include "bluenrg_lp_stack.h"
#include "Beacon_config.h"
#include "OTA_btl.h"
#include "bluenrg_lp_hal_power_manager.h"
#include "bluenrg_lp_hal_vtimer.h"
#include "bluenrg_lp_evb_com.h"
#include "bleplat.h"
#include "nvm_db.h"
#include "bluenrg_lp_api.h"
#include "NG_Tick_BLE.h"
#include "NG_Tick_uart.h"
#include "app_state.h"
#include "gap_profile.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_FlashUtil.h"
#include "NG_Tick_NFC.h"
#include "NG_Tick_Acc.h"
#include "NG_Tick_sys.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Set to 1 to enable the service  */
#define DEVICE_INFO_SERVICE    1
#define BATTERY_SERVICE        0
#define NAME_IN_ADVERTISEMENT  0

/* PHY used in extended advertising events. One between: LE_1M_PHY,
  LE_2M_PHY and LE_CODED_PHY.  */
#define EXT_ADV_PHY LE_2M_PHY

/*Advertising interval in ms*/
#define ADV_INTERVAL 4800    // Interval is in 625us ticks
//#define ADV_INTERVAL 400   // Interval is in 625us ticks for 250ms
//#define ADV_INTERVAL 800   // Interval is in 625us ticks for 500ms

/*  UUID'S  */
#if(DEVICE_INFO_SERVICE)
#define DEVICE_INFORMATION_SERVICE_UUID   0x180A
#define MANUFACTURER_NAME_UUID            0x2A29
#define MODEL_NUMBER_UUID                 0x2A24
#define SERIAL_NUMBER_UUID                0x2A25
#endif

#define ONE_KEY_SERVICE_UUID        0xFDF5
#define OPEN_LINK_DATA_UUID         0x66,0xFC,0xC4,0x83,0x5E,0x24,0xA7,0x9F,0xD9,0x44,0x04,0x0D,0x7E,0xB4,0xC7,0x80
#define FIRMWARE_REVISION_UUID      0x2A26
#define SECURITY_IV_UUID            0x65,0x01,0x9B,0x5F,0x80,0x00,0x00,0x80,0x00,0x10,0x00,0x00,0x5F,0xFD,0x20,0x00      
#define SECURITY_CONTROL_UUID       0x65,0x01,0x9B,0x5F,0x80,0x00,0x00,0x80,0x00,0x10,0x00,0x00,0x5F,0xFD,0x21,0x00

#if(BATTERY_SERVICE)
#define BATTERY_SERVICE_UUID        0x180F
#define BATTERY_LEVEL_UUID          0x2A19
#define PRESENTATION_FORMAT_UUID    0x2904
#define CHARACTERISTIC_CONFIG_UUID  0x2901      
#endif

#define OTA_SERVICE_UUID   0xF0,0x19,0x21,0xB4,0x47,0x8F,0xA4,0xBF,0xA1,0x4F,0x63,0xFD,0xEE,0xD6,0x14,0x1D  
#define OTA_CTRL_UUID      0x63,0x60,0x32,0xE0,0x37,0x5E,0xA4,0x88,0x53,0x4E,0x6D,0xFB,0x64,0x35,0xBF,0xF7


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Handles for characteristice */
uint16_t OTACharHandle;
uint16_t OpenLinkCharHandle;

uint16_t connection_handle = 0;
uint16_t connection_timeout_ticks = 0;

volatile int app_flags = SET_CONNECTABLE;
volatile uint16_t connection_interval = 0;

/* Set AD Type Flags at beginning on Advertising packet  */
static uint8_t adv_data[] = {
    /* Advertising data: Flags AD Type */
    0x02,  /* Length of field */
    AD_TYPE_FLAGS, 
    FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE | FLAG_BIT_BR_EDR_NOT_SUPPORTED,

    0x03,  /* Length of field */
    AD_TYPE_16_BIT_SERV_UUID_CMPLT_LIST,
    ONE_KEY_UUID_LOW,
    ONE_KEY_UUID_HIGH,
    
    /* Advertising data: manufacturer specific data */
    NORMAL_MODE_ADVERTISEMENT_LENGTH,  /* Length of field */
    AD_TYPE_MANUFACTURER_SPECIFIC_DATA,  //manufacturer type
    COMPANY_ID_LOW,  /* Company identifier code LOW */
    COMPANY_ID_HIGH, /* Company identifier code HIGH */
    DEFAULT_MPBID_0,       /* MPBID 0 adv_data[11] */
    DEFAULT_MPBID_1,       /* MPBID 1 adv_data[12] */
    DEFAULT_MPBID_2,       /* MPBID 2 adv_data[13] */
    DEFAULT_MPBID_3,       /* MPBID 3 adv_data[14] */
    DEFAULT_MPBID_4,       /* MPBID 4 adv_data[15] */
    DEFAULT_COIN_CELL_ADVERTISING,       /* Coin cell Voltage adv_data[16] */
    (FIRMWARE_VERSION_ID_ADVERTISING >> 1),  /* Firmware version ID adv_data[17] */
    // Tool generated
    ((FIRMWARE_VERSION_ID_ADVERTISING & 0x01) << 7) | 0x7F, 
    0xFF, 0xFF, 0xFF, 
    0xFF, 0xFF, 0xFF,
};

static uint8_t adv_data_ship[] = {
    /* Advertising data: Flags AD Type */
    0x02,  /* Length of field */
    AD_TYPE_FLAGS, 
    FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE | FLAG_BIT_BR_EDR_NOT_SUPPORTED,

    0x03,  /* Length of field */
    AD_TYPE_16_BIT_SERV_UUID_CMPLT_LIST,
    ONE_KEY_UUID_LOW,
    ONE_KEY_UUID_HIGH,
    
    /* Advertising data: manufacturer specific data */
    SHIP_MODE_ADVERTISEMENT_LENGTH,  /* Length of field */
    AD_TYPE_MANUFACTURER_SPECIFIC_DATA,  //manufacturer type
    COMPANY_ID_LOW,  /* Company identifier code LOW */
    COMPANY_ID_HIGH, /* Company identifier code HIGH */
    DEFAULT_MPBID_0,       /* MPBID 0 adv_data[11] */
    DEFAULT_MPBID_1,       /* MPBID 1 adv_data[12] */
    DEFAULT_MPBID_2,       /* MPBID 2 adv_data[13] */
    DEFAULT_MPBID_3,       /* MPBID 3 adv_data[14] */
    DEFAULT_MPBID_4,       /* MPBID 4 adv_data[15] */
};

static uint8_t ibeacon1_adv_data[] = 
{
    /* Advertising data: Flags AD Type */
    0x02,  /* Length of field */
    AD_TYPE_FLAGS, 
    FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE | FLAG_BIT_BR_EDR_NOT_SUPPORTED,
  
    /* Advertising data: manufacturer specific data */
    26, //len
    AD_TYPE_MANUFACTURER_SPECIFIC_DATA,  //manufacturer type
    COMPANY_ID_LOW,  /* Company identifier code LOW */
    COMPANY_ID_HIGH, /* Company identifier code HIGH */
    0x02,       // ID
    0x15,       //Length of the remaining payload
    0xf3, 0x4e, 0xba, 0xC4, 0x7C, 0xD7, 0x40, 0x27, //Location UUID
    0x87, 0x8E, 0x55, 0xF4, 0xE7, 0x1D, 0x03, 0x09,
    0x00, 0x01, // Major number 
    0x00, 0x01, // Minor number 
    (uint8_t)-56,         // Tx power measured at 1 m of distance (in dBm)
};

static uint8_t ibeacon2_adv_data[] = 
{
    /* Advertising data: Flags AD Type */
    0x02,  /* Length of field */
    AD_TYPE_FLAGS, 
    FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE | FLAG_BIT_BR_EDR_NOT_SUPPORTED,
  
    /* Advertising data: manufacturer specific data */
    26, //len
    AD_TYPE_MANUFACTURER_SPECIFIC_DATA,  //manufacturer type
    COMPANY_ID_LOW,  /* Company identifier code LOW */
    COMPANY_ID_HIGH, /* Company identifier code HIGH */
    0x02,       // ID
    0x15,       //Length of the remaining payload
    0xF4, 0x6D, 0xF5, 0xA7, 0x63, 0xE6, 0x43, 0xAB, //Location UUID
    0x8D, 0xBA, 0xFE, 0x65, 0x2A, 0x72, 0x12, 0x39,
    0x00, 0x01, // Major number 
    0x00, 0x01, // Minor number 
    (uint8_t)-56,         // Tx power measured at 1 m of distance (in dBm)
};

static uint8_t ibeacon3_adv_data[] = 
{
    /* Advertising data: Flags AD Type */
    0x02,  /* Length of field */
    AD_TYPE_FLAGS, 
    FLAG_BIT_LE_GENERAL_DISCOVERABLE_MODE | FLAG_BIT_BR_EDR_NOT_SUPPORTED,
  
    /* Advertising data: manufacturer specific data */
    26, //len
    AD_TYPE_MANUFACTURER_SPECIFIC_DATA,  //manufacturer type
    COMPANY_ID_LOW,  /* Company identifier code LOW */
    COMPANY_ID_HIGH, /* Company identifier code HIGH */
    0x02,       // ID
    0x15,       //Length of the remaining payload
    0xF5, 0x52, 0x3C, 0xE8, 0x68, 0xF7, 0x4B, 0xC0, //Location UUID
    0x9F, 0x1A, 0x47, 0x25, 0x68, 0x5E, 0xC7, 0x7E,
    0x00, 0x01, // Major number 
    0x00, 0x01, // Minor number 
    (uint8_t)-56,         // Tx power measured at 1 m of distance (in dBm)
};

#define SIZE_OF_ADV_DATA_BASE 25

#define HIGH_NIBBLE   1
#define LOW_NIBBLE    2

#if(DEVICE_INFO_SERVICE)

static const uint8_t default_manufacturer_name[15] = {'M','i','l','w','a','u','k','e','e',' ','T','o','o','l'};
static const uint8_t default_model_number[11] = {'B','l','u','e','N','R','G','-','L','P'};

static uint8_t manufacturer_name_buff[15];
static const ble_gatt_val_buffer_def_t manufacturer_name_val_buff = {
    .buffer_len = 15,
    .val_len = 15,
    .op_flags = BLE_GATT_SRV_OP_VALUE_VAR_LENGTH_FLAG | BLE_GATT_SRV_OP_MODIFIED_EVT_ENABLE_FLAG,
    .buffer_p = manufacturer_name_buff,
};

static uint8_t model_number_buff[11];
static const ble_gatt_val_buffer_def_t model_number_val_buff = {
    .buffer_len = 11,
    .val_len = 11,
    .op_flags = BLE_GATT_SRV_OP_VALUE_VAR_LENGTH_FLAG | BLE_GATT_SRV_OP_MODIFIED_EVT_ENABLE_FLAG,
    .buffer_p = model_number_buff,
};

static uint8_t serial_number_buff[15];
static const ble_gatt_val_buffer_def_t serial_number_val_buff = {
    .buffer_len = 15,
    .val_len = 15,
    .op_flags = BLE_GATT_SRV_OP_VALUE_VAR_LENGTH_FLAG | BLE_GATT_SRV_OP_MODIFIED_EVT_ENABLE_FLAG,
    .buffer_p = serial_number_buff,
};

/* Device Information characteristics definition */
static const ble_gatt_chr_def_t device_information_chars[] = {
    {      
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 15u,
        .uuid = BLE_UUID_INIT_16(MANUFACTURER_NAME_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&manufacturer_name_val_buff,
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 10u,
        .uuid = BLE_UUID_INIT_16(MODEL_NUMBER_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&model_number_val_buff,
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 9u,
        .uuid = BLE_UUID_INIT_16(SERIAL_NUMBER_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&serial_number_val_buff,
    },
};

/* Device Information Service definition */
static const ble_gatt_srv_def_t device_information_service = {
   .type = BLE_GATT_SRV_PRIMARY_SRV_TYPE,
   .uuid = BLE_UUID_INIT_16(DEVICE_INFORMATION_SERVICE_UUID),
   .chrs = {
       .chrs_p = (ble_gatt_chr_def_t *)device_information_chars,
       .chr_count = 3U,
   },
};
#endif

#if(DEVICE_INFO_SERVICE)
static uint8_t default_firmware_revision[8] = {'0','0','0','0','0','0','0','0'};
#endif

static uint8_t open_link_data_buff[20];

static uint8_t firmware_revision_buff[8];
static const ble_gatt_val_buffer_def_t firmware_revision_val_buff = {
    .buffer_len = 8,
    .val_len = 8,
    .op_flags = BLE_GATT_SRV_OP_VALUE_VAR_LENGTH_FLAG | BLE_GATT_SRV_OP_MODIFIED_EVT_ENABLE_FLAG,
    .buffer_p = firmware_revision_buff,
};

static uint8_t security_iv_buff[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
static const ble_gatt_val_buffer_def_t security_iv_val_buff = {
    .buffer_len = 16,
    .buffer_p = security_iv_buff,
};

static uint8_t security_ctrl_buff[4] = {0x00,0x00,0x00,0x00};
static const ble_gatt_val_buffer_def_t security_ctrl_val_buff = {
    .buffer_len = 4,
    .buffer_p = security_ctrl_buff,
};

BLE_GATT_SRV_CCCD_DECLARE(openlink_chr_srv_changed,
                          NUM_LINKS,
                          BLE_GATT_SRV_CCCD_PERM_DEFAULT,
                          BLE_GATT_SRV_OP_MODIFIED_EVT_ENABLE_FLAG);

/* One Key characteristics definition */
static const ble_gatt_chr_def_t open_link_chars[] = {
    {      
        .properties = (BLE_GATT_SRV_CHAR_PROP_NOTIFY | BLE_GATT_SRV_CHAR_PROP_WRITE_NO_RESP),
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 20u,
        .uuid = BLE_UUID_INIT_128(OPEN_LINK_DATA_UUID),
        .descrs = 
        {
            .descrs_p = &BLE_GATT_SRV_CCCD_DEF_NAME(openlink_chr_srv_changed),
            .descr_count = 1U,
        },
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 8u,
        .uuid = BLE_UUID_INIT_16(FIRMWARE_REVISION_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&firmware_revision_val_buff,
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_WRITE,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 16u,
        .uuid = BLE_UUID_INIT_128(SECURITY_IV_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&security_iv_val_buff,
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 4u,
        .uuid = BLE_UUID_INIT_128(SECURITY_CONTROL_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&security_ctrl_val_buff,
    },
};

/* Metco Open Link Service definition */
static const ble_gatt_srv_def_t open_link_service = {
   .type = BLE_GATT_SRV_PRIMARY_SRV_TYPE,
   .uuid = BLE_UUID_INIT_16(ONE_KEY_SERVICE_UUID),
   .chrs = {
       .chrs_p = (ble_gatt_chr_def_t *)open_link_chars,
       .chr_count = 4U,
   },
};

#if(BATTERY_SERVICE)

static uint8_t battery_level_buff[1] = {100};
static const ble_gatt_val_buffer_def_t battery_level_val_buff = {
    .buffer_len = 1,
    .buffer_p = battery_level_buff,
};

static uint8_t presentation_format_buff[7];
static const ble_gatt_val_buffer_def_t presentation_format_val_buff = {
    .buffer_len = 7,
    .val_len = 7,
    .op_flags = BLE_GATT_SRV_OP_VALUE_VAR_LENGTH_FLAG | BLE_GATT_SRV_OP_MODIFIED_EVT_ENABLE_FLAG,
    .buffer_p = presentation_format_buff,
};

static uint8_t characteristic_config_buff[2];
static const ble_gatt_val_buffer_def_t characteristic_config_val_buff = {
    .buffer_len = 2,
    .buffer_p = characteristic_config_buff,
};


/* Battery Service characteristics definition */
static const ble_gatt_chr_def_t battery_chars[] = {
    {      
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 1u,
        .uuid = BLE_UUID_INIT_16(BATTERY_LEVEL_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&battery_level_val_buff,
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_READ,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 7u,
        .uuid = BLE_UUID_INIT_16(PRESENTATION_FORMAT_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&presentation_format_val_buff,
    },
    {
        .properties = BLE_GATT_SRV_CHAR_PROP_READ | BLE_GATT_SRV_CHAR_PROP_WRITE,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 2u,
        .uuid = BLE_UUID_INIT_16(CHARACTERISTIC_CONFIG_UUID),
        .val_buffer_p = (ble_gatt_val_buffer_def_t *)&characteristic_config_val_buff,
    },
};

/* Battery Service definition */
static const ble_gatt_srv_def_t battery_service = {
   .type = BLE_GATT_SRV_PRIMARY_SRV_TYPE,
   .uuid = BLE_UUID_INIT_16(BATTERY_SERVICE_UUID),
   .chrs = {
       .chrs_p = (ble_gatt_chr_def_t *)battery_chars,
       .chr_count = 3U,
   },
};
#endif

#if(CONFIG_OTA_USE_SERVICE_MANAGER)   
  static uint8_t ota_control_buff[8] = {0,0,0,0,0,0,0,0};
#endif

/* OTA Service characteristics definition */
static const ble_gatt_chr_def_t ota_chars[] = {
    {      
        .properties = BLE_GATT_SRV_CHAR_PROP_WRITE,
        .permissions = BLE_GATT_SRV_PERM_NONE,
        .min_key_size = 4u,
        .uuid = BLE_UUID_INIT_128(OTA_CTRL_UUID),
    },
};

/* OTA Service definition */
static const ble_gatt_srv_def_t ota_service = {
   .type = BLE_GATT_SRV_PRIMARY_SRV_TYPE,
   .uuid = BLE_UUID_INIT_128(OTA_SERVICE_UUID),
   .chrs = {
       .chrs_p = (ble_gatt_chr_def_t *)ota_chars,
       .chr_count = 1U,
   },
};

NO_INIT(uint32_t dyn_alloc_a[DYNAMIC_MEMORY_SIZE>>2]);

/* Private function prototypes -----------------------------------------------*/
#if(DEVICE_INFO_SERVICE)
static uint8_t Add_Device_Information_Service(void);
static uint8_t convert_byte_to_ASCII( uint8_t byte, uint8_t nibble);
#endif

#if(BATTERY_SERVICE)
static uint8_t Add_Battery_Service(void);
#endif

static uint8_t Add_OTA_Service(void);
static uint8_t Add_Open_Link_Service(void);

void BLE_Make_Connection(void); /* Reset the connection */

extern void uartRxCallback(uint8_t *, uint8_t, uint8_t);  /* LOLPacketReceive.c */

/* Private functions ---------------------------------------------------------*/

/**
  ******************************************************************************
  * @func   BLE_ModulesInit()
  * @brief  
  * @param  None
  * @gvar   
  * @gfunc  LL_AHB_EnableClock(), BLE_STACK_Init(), HAL_VTIMER_Init(), BLEPLAT_Init()
  * @retval None
  ******************************************************************************
  */
void BLE_ModulesInit(void)
{
    uint8_t ret;
    
    BLE_STACK_InitTypeDef BLE_STACK_InitParams = BLE_STACK_INIT_PARAMETERS;
    
    LL_AHB_EnableClock(LL_AHB_PERIPH_PKA|LL_AHB_PERIPH_RNG);
    
    /* BlueNRG-LP stack init */
    ret = BLE_STACK_Init(&BLE_STACK_InitParams);
    if (ret != BLE_STATUS_SUCCESS)
    {
#ifdef ENABLE_RND_MSG
        printf("Error in BLE_STACK_Init() 0x%02x\r\n", ret);
#endif
        while(1);
    }
    
    HAL_VTIMER_InitType VTIMER_InitStruct = {HS_STARTUP_TIME, INITIAL_CALIBRATION, CALIBRATION_INTERVAL};
    HAL_VTIMER_Init(&VTIMER_InitStruct);
    
    BLEPLAT_Init();  
}  /* BLE_ModulesInit() */

/**
  ******************************************************************************
  * @func   BLE_ModulesTick()
  * @brief  
  * @param  None
  * @gvar   None
  * @gfunc  HAL_VTIMER_Tick(), BLE_STACK_Tick(), NVMDB_Tick()
  * @retval None
  ******************************************************************************
  */
void BLE_ModulesTick(void)
{
    HAL_VTIMER_Tick();  /* Timer tick */
    BLE_STACK_Tick();   /* Bluetooth stack tick */
    NVMDB_Tick();       /* NVM manager tick */

    if(APP_FLAG(SET_CONNECTABLE))
    {
        BLE_Make_Connection();
        APP_FLAG_CLEAR(SET_CONNECTABLE);
    }

    if(APP_FLAG(OPENLINK_DATA_RX))
    {
        uartRxCallback( &open_link_data_buff[1], LOL_CHAN_BLE, open_link_data_buff[0]);
        APP_FLAG_CLEAR(OPENLINK_DATA_RX);
    }
}  /* BLE_ModulesTick () */

uint8_t mac_address[CONFIG_DATA_PUBADDR_LEN] = {0};
uint8_t mac_length = CONFIG_DATA_PUBADDR_LEN;
uint8_t mac_address_rev[CONFIG_DATA_PUBADDR_LEN] = {0};

/**
  ******************************************************************************
  * @func   BLE_Device_Init()
  * @brief  BLE device init
  * @param  None
  * @gvar   
  * @gfunc  aci_hal_write_config_data(), aci_hal_set_tx_power_level(), aci_gatt_srv_init(),
  *         aci_gap_init()
  * @retval None
  ******************************************************************************
  */
void BLE_Device_Init(void)
{
    uint8_t ret;
    uint16_t service_handle;
    uint16_t dev_name_char_handle;
    uint16_t appearance_char_handle;
    
#if(DEVICE_INFO_SERVICE)
    char temp_buff[12] = {0xff};
#endif
    


#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx("Device Config Data Written\r\n");
#endif
    
    /* Set the TX Power to 0 dBm */
    ret = aci_hal_set_tx_power_level(0,25);
    if (ret != 0)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_hal_set_tx_power_level() 0x%04xr\n", ret);
#endif
        while(1);
    }
    
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx("TX Power Set\r\n");
#endif
    
    /* Init the GATT */
    ret = aci_gatt_srv_init();
    if (ret != 0) 
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gatt_srv_init() 0x%04xr\n", ret);
#endif
    }
    else
    {
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx("GATT Initialized\r\n");
#endif
    }
    
    /* Init the GAP as PUBLIC because we want reversed MAC */
    //ret = aci_gap_init(0x01, 0x00, 0x08, STATIC_RANDOM_ADDR, &service_handle, &dev_name_char_handle, &appearance_char_handle);
    ret = aci_gap_init(0x01, 0x00, 0x08, PUBLIC_ADDR, &service_handle, &dev_name_char_handle, &appearance_char_handle);
    if (ret != 0)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gap_init() 0x%04x\r\n", ret);
#endif
    }
    else
    {
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx("GAP Initialized\r\n");
#endif
    }

    aci_hal_read_config_data(CONFIG_DATA_STORED_STATIC_RANDOM_ADDRESS, &mac_length, mac_address);

    /* swap the mac for advertising */
    mac_address_rev[5] = mac_address[0];
    mac_address_rev[4] = mac_address[1];
    mac_address_rev[3] = mac_address[2];
    mac_address_rev[2] = mac_address[3];
    mac_address_rev[1] = mac_address[4];
    mac_address_rev[0] = mac_address[5];
    
    /* write the reversed mac as a public address */
    aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, mac_address_rev);
    
#if(DEVICE_INFO_SERVICE)
    temp_buff[0] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[0], HIGH_NIBBLE);
    temp_buff[1] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[0], LOW_NIBBLE);
    temp_buff[2] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[1], HIGH_NIBBLE);
    temp_buff[3] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[1], LOW_NIBBLE);
    temp_buff[4] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[2], HIGH_NIBBLE);
    temp_buff[5] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[2], LOW_NIBBLE);
    temp_buff[6] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[3], HIGH_NIBBLE);
    temp_buff[7] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[3], LOW_NIBBLE);
    temp_buff[8] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[4], HIGH_NIBBLE);
    temp_buff[9] = convert_byte_to_ASCII(SystemIB.SysFlashIB.aMPBID[4], LOW_NIBBLE);
    
    memcpy(serial_number_buff, temp_buff, sizeof(temp_buff));
    serial_number_buff[10] = 'M';
    serial_number_buff[11] = 'K';
    serial_number_buff[12] = 'E';
    serial_number_buff[13] = 0x00;
#endif
    
    adv_data[11] = SystemIB.SysFlashIB.aMPBID[0];
    adv_data[12] = SystemIB.SysFlashIB.aMPBID[1];
    adv_data[13] = SystemIB.SysFlashIB.aMPBID[2];
    adv_data[14] = SystemIB.SysFlashIB.aMPBID[3];
    adv_data[15] = SystemIB.SysFlashIB.aMPBID[4];
    
    /* Copy mac address into sys params 
     * !! THIS IS THE ONLY PLACE THIS IS DONE */
    memcpy((uint8_t *)&SystemIB.SysRAMIB.aBLEMACAddr, mac_address, mac_length);
    
    /* Set firmware version 
     * !! THIS IS A CHARACTERISTIC DATA STRUCTURE */
    default_firmware_revision[0] = convert_byte_to_ASCII(FIRMWARE_VERSION_REV, HIGH_NIBBLE);
    default_firmware_revision[1] = convert_byte_to_ASCII(FIRMWARE_VERSION_REV, LOW_NIBBLE);
    default_firmware_revision[2] = convert_byte_to_ASCII(FIRMWARE_VERSION_MAJOR, HIGH_NIBBLE);
    default_firmware_revision[3] = convert_byte_to_ASCII(FIRMWARE_VERSION_MAJOR, LOW_NIBBLE);
    default_firmware_revision[4] = convert_byte_to_ASCII(FIRMWARE_VERSION_MINOR, HIGH_NIBBLE);
    default_firmware_revision[5] = convert_byte_to_ASCII(FIRMWARE_VERSION_MINOR, LOW_NIBBLE);
    default_firmware_revision[6] = convert_byte_to_ASCII(FIRMWARE_VERSION_SUB, HIGH_NIBBLE);
    default_firmware_revision[7] = convert_byte_to_ASCII(FIRMWARE_VERSION_SUB, LOW_NIBBLE);
}  /* BLE_Device_Init() */

static uint16_t BLE_Advertising_Interval = ADV_INTERVAL;

/**
  ******************************************************************************
  * @func   Start_Beaconing()
  * @brief  start beacon
  * @param  None
  * @gvar   
  * @gfunc  aci_gap_set_advertising_configuration(), aci_gap_set_advertising_data(),
  *         aci_gap_set_advertising_enable(), aci_hal_set_radio_activity_mask()
  * @retval None
  ******************************************************************************
  */
static void Start_Beaconing(void)
{  
    uint8_t ret;
    Advertising_Set_Parameters_t Advertising_Set_Parameters[1];
#if(DEVICE_INFO_SERVICE)
    Add_Device_Information_Service();
#endif
    
#if(BATTERY_SERVICE)
    Add_Battery_Service();
#endif

    Add_Open_Link_Service();
    Add_OTA_Service();
     
    /* Set advertising configuration for legacy advertising. */  
    // ADV_INTERVAL were both set to 160, JL changed it to #define variable 
    ret = aci_gap_set_advertising_configuration(
            0, GAP_MODE_GENERAL_DISCOVERABLE,
            ADV_PROP_CONNECTABLE|ADV_PROP_SCANNABLE|ADV_PROP_LEGACY,
            BLE_Advertising_Interval, BLE_Advertising_Interval,
            ADV_CH_ALL,
            0,NULL, /* No peer address */
            ADV_NO_WHITE_LIST_USE,
            0, /* 0 dBm */
            LE_1M_PHY, /* Primary advertising PHY */
            0, /* 0 skips */
            LE_1M_PHY, /* Secondary advertising PHY. Not used with legacy advertising. */
            0, /* SID */
            0 /* No scan request notifications */);
    if (ret != BLE_STATUS_SUCCESS)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gap_set_advertising_configuration() 0x%04x\r\n", ret);
#endif
        return;
    }

    if(SystemIB.SysFlashIB.uShipModeStatus == SHIP_MODE)
    {
        ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(adv_data_ship), adv_data_ship);
    }
    else
    {
        ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(adv_data), adv_data);
    }
    
    if (ret != BLE_STATUS_SUCCESS)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gap_set_advertising_data() 0x%04x\r\n", ret);
#endif
        return;
    }

    Advertising_Set_Parameters[0].Advertising_Handle = 0;
    Advertising_Set_Parameters[0].Duration = 0;
    Advertising_Set_Parameters[0].Max_Extended_Advertising_Events = 0;

    ret = aci_gap_set_advertising_enable(ENABLE, 1, Advertising_Set_Parameters);

    if (ret != BLE_STATUS_SUCCESS)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gap_set_advertising_enable() 0x%04x\r\n", ret);
#endif
        return;
    }
    else
    {
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx("Advertising Enable Success\r\n");
        printf ("aci_gap_set_advertising_enable() --> SUCCESS\r\n");
#endif
    }

    ret = aci_hal_set_radio_activity_mask(0x0002); // RR : modfy the mask depending on the events that you'd like the stack to raise
//    ret = aci_hal_set_radio_activity_mask(0x0036); // RR : modfy the mask depending on the events that you'd like the stack to raise

#if(DEVICE_INFO_SERVICE)
    /* This fills in the characteristic data structures */
    memcpy(manufacturer_name_buff, default_manufacturer_name, sizeof(manufacturer_name_buff));
    memcpy(model_number_buff, default_model_number, sizeof(model_number_buff));
    memcpy(firmware_revision_buff, default_firmware_revision, sizeof(firmware_revision_buff)); 
#endif
}  /* Start_Beaconing() */


/**
  ******************************************************************************
  * @func   BLE_Init_All()
  * @brief  Init BLE
  * @param  None
  * @gvar   None
  * @gfunc  BLE_ModulesInit(), BLE_Device_Init(), Start_Beaconing()
  * @retval None
  ******************************************************************************
  */
void BLE_Init_All(void) 
{
    // Init BLE stack, HAL virtual timer and NVM modules
    BLE_ModulesInit(); 
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx("Modules Initialized\r\n");
#endif
    
    // Init the Bluetooth LE stack layesrs
    BLE_Device_Init();
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx("Device Initialized\r\n");
#endif
    
    // Start Beacon Non Connectable Mode
    Start_Beaconing();
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx("Beacon Initialized\r\n");
#endif
    
}  /* BLE_Init_All() */

#if(DEVICE_INFO_SERVICE)
/*******************************************************************************
* Function Name  : Add_Device_Information_Service
* Description    : Add the device information service.
* Input          : None
* Return         : Status.
*******************************************************************************/
static uint8_t Add_Device_Information_Service(void)
{
  uint8_t ret;
  
    ret = aci_gatt_srv_add_service((ble_gatt_srv_def_t *)&device_information_service);
    if (ret != BLE_STATUS_SUCCESS)
    {
        goto fail;
    }

#ifdef ENABLE_RND_MSG
    printf("Device Information Service added.\n");
#endif
  return BLE_STATUS_SUCCESS; 

fail:
#ifdef ENABLE_RND_MSG
  printf("Error while adding Device information service.\n");
#endif
  return BLE_STATUS_ERROR ;
}
#endif

/*******************************************************************************
* Function Name  : Add_Open_Link_Service
* Description    : Add the open link service.
* Input          : None
* Return         : Status.
*******************************************************************************/
static uint8_t Add_Open_Link_Service(void)
{
  uint8_t ret;
  
    ret = aci_gatt_srv_add_service((ble_gatt_srv_def_t *)&open_link_service);
    if (ret != BLE_STATUS_SUCCESS)
    {
        goto fail;
    }

    OpenLinkCharHandle = aci_gatt_srv_get_char_decl_handle((ble_gatt_chr_def_t *)&open_link_chars[0]);

#ifdef ENABLE_RND_MSG
    printf("Open Link Service added.\n");
#endif
  return BLE_STATUS_SUCCESS; 

fail:
#ifdef ENABLE_RND_MSG
  printf("Error while adding Open Link Service.\n");
#endif
  return BLE_STATUS_ERROR ;
}

#if(BATTERY_SERVICE)
/*******************************************************************************
* Function Name  : Add_Battery_Service
* Description    : Add the battery service.
* Input          : None
* Return         : Status.
*******************************************************************************/
static uint8_t Add_Battery_Service(void)
{
  uint8_t ret;
  
    ret = aci_gatt_srv_add_service((ble_gatt_srv_def_t *)&battery_service);
    if (ret != BLE_STATUS_SUCCESS)
    {
        goto fail;
    }

#ifdef ENABLE_RND_MSG
    printf("Battery Service added.\n");
#endif
  return BLE_STATUS_SUCCESS; 

fail:
#ifdef ENABLE_RND_MSG
  printf("Error while adding Battery Service.\n");
#endif
  return BLE_STATUS_ERROR ;
}
#endif

/*******************************************************************************
* Function Name  : Add_OTA_Service
* Description    : Add the OTA service.
* Input          : None
* Return         : Status.
*******************************************************************************/
static uint8_t Add_OTA_Service(void)
{
  uint8_t ret;
  
    ret = aci_gatt_srv_add_service((ble_gatt_srv_def_t *)&ota_service);
    if (ret != BLE_STATUS_SUCCESS)
    {
        goto fail;
    }

        OTACharHandle = aci_gatt_srv_get_char_decl_handle((ble_gatt_chr_def_t *)&ota_chars[0]);

#ifdef ENABLE_RND_MSG
    printf("OTA Service added.\n");
#endif
  return BLE_STATUS_SUCCESS; 

fail:
#ifdef ENABLE_RND_MSG
  printf("Error while adding OTA Service.\n");
#endif
  return BLE_STATUS_ERROR ;
}

/*******************************************************************************
  * @func   pBLEStartNextTransfers()
  * @brief  initiates TX response from Open Link over BLE
  * @param  uiPtr = msg ptr (to be tx)
  * @param  iLen = lenght to be tx
  * @gvar   None
  * @gfunc  aci_gatt_srv_notify(), memcpy(),
  * @retval None
  *****************************************************************************/
void pBLEStartNextTransfers( uint8_t *uiPtr, int16_t iLen)
{
    tBleStatus ret;
    
    /* Copy the data to open link array */
    memcpy(&open_link_data_buff[1], uiPtr, iLen);
    open_link_data_buff[0] = (uint8_t)iLen;

    //Send GATT notification response
    ret = aci_gatt_srv_notify( connection_handle, OpenLinkCharHandle + 1, 0, (uint16_t)open_link_data_buff[0], &open_link_data_buff[1]);
    if (ret != BLE_STATUS_SUCCESS)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gatt_srv_notify(): 0x%02x\r\n", ret);
#endif
    }

    /* Reset connection timeout count because we had open link activity */
    connection_timeout_ticks = 0;        
}       /* pStartNextTransfers() */

/*******************************************************************************
  * @func   BLE_Make_Connection()
  * @brief  initiates Advertising after a BLEconnection has been 
  *         terminated/disconnected
  * @param  None
  * @gvar   tBleStatus ret, 
  *         Advertising_Set_Parameters_t Advertising_Set_Parameters[1]
  * @gfunc  aci_gap_set_advertising_enable(), 
  * @retval None
  *****************************************************************************/
void BLE_Make_Connection(void)
{  
    tBleStatus ret;
    Advertising_Set_Parameters_t Advertising_Set_Parameters[1];

    
    Advertising_Set_Parameters[0].Advertising_Handle = 0;
    Advertising_Set_Parameters[0].Duration = 0;
    Advertising_Set_Parameters[0].Max_Extended_Advertising_Events = 0;

    ret = aci_gap_set_advertising_enable(ENABLE, 1, Advertising_Set_Parameters);

    if (ret != BLE_STATUS_SUCCESS)
    {
#ifdef ENABLE_RND_MSG
        printf ("Error in aci_gap_set_advertising_enable(): 0x%02x\r\n", ret);
#endif
    }
    else
    {
#ifdef ENABLE_RND_MSG
        printf ("aci_gap_set_advertising_enable() --> SUCCESS\r\n");
#endif
    }
}

/****************************************************************
*
* START OF CALLBACK CODE 
*****************************************************************/

/*******************************************************************************
  * @func   hci_le_connection_complete_event()
  * @brief  Callback that is executed on BLE connection complete 
  * @param  uint8_t Status,
  *         uint16_t Connection_Handle,
  *         uint8_t Role,
  *         uint8_t Peer_Address_Type,
  *         uint8_t Peer_Address[6],
  *         uint16_t Conn_Interval,
  *         uint16_t Conn_Latency,
  *         uint16_t Supervision_Timeout,
  *         uint8_t Master_Clock_Accuracy)
  * @gvar   None
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void hci_le_connection_complete_event( uint8_t Status,
                                       uint16_t Connection_Handle,
                                       uint8_t Role,
                                       uint8_t Peer_Address_Type,
                                       uint8_t Peer_Address[6],
                                       uint16_t Conn_Interval,
                                       uint16_t Conn_Latency,
                                       uint16_t Supervision_Timeout,
                                       uint8_t Master_Clock_Accuracy )
{
    if( Status == BLE_STATUS_SUCCESS )
    {
        connection_handle = Connection_Handle;
        connection_interval = Conn_Interval;
  
        APP_FLAG_SET( CONNECTED );  
    }
} /* end hci_le_connection_complete_event() */


/*******************************************************************************
  * @func   hci_le_enhanced_connection_complete_event()
  * @brief  Callback that is executed on BLE enhanced connection complete 
  * @param  uint8_t Status,
  *         uint16_t Connection_Handle,
  *         uint8_t Role,
  *         uint8_t Peer_Address_Type,
  *         uint8_t Peer_Address[6],
  *         uint8_t Local_Resolvable_Private_Address[6],
  *         uint8_t Peer_Resolvable_Private_Address[6],
  *         uint16_t Conn_Interval,
  *         uint16_t Conn_Latency,
  *         uint16_t Supervision_Timeout,
  *         uint8_t Master_Clock_Accuracy)
  * @gvar   None
  * @gfunc  hci_le_connection_complete_event() 
  * @retval None
  *****************************************************************************/
void hci_le_enhanced_connection_complete_event( uint8_t Status,
                                                uint16_t Connection_Handle,
                                                uint8_t Role,
                                                uint8_t Peer_Address_Type,
                                                uint8_t Peer_Address[6],
                                                uint8_t Local_Resolvable_Private_Address[6],
                                                uint8_t Peer_Resolvable_Private_Address[6],
                                                uint16_t Conn_Interval,
                                                uint16_t Conn_Latency,
                                                uint16_t Supervision_Timeout,
                                                uint8_t Master_Clock_Accuracy )
{  
    hci_le_connection_complete_event( Status,
                                      Connection_Handle,
                                      Role,
                                      Peer_Address_Type,
                                      Peer_Address,
                                      Conn_Interval,
                                      Conn_Latency,
                                      Supervision_Timeout,
                                      Master_Clock_Accuracy );
}

/*******************************************************************************
  * @func   hci_disconnection_complete_event()
  * @brief  Callback that is executed on BLE disconnection complete 
  * @param  uint8_t Status,
  *         uint16_t Connection_Handle,
  *         uint8_t Reason,
  * @gvar   None
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void hci_disconnection_complete_event( uint8_t  Status,
                                       uint16_t Connection_Handle,
                                       uint8_t  Reason )
{
    APP_FLAG_CLEAR( CONNECTED );

    /* Make the device connectable again. */
    APP_FLAG_SET( SET_CONNECTABLE );
    APP_FLAG_CLEAR( NOTIFICATIONS_ENABLED );
    APP_FLAG_CLEAR( OPENLINK_DATA_RX );

#ifdef ENABLE_RND_MSG
    printf("Disconnection, Reason: 0x%04X\n", Reason); 
#endif  
} /* end hci_disconnection_complete_event() */


/*******************************************************************************
  * @func   aci_gatt_srv_write_event()
  * @brief  Callback that is executed on BLE write of characteristic data 
  * @param  uint16_t Connection_Handle,
  *         uint8_t  Response_Needed,
  *         uint16_t Attribute_Handle,
  *         uint16_t Data_Length,
  *         uint8_t  Data[],
  * @gvar   uint8_t att_error, uint8_t i
  * @gfunc  aci_gatt_srv_resp() 
  * @retval None
  *****************************************************************************/
void aci_gatt_srv_write_event( uint16_t Connection_Handle,
                               uint8_t  Resp_Needed,
                               uint16_t Attribute_Handle,
                               uint16_t Data_Length,
                               uint8_t  Data[] )
{
    
    uint8_t att_error = BLE_ATT_ERR_NONE;
    uint8_t i;

    if( Attribute_Handle == OpenLinkCharHandle + 1 )
    {
        for(i = 0U; i < Data_Length; i++)
        {
            open_link_data_buff[i+1] = Data[i];
        }
        open_link_data_buff[0] = i;

        APP_FLAG_SET(OPENLINK_DATA_RX);        

        att_error = BLE_ATT_ERR_NONE;
        if( Resp_Needed == 1U )
        {
            aci_gatt_srv_resp(Connection_Handle, Attribute_Handle, att_error, 0,  NULL);
        }   
    }
#if(CONFIG_OTA_USE_SERVICE_MANAGER)   
    else if( Attribute_Handle == OTACharHandle + 1 )
    {
        for(i = 0U; i < Data_Length; i++)
        {
            ota_control_buff[i] = Data[i];
        }
        /* TODO:JRG Check the Admin password */
        if((ota_control_buff[0] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0xFF00000000000000) >> 56)) &&
           (ota_control_buff[1] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0x00FF000000000000) >> 48)) &&
           (ota_control_buff[2] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0x0000FF0000000000) >> 40)) &&
           (ota_control_buff[3] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0x000000FF00000000) >> 32)) &&
           (ota_control_buff[4] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0x00000000FF000000) >> 24)) &&
           (ota_control_buff[5] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0x0000000000FF0000) >> 16)) &&
           (ota_control_buff[6] == (uint8_t)((SystemIB.SysFlashIB.uAdminPassword & 0x000000000000FF00) >> 8)) &&
           (ota_control_buff[7] == (uint8_t)(SystemIB.SysFlashIB.uAdminPassword & 0x00000000000000FF)))      
        {
            OTA_Jump_To_Service_Manager_Application();
        }
        att_error = BLE_ATT_ERR_NONE;

        if( Resp_Needed == 1U )
        {
            aci_gatt_srv_resp( Connection_Handle, Attribute_Handle, att_error, 0,  NULL );
        }
    }   
#endif
}

/*******************************************************************************
  * @func   get_ble_MAC_data_pointer()
  * @brief  Method to return the MAC address pointer 
  * @param  None
  * @gvar   None
  * @gfunc  None 
  * @retval uint8_t * (mac address pointer)
  *****************************************************************************/
uint8_t* get_ble_MAC_data_pointer( void )
{
    return( mac_address );
}


/*******************************************************************************
  * @func   add_MPBID_to_advertising_data()
  * @brief  Method to add MPBID data to advertising data array 
  * @param  uint8_t * mpbid
  * @gvar   None
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void add_MPBID_to_advertising_data( uint8_t* mpbid )
{
    /* Update MPBID in ship mode array */
    adv_data_ship[11] = mpbid[0];
    adv_data_ship[12] = mpbid[1];
    adv_data_ship[13] = mpbid[2];
    adv_data_ship[14] = mpbid[3];
    adv_data_ship[15] = mpbid[4];

    /* Update MPBID in normal mode array */
    adv_data[11] = mpbid[0];
    adv_data[12] = mpbid[1];
    adv_data[13] = mpbid[2];
    adv_data[14] = mpbid[3];
    adv_data[15] = mpbid[4];
}


/*******************************************************************************
  * @func   add_coin_voltage_to_advertising_data()
  * @brief  Method to add coin cell voltage data to advertising data array 
  * @param  uint8_t coin
  * @gvar   uint8_t temp_data;
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void add_coin_voltage_to_advertising_data( uint8_t coin )
{
    uint8_t temp_data;
    
    temp_data = adv_data[16];
    adv_data[16] = (coin & 0x7F) | (temp_data & 0x80);
}


/*******************************************************************************
  * @func   add_lock_status_to_advertising_data()
  * @brief  Method to add lock status data to advertising data array 
  * @param  uint8_t lock
  * @gvar   uint8_t temp_data;
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void add_lock_status_to_advertising_data( uint8_t lock )
{
    uint8_t temp_data;
    
    temp_data = adv_data[16];
    adv_data[16] = ((lock & 0x01) << 7) | (temp_data & 0x7F);
}


/*******************************************************************************
  * @func   add_histogram_to_advertising_data()
  * @brief  Method to add histogram data to advertising data array 
  * @param  uint8_t* histogram
  * @gvar   uint8_t temp_data;
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void add_histogram_to_advertising_data( uint8_t* histogram )
{
    uint8_t temp_data;
    
    adv_data[24] = histogram[0];
    adv_data[23] = histogram[1];
    adv_data[22] = histogram[2];
    adv_data[21] = histogram[3];
    adv_data[20] = histogram[4];
    adv_data[19] = histogram[5];
    temp_data = adv_data[18];
    adv_data[18] = (histogram[6] & HISTOGRAM_MASK) | (temp_data & FWVER_L_MASK);
}


/*******************************************************************************
  * @func   add_last_used_to_advertising_data()
  * @brief  Method to add last used data to advertising data array 
  * @param  uint8_t* last_used
  * @gvar   uint8_t temp_data;
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void add_last_used_to_advertising_data( uint8_t last_used )
{
    uint8_t temp_data;
    
    temp_data = adv_data[17];
    adv_data[17] = ((last_used & 0x07) << 5) | (temp_data & 0x1F);
}

static uint16_t advertising_ticks = 0;

static uint16_t beacon_1_ticks = 0;
static uint16_t beacon_2_ticks = 0;
static uint16_t beacon_3_ticks = 0;

static uint16_t beacon_1_rate = IBEACON_1_RATE * TICKS_PER_MINUTE;
static uint16_t beacon_2_rate = IBEACON_2_RATE * TICKS_PER_MINUTE; /* Rate in minutes */
static uint16_t beacon_3_rate = IBEACON_3_RATE * TICKS_PER_MINUTE; /* Rate in minutes */

uint8_t advertising_mode = 0;

uint8_t last_mode = PRODUCTION_MODE;


/*******************************************************************************
  * @func   Update_Advertising_Service()
  * @brief  Method to change advertising data arrays for iBeacons or for 
  *         normal or ship mode advertising. 
  * @param  None
  * @gvar   tBleStatus ret, 
  *         Advertising_Set_Parameters_t Advertising_Set_Parameters[1]
  * @gfunc  aci_gap_set_advertising_enable(), aci_gap_set_advertising_data()
  * @retval None
  *****************************************************************************/
void Update_Advertising_Service(void)
{
    tBleStatus ret;
    Advertising_Set_Parameters_t Advertising_Set_Parameters[1];

    if((advertising_mode == BLE_ADVERTISING_NORMAL) && (SystemIB.SysFlashIB.uShipModeStatus == NORMAL_MODE))
    {
        /* Check for iBeacon 1 advertising */
        beacon_1_ticks++;
        if(beacon_1_ticks == beacon_1_rate)
        {
            beacon_1_ticks = 0;
            advertising_ticks = 0;
            advertising_mode = BLE_ADVERTISING_IBEACON;
        }

        /* Check for iBeacon 2 advertising */
        beacon_2_ticks++;
        if(beacon_2_ticks == beacon_2_rate)
        {
            beacon_2_ticks = 0;
            advertising_ticks = 0;
            advertising_mode = BLE_ADVERTISING_IBEACON;
        }

        /* Check for iBeacon 3 advertising */
        beacon_3_ticks++;
        if(beacon_3_ticks == beacon_3_rate)
        {
            beacon_3_ticks = 0;
            advertising_ticks = 0;
            advertising_mode = BLE_ADVERTISING_IBEACON;
        }
    }    
    /* Check for any iBeacon advertising */
    if((advertising_mode == BLE_ADVERTISING_IBEACON) && (advertising_ticks == 0))
    {
        /* Set iBeacon rate */
        //TODO:JRG This will be a variable */
        BLE_Advertising_Interval = IBEACON_ADVERTISING_RATE;
        
        /* Turn off advertising to set data set */
        ret = aci_gap_set_advertising_enable(DISABLE, 1, Advertising_Set_Parameters);

        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_enable() 0x%04x\r\n", ret);
#endif
        }

        /* Set advertising configuration for normal or ship mode advertising */  
        ret = aci_gap_set_advertising_configuration(
              0, GAP_MODE_GENERAL_DISCOVERABLE,
              ADV_PROP_CONNECTABLE|ADV_PROP_SCANNABLE|ADV_PROP_LEGACY,
              BLE_Advertising_Interval, BLE_Advertising_Interval,
              ADV_CH_ALL,
              0,NULL, /* No peer address */
              ADV_NO_WHITE_LIST_USE,
              0, /* 0 dBm */
              LE_1M_PHY, /* Primary advertising PHY */
              0, /* 0 skips */
              LE_1M_PHY, /* Secondary advertising PHY. Not used with legacy advertising. */
              0, /* SID */
              0 /* No scan request notifications */);
        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_configuration() 0x%04x\r\n", ret);
#endif
        }

        /* Check for iBeacon 3, 2 or 1 data set */
        if(beacon_3_ticks == 0)
        {
            ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(ibeacon3_adv_data), ibeacon3_adv_data);
        }
        else if(beacon_2_ticks == 0)
        {
            ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(ibeacon2_adv_data), ibeacon2_adv_data);
        }
        else if(beacon_1_ticks == 0)
        {
            ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(ibeacon1_adv_data), ibeacon1_adv_data);
        }

        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_data() 0x%04x\r\n", ret);
#endif
        }

        /* Turn advertising back on */
        Advertising_Set_Parameters[0].Advertising_Handle = 0;
        Advertising_Set_Parameters[0].Duration = 0;
        Advertising_Set_Parameters[0].Max_Extended_Advertising_Events = 0;

        ret = aci_gap_set_advertising_enable(ENABLE, 1, Advertising_Set_Parameters);

        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_enable() 0x%04x\r\n", ret);
#endif
        }        
    }

    if(advertising_mode == BLE_ADVERTISING_IBEACON)
    {
       advertising_ticks++;
    }
    
    /* Check for changes to adverting data */
    if((advertising_ticks == ADVERTISING_RATE) || (last_mode != SystemIB.SysFlashIB.uShipModeStatus))
    {
        advertising_ticks = 0;
        advertising_mode = BLE_ADVERTISING_NORMAL;
        BLE_Advertising_Interval = ADV_INTERVAL;
        
        /* Turn off advertising to set data set */
        ret = aci_gap_set_advertising_enable(DISABLE, 1, Advertising_Set_Parameters);

        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_enable() 0x%04x\r\n", ret);
#endif
        }

        /* Set advertising configuration for normal or ship mode advertising */  
        ret = aci_gap_set_advertising_configuration(
              0, GAP_MODE_GENERAL_DISCOVERABLE,
              ADV_PROP_CONNECTABLE|ADV_PROP_SCANNABLE|ADV_PROP_LEGACY,
              BLE_Advertising_Interval, BLE_Advertising_Interval,
              ADV_CH_ALL,
              0,NULL, /* No peer address */
              ADV_NO_WHITE_LIST_USE,
              0, /* 0 dBm */
              LE_1M_PHY, /* Primary advertising PHY */
              0, /* 0 skips */
              LE_1M_PHY, /* Secondary advertising PHY. Not used with legacy advertising. */
              0, /* SID */
              0 /* No scan request notifications */);
        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_configuration() 0x%04x\r\n", ret);
#endif
        }

        /* Check for Normal or ShipMode advertising data set */
        if(SystemIB.SysFlashIB.uShipModeStatus == SHIP_MODE)
        {
            ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(adv_data_ship), adv_data_ship);
        }
        else
        {
            ret = aci_gap_set_advertising_data(0, ADV_COMPLETE_DATA, sizeof(adv_data), adv_data);
        }
        
        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_data() 0x%04x\r\n", ret);
#endif
        }

        /* Turn advertising back on */
        Advertising_Set_Parameters[0].Advertising_Handle = 0;
        Advertising_Set_Parameters[0].Duration = 0;
        Advertising_Set_Parameters[0].Max_Extended_Advertising_Events = 0;

        ret = aci_gap_set_advertising_enable(ENABLE, 1, Advertising_Set_Parameters);

        if (ret != BLE_STATUS_SUCCESS)
        {
#ifdef ENABLE_RND_MSG
            printf ("Error in aci_gap_set_advertising_enable() 0x%04x\r\n", ret);
#endif
        }                
    }
    /* Save the mode for comparison next time */
    last_mode = SystemIB.SysFlashIB.uShipModeStatus;    

    /* Check for disconnect Timeout */
    connection_timeout_ticks++;
    if( CONNECTION_TIMEOUT_RATE == connection_timeout_ticks )
    {
        BLE_Disconnect(); /* Does not trigger a disconnect callback */
    }
}


/*******************************************************************************
  * @func   BLE_Disconnect()
  * @brief  Method to disconnect and terminate the connection 
  * @param  None
  * @gvar   None
  * @gfunc  None 
  * @retval None
  *****************************************************************************/
void BLE_Disconnect( void )
{
    /* Disconnect */
    hci_disconnect( connection_handle, 0x13 );
    
    /* Make the device connectable again. */
    APP_FLAG_CLEAR( CONNECTED );
    APP_FLAG_SET( SET_CONNECTABLE );
    APP_FLAG_CLEAR( NOTIFICATIONS_ENABLED );
    APP_FLAG_CLEAR( OPENLINK_DATA_RX );

}


/*******************************************************************************
  * @func   set_ibeacon_rate()
  * @brief  Method to set the iBeacon rate for one of the iBeacons 
  * @param  uint8_t beacon, 
  *         uint16_t rate
  * @gvar   None
  * @gfunc  None 
  * @retval Noine
  *****************************************************************************/
void set_ibeacon_rate( uint8_t beacon, uint8_t rate)
{
    if( IBEACON_2_ID == beacon )
    {
        beacon_2_rate = rate * 30;
    }
    else if( IBEACON_3_ID == beacon )
    {
        beacon_3_rate = rate * 30;
    }
}


/*******************************************************************************
  * @func   convert_byte_to_ASCII()
  * @brief  Method to convert a HEX nibble (high or Low) to an ASCII byte 
  * @param  uint8_t byte, 
  *         uint8_t nibble
  * @gvar   uint8_t data
  * @gfunc  None 
  * @retval uint8_t data (ASCII data of the nibble)
  *****************************************************************************/
static uint8_t convert_byte_to_ASCII( uint8_t byte, uint8_t nibble )
{
    uint8_t data = 0;
    
    if(nibble == HIGH_NIBBLE)
    {
        data = byte >> 4;
    }
    else
    {
        data = byte & 0xF;
    }
    if(data > 9)
    {
        data += 0x37;
    }
    else
    {
        data += 0x30;    
    }
    return( data );
}
