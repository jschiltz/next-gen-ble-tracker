/**
  ******************************************************************************
  * @file    Examples_LL/USART/USART_TxRx_DMA_Init/Src/bluenrg_lp_it.c
  * @author  RF Application Team
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_system.h"
#include "bluenrg_lp_ll_exti.h"
#include "bluenrg_lp_ll_utils.h"
#include "bluenrg_lp_ll_tim.h"
#include "bluenrg_lp_ll_rtc.h"
#include "NG_Tick_main.h"
#include "bluenrg_lp_hal.h"
#include "bluenrg_lp_it.h"
#include "CallBack.h"
#include "NG_Tick_NFC.h"
#include "NG_Tick_Acc.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_uart.h"
#include "LOLConfig.h"
#include "NG_Tick_LOL_CmdVar.h"

#include "ble_const.h"
#include "bluenrg_lp_stack.h"
#include "bluenrg_lp_hal_vtimer.h"
#include "hal_miscutil.h"
#include "crash_handler.h"
    
/* Private includes ----------------------------------------------------------*/


/* Private typedef -----------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define DEBOUNCE_CNT    16      /* 16ms for each count */
#define SYS1SEC_CNT     1000    /* 1 scond counter */

/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
//static __IO uint32_t debounce_cnt_AccInt1 = 0;     /* debounce counter, accelerometer int1 */
//static __IO uint32_t debounce_cnt_AccInt2 = 0;     /* debounce counter, accelerometer int2 */
static __IO uint32_t debounce_cnt_NFC = 0;         /* debounce counter, NFC */
__IO uint32_t uiSysTick = 0;
__IO int32_t iSys_1SecCnt = SYS1SEC_CNT;           /* 1 second counter */
__IO int16_t iSys_1SecF = FALSE;                   /* 1 second flag */        
__IO int16_t iNFC_DelayCnt = 0;                    /* NFC delay counter */        

/* Private function prototypes -----------------------------------------------*/


/* Private user code ---------------------------------------------------------*/


/* External variables --------------------------------------------------------*/


/******************************************************************************/
/*           Cortex Processor Interruption and Exception Handlers             */ 
/******************************************************************************/
/**
  ******************************************************************************
  * @func   NMI_IRQHandler()
  * @brief  handles Non maskable interrupt
  * @param  None
  * @gvar   
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void NMI_IRQHandler(void)
{
}       /* NMI_IRQHandler() */

/**
  ******************************************************************************
  * @func   HardFault_IRQHandler()
  * @brief  handles Hard fault interrupt
  * @param  None
  * @gvar   
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void HardFault_IRQHandler(void)
{
    while (1)
    {
    }
}       /* HardFault_IRQHandler() */


/**
  ******************************************************************************
  * @func   SVC_IRQHandler()
  * @brief  handles System service call via SWI instruction
  * @param  None
  * @gvar   
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void SVC_IRQHandler(void)
{
}       /* SVC_IRQHandler() */


/**
  ******************************************************************************
  * @func   PendSV_IRQHandler()
  * @brief  handles Pendable request for system service
  * @param  None
  * @gvar   
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void PendSV_IRQHandler(void)
{
}       /* PendSV_IRQHandler() */


/**
  ******************************************************************************
  * @func   SysTick_IRQHandler()
  * @brief  handles SysTick Handler
  * @param  None
  * @gvar   debounce_cnt_NFC
  *         uwTick, iAudioEngineSysCounter
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void SysTick_IRQHandler(void)
{
//    debounce_cnt_AccInt1++;     /* accelerometer int1 debounce */
//    debounce_cnt_AccInt2++;     /* accelerometer int2 debounce */
    debounce_cnt_NFC++;         /* NFC interrupt debounce */
    uwTick++;                   /* NFC i2c driver */
    iAudioEngineSysCounter--;   /* audio engine system counter */
    uiSysTick++;                /* wakeup reference counter */
    iRxTimeout--;               /* uart rx time out couter */
    bLOL_1msF = TRUE;           /* LOL 1ms flag */
    iSys_1SecCnt--;
    if (iSys_1SecCnt < 0)
    {
        iSys_1SecCnt = SYS1SEC_CNT;
        iSys_1SecF = TRUE;
    }  /* if */
    iNFC_DelayCnt--;            /* dec NFC delay counter */
}       /* SysTick_IRQHandler() */

/*****************************************************************************/
/* BLUENRG_LP Peripheral Interrupt Handlers                                   */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_bluenrg_lp.s).                   */
/******************************************************************************/

/**
  ******************************************************************************
  * @func   DMA_IRQHandler()
  * @brief  handles DMA channel7 global interrupt
  * @param  None
  * @gvar   
  * @gfunc  LL_DMA_IsActiveFlag_TC7(), LL_DMA_ClearFlag_GI7(), DMA1_TransmitComplete_Callback(),
  *         LL_DMA_IsActiveFlag_TE7(), USART_TransferError_Callback(), 
  * @retval None
  ******************************************************************************
  */
void DMA_IRQHandler(void)
{
    if (LL_DMA_IsActiveFlag_TC7(DMA1))
    {
        LL_DMA_ClearFlag_GI7(DMA1);
        DMA1_TransmitComplete_Callback();    /* function Transmission complete Callback */
    }
    else if (LL_DMA_IsActiveFlag_TE7(DMA1))
    {
        USART_TransferError_Callback();    /* Call Error function */
    }
}       /* DMA_IRQHandler() */

/**
  ******************************************************************************
  * @func   USART1_IRQHandler()
  * @brief  handles USART1 global interrupt
  * @param  None
  * @gvar   
  * @gfunc  LL_USART_IsActiveFlag_RXNE(), LL_USART_IsEnabledIT_RXNE(),
  *         USART_CharReception_Callback(), Error_Callback
  * @retval None
  ******************************************************************************
  */
void USART1_IRQHandler(void)
{
    /* Check RXNE flag value in ISR register */
    if (LL_USART_IsActiveFlag_RXNE(USART1) && LL_USART_IsEnabledIT_RXNE(USART1))
    {
        /* RXNE flag will be cleared by reading of RDR register (done in call) */
        /* Call function in charge of handling Character reception */
        USART_CharReception_Callback();
    }
    else
    {
        Error_Callback();    /* Call Error function */
    }
}       /* USART1_IRQHandler() */


/**
  ******************************************************************************
  * @func   GPIOA_IRQHandler()
  * @brief  Handle Port A interrupt request
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void GPIOA_IRQHandler(void)
{
}       /* GPIOA_IRQHandler() */

/**
  ******************************************************************************
  * @func   GPIOB_IRQHandler()
  * @brief  Handle Port B interrupt request, button 2, accelerometer & NFC
  * @param  None
  * @gvar   debounce_cnt_NFC 
  * @gfunc  LL_EXTI_IsInterruptPending(), LL_EXTI_ClearInterrupt(),
  *         AccInt1_Callback()
  * @retval None
  ******************************************************************************
  */
void GPIOB_IRQHandler(void)
{

    static uint32_t debounce_last_NFC = 0;

    /* check for NFC interrupt */
    if (LL_EXTI_IsInterruptPending(ST25DV_EXTI_LINE) != RESET)
    {
        LL_EXTI_ClearInterrupt(ST25DV_EXTI_LINE);
        
        if ((debounce_cnt_NFC - debounce_last_NFC) >= DEBOUNCE_CNT)
        {
            debounce_last_NFC = debounce_cnt_NFC;   /* update last with current cnt */
            NFCInt_Callback();   /* NFC callback */
        }
    }     /* if NFC interrupt */
  
}       /* GPIOB_IRQHandler() */

/**
  ******************************************************************************
  * @func   TIM1_IRQHandler()
  * @brief  handles TIM1 global interrupt
  * @param  None
  * @gvar    
  * @gfunc  LL_TIM_IsActiveFlag_CC6(), LL_TIM_ClearFlag_CC6(), TimerCaptureCompare_Callback()
  * @retval None
  ******************************************************************************
  */
void TIM1_IRQHandler(void)
{
    /* Check whether CC6 interrupt is pending */
    if (LL_TIM_IsActiveFlag_CC6(TIM1) == 1)
    {
        /* Clear the update interrupt flag*/
        LL_TIM_ClearFlag_CC6(TIM1);

        /* TIM1 capture/compare interrupt processing(function defined in TIM_PWMOutput_Init_main.c) */
//        TimerCaptureCompare_Callback();
    }
}

/**
  ******************************************************************************
  * @func   RTC_IRQHandler()
  * @brief  handles RTC global interrupt
  * @param  None
  * @gvar    
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void RTC_IRQHandler(void)
{
    if (LL_RTC_IsActiveFlag_WUT(RTC))
    {
        LL_RTC_ClearFlag_WUT(RTC);
    }
}  /*RTC_IRQHandler() */


//BLE Handlers

/**
  ******************************************************************************
  * @func   BLE_WKUP_IRQHandler()
  * @brief  handles BLE wakeup interrupt
  * @param  None
  * @gvar    
  * @gfunc  HAL_VTIMER_WakeUpCallback()
  * @retval None
  ******************************************************************************
  */
void BLE_WKUP_IRQHandler(void)
{
  HAL_VTIMER_WakeUpCallback();
}  /* BLE_WKUP_IRQHandler() */

/**
  ******************************************************************************
  * @func   BLE_WKUP_IRQHandler()
  * @brief  handles BLE wakeup interrupt
  * @param  None
  * @gvar   None
  * @gfunc  HAL_VTIMER_TimeoutCallback()
  * @retval None
  ******************************************************************************
  */
void CPU_WKUP_IRQHandler(void) 
{
  HAL_VTIMER_TimeoutCallback();
}  /* CPU_WKUP_IRQHandler() */

/**
  ******************************************************************************
  * @func   BLE_ERROR_IRQHandler()
  * @brief  handles BLE wakeup interrupt
  * @param  None
  * @gvar   BLUE
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void BLE_ERROR_IRQHandler(void)
{
  volatile uint32_t debug_cmd;
  
  BLUE->DEBUGCMDREG |= 1;

  /* If the device is configured with 
     System clock = 64 MHz and BLE clock = 16 MHz
     a register read is necessary to end fine  
     the clear interrupt register operation,
     due the AHB down converter latency */ 
  debug_cmd = BLUE->DEBUGCMDREG;
}  /* BLE_ERROR_IRQHandler() */

/**
  ******************************************************************************
  * @func   BLE_TX_RX_IRQHandler()
  * @brief  handles BLE tx/rx interrupt
  * @param  None
  * @gvar   BLUE
  * @gfunc  BLE_STACK_RadioHandler(), HAL_VTIMER_RadioTimerIsr()
  * @retval None
  ******************************************************************************
  */
void BLE_TX_RX_IRQHandler(void)
{
  uint32_t blue_status = BLUE->STATUSREG;
  uint32_t blue_interrupt = BLUE->INTERRUPT1REG;

  /** clear all pending interrupts */
  BLUE->INTERRUPT1REG = blue_interrupt;

  BLE_STACK_RadioHandler(blue_status|blue_interrupt);
  HAL_VTIMER_RadioTimerIsr();

  /* If the device is configured with 
     System clock = 64 MHz and BLE clock = 16 MHz
     a register read is necessary to end fine  
     the clear interrupt register operation,
     due the AHB down converter latency */ 
  blue_interrupt = BLUE->INTERRUPT1REG;
}  /* BLE_TX_RX_IRQHandler() */