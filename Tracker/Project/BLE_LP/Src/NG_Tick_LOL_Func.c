/**
  ******************************************************************************
  * @file    NG_Tick_LOL_Func.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link functions.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "system_BlueNRG_LP.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_FlashUtil.h"    /* must come before NG_Tick_sys.h */
#include "NG_Tick_NFC.h"          /* must come before NG_Tick_sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "LOLConfig.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_CmdVar.h"
#include "NG_Tick_LOL_Func.h"


/* constant declaration ------------------------------------------------------*/
#define AE_STOP         0
#define AE_START        1

/* variable declaration ------------------------------------------------------*/
union NGT_DATASWAP64_TYPE unDataSwap64;
union NGT_DATASWAP32_TYPE unDataSwap32;
union NGT_DATASWAP16_TYPE unDataSwap16;

uint16_t        uiESwapData16 = 0;
uint32_t        uiESwapData32 = 0;
uint64_t        uiESwapData64 = 0;

/* function prototype --------------------------------------------------------*/
void EndianSwapRead16( uint8_t *);
void EndianSwapRead32( uint8_t *, uint8_t);
void EndianSwapRead( uint8_t *, uint8_t);
void EndianSwap16( uint16_t *,  uint16_t);
void EndianSwap32( uint32_t *,  uint32_t);
void EndianSwap64( uint64_t *,  uint64_t);
void CheckLoL(void);
uint8_t xLoLGC_MfgTestAE( uint16_t);
uint8_t xLoLGC_BLEGCBuzzer( uint8_t, uint8_t, uint8_t);

/**
  ******************************************************************************
  * @func   EndianSwapRead16()
  * @brief  Place bytes into array.  Src is big while Dest is little.
  * @param  ptr to input array (source)
  * @gvar   unDataSwap16
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void EndianSwapRead16( uint8_t *uiSrcPtr)
{
    unDataSwap16.uiVal = 0;
    unDataSwap16.uiByte[1] = *uiSrcPtr++;
    unDataSwap16.uiByte[0] = *uiSrcPtr;
}  /* EndianSwapRead16() */

/**
  ******************************************************************************
  * @func   EndianSwapRead32()
  * @brief  Place bytes into array.  Src is big while Dest is little.
  * @param  ptr to input array (source)
  * @param  counter, nuber of byte to accummulate
  * @gvar   unDataSwap32
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void EndianSwapRead32( uint8_t *uiSrcPtr, uint8_t iCnt)
{
    unDataSwap32.uiVal = 0;
    uint8_t *uiDestPtr = &unDataSwap32.uiByte[DATA_FORMAT32_BC-1];

    while (iCnt-- > 0)
    {
        *uiDestPtr-- = *uiSrcPtr++;
    }  /* while */
}  /* EndianSwapRead() */


/**
  ******************************************************************************
  * @func   EndianSwapRead()
  * @brief  Place bytes into array.  Src is big while Dest is little.
  * @param  ptr to input array (source)
  * @param  counter, nuber of byte to accummulate
  * @gvar   unDataSwap64
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void EndianSwapRead( uint8_t *uiSrcPtr, uint8_t iCnt)
{
    unDataSwap64.uiVal = 0;
    uint8_t *uiDestPtr = &unDataSwap64.uiByte[DATA_FORMAT64_BC-1];

    while (iCnt-- > 0)
    {
        *uiDestPtr-- = *uiSrcPtr++;
    }  /* while */
}  /* EndianSwapRead() */


/**
  ******************************************************************************
  * @func   EndianSwap16()
  * @brief  swap uinteger in 16 bits
  * @param  *ptr to dest, value of source
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void EndianSwap16( uint16_t *uiDestVal,  uint16_t uiSrcVal)
{
    *uiDestVal = ((uiSrcVal >> 8) & 0x00ff) | ((uiSrcVal << 8) & 0xff00);
}  /* EndianSwap16() */

/**
  ******************************************************************************
  * @func   EndianSwap32()
  * @brief  swap uinteger in 32 bits
  * @param  *ptr to dest, value of source
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void EndianSwap32( uint32_t *uilDestVal, uint32_t uilSrcVal)
{
                                 /*    0 1 2 3 */
    *uilDestVal = ((uilSrcVal >> 24) & (uint32_t)0x000000ff) |
                  ((uilSrcVal >> 8)  & (uint32_t)0x0000ff00) |
                  ((uilSrcVal << 8)  & (uint32_t)0x00ff0000) |
                  ((uilSrcVal << 24) & (uint32_t)0xff000000);
}  /* EndianSwap32() */

/**
  ******************************************************************************
  * @func   EndianSwap64()
  * @brief  swap uinteger in 64 bits
  * @param  *ptr to dest, value of source
  * @gvar   None
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void EndianSwap64( uint64_t *uilDestVal,  uint64_t uilSrcVal)
{
                                 /*    0 1 2 3 4 5 6 7 */
    *uilDestVal = ((uilSrcVal >> 56) & 0x00000000000000ff) |
                  ((uilSrcVal >> 40) & 0x000000000000ff00) |
                  ((uilSrcVal >> 24) & 0x0000000000ff0000) |
                  ((uilSrcVal >> 8)  & 0x00000000ff000000) |
                  ((uilSrcVal << 8)  & 0x000000ff00000000) |
                  ((uilSrcVal << 24) & 0x0000ff0000000000) |
                  ((uilSrcVal << 40) & 0x00ff000000000000) |
                  ((uilSrcVal << 56) & 0xff00000000000000);
}  /* EndianSwap64() */

/**
  ******************************************************************************
  * @func   CheckLoL()
  * @brief  Check LoL, check 1ms flag, if set, check security, rx buffer & timeout
  * @param  None
  * @gvar   bLOL_1msF
  * @gfunc  LOLTimerTick()
  * @retval None
  ******************************************************************************
  */
void CheckLoL()
{
    if (bLOL_1msF)
    {
        bLOL_1msF = FALSE;
        LOLTimerTick();
    }  /* if */
}  /* CheckLoL() */

/**
  ******************************************************************************
  * @func   xLoLGC_MfgTestAE()
  * @brief  manufacturing buzzer test, 4KHz for 5 sec
  * @param  play duration, in ms - ignored
  * @gvar   sAudioEngine
  * @gfunc  none
  * @retval LOL_ACK_RESP if ae is queued, NAKUNKNOWNERROR if not queued
  ******************************************************************************
  */
uint8_t xLoLGC_MfgTestAE( uint16_t uDuration)
{
    uint8_t uStatus = NAKUNKNOWNERROR;  /* init to unknown error */
    
    if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
    {
        sAudioEngine.uiAE_NewSongId = SONG_INDEX_4K;
        sAudioEngine.uiAE_NewCmd = TRUE;
        uStatus = LOL_ACK_RESP;
        SystemIB.SysRAMIB.uNFCBuzzerEvents++;  //^&*(
    }  /* if */
    return(uStatus);
}  /* xLoLGC_MfgTestAE() */

/**
  ******************************************************************************
  * @func   xLoLGC_BLEGCBuzzer()
  * @brief  Play tone if 1} AE in idle, 2) tone id is valid
  *         Abort AE if to stop
  * @param  play/stop
  * @param  tone id
  * @param  repeat count
  * @gvar   sAudioEngine
  * @gfunc  none
  * @retval LOL_ACK_RESP if ae is queued to stop or tone id accepted
  *         NAKILLEGALVALUE if tone id is not in table
  *         NAKUNKNOWNERROR if not queued
  ******************************************************************************
  */
uint8_t xLoLGC_BLEGCBuzzer( uint8_t bPlay, uint8_t uSongId, uint8_t uRepeat)
{
    uint8_t uStatus = NAKUNKNOWNERROR;  /* init to unknown error */

    switch (bPlay)
    {
        case AE_STOP:
            sAudioEngine.uiAE_Abort = TRUE;
            uStatus = LOL_ACK_RESP;
        break;  /* AE_STOP */
        
        case AE_START:
            /* check if song id is within table */
            if ((LAST_SONG_INDEX >= uSongId) && (SONG_INDEX_NONE != uSongId))
            {
                if (AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State)
                {
                    sAudioEngine.uiAE_NewSongId = uSongId;
                    sAudioEngine.uiAE_NewCmd = TRUE;
                    sAudioEngine.iAE_ProgressRepeatOverride = (uint16_t)uRepeat;
                    uStatus = LOL_ACK_RESP;
                    SystemIB.SysRAMIB.uNFCBuzzerEvents++;  //^&*(
                }  /* if */
            }
            else
            {
                uStatus = NAKILLEGALVALUE;  /* illegal value, not in table */
            }
        break;  /* AE_STOP */
        
        default:
            uStatus = NAKILLEGALVALUE;
        break;  /* AE_STOP */
    };  /* switch() */
    return(uStatus);
}  /* xLoLGC_BLEGCBuzzer() */



