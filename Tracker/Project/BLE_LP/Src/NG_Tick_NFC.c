/**
  ******************************************************************************
  * @file    NG_Tick_NFC.c
  * @author  Thomas W Liu
  * @brief   Source file of NFC.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bluenrg_lp.h"
#include "NG_Tick_uart.h"
#include "lib_NDEF_config.h"
#include "tagtype5_wrapper.h"

#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_app.h"

#include "LOLConfig.h"
#include "NG_Tick_LOL_Cmd.h"


#define EN_RD_NFC_MSG   /* !@#$ */
#define EN_RD_NFC_NDEF_MSG

#define R4REMOVED       /* R4 is removed from PCBA-ESB */
#define ENABLE_NFC_DELAY        /* enable delay after write & read */

/* constant variable declaration ------------------------------------------------------*/
#ifdef EN_RD_NFC_MSG   /* !@#$ */
const char ccNFCStr_RstMBEnDyn_OK[] = "NFC_ResetMBEn_Dyn:  NFCTAG OK\r\n";
const char ccNFCStr_RstMBEnDyn_NOK[] = "NFC_ResetMBEn_Dyn: NFCTAG !OK\r\n";
const char ccNFCStr_SelectProtocol_OK[] = "NFC_SelProtocol:  NDEF OK\r\n";
const char ccNFCStr_SelectProtocol_NOK[] = "NFC_SelProtocol: NDEF !OK\r\n";
const char ccNFCStr_WrData_OK[] = "NFCTAG_WrData:  OK\r\n";
const char ccNFCStr_WrData_NOK[] = "NFCTAG_WrData: Not OK\r\n";
const char ccNFCStr_WrNDEFData_OK[] = "NFCTAG_WrData:  NDEF OK\r\n";
const char ccNFCStr_WrNDEFData_NOK[] = "NFCTAG_WrData: NDEF !OK\r\n";
const char ccNFCStr_RdData_OK[] = "NFCTAG_RdData:  OK\r\n";
const char ccNFCStr_RdData_NOK[] = "NFCTAG_RdData: !OK\r\n";
uint8_t cNFCStr_RstMBEnDyn_Status[] = "NFC_ResetMBEn_Dyn status: ";
uint8_t cNFCStr_WrNDEFData_Status[] = "NFCTAG_WrNDEFData status: ";
#endif

const char uccNGTStr_NFC_NF[] = "NFC not found\r\n";
const char uccNGTStr_NFC_Init[] = "NFC initializing\r\n";
const char uccNGTStr_NFC_InitF[] = "NFC initializing Finished\r\n";

/* constant variable declaration ------------------------------------------------------*/
/* cannot make this string constant, MPBID LoL command write to this */
/*                                   10        20   25  30        40        50        60        70        80*/
/*                        01234567890123456789012345678901234567890123456789012345678901234567890123456789*/
uint8_t iNFCRecord0[] = {"t.mke.tl/t?m=0100ffffff&v=7fffffffffffffffff&b=0000&n=0000&r=0000"};


/* variable declaration ------------------------------------------------------*/
uint8_t msg_NFT_Test_UM_Rd[MSG_UM_READ_SIZE] = { 0 };   /* read buffer */
uint8_t iArrayNFCMemory[NFC_MEM_STR_SIZE] = { 0 };      /* write buffer */

/* variable declaration ------------------------------------------------------*/
const uint8_t uccNGTStr_NFC_Int[] = "NFC Interrupt";
uint8_t iLenRecord0 = sizeof(iNFCRecord0) - 1;  /* exclude terminator */

__IO uint8_t ubNFCInt_flag;     /* boolean NFC interrupt 2 flag */
int16_t ubNFCInt_Cnt = 0;
int32_t st25dvFound = 0;        /* device found, default to device not found */
uint8_t offset = 00; //50;

int32_t iStatus_NFCTAG_RstMBEn_Dyn = 0; /* status from rest MBEN_Dyn register */
int32_t iStatus_SelectProtocol = 0;     /* status from select protocol */
int16_t iNDEF_Status = 0;               /* status from NDEF_Wrapper_WriteData() */
int16_t iNFC_Status = 0;                /* status any NFC memory operation */

/* External variables --------------------------------------------------------*/
extern I2C_HandleTypeDef hi2cx; /* declared in NG_Tick_NFC_i2c.c */


/* function prototype --------------------------------------------------------*/
void NFC_Init_All(void);
int32_t SearchNFC(void);
void NFC_PowerIO_Init(void);
void NFC_Interrupt_Init(void);
void Config_NFC4_GPO(uint16_t);
void CheckNFC(void);
void WipeNFCUserMemory(void);
int16_t WriteNDEFURLDataToNFCMemory(void);
int16_t WriteDynDataToNFCMemory(void);
int16_t ReadNFCMemToDynData(void);
int16_t WriteDataToNFCMemory(void);
void WriteAltDataToNFCMemory(void);
uint8_t xLoLMT_NFCRead( uint8_t *);
uint8_t xLoLMT_NFCWrite( uint8_t *);
void xNFC_TimeDelay( int16_t);

/* external variable declaration ---------------------------------------------*/

/* external function prototype -----------------------------------------------*/
  

/**
  ******************************************************************************
  * @func   NFC_Init_All()
  * @brief  Init NFC power pin, init variables.
  *         - Search NFC device.  Retry max retry if not found. Delay 50ms before next attempt.
  *         - init interrupt io pin
  *         - config GPO pin
  * @param  None
  * @gvar   SystemCB
  * @gfunc  NFC_PowerIO_Init(), SearchNFC(), NFC_Interrupt_Init(), Config_NFC4_GPO()
  * @retval TRUE if st25dv is found
  *         FALSE if st25dv is not found
  ******************************************************************************
  */
void NFC_Init_All(void)
{
    NFC_PowerIO_Init();
    
    /* !@#$ to support RP2.5+ PCBA, must turn on power to access NFC */
#ifdef ENABLE_RND_MSG
    cStartTx_WaitTilEndOfTx(uccNGTStr_NFC_Init);
#endif
    
    SystemCB.NFCCB.iRetryCnt = SystemCB.NFCCB.iMaxRetry;
    while ((!SystemCB.NFCCB.uFound) && (SystemCB.NFCCB.iRetryCnt-- > 0))
    {
        if (SearchNFC())
        {
            SystemCB.NFCCB.uFound = TRUE;
#ifdef ENABLE_RND_MSG
            cStartTx_WaitTilEndOfTx(uccNGTStr_NFC_InitF);
#endif
        }
        else
        {
#ifdef ENABLE_RND_MSG
            cStartTx_WaitTilEndOfTx(uccNGTStr_NFC_NF);
#endif
            uint32_t uwFirstTick;       /* local var declaration */
            uwFirstTick = uwTick;
            while ((uwTick - uwFirstTick) < RETRYDLY_FINDNFC);  /* delay before next attempt */
        }
    }  /* while */
    
    if (TRUE == SystemCB.NFCCB.uFound)
    {
        NFC_Interrupt_Init();
        Config_NFC4_GPO(ST25DV_GPO_INT_ENABLE);
        SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_NFC;
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx(uccNGTStr_NFC_InitF);
#endif
#if 0
        WipeNFCUserMemory();  /* wipe NFC memory */
        WriteNDEFURLDataToNFCMemory();
#endif
    }
    else
    {
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx(uccNGTStr_NFC_NF);
#endif
    }  /* if */
}  /* NFC_Init_All() */


/**
  ******************************************************************************
  * @func   SearchNFC()
  * @brief  Search NFC, st25dv, via i2c bus
  * @param  None
  * @gvar   st25dvFound
  * @gfunc  NFC04A1_NFCTAG_Init()
  * @retval TRUE if st25dv is found
  *         FALSE if st25dv is not found
  ******************************************************************************
  */
int32_t SearchNFC(void)
{
    int32_t status;

    /* Init ST25DV driver & check device status */
    status = NFC04A1_NFCTAG_Init(NFC04A1_NFCTAG_INSTANCE);
    st25dvFound = FALSE;          /* default to device not found */
    if (NFCTAG_OK == status)      /* is NEC TAG found */
    {
        st25dvFound = TRUE;       /* set new value, device found */
    }  /* if status  */
    
    return(st25dvFound);
}  /* SearchNFC() */

/**
  ******************************************************************************
  * @func   NFC_PowerIO_Init()
  * @brief  initialize gpio to power NFC(st25dv)
  *         support RP2.5 PCBA:  PB3 is set as input
  *         support RP2.5+ PCBA:  PB3 is set as output, as power to NFC chip
  * @param  None
  * @gvar   None
  * @gfunc  ST25DV_GPIO_CLK_ENABLE(), LL_GPIO_SetOutputPin(),
  *         LL_GPIO_SetPinMode(), LL_GPIO_SetPinSpeed(), LL_GPIO_SetPinPull()
  * @retval None
  ******************************************************************************
  */
void NFC_PowerIO_Init(void)
{
#ifdef R4REMOVED
    /* set as input, support RP2.5+ PCBA, R8 removed */
    /* set as output pin, to power NFC chip */
    ST25DV_GPIO_CLK_ENABLE();
//    LL_GPIO_SetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
    LL_GPIO_ResetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);

    LL_GPIO_SetPinMode( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinSpeed( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_PULL_DOWN);
    LL_GPIO_SetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
#else
/* set as input, support RP2.5+ PCBA */
    ST25DV_GPIO_CLK_ENABLE();
    LL_GPIO_SetOutputPin( ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
    LL_GPIO_SetPinMode( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinSpeed( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull( ST25DV_GPIO_PORT, ST25DV_PWR_PIN, LL_GPIO_PULL_NO);
#endif
}  /* NFC_PowerIO_Init() */

/**
  ******************************************************************************
  * @func   NFC_Interrupt_Init()
  * @brief  initialize gpio for interrupt from st25dv.
  * @param  None
  * @gvar   None
  * @gfunc  ST25DV_GPIO_CLK_ENABLE(), ST25DV_SYSCFG_CLK_ENABLE(), LL_GPIO_SetPinMode(),
  *         LL_GPIO_SetPinSpeed(), LL_GPIO_SetPinPull(), ST25DV_EXTI_LINE_ENABLE(),
  *         ST25DV_EXTI_RISING_TRIG_ENABLE(), LL_EXTI_IsInterruptPending(),
  *         LL_EXTI_ClearInterrupt(), NVIC_SetPriority(),
  *         NVIC_EnableIRQ(), NVIC_SetPriority()
  * @retval None
  ******************************************************************************
  */
void NFC_Interrupt_Init(void)
{
    /* Enable the Clock */
    ST25DV_GPIO_CLK_ENABLE();
    ST25DV_SYSCFG_CLK_ENABLE();
    
    /* Configure GPIO for NFC */
    LL_GPIO_SetPinMode( ST25DV_GPIO_PORT, ST25DV_PIN, LL_GPIO_MODE_INPUT);
    LL_GPIO_SetPinSpeed( ST25DV_GPIO_PORT, ST25DV_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull( ST25DV_GPIO_PORT, ST25DV_PIN, LL_GPIO_PULL_NO);
    
    /* Enable a rising trigger External line 10 Interrupt */
    ST25DV_EXTI_LINE_ENABLE();
    ST25DV_EXTI_RISING_TRIG_ENABLE();

    /* Clear the event occurred on the interrupt portb6, push2 button */
    if (LL_EXTI_IsInterruptPending(ST25DV_EXTI_LINE) != RESET)
    {
        LL_EXTI_ClearInterrupt(ST25DV_EXTI_LINE);
    }
    
    /* Configure NVIC for ST25DV_EXTI_IRQn */
    NVIC_SetPriority( ST25DV_EXTI_IRQn, IRQ_HIGH_PRIORITY);
    NVIC_EnableIRQ( ST25DV_EXTI_IRQn);
    
    /* Configure NVIC for SysTick_IRQn */
    NVIC_SetPriority( SysTick_IRQn, IRQ_HIGH_PRIORITY);
}       /* NFC_Interrupt_Init() */


/**
  ******************************************************************************
  * @func   void Config_NFC4_GPO()
  * @brief  Configure GPO interrupt mode
  * @param  gpo register value
  * @gvar   None
  * @gfunc  NFC04A1_NFCTAG_PresentI2CPassword(), NFC04A1_NFCTAG_ConfigIT()
  * @retval None
  * @note   assume NFC is already found, will NOT perform NFC check
  ******************************************************************************
  */
void Config_NFC4_GPO(uint16_t gpo_reg_val)
{
    const ST25DV_PASSWD st25dv_i2c_password = {.MsbPasswd = 0, .LsbPasswd=0};

#if 1
    /* Present configuration password */
    NFC04A1_NFCTAG_PresentI2CPassword( NFC04A1_NFCTAG_INSTANCE, st25dv_i2c_password);
    
    /* Set GPO Configuration */
    NFC04A1_NFCTAG_ConfigIT( NFC04A1_NFCTAG_INSTANCE, gpo_reg_val);
#else
    /* Init ST25DV driver */
    while (NFC04A1_NFCTAG_Init(NFC04A1_NFCTAG_INSTANCE) != NFCTAG_OK);

    /* Set EXTI settings for GPO Interrupt */
    NFC04A1_GPO_Init();

    /* Present configuration password */
    NFC04A1_NFCTAG_PresentI2CPassword(NFC04A1_NFCTAG_INSTANCE, st25dv_i2c_password );

    /* Set GPO Configuration */
    //NFC04A1_NFCTAG_ConfigIT(NFC04A1_NFCTAG_INSTANCE,ST25DV_GPO_ENABLE_MASK | ST25DV_GPO_FIELDCHANGE_MASK );
#endif
}       /* Config_NFC4_GPO() */
  

/**
  ******************************************************************************
  * @func   CheckNFC()
  * @brief  check current NFC state:  idle, write all, write dynmaics data
  *         idle: check either wait system flash, read or write.
  *         write all: write all NFC data into NFC memory
  *         write dyn: write dynamic data into NFC memory
  *         update: if error, increment error counter
  * @param  None
  * @gvar   ubNFCInt_flag, ubNFCInt_Cnt, SystemCB, SystemIB
  * @gfunc  SearchNFC(), WriteNDEFURLDataToNFCMemory(), xNFC_TimeDelay(), LL_GPIO_ResetOutputPin(),
  *         WriteDynDataToNFCMemory(), LL_GPIO_ResetOutputPin()
  * @retval None
  ******************************************************************************
  */
void CheckNFC()
{
    if (ubNFCInt_flag)
    {
        SystemIB.SysRAMIB.uNFCEvents++;  //^&*(
                
#if 0 /* $%^& test code, delete later */
        if (SHIP_MODE == SystemIB.SysFlashIB.uShipModeStatus)
        {
AudioEngine_Init_All();
            sAppCB.uAction = APPCB_ACT_ACTIVATE;  /* go to activation mode */
        }  /* if */
#endif

        if ((AUDIOENG_ST_IDLE == sAudioEngine.uiAE_State) && (FALSE == sAudioEngine.uiAE_NewCmd))
        {
            sAudioEngine.uiAE_NewSongId = SONG_INDEX_ACK1;  //SONG_INDEX_TT1;
            sAudioEngine.uiAE_NewCmd = TRUE;
            SystemIB.SysRAMIB.uNFCBuzzerEvents++;  //^&*(
        }
        
        ubNFCInt_flag = FALSE;
        ubNFCInt_Cnt++;
          
#ifdef ENABLE_RND_MSG
        CrEFUint8_StartTx_WaitTilEndOfTx_N( (uint8_t *)uccNGTStr_NFC_Int, ubNFCInt_Cnt);   /* send interrupt message */
#endif
    }  /* if */
    
    switch (SystemCB.NFCCB.uState)
    {
        case NFCCB_ST_IDLE:  /* idle */
            switch (SystemCB.NFCCB.uAction)
            {
                case NFCCB_ACT_WAITSYSF:
                    /* code:  wait for system flash memory finished writing, then go to write all data to NFC emmory */
                    if ((FLASHCB_ST_IDLE == SystemCB.FlashCB.uState) && (FLASHCB_ACT_IDLE == SystemCB.FlashCB.uAction))
                    {
                        SystemCB.NFCCB.uAction = NFCCB_ACT_WRITE;
                    }  /* if */
                break;  /* NFCCB_ACT_WAITSYSF */
                
                case NFCCB_ACT_WRITE:
                    SystemCB.NFCCB.uState = NFCCB_ST_WRITEALL;
                    SystemCB.NFCCB.uAction = NFCCB_ACT_IDLE;
                break;  /* NFCCB_ACT_WRITE */
                
                case NFCCB_ACT_WRITEDYN:
                    /* turn on NFC power, add delay before dynamic write */
                    LL_GPIO_SetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
                    xNFC_TimeDelay(NFC_PREWRITE_DELAY_MS);
                    SystemCB.NFCCB.uState = NFCCB_ST_WRITEDYN;
                    SystemCB.NFCCB.uAction = NFCCB_ACT_IDLE;
                break;  /* NFCCB_ACT_WRITEDYN */
            }
        break;  /* NFCCB_ST_IDLE */
        
        case NFCCB_ST_WRITEALL:  /* NFCCB_ST_WRITEALL */
            /* check for i2c token */
            if (SystemIB.SysRAMIB.uI2CToken != I2C_TOKEN_NFC)
            {
                SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_NFC;
                SearchNFC();
            }  /* if */
            if (!WriteNDEFURLDataToNFCMemory())
            {
                SystemCB.NFCCB.uErrCode = NFCCB_ERR_WRITE;
                SystemCB.NFCCB.uwErrCnt++;
            }
            SystemCB.NFCCB.uState = NFCCB_ST_IDLE;
            
            /* if in 2ship mode, ship mode power down or ship mode, add delay before power down */
            if (SHIP_MODE == SystemIB.SysFlashIB.uShipModeStatus)
            {
                xNFC_TimeDelay(NFC_AFTWRITE_DELAY_MS);
                LL_GPIO_ResetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
            }
        break;  /* NFCCB_ST_WRITEALL */
        
        case NFCCB_ST_WRITEDYN:  /* NFCCB_ST_WRITEDYN */
            /* check for i2c token */
            if (SystemIB.SysRAMIB.uI2CToken != I2C_TOKEN_NFC)
            {
                SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_NFC;
                SearchNFC();
            }  /* if */
            if (!WriteDynDataToNFCMemory())
            {
                SystemCB.NFCCB.uErrCode = NFCCB_ERR_WRITEDYN;
                SystemCB.NFCCB.uwErrCnt++;
            }
            SystemCB.NFCCB.uState = NFCCB_ST_IDLE;
            
            /* after dynamic write/read, add delay before power down */
            xNFC_TimeDelay(NFC_AFTWRITE_DELAY_MS);
            LL_GPIO_ResetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
        break;  /* NFCCB_ST_WRITEDYN */
    }  /* switch */
}  /* CheckNFC() */


/**
  ******************************************************************************
  * @func   WipeNFCUserMemory()
  * @brief  wipe all NFC user memory to zeros, total 512 bytes
  *         write and read buffers are 128 bytes, so write and read 4 times each
  * @param  None
  * @gvar   msg_NFT_Test_UM_Rd, iStatus_NFCTAG_RstMBEn_Dyn, iStatus_SelectProtocol
  * @gfunc  NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         NFC04A1_NFCTAG_WriteData(), NFC04A1_NFCTAG_ReadData()
  * @retval None
  ******************************************************************************
  */
void WipeNFCUserMemory(void)
{
    /* init buffer, reset & select type 5 protocol */
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
    iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);
    
    /* write zero and read back */
    NFC04A1_NFCTAG_WriteData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, NFC_MEM_STR_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
#endif
    NFC04A1_NFCTAG_WriteData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, NFC_MEM_STR_SIZE, NFC_MEM_STR_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
#endif
    NFC04A1_NFCTAG_WriteData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, NFC_MEM_STR_SIZE << 1, NFC_MEM_STR_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
#endif
    NFC04A1_NFCTAG_WriteData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, NFC_MEM_STR_SIZE * 3, NFC_MEM_STR_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
#endif
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, MSG_UM_READ_SIZE, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, MSG_UM_READ_SIZE << 1, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, MSG_UM_READ_SIZE * 3, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
}  /* WipeNFCUserMemory() */

/**
  ******************************************************************************
  * @func   WriteNDEFURLDataToNFCMemory()
  * @brief  Init CCF.  Write CCF and NDEF URL to memory
  * @param  None
  * @gvar   CCFileStruct, iStatus_NFCTAG_RstMBEn_Dyn, iStatus_SelectProtocol,
  *         iArrayNFCMemory, iNFCRecord0, iLenRecord0, iNDEF_Status
  * @gfunc  NfcType5_TT5Init(), NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         memset(), NfcType5_WriteNDEF(), NFC04A1_NFCTAG_ReadData(), memcmp(), xNFC_TimeDelay()
  * @retval TRUE if ok, FALSE = any op error
  ******************************************************************************
  */
int16_t WriteNDEFURLDataToNFCMemory(void)
{
    int16_t iStatus = FALSE;
    
    /* init CCFile information */
    CCFileStruct.MagicNumber  = NFCT5_MAGICNUMBER_E1_CCFILE;
    CCFileStruct.Version      = NFCT5_VERSION_V1_0;
    CCFileStruct.MemorySize   = 0x40;
    CCFileStruct.TT5Tag       = 0x0;
    
    NfcType5_TT5Init();         /* init for type 5, init CCFile */
    iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
//    CrEFUint8_StartTx_WaitTilEndOfTx_N( cNFCStr_RstMBEnDyn_Status, iStatus_NFCTAG_RstMBEn_Dyn);
    iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);
    
    /* init URL information in NDEF record format */
    memset( iArrayNFCMemory, 0, NFC_MEM_STR_SIZE);
    iArrayNFCMemory[NDEF_INDEX_REC_HDR]= 0xD1;    /* record header */
    iArrayNFCMemory[NDEF_INDEX_TYPE_LEN] = 0x01;  /* type length = 1 */
//    iArrayNFCMemory[NDEF_INDEX_PL_LEN] = 0x16;    /* payload length */
    iArrayNFCMemory[NDEF_INDEX_PL_LEN] = 0x42;    /* payload length */
    iArrayNFCMemory[NDEF_INDEX_REC_TYPE] = 0x55;  /* URI record type = U */
    iArrayNFCMemory[NDEF_INDEX_HDR_ID] = 0x04;    /* URI header id = https:// */
    memcpy( &iArrayNFCMemory[NDEF_INDEX_URL_BODY], iNFCRecord0, iLenRecord0);

    /* write URL NDEF record to memory */
    iNDEF_Status = NfcType5_WriteNDEF( iLenRecord0 + NDEF_INDEX_URL_BODY, iArrayNFCMemory);    // NDEF record - URL
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
#endif
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    iNFC_Status = NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    if (NFCTAG_OK == iNFC_Status)
    {
        /* skip CCF for comparison */
        if (0 == memcmp( iArrayNFCMemory, &msg_NFT_Test_UM_Rd[CCF_INDEX_REC_HDR], iLenRecord0))
        {
            iStatus = TRUE;
        }
    }
    return(iStatus);
}  /* WriteNDEFURLDataToNFCMemory() */

/**
  ******************************************************************************
  * @func   WriteDynDataToNFCMemory()
  * @brief  write record 1 data to NFC memory
  * @param  None
  * @gvar   msg_NFT_Test_UM_Rd[], iStatus_NFCTAG_RstMBEn_Dyn, iStatus_SelectProtocol,
  *         iArrayNFCMemory[], iNFCRecord1, iLenRecord1
  * @gfunc  memset(), NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         sprintf(), memcpy(), NDEF_Wrapper_WriteData(), 
  *         NFC04A1_NFCTAG_ReadData(), memcmp()
  * @retval TRUE if ok, FALSE = any op error
  ******************************************************************************
  */
int16_t WriteDynDataToNFCMemory(void)
{
    int16_t iStatus = FALSE;
    int16_t index;
    int16_t iCnt;
    char cStr[SIZE_RECX_STR];
    
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
    iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);
    
    memset( cStr, 0, SIZE_RECX_STR);
    for (iCnt = 0, index = 0; iCnt < NFC_BLE_ADV_SIZE; iCnt++, index += 2)
    {
        sprintf( &cStr[index], "%02x", SystemIB.SysRAMIB.uBLEAdv[iCnt]);
    }  /* for */
    memcpy( &iArrayNFCMemory[NFC_RMOS_BADV], cStr, NFC_BLE_ADV_SIZE << 1);
    
    sprintf( cStr, "%04x", SystemIB.SysRAMIB.uNFCBuzzerEvents);
    memcpy( &iArrayNFCMemory[NFC_RMOS_BUZZEV], cStr, NFC_DIG_BUZZEV);
    sprintf( cStr, "%04x", SystemIB.SysRAMIB.uNFCEvents);
    memcpy( &iArrayNFCMemory[NFC_RMOS_NFCEV], cStr, NFC_DIG_NFCEV);
    sprintf( cStr, "%04x", SystemIB.SysRAMIB.uNFCResetEvents);
    memcpy( &iArrayNFCMemory[NFC_RMOS_RSTEV], cStr, NFC_DIG_RSTEV);

    /* write dynamic data in the NFC memory */
    NDEF_Wrapper_WriteData( &iArrayNFCMemory[NFC_RMOS_BADV], NFC_MEMOS_BADV, NFC_MEMLEN_DYNM);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_WRITE_DELAY_MS);
#endif
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    iNFC_Status = NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    if (NFCTAG_OK == iNFC_Status)
    {
        /* skip CCF for comparison */
        if (0 == memcmp( iArrayNFCMemory, &msg_NFT_Test_UM_Rd[CCF_INDEX_REC_HDR], iLenRecord0))
        {
            iStatus = TRUE;
        }
    }
    return(iStatus);
}  /* WriteDynDataToNFCMemory() */

/**
  ******************************************************************************
  * @func   ReadNFCMemToDynData()
  * @brief  read NFC dynamic data
  * @param  None
  * @gvar   iStatus_NFCTAG_RstMBEn_Dyn, iStatus_SelectProtocol, iNFC_Status,
  *         iArrayNFCMemory[], SystemIB
  * @gfunc  LL_GPIO_SetOutputPin(), xNFC_TimeDelay(), NFC04A1_NFCTAG_ResetMBEN_Dyn(),
  *         NfcTag_SelectProtocol(), memset(), NFC04A1_NFCTAG_ReadData(), memcpy(),
  *         sscanf(), LL_GPIO_ResetOutputPin()
  * @retval TRUE if ok, FALSE = any op error
  ******************************************************************************
  */
int16_t ReadNFCMemToDynData(void)
{
    int16_t iStatus = FALSE;
    int iNum;
    char cStr[SIZE_RECX_STR];
    
    /* turn on NFC power, add delay before dynamic write */
    LL_GPIO_SetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    
//    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
    iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);

    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    iNFC_Status = NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
#ifdef ENABLE_NFC_DELAY
    xNFC_TimeDelay(NFC_READ_DELAY_MS);
#endif
    if (NFCTAG_OK == iNFC_Status)
    {
	memcpy( iArrayNFCMemory, &msg_NFT_Test_UM_Rd[NDEF_INDEX_URL_BODY+1], iLenRecord0 + NDEF_INDEX_URL_BODY + 1);

        /* restore buzzer events from NFC memory */
        memset( cStr, 0, SIZE_RECX_STR);
        memcpy( cStr, &iArrayNFCMemory[NFC_RMOS_BUZZEV], NFC_DIG_BUZZEV);
        sscanf( cStr, "%x", &iNum);
        SystemIB.SysRAMIB.uNFCBuzzerEvents = (int16_t)iNum;

        /* restore NFC events from NFC memory */
        memset( cStr, 0, SIZE_RECX_STR);
        memcpy( cStr, &iArrayNFCMemory[NFC_RMOS_NFCEV], NFC_DIG_NFCEV);
        sscanf( cStr, "%x", &iNum);
        SystemIB.SysRAMIB.uNFCEvents = (int16_t)iNum;

        /* restore buzzer events from NFC memory */
        memset( cStr, 0, SIZE_RECX_STR);
        memcpy( cStr, &iArrayNFCMemory[NFC_RMOS_RSTEV], NFC_DIG_RSTEV);
        sscanf( cStr, "%x", &iNum);
        SystemIB.SysRAMIB.uNFCResetEvents = (int16_t)iNum;
    
        iStatus = TRUE;
    }
    /* if in 2ship mode, ship mode power down or ship mode, add delay before power down */
    if ((SHIP_MODE == SystemIB.SysFlashIB.uShipModeStatus) ||
        (NORMAL_MODE == SystemIB.SysFlashIB.uShipModeStatus))
    {
        xNFC_TimeDelay(NFC_AFTWRITE_DELAY_MS);
        LL_GPIO_ResetOutputPin(ST25DV_GPIO_PORT, ST25DV_PWR_PIN);
    }
    return(iStatus);
}  /* ReadNFCMemToDynData() */


/**
  ******************************************************************************
  * @func   WriteDataToNFCMemory()
  * @brief  Init CCF, write NDEF URL, record 1,2 and 3 to user memory
  * @param  None
  * @gvar   CCFileStruct, iStatus_NFCTAG_RstMBEn_Dyn, iStatus_SelectProtocol,
  *         iArrayNFCMemory, iNDEF_Status, iNFCRecord0, iLenRecord0, msg_NFT_Test_UM_Rd,
  *         iNFCRecord1, iLenRecord1, iNFCRecord2, iLenRecord2, iNFCRecord3, iLenRecord3
  * @gfunc  NfcType5_TT5Init(), NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         memset(), NfcType5_WriteNDEF(), NFC04A1_NFCTAG_ReadData(), SwapMsgToDoubleQuote(),
  *         NDEF_Wrapper_WriteData(), 
  * @retval None
  ******************************************************************************
  */
int16_t WriteDataToNFCMemory(void)
{
    int16_t iStatus = FALSE;
    
    /* init CCFile information */
    CCFileStruct.MagicNumber  = NFCT5_MAGICNUMBER_E1_CCFILE;
    CCFileStruct.Version      = NFCT5_VERSION_V1_0;
    CCFileStruct.MemorySize   = 0x40;
    CCFileStruct.TT5Tag       = 0x0;
    
    NfcType5_TT5Init();         /* init for type 5, init CCFile */
    iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
//    CrEFUint8_StartTx_WaitTilEndOfTx_N( cNFCStr_RstMBEnDyn_Status, iStatus_NFCTAG_RstMBEn_Dyn);
    iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);
    
    /* init URL information in NDEF record format */
    memset( iArrayNFCMemory, 0, NFC_MEM_STR_SIZE);
    iArrayNFCMemory[NDEF_INDEX_REC_HDR]= 0xD1;    /* record header */
    iArrayNFCMemory[NDEF_INDEX_TYPE_LEN] = 0x01;  /* type length = 1 */
    iArrayNFCMemory[NDEF_INDEX_PL_LEN] = 0x16;    /* payload length */
    iArrayNFCMemory[NDEF_INDEX_REC_TYPE] = 0x55;  /* URI record type = U */
    iArrayNFCMemory[NDEF_INDEX_HDR_ID] = 0x04;    /* URI header id = https:// */
    memcpy( &iArrayNFCMemory[NDEF_INDEX_URL_BODY], iNFCRecord0, iLenRecord0);

    /* write URL NDEF record to memory */
    iNDEF_Status = NfcType5_WriteNDEF( iLenRecord0 + NDEF_INDEX_URL_BODY, iArrayNFCMemory);    // NDEF record - URL
    iNFC_Status = NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    if (NFCTAG_OK == iNFC_Status)
    {
        iStatus = memcmp( iArrayNFCMemory, msg_NFT_Test_UM_Rd, iLenRecord0 + NDEF_INDEX_URL_BODY);
    }
    return(iStatus);
}  /* WriteDataToNFCMemory() */

/**
  ******************************************************************************
  * @func   WriteAltDataToNFCMemory()
  * @brief  write alternate records 1, 2 and 3 to NFC memory, via i2c bus
  * @param  None
  * @gvar   iArrayNFCMemory, iNDEF_Status, iNFCRecordA, iLenRecordA, msg_NFT_Test_UM_Rd,
  *         iNFCRecordB, iLenRecordB, iNFCRecordC, iLenRecordC
  * @gfunc  NfcType5_TT5Init(), NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         memset(), NfcType5_WriteNDEF(), NFC04A1_NFCTAG_ReadData(), SwapMsgToDoubleQuote(),
  *         NDEF_Wrapper_WriteData(), 
  * @retval None
  ******************************************************************************
  */
void WriteAltDataToNFCMemory(void)
{
#if 0  // %^&*
    /* init record 1 */
    memset( iArrayNFCMemory, 0, NFC_MEM_STR_SIZE);
    memcpy( iArrayNFCMemory, iNFCRecordA, iLenRecordA);
    SwapMsgToDoubleQuote( iArrayNFCMemory, iLenRecordA);
    iNDEF_Status = NDEF_Wrapper_WriteData( iArrayNFCMemory, NFC_OS_REC1, iLenRecordA);
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    
   /* init record 2 */
    memset( iArrayNFCMemory, 0, NFC_MEM_STR_SIZE);
    memcpy( iArrayNFCMemory, iNFCRecordB, iLenRecordB);
    SwapMsgToDoubleQuote( iArrayNFCMemory, iLenRecordB);
    iNDEF_Status = NDEF_Wrapper_WriteData( iArrayNFCMemory, NFC_OS_REC2, iLenRecordB);
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);

    /* init record 3 */
    memset( iArrayNFCMemory, 0, NFC_MEM_STR_SIZE);
    memcpy( iArrayNFCMemory, iNFCRecordC, iLenRecordC);
    SwapMsgToDoubleQuote( iArrayNFCMemory, iLenRecordC);
    iNDEF_Status = NDEF_Wrapper_WriteData( iArrayNFCMemory, NFC_OS_REC3, iLenRecordC);
    memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
    NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
#endif
}  /* WriteAltDataToNFCMemory() */

/**
  ******************************************************************************
  * @func   xLoLMT_NFCRead()
  * @brief  read NFC memory from address, with so many byte count
  * @param  pointer to message
  * @gvar   msg_NFT_Test_UM_Rd[], iArrayNFCMemory[]
  * @gfunc  memset(), memcpy(), NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         NFC04A1_NFCTAG_ReadData()
  * @retval LOL_RD_RESP, NAKUNKNOWNERROR
  ******************************************************************************
  */
uint8_t xLoLMT_NFCRead( uint8_t *uPtr)
{
    uint8_t uSrcAddr;
    uint8_t uByteCnt;
    uint8_t uStatus = LOL_ACK_RESP;

    switch (uPtr[2])
    {
        case 0x80:
            memset( msg_NFT_Test_UM_Rd, 0, NFC_MEM_STR_SIZE);
            memcpy( msg_NFT_Test_UM_Rd, &iArrayNFCMemory[0], 32);
            uStatus = LOL_RD_RESP;
        break;
        
        case 0x90:
            memset( msg_NFT_Test_UM_Rd, 0, NFC_MEM_STR_SIZE);
            memcpy( msg_NFT_Test_UM_Rd, &iArrayNFCMemory[32], 32);
            uStatus = LOL_RD_RESP;
        break;
        
        case 0xa0:
            memset( msg_NFT_Test_UM_Rd, 0, NFC_MEM_STR_SIZE);
            memcpy( msg_NFT_Test_UM_Rd, &iArrayNFCMemory[64], 32);
            uStatus = LOL_RD_RESP;
        break;
        
        default:
            /* init buffer, reset & select type 5 protocol */
            memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
            iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
            iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);
            uSrcAddr = uPtr[2];
            uByteCnt = uPtr[3];
            uStatus = NAKUNKNOWNERROR;
            if (NFCTAG_OK == NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, uSrcAddr, uByteCnt))
            {
                uStatus = LOL_RD_RESP;
            }
        break;
    }  /* switch */
    return(uStatus);
}  /* xLoLMT_NFCRead() */


/**
  ******************************************************************************
  * @func   memvalcmp()
  * @brief  compare n bytes of memory to value, byte by byte
  * @param  source ptr of memory
  * @param  value compared to
  * @param  number of bytes in memory to compare
  * @gvar   None
  * @gfunc  None
  * @retval TRUE if all memory equal to value
  *         FALSE if any memory not equal to value
  ******************************************************************************
  */
uint8_t memvalcmp( uint8_t *pSrc, uint8_t value, int8_t len)
{
    int8_t iCnt;
    uint8_t bMatch = TRUE;
    
    iCnt = len;
    while (bMatch && (iCnt-- > 0))
    {
        if (value != *pSrc++)
        {
            bMatch = FALSE;
        }  /* if */
    }  /* while */
    return(bMatch);
}  /* memvalcmp */

/**
  ******************************************************************************
  * @func   xLoLMT_NFCWrite()
  * @brief  Parse NFC commands: erase, write, write dynamic
  * @param  source ptr of commands & parameters
  * @gvar   iStatus_NFCTAG_RstMBEn_Dyn, iStatus_SelectProtocol, iNDEF_Status, iNFC_Status
  * @gfunc  memset(), NFC04A1_NFCTAG_ResetMBEN_Dyn(), NfcTag_SelectProtocol(),
  *         NFC04A1_NFCTAG_WriteData(), NFC04A1_NFCTAG_ReadData(),
  *         WriteNDEFURLDataToNFCMemory(), WriteDynDataToNFCMemory()
  * @retval LOL_ACK_RESP, NAKMALFORMEDPACKET, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_NFCWrite( uint8_t *uPtr)
{
    uint16_t uSrcAddr;
    uint8_t uByteCnt;
    uint8_t uStatus;

    switch (uPtr[2])
    {
        case LOL_LM_NFC_ERASE:
            uSrcAddr = uPtr[3] | (uPtr[4] >> 8);
            uByteCnt = uPtr[5];
            memset( msg_NFT_Test_UM_Rd, 0, MSG_UM_READ_SIZE);
            iStatus_NFCTAG_RstMBEn_Dyn = NFC04A1_NFCTAG_ResetMBEN_Dyn(NFC04A1_NFCTAG_INSTANCE);
            iStatus_SelectProtocol = NfcTag_SelectProtocol(NFCTAG_TYPE5);
            iNDEF_Status = NFC04A1_NFCTAG_WriteData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, uSrcAddr, uByteCnt);
            iNFC_Status = NFC04A1_NFCTAG_ReadData( NFC04A1_NFCTAG_INSTANCE, msg_NFT_Test_UM_Rd, uSrcAddr, uByteCnt);
            uStatus = NAKUNKNOWNERROR;
            if (NFCTAG_OK == iNFC_Status)
            {
                if (memvalcmp( msg_NFT_Test_UM_Rd, 0, uByteCnt))
                {
                    uStatus = LOL_ACK_RESP;
                }
            }
        break;  /* LOL_LM_NFC_ERASE */
      
        case LOL_LM_NFC_WRITE:  /* write URL NDEF to memory */
            uStatus = NAKUNKNOWNERROR;
            if (WriteNDEFURLDataToNFCMemory())
            {
                uStatus = LOL_ACK_RESP;
            }
        break;  /* LOL_LM_NFC_WRITE */
      
        case LOL_LM_NFC_WRITEDYN:  /* write dynamic data */
            uStatus = NAKUNKNOWNERROR;
            if (WriteDynDataToNFCMemory())
            {
                uStatus = LOL_ACK_RESP;
            }
        break;  /* LOL_LM_NFC_WRITEDYN */
      
        default:
            uStatus = NAKILLEGALVALUE;
        break;
    }  /* switch */
    return(uStatus);
}  /* xLoLMT_NFCWrite() */


/**
  ******************************************************************************
  * @func   xNFC_TimeDelay()
  * @brief  delay certian time
  * @param  time in ms
  * @gvar   iNFC_DelayCnt
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void xNFC_TimeDelay( int16_t iDelay)
{
    iNFC_DelayCnt = iDelay;
    while (iNFC_DelayCnt > 0);
}  /* xNFC_TimeDelay() */
