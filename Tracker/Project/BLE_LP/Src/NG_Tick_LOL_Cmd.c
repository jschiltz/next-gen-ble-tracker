/**
  ******************************************************************************
  * @file    NG_Tick_LOL_Cmd.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link commands.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Tools
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
//#include "main.h"
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp.h"
#include "bluenrg_lp_ll_bus.h"
#include "NG_Tick_LOL_Cmd.h"
//#include "NG_Tick_LOL_test.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_sys.h"

/* constant variable declaration ---------------------------------------------*/
//static const uint16_t memoryMapVersion = 0x0022;        // Test data for a read of the version of the virtual memory map

/* constant variable declaration ------------------------------------------------------*/


/* variable declaration ------------------------------------------------------*/
__IO BOOL bLOL_1msF = FALSE;            /* LOL 1ms flag, set by 1ms interrupt, reset in main.c */
uint64_t userPassword = userPass;
uint64_t adminPassword = adminPass;
uint64_t servicePassword = servicePass;
uint64_t METCOPassword = metcoPass;
uint16_t extendedMemoryOffset = 0x1234;
#if 0
#define SIZE_DEVICE_NAME_BUFF    8
char deviceName[SIZE_DEVICE_NAME_BUFF] = {'T', 'E', 'S', 'T', ' ', 'D', 'E', 'V' };
#else
#define SIZE_DEVICE_NAME_BUFF    23
/*                                                   10        20        30 */
/*                                          123456789012345678901234567890 */
char deviceName[SIZE_DEVICE_NAME_BUFF] = { "Next Generation Tracker" };
#endif

#if 0
#define SIZE_MPBID_BUFF    5
#if 0
char deviceMPBID[SIZE_MPBID_BUFF] = {'M', 'P', 'B', 'I', 'D' };  // 0x4E, 0x47, 0x54, 0x30, 0x78 = NGT0x 
#else
char deviceMPBID[SIZE_MPBID_BUFF] = { 0x02, 0x15, 0xF3, 0x4E, 0xBA };
#endif
#endif

uint8_t mailboxArray[4] = { 0x0A, 0x00, 0x01, 0x8A};       // data to test the mailbox functionality
uint8_t MBresponseArray[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
uint8_t requestMPBID[3] = { 0x00, 0x04, 0x05 };
uint8_t maxPacketLength = 20;
uint32_t deviceSWPN = 0x98765432;       /* expect 4 bytes payload response */
uint32_t deviceFwVer = 0x12345678;      /* expect 4 bytes payload response */
uint8_t nakData[2] = {NAKMAXMESSAGE, 0xFF};
uint8_t LOLRXBuff0[CHAN0_RX_BUFF_SIZE];
uint8_t LOLTXBuff0[CHAN0_TX_BUFF_SIZE];
uint8_t LOLRXBuff1[CHAN1_RX_BUFF_SIZE];
uint8_t LOLTXBuff1[CHAN1_TX_BUFF_SIZE];

bool ms_time = FALSE;        // Not time to kick out the 1 mS tick yet
sendVals sendPacket;
ackTransactionVals ackVals;
ackTransactionVals slaveAckVals;
uint32_t testCounter;       // use this counter to send a packet for the Master on channel 0

/* function prototype --------------------------------------------------------*/
void LOL_NGT_Init(void);
void debug_assert(bool);
void begin_super_loop( void );
eCommsErr Tool_data_tx( uint8_t *, uint8_t, uint8_t);  /* !@#$ */
void sendDevNameChannel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void sendSWPNChannel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void slave5( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void simpleAck( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void sendMaxPktLn( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void mailbox( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void MBCompleteCallback1( bool, uint8_t *, uint8_t, uint8_t);
void passwordEnable( uint8_t *, uint8_t, uint8_t , uint8_t, uint8_t );
eMM_Sec_Lev PasswordVerification( uint8_t *);
void setSecLevel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void readMemMapVersion( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void RWExtMemOffset( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void sendMPBIDChannel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void sendFwVersion( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateOfMfg( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateOfService( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateLastUSed( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateTimeRef( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMPlatformId( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMBLEMACAddr( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);

/**
  ******************************************************************************
  * @func   LOL_NGT_Init()
  * @brief  initliaze LOL channel 0 & 1
  * @param  None
  * @gvar   uwNbRxChars, pBufferReadyForRx, iRxMsgCnt, ubRxBufferReadyF
  * @gfunc  LOLInitChannel()
  * @retval None
  ******************************************************************************
  */
void LOL_NGT_Init()
{
    LOLInitChannel( LOL_CHAN_UART, LOLTXBuff0, CHAN0_TX_BUFF_SIZE, LOLRXBuff0,
                   CHAN0_RX_BUFF_SIZE, slaveArray0, SLAVEARRAY0SIZE);
    LOLInitChannel( LOL_CHAN_BLE, LOLTXBuff1, CHAN1_TX_BUFF_SIZE, LOLRXBuff1,
                   CHAN1_RX_BUFF_SIZE, slaveArray1, SLAVEARRAY1SIZE);
}  /* LOL_NGT_Init() */



/**
  ******************************************************************************
  * @func   debug_assert()
  * @brief  debugging
  * @param  None
  * @gvar   
  * @gfunc  fault_disable_system()
  * @retval None
  ******************************************************************************
  */
void debug_assert(bool expression)
{
    #if defined (DEBUG_BUILD)
        if(!expression)
        {
            fault_disable_system( );
        }
    #endif
}  /* debug_assert() */

/***************************************************************************//**
* \fn void begin_super_loop()
* \brief Invokes the start of the application's super loop
* \details Sets the super loop main status flag to invoke the execution of the
* super loop.
* @param[out] bsuper_loop_ready 1ms Super Loop Start Flag
*******************************************************************************/
void begin_super_loop( void )
{
    ms_time = true;
}  /* begin_super_loop() */

/******************************************************************************/
/** \fn	eCommsErr Tool_data_tx(uint8_t *data, uint8_t data_len, uint8_t target);
* \brief    Tool side function used by the library to transmit data
* \details  This function is exposed from the main calling program. It is expected to
*	be called by the library to send OL packets for data transmission
*       \param data	- input array/pointer of bytes
*	\param data_len	- length of input array
*	\param target	- source of packet (BTUART, BATTERY_TERMINAL, etc.)
*	\return		- a eCommsErr with the result of processing the send request.
* \author   Original by: Jay Walsh
* \author   Stolen by: Al Lukowitz
* \pre		none
* \warning	<b> <i> Must exist in main tool code, library expects it!  </i> </b>
*			\n declare your return variable \b volatile
* \code
* uint8_t * data[]={1,2,3,4,5,6,7,8};
* volatile eCommsErr ret_val;
* ret_val = Tool_data_tx(data, 8,BT_UART);
* prinf("%d",ret_val);
* >><some eComsErr value>
* \endcode
*******************************************************************************/
/* Function to send generic data out via the target source. */
eCommsErr Tool_data_tx(uint8_t *data, uint8_t packet_len, uint8_t target) 
{
//    uart_dma_tx(target, packet_len, data);  /* !@#$% insert tx here */
    switch (target)
    {
        case LOL_CHAN_UART:
            pStartNextTransfers( data, packet_len);
        break;  /* LOL_CHAN_UART */
        
        case LOL_CHAN_BLE:
        break;  /* LOL_CHAN_BLE */
    }  /* switch() */
    return(eCommsErr_NONE);
}  /* eCommsErr() */

/******************************************************************************/
/**
* \fn void sendDevNameChannel()
* \brief Process a Read/Write Common Memory Map
* \details Note, write is not legal by definition.  It will be rejected before
*          this routine is called because of the eMM_Sec_Lev__NONE secrutiy setting
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void sendDevNameChannel(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                        uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.ackNack = true;
    if (0x01 == (SID & READWRITEBIT))
    {
        for (int iCnt = 0; iCnt < payloadLength - 2; iCnt++)
        {
            // If we are writing, move the data over!
            deviceName[iCnt] = payloadPtr[iCnt + 2];
        }
    }
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = (uint8_t *)deviceName;
    slaveAckVals.payloadLength = SIZE_DEVICE_NAME_BUFF;
    LOLSendACK(&slaveAckVals);
}  /* sendDevNameChannel() */

/**
  ******************************************************************************
  * @func   sendSWPNChannel()
  * @func   fLMMSWPN()
  * @brief  Read/Write local memory - SW Part Number Request
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void sendSWPNChannel(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* write MPBID, prep ptr to update internal value */
        if (payloadLength != 6)
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = false;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }
        else
        {
//            SystemIB.ulDeviceSWPN = 0;
            SystemIB.ulDeviceSWPN = payloadPtr[2] | (payloadPtr[3] << 8) | (payloadPtr[4] << 16) | (payloadPtr[5] << 24);
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read SWPN, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.ulDeviceSWPN;
        slaveAckVals.payloadLength = LOL_SWPN_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* sendSWPNChannel() */

/******************************************************************************/
/**
* \fn void slave5()
* \brief Call LOLSendACK to ACK the packet
* \details
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void slave5(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
            uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = payloadPtr;
    slaveAckVals.payloadLength = payloadLength-4;
    slaveAckVals.ackNack = true;
    LOLSendACK(&slaveAckVals);
}  /* slave5() */

/******************************************************************************/
/**
* \fn void simpleAck()
* \brief Call LOLSendACK to ACK the packet
* \details
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void simpleAck(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
               uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = NULL;
    slaveAckVals.payloadLength = 0;
    slaveAckVals.ackNack = true;
    LOLSendACK(&slaveAckVals);
}  /* simpleAck() */

/******************************************************************************/
/**
* \fn void sendMaxPktLn()
* \brief Respond with the max packet length
* \details
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void sendMaxPktLn(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                  uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    if (0x01 == (SID & READWRITEBIT))
    {
        slaveAckVals.data = NULL;
        slaveAckVals.payloadLength = 0;
        slaveAckVals.ackNack = false;
    }
    else
    {
        slaveAckVals.data = &maxPacketLength;
        slaveAckVals.payloadLength = 1;
        slaveAckVals.ackNack = true;
    }
    LOLSendACK(&slaveAckVals);
}  /* sendMaxPktLn() */

mailboxVals mailboxVariables;
#if 0
/*************************************************************************/
/**
* \fn void mailbox(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID, uint8_t payloadLength, uint8_t channelNumber)
*
* \brief Process a mailbox request that was received on channel 0 by forwarding the request to channel 1
*
* \details The following are performed upon the reception of a mailbox write request:
* \details - Store the SID that will be used to ACK/NAK the original mailbox request once channel 1 sends an ACK/NAK
* \details - Store the SID that was requested to be sent to channel 1, LOL will generate it's own SID
* \details - Populate the sendPacket structure to send the message to channel 1 using the LOL send function
*
* \details The following are performed upon the reception of a mailbox read request:
* \details - Create ACK/NAK response using saved SID from read request and saved response data from channel 1
*
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
*
* \return None
*
*****************************************************************************/
void mailbox(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID, uint8_t payloadLength, uint8_t channelNumber)
{
    if ((0x01 == (SID & READWRITEBIT)) && (payloadPtr[MAILBOXMESSAGETYPE] == eBLE_GET_FW_VERSION))
    {
        // Then we need to send the eBLE_GET_FW_VERSION request to channel 1
        sendPacket.txErrorCallBack = &testFailureCallback1;     // just call the standard failure callback
        sendPacket.txCompleteCallback = &MBCompleteCallback1;   // Called when channel 1 responds to the request
        sendPacket.channelNum = CHANNELONE;                     // To be sent on channel 1
        sendPacket.OLMessage = (eOL_MsgTypes)payloadPtr[2];     // Use the original message
        sendPacket.data = NULL;                                 // the eBLE_GET_FW_VERSION has no payload, set to NULL
        sendPacket.payloadLength = 0;                           // the eBLE_GET_FW_VERSION has no payload, set to NULL
        sendPacket.write = FALSE;                               // This is a read command
        sendPacket.BLE = FALSE;                                 // Point to Point communication, set BLE bit to false
        sendPacket.timeoutTime = 10000;      // I need 10 seconds to get the correct reponse typed in!!  This will be shorter in your code
        LOLSend(&sendPacket);                                   // Have LOL send the packet
        mailboxVariables.BLESID = payloadPtr[MAILBOXSID];       // Save the original SID to be used in the response that will be generated
        mailboxVariables.originalSID = SID;                     // Save the SID so the message can be ACKed when channel 1 responds
    }
    else if (0x00 == (SID & READWRITEBIT))
    {
        // Then we need to send a ACK with data containing the response received on channel 1
        slaveAckVals.channelNum = CHANNELZERO;                  // Send the ACK back on channel 0
        slaveAckVals.data = mailboxVariables.data;              // Use the returned paylaod data
        slaveAckVals.payloadLength = mailboxVariables.length;   // Use the returned length
        slaveAckVals.SID = SID;                                 // Use the original
        slaveAckVals.ackNack = true;    // will always be a ACK
        LOLSendACK(&slaveAckVals);      // The return value could be checked to generate a NAK if the ACK was not sent
    }
    else
    {
        // That is all I know about, so NAK it!!
        nakData[1] = NAKILLEGALMEMORYADDRESS;       // the reason we are sending the NAK
        slaveAckVals.channelNum = CHANNELZERO;      // Send the nack out on channel 0
        slaveAckVals.data = nakData;                // Use the pre-set up nak buffer
        slaveAckVals.payloadLength = NAKWDATASIZE;  // NAK with data will always have 2 bytes
        slaveAckVals.SID = SID;                     // Received SID
        slaveAckVals.ackNack = false;               // Set to false, will be a NAK
        LOLSendACK(&slaveAckVals);                  // Have LOL send it
    }
}  /* mailbox() */
#endif

/******************************************************************************/
/**
* \fn void MBCompleteCallback1()
* \brief Function that will be called on a mailbox transmission complete
* \details Recreate response from channel 1 but use original SID as requested from channel 0
* \details Store data from message, if any exists
* \details ACK original message on channel 0
* \param[in] ackNack - True if this is an ACK, else it's a NAK
* \param[in] payloadPtr - pointer to the payload
* \param[in] payloadLength - how many bytes are in the buffer?
* \return None
*******************************************************************************/
void MBCompleteCallback1(bool ackNack, uint8_t * payloadPtr, uint8_t payloadLength, uint8_t channelNumber)
{
    // We need to rebuild the message and store it in MBresponseArray, without the checksum
    mailboxVariables.data = MBresponseArray;    // Point to the array that will store recreated response message
    MBresponseArray[MESSAGESIDPOS] = mailboxVariables.BLESID;   // original SID for BLE from Channel 0
    // do we have data to store?
    if (payloadLength > 0)
    {
        // there is data, store it!
        mailboxVariables.length = payloadLength + TXMAILBOXOVERHEAD;        // This will be for the response on channel 0
        MBresponseArray[PAYLOADLENGTHPOS] = payloadLength;                  // Store length of message returned on channel 1
        memcpy(&MBresponseArray[PAYLOADPOS], payloadPtr, payloadLength);    // Move the payload data
        if (true == ackNack)
        {
            MBresponseArray[MESSAGEIDPOS] = eACK_WITH_DATA;                 // Set correct message ID
        }
        else
        {
            MBresponseArray[MESSAGEIDPOS] = eNAK_WITH_DATA;
        }
    }
    else
    {
        mailboxVariables.length = ACKNACKLENGTH;        // No data, it will just be a 2 byte response
        if (true == ackNack)
        {
            MBresponseArray[MESSAGEIDPOS] = eACK;       // Set correct message ID
        }
        else
        {
            MBresponseArray[MESSAGEIDPOS] = eNAK;
        }
    }

    // send channel 0 the simple ACK so he can ask for the results
    slaveAckVals.channelNum = CHANNELZERO;              // respond for channel 0
    slaveAckVals.SID = mailboxVariables.originalSID;    // Use the orignial SID
    slaveAckVals.data = NULL;                           // No payload data, set to NULL
    slaveAckVals.payloadLength = 0;                     // No payload data, set to 0
    slaveAckVals.ackNack = true;                        // It is an ACK
    LOLSendACK(&slaveAckVals);                          // Use LOL to send it
}  /* MBCompleteCallback1() */

/*******************************************************************************
* \fn void passwordEnable()
* \brief Update the password if it is valid
* \details Call PasswordVerification with the received data and update the password if appropriate
* \param[in] payloadPtr - pointer to where the password to be checked is
* \param[in] messageID - better be 0x01
* \param[in] SID - SID
* \param[in] payloadLength - better be 0x0A
* \param[in] channelNumber - Channel the command came in on
*******************************************************************************/
void passwordEnable(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                    uint8_t payloadLength, uint8_t channelNumber)
{
    eMM_Sec_Lev newSecurityLevel = PasswordVerification(&payloadPtr[PAYLOADLENGTHPOS]);
    LOLChangeSecurity(channelNumber, newSecurityLevel, DEFAULTSECURITYTIMEOUT);

    if (newSecurityLevel != eMM_Sec_Lev__ALL)
    {
        // Respond with a simple ACK
        slaveAckVals.channelNum = channelNumber;
        slaveAckVals.SID = SID;
        slaveAckVals.data = NULL;
        slaveAckVals.payloadLength = 0;
        slaveAckVals.ackNack = true;
        LOLSendACK(&slaveAckVals);
    }
    // else just quietly go away
}  /* passwordEnable() */

/******************************************************************************/
/**
* \fn eMM_Sec_Lev PasswordVerification(uint8_t * inputPassword)
* \brief Example code for Password Verification
* \details Will call LOLCompare function to securely validate a password
* \param[in] inputPassword - password to be verified
* \return eMM_Sec_Lev Will return the new level of software based on the input password
*******************************************************************************/
eMM_Sec_Lev PasswordVerification(uint8_t * inputPassword)
{
    eMM_Sec_Lev retVal = eMM_Sec_Lev__ALL;

    // Check the lowest level of security first and work our way up.
    if (LOLCompare(inputPassword, (uint8_t *)&userPassword, PASSWORDLENGTH))
    {
        // Set to User
        retVal = eMM_Sec_Lev__USER;
    }
    else if (LOLCompare(inputPassword, (uint8_t *)&adminPassword, PASSWORDLENGTH))
    {
        // Set to Admin
        retVal = eMM_Sec_Lev__USER_ADMIN;
    }
    else if (LOLCompare(inputPassword, (uint8_t *)&servicePassword, PASSWORDLENGTH))
    {
        // Set to Service
        retVal = eMM_Sec_Lev__SERVICE;
    }
    else if (LOLCompare(inputPassword, (uint8_t *)&METCOPassword, PASSWORDLENGTH))
    {
        // Set to MANUFACTURING
        retVal = eMM_Sec_Lev__MANUFACTURING;
    }
    return(retVal);
}  /* PasswordVerification() */

/*******************************************************************************
* \fn setSecLevel()
* \brief changing the password for different security levels
* \param[in] payloadPtr - pointer to payload buffer that has address of which security level to use
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void setSecLevel(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                 uint8_t payloadLength, uint8_t channelNumber)
{
    /* Prepare the ACK/NACK value! */
    uint8_t nakPayload[LOCAL_RETPL_LEN];  /* [2] */
    
    nakPayload[0] = NAKMAXMESSAGE;
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = NULL;
    slaveAckVals.payloadLength = 0;
    slaveAckVals.ackNack = true;    /* Assume ACK */

    /* Dig the address out of the payload */
    uint16_t address = (payloadPtr[PAYLOADADDRPOS0] << 8) | payloadPtr[PAYLOADADDRPOS1];
    uint64_t newPassword = 0;

    /* Is this a read or a write of the password? */
    if (0x01 == (SID & READWRITEBIT))
    {
        if (payloadLength == CHANGEPASSWORDLENGTH)
        {
            // Compute new password
            newPassword = OL_Bytes2U64(&payloadPtr[2]);
            /* Check to make sure the password is not a duplicate */
//            if (newPassword != userPassword && newPassword != adminPassword && newPassword != servicePassword && newPassword != METCOPassword)
            if ((newPassword != userPassword) && (newPassword != adminPassword)
                && (newPassword != servicePassword) && (newPassword != METCOPassword))
            {
                /* Write the new password */
                switch (address)
                {
                    case eUSER_PASSWORD:
                        userPassword = newPassword;
                    break;  /* eUSER_PASSWORD */
                    
                    case eADMIN_PASSWORD:
                        adminPassword = newPassword;
                    break;  /* eADMIN_PASSWORD */
                    
                    case eSERVICE_PASSWORD:
                        servicePassword = newPassword;
                    break;  /* eSERVICE_PASSWORD */
                    
                    case eMETCO_PASSWORD:
                        METCOPassword = newPassword;
                    break;  /* eMETCO_PASSWORD*/
                    
                    default:  /* Nothing we know about, NAK it!! */
                        slaveAckVals.ackNack = false;
                        nakPayload[1] = NAKUNKNOWNSPECIFIER;
                    break;
                }  /* switch */
            }  /* if newPassword */
            else
            {
                /* NAK it!  No duplicate passwords allowed */
                slaveAckVals.ackNack = false;
                nakPayload[1] = NAKILLEGALVALUE;
            }  /* if else newPassword */
        }  /* if payload length */
        else
        {
            /* NAK it!  Packet is not formated correctly */
            slaveAckVals.ackNack = false;
            nakPayload[1] = NAKMALFORMEDPACKET;
        }  /* if else payload length */
    }  /* if SID */
    else
    {
        slaveAckVals.payloadLength = PASSWORDLENGTH;
        // Read the old password
        switch (address)
        {
            case eUSER_PASSWORD:
                slaveAckVals.data = (uint8_t *)&userPassword;
            break;  /* eUSER_PASSWORD */
            
            case eADMIN_PASSWORD:
                slaveAckVals.data = (uint8_t *)&adminPassword;
            break;  /* eADMIN_PASSWORD */
            
            case eSERVICE_PASSWORD:
                slaveAckVals.data = (uint8_t *)&servicePassword;
            break;  /* eSERVICE_PASSWORD */
            
            case eMETCO_PASSWORD:
                slaveAckVals.data = (uint8_t *)&METCOPassword;
            break;  /* eMETCO_PASSWORD */
            
            default:
                // Nothing we know about, NAK it!!
                nakPayload[1] = NAKUNKNOWNSPECIFIER;
                slaveAckVals.ackNack = false;
            break;
        }  /* switch */
    }  /* if else SID */

    if (false == slaveAckVals.ackNack)
    {  /* Get the NAK set up! */
        slaveAckVals.data = nakPayload;
        slaveAckVals.payloadLength = 2;
    }

    LOLSendACK(&slaveAckVals);    /* Send the ACK/NAK */
}  /* setSecLevel() */

/******************************************************************************/
/**
* \fn void readMemMapVersion()
* \brief Example code for reading the Memory Map Version.
* \details Note, write is not legal by definition.  It will be rejected before
*                this routine is called because of the eMM_Sec_Lev__NONE secrutiy setting
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void readMemMapVersion(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                       uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
//    slaveAckVals.data = (uint8_t *)&memoryMapVersion;
    slaveAckVals.data = (uint8_t *)&SystemIB.uiMMVer;
    slaveAckVals.payloadLength = LOL_MMVER_PAYLOAD_LEN;  /* 2 */
    slaveAckVals.ackNack = true;
    LOLSendACK(&slaveAckVals);
}  /* readMemMapVersion() */

/******************************************************************************/
/**
* \fn void RWExtMemOffset()
* \brief reading and writing the Extended Memory Map Offset
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void RWExtMemOffset(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                    uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN]; /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    /* check for read/write */
    if (0x01 == (SID & READWRITEBIT))
    {
        /* write, check for payload length */
        if (payloadLength != 4)
        {
            /* NAK it!! if payload length is not correct */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = 2;
            slaveAckVals.ackNack = false;
        }
        else
        {   /* payload length is correct, write new value */
            extendedMemoryOffset = (payloadPtr[3] << 8) | payloadPtr[2];
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = 0;
        }  /* if else payload Length */
    }  /* if SID */
    else
    {
        // Read value
        slaveAckVals.data = (uint8_t *)&extendedMemoryOffset;
        slaveAckVals.payloadLength = 2;
    }  /* if else SID */

    LOLSendACK(&slaveAckVals);
}  /* RWExtMemOffset() */

/**
  ******************************************************************************
  * @func   sendMPBIDChannel()
  * @func   fLMMPBODVersion()
  * @brief  Write local memory - manufacturing test, buzzer
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void sendMPBIDChannel(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                      uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* write MPBID, prep ptr to update internal value */
        if (payloadLength != 7)
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
            slaveAckVals.ackNack = false;
        }
        else
        {
            for (int iCnt = 0; iCnt < payloadLength - 2; iCnt++)
            {
                // If we are writing, move the data over!
                SystemIB.aMPBID[iCnt] = payloadPtr[iCnt + 2];  /* deviceMPBID[iCnt] */
            }
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        }  /* if else payloadLength */
    }  /* if SID */
    else
    {   
        /* read MPBID, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.aMPBID;  /* deviceMPBID */
        slaveAckVals.payloadLength = SYSTEM_LOL_MPBID_SIZE;  /* 5 */
    }  /* else SID */
    LOLSendACK(&slaveAckVals);
}  /* sendMPBIDChannel() */


/**
  ******************************************************************************
  * @func   sendFwVersion()
  * @func   fLMFwVersion()
  * @brief  Write local memory - manufacturing test, buzzer
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void sendFwVersion(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                   uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    /* check for read/write, if write, response nack, else response ack with data */
    if (1 == (SID & READWRITEBIT))
    {
        /* write firmware version, return nack */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        slaveAckVals.ackNack = false;
    }  /* if */
    else
    {
        slaveAckVals.data = (uint8_t *)&SystemIB.ulDeviceFwVer;
        slaveAckVals.payloadLength = LOL_FWVER_PAYLOAD_LEN;  /* 4 */
    }  /* else */
    LOLSendACK(&slaveAckVals);
}  /* sendFwVersion() */

/**
  ******************************************************************************
  * @func   fLMDateOfMfg()
  * @brief  Read/Write local memory - date of manufacturing
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateOfMfg(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* write BOD, prep ptr to update internal value */
        if (payloadLength != 6)
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = false;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }
        else
        {
#if 0
            SystemIB.ulBornOnDate = 0;
            for (int iCnt = 0; iCnt < payloadLength - 2; iCnt++)
            {
                // If we are writing, move the data over!
//                SystemIB.ulBornOnDate[iCnt] = payloadPtr[iCnt + 2];
                SystemIB.ulBornOnDate = (SystemIB.ulBornOnDate << 8) | payloadPtr[iCnt + 2];
            }
#endif
            SystemIB.ulBornOnDate = payloadPtr[2] | (payloadPtr[3] << 8) | (payloadPtr[4] << 16) | (payloadPtr[5] << 24);
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read SWPN, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.ulBornOnDate;
        slaveAckVals.payloadLength = LOL_BOD_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateOfMfg() */


/**
  ******************************************************************************
  * @func   fLMDateOfService()
  * @brief  Read/Write local memory - date of operation/service, activation
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateOfService(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* write MPBID, prep ptr to update internal value */
        if (payloadLength != 6)
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = false;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }
        else
        {
            SystemIB.ulDateFirstServ = payloadPtr[2] | (payloadPtr[3] << 8) | (payloadPtr[4] << 16) | (payloadPtr[5] << 24);
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read DFO, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.ulDateFirstServ;
        slaveAckVals.payloadLength = LOL_DT_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateOfService() */

//uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

/**
  ******************************************************************************
  * @func   fLMDateLastUSed()
  * @brief  read local memory - data last used or tool last used
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateLastUSed(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* can't write, nack */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.ackNack = false;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if write */
    else
    {   
        /* read date last used (too last used, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.ulDateLastUsed;
        slaveAckVals.payloadLength = LOL_DT_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateLastUSed() */


/**
  ******************************************************************************
  * @func   fLMDateTimeRef()
  * @brief  read/write local memory - date time reference
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateTimeRef(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* write date time reference, prep ptr to update internal value */
        if (payloadLength != 6)
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = false;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }
        else
        {
            SystemIB.ulDateTimeRef = payloadPtr[2] | (payloadPtr[3] << 8) | (payloadPtr[4] << 16) | (payloadPtr[5] << 24);
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }  /* if else payload */
    }  /* if write */
    else
    {   
        /* read date last used (too last used, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.ulDateTimeRef;
        slaveAckVals.payloadLength = LOL_DT_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateTimeRef() */


/**
  ******************************************************************************
  * @func   fLMPlatformId()
  * @brief  read local memory - platform identifier
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMPlatformId(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* if write, thennack */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.ackNack = false;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if write */
    else
    {   
        /* read platform id, prep ptr to send internal value */
        slaveAckVals.data = (uint8_t *)&SystemIB.uPlatformId;
        slaveAckVals.payloadLength = LOL_PFID_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMPlatformId() */

/**
  ******************************************************************************
  * @func   fLMBLEMACAddr()
  * @brief  read/write local memory - date time reference
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMBLEMACAddr(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = true;
    
    if (0x01 == (SID & READWRITEBIT))
    {   
        /* write date time reference, prep ptr to update internal value */
        if (payloadLength != 8)
        {
            // NAK it!!
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = false;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }
        else
        {
            for (int iCnt = 0; iCnt < (payloadLength - 2); iCnt++)
            {
                SystemIB.aBLEMACAddr[iCnt] = payloadPtr[iCnt + 2];
            }  /* for */
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }  /* if else payload */
    }  /* if write */
    else
    {   
        /* read date last used (too last used, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.aBLEMACAddr;
        slaveAckVals.payloadLength = LOL_BLEMAC_PAYLOAD_LEN;
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMBLEMACAddr() */




