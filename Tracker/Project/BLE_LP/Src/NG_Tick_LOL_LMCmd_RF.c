/**
  ******************************************************************************
  * @file    NG_Tick_LOL_LMCmd_RF.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link local memory command - RF specific.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* IMPORTANT!!!!!!!!!!!!!!!! 
YOU MUST HAVE THE FULL BLE STACK ENABLED IN THE PRE-COMPLIER DEFINES IN ORDER 
FOR THE RF TEST COMMANDS TO WORK.  IF YOU HAVE THE BASIC BLE STACK CONFIGURED
THE CODE WILL NOT COMPILE
*/

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_gpio.h"
#include "bluenrg_lp.h"
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_CmdVar.h"
#include "NG_Tick_FlashUtil.h"    /* must come before NG_Tick_sys.h */
#include "NG_Tick_NFC.h"          /* must come before NG_Tick_sys.h */
#include "NG_Tick_Acc.h"          /* must comes before sys.h */
#include "NG_Tick_sys.h"

#include "NG_Tick_app.h"
#include "NG_Tick_LOL_LMCmd_RF.h"
#include "bluenrg_lp_api.h"
#include "stack_user_cfg.h"

/* constant variable declaration ---------------------------------------------*/

/* constant variable declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
//NGT_SYSSTATUS_TYPE sSysStatus;
uint8_t aRFMsg[16];
bool CW_mode = false;

/* function prototype --------------------------------------------------------*/
//void xLoLMT_SysStatus(void);
//void xLoLMT_ProductionStatus(void);
uint8_t xLoLMT_RFTXRead( uint8_t *);
uint8_t xLoLMT_RFTXWrite( uint8_t *);
uint8_t xLoLMT_RFRXRead( uint8_t *);
uint8_t xLoLMT_RFRXWrite( uint8_t *);
uint8_t xLoLMT_RFOTARead( uint8_t *);
uint8_t xLoLMT_RFOTAWrite( uint8_t *);
void fLMT_RF_TX( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_RF_RX( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMT_RF_OTA( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);


/**
  ******************************************************************************
  * @func   xLoLMT_RFTXRead()
  * @brief  RF TX read command
  * @param  pointer to message
  * @gvar   aRFMsg[]
  * @gfunc  
  * @retval LOL_RD_RESP, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_RFTXRead( uint8_t *uPtr)
{
    uint8_t uStatus = NAKILLEGALVALUE;

        return(uStatus);
}  /* xLoLMT_RFTXRead() */

/**
  ******************************************************************************
  * @func   xLoLMT_RFTXWrite()
  * @brief  RF TX write command
  * @param  pointer to message
  * @gvar   aRFMsg[]
  * @gfunc  
  * @retval LOL_RD_RESP, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_RFTXWrite( uint8_t *uPtr)
{
    uint8_t uStatus = NAKILLEGALVALUE;
#ifdef RF_TEST_MODES
    switch (uPtr[2])
    {
        /* You must power cycle the device if you setup to transmit a CW
          and then want to change and transmit a modulated signal.  However,
          the same is not true if you start with a modulated signal and want to
          do a CW - that's OK.  It's a weird bug that wasn't worth the effort to 
          chase down just for some RF test code that would only be used in a 
          lab setting*/
      
        case LOL_LM_RF_TX_LOW_1MBPS:

            /*set power*/
            aci_hal_set_tx_power_level(0x00,0x19);
                
            /* Send continuous modulated tone, 85% duty cycle */
            hci_le_enhanced_transmitter_test(0x00,0xFF,0x02,0x01);     

            uStatus = LOL_ACK_RESP;

            CW_mode = false;
                        
        break;  /* LOL_LM_RF_TX_LOW_1MBPS */
        
        case LOL_LM_RF_TX_MID_1MBPS:
                       
            /*set power*/
            aci_hal_set_tx_power_level(0x00,0x19);
                
            /* Send continuous modulated tone, 85% duty cycle */
            hci_le_enhanced_transmitter_test(0x13,0xFF,0x02,0x01);     

            uStatus = LOL_ACK_RESP;

            CW_mode = false;
              
        break;  /* LOL_LM_RF_TX_MID_1MBPS */
        
        case LOL_LM_RF_TX_HIGH_1MBPS:          

            /*set power*/
            aci_hal_set_tx_power_level(0x00,0x19);
                
            /* Send continuous modulated tone, 85% duty cycle */
            hci_le_enhanced_transmitter_test(0x27,0xFF,0x02,0x01);     

            uStatus = LOL_ACK_RESP;

            CW_mode = false;
              
        break;  /* LOL_LM_RF_TX_HIGH_1MBPS */
        
        case LOL_LM_RF_TX_LOW_2MBPS:

            aci_hal_set_tx_power_level(0x00,0x19);
            
            /* Send continuous modulated tone, 85% duty cycle */
            hci_le_enhanced_transmitter_test(0x00,0xFF,0x02,0x02);     
            CW_mode = false;
            
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_LOW_2MBPS */
        
        case LOL_LM_RF_TX_MID_2MBPS:

            aci_hal_set_tx_power_level(0x00,0x19);
            
            /* Send continuous modulated tone, 85% duty cycle */
            hci_le_enhanced_transmitter_test(0x13,0xFF,0x02,0x02);    
            CW_mode = false;
            
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_MID_2MBPS */
        
        case LOL_LM_RF_TX_HIGH_2MBPS:

            aci_hal_set_tx_power_level(0x00,0x19);
            
            /* Send continuous modulated tone, 85% duty cycle */
            hci_le_enhanced_transmitter_test(0x27,0xFF,0x02,0x02);    
            CW_mode = false;
            
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_HIGH_2MBPS */

        case LOL_LM_RF_TX_LOW_CW:

            aci_hal_set_tx_power_level(0x00,0x19);
            
            /* Send continuous wave, unmodulated */
            aci_hal_tone_start(0x00,0x00);
            CW_mode = true;
            
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_LOW_CW */
        
        case LOL_LM_RF_TX_MID_CW:

            aci_hal_set_tx_power_level(0x00,0x19);
            
            /* Send continuous wave, unmodulated */
            aci_hal_tone_start(0x13,0x00);     
            CW_mode = true;
         
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_MID_CW */
        
        case LOL_LM_RF_TX_HIGH_CW:

            aci_hal_set_tx_power_level(0x00,0x19);
            
            /* Send continuous wave, unmodulated */
            aci_hal_tone_start(0x27,0x00);     
            CW_mode = true;
         
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_HIGH_CW */
        
        case LOL_LM_RF_TX_STOP:
            
            /* Stop TX */
          if (CW_mode)
          {
            aci_hal_tone_stop();
            CW_mode = false;
          }
          
            hci_reset();

            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_TX_HIGH_CW */
        default:
            uStatus = NAKILLEGALVALUE;
        break;
    }  /* switch */
#endif    
    return(uStatus);
}  /* xLoLMT_RFTXWrite() */

/**
  ******************************************************************************
  * @func   xLoLMT_RFRXRead()
  * @brief  RF RX read command
  * @param  pointer to message
  * @gvar   aRFMsg[]
  * @gfunc  
  * @retval LOL_RD_RESP, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_RFRXRead( uint8_t *uPtr)
{
    uint8_t uStatus = NAKILLEGALVALUE;

    return(uStatus);
}  /* xLoLMT_RFTXRead() */

/**
  ******************************************************************************
  * @func   xLoLMT_RFRXWrite()
  * @brief  RF RX write command
  * @param  pointer to message
  * @gvar   aRFMsg[]
  * @gfunc  
  * @retval LOL_RD_RESP, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_RFRXWrite( uint8_t *uPtr)
{
    uint8_t uStatus = NAKILLEGALVALUE;

#ifdef RF_TEST_MODES    
    uint8_t status;
    uint16_t number_of_packets;

    switch (uPtr[2])
    {
        case LOL_LM_RF_RX_LOW_1MBPS:
                
              status = hci_reset();
              if (status != BLE_STATUS_SUCCESS)
              {
                uStatus = NAKUNKNOWNERROR;
              }
              else
              {
                status = hci_le_enhanced_receiver_test(0x00,0x01,0x00);
                if (status != BLE_STATUS_SUCCESS) 
                {
                    uStatus = NAKUNKNOWNERROR;
                }
                else
                {
                    uStatus = LOL_ACK_RESP;
                }
              }
              
            //uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_RX_LOW_1MBPS */
        
        case LOL_LM_RF_RX_MID_1MBPS:
                        
              status = hci_reset();
              if (status != BLE_STATUS_SUCCESS)
              {
                uStatus = NAKUNKNOWNERROR;
              }
              else
              {
                status = hci_le_enhanced_receiver_test(0x13,0x01,0x00);
                if (status != BLE_STATUS_SUCCESS) 
                {
                    uStatus = NAKUNKNOWNERROR;
                }
                else
                {
                    uStatus = LOL_ACK_RESP;
                }
              }
            //uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_RX_MID_1MBPS */
        
        case LOL_LM_RF_RX_HIGH_1MBPS:
                
              status = hci_reset();
              if (status != BLE_STATUS_SUCCESS)
              {
                uStatus = NAKUNKNOWNERROR;
              }
              else
              {
                status = hci_le_enhanced_receiver_test(0x27,0x01,0x00);
                if (status != BLE_STATUS_SUCCESS) 
                {
                    uStatus = NAKUNKNOWNERROR;
                }
                else
                {
                    uStatus = LOL_ACK_RESP;
                }
              }
              
              //uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_RX_HIGH_1MBPS */
        
        case LOL_LM_RF_RX_STOP:
              
              status = hci_le_test_end(&number_of_packets);
              if (status != BLE_STATUS_SUCCESS)
              {
                uStatus = NAKUNKNOWNERROR;
              }
              else
              {
                uStatus = LOL_ACK_RESP;
              }
              
              //uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_RX_HIGH_1MBPS */     
        
        default:
            uStatus = NAKILLEGALVALUE;
        break;
    }  /* switch */

#endif    
    
    return(uStatus);
}  /* xLoLMT_RFRXWrite() */

/**
  ******************************************************************************
  * @func   xLoLMT_RFOTARead()
  * @brief  RF OTA read command
  * @param  pointer to message
  * @gvar   aRFMsg[]
  * @gfunc  
  * @retval LOL_RD_RESP, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_RFOTARead( uint8_t *uPtr)
{
    uint8_t uStatus = NAKILLEGALVALUE;

    switch (uPtr[2])
    {
        case LOL_LM_RF_OTA_OFF:
            /* @#$% insert code here, insert answer in aRFMsg[] */
            aRFMsg[0] = 0x48;  /* example */
            uStatus = LOL_RD_RESP;
        break;  /* LOL_LM_RF_OTA_OFF */
        
        case LOL_LM_RF_OTA_ON:
            /* @#$% insert code here, insert answer in aRFMsg[] */
            uStatus = LOL_RD_RESP;
        break;  /* LOL_LM_RF_OTA_ON */
        
        default:
            uStatus = NAKILLEGALVALUE;
        break;
    }  /* switch */
    return(uStatus);
}  /* xLoLMT_RFOTARead() */

/**
  ******************************************************************************
  * @func   xLoLMT_RFOTAWrite()
  * @brief  RF OTA write command
  * @param  pointer to message
  * @gvar   aRFMsg[]
  * @gfunc  
  * @retval LOL_RD_RESP, NAKILLEGALVALUE
  ******************************************************************************
  */
uint8_t xLoLMT_RFOTAWrite( uint8_t *uPtr)
{
    uint8_t uStatus = NAKILLEGALVALUE;

    switch (uPtr[2])
    {
        case LOL_LM_RF_OTA_OFF:
            /* @#$% insert code here, copy sub-parameter to aRFMsg[] */
            aRFMsg[0] = uPtr[3];  /* example, first sub-parameter if any */
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_OTA_OFF */
        
        case LOL_LM_RF_OTA_ON:
            /* @#$% insert code here, copy sub-parameter to aRFMsg[] */
            uStatus = LOL_ACK_RESP;
        break;  /* LOL_LM_RF_OTA_ON */
        
        default:
            uStatus = NAKILLEGALVALUE;
        break;
    }  /* switch */
    return(uStatus);
}  /* xLoLMT_RFOTAWrite() */


/**
  ******************************************************************************
  * @func   fLMT_RF_TX()
  * @brief  RF memory operations: RF transmit, read, write
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   
  * @gfunc  LOLSendACK(), xLoLMT_RFTXRead(), xLoLMT_RFTXWrite()
  * @retval None
  ******************************************************************************
  */
void fLMT_RF_TX(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        uStatus = xLoLMT_RFTXRead(payloadPtr);
    }
    else
    {
        uStatus = xLoLMT_RFTXWrite(payloadPtr);
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)aRFMsg;  /* status */
            slaveAckVals.payloadLength = payloadPtr[3];  /* @#$% return byte count, must change */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        case NAKUNKNOWNERROR:  /* unknown error, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKUNKNOWNERROR;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */        
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_RF_TX() */


/**
  ******************************************************************************
  * @func   fLMT_RF_RX()
  * @brief  RF memory operations: RF receive, read, write
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   
  * @gfunc  LOLSendACK(), xLoLMT_RFTXRead(), xLoLMT_RFTXWrite()
  * @retval None
  ******************************************************************************
  */
void fLMT_RF_RX(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        uStatus = xLoLMT_RFRXRead(payloadPtr);
    }
    else
    {
        uStatus = xLoLMT_RFRXWrite(payloadPtr);
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)aRFMsg;  /* status */
            slaveAckVals.payloadLength = payloadPtr[3];  /* @#$% return byte count, must change */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        case NAKUNKNOWNERROR:  /* unknown error, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKUNKNOWNERROR;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */  
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_RF_RX() */

/****/
/**
  ******************************************************************************
  * @func   fLMT_RF_OTA()
  * @brief  RF memory operations: RF OTA, read, write
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   
  * @gfunc  LOLSendACK(), xLoLMT_RFTXRead(), xLoLMT_RFTXWrite()
  * @retval None
  ******************************************************************************
  */
void fLMT_RF_OTA(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t uStatus = NAKMALFORMEDPACKET;

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = FALSE;
    
    if (LOL_SID_RD == (SID & READWRITEBIT))
    {
        uStatus = xLoLMT_RFOTARead(payloadPtr);
    }
    else
    {
        uStatus = xLoLMT_RFOTAWrite(payloadPtr);
    }  /* if SID */
    
    switch (uStatus)
    {
        case LOL_RD_RESP:  /* read respond */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = (uint8_t *)aRFMsg;  /* status */
            slaveAckVals.payloadLength = payloadPtr[3];  /* @#$% return byte count, must change */
        break;  /* LOL_RD_RESP */
        
        case LOL_ACK_RESP:  /* all good, ack */
            slaveAckVals.ackNack = TRUE;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
        break;  /* LOL_ACK_RESP */
        
        case NAKILLEGALVALUE:  /* illegal value, return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKILLEGALVALUE;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* NAKILLEGALVALUE */
        
        default: /* return nack */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        break;  /* default */
    }  /* switch */
    LOLSendACK(&slaveAckVals);
}  /* fLMT_RF_OTA() */






