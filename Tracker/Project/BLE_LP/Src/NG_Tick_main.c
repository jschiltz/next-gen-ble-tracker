/**
  ******************************************************************************
  * @file    NG_Tick_main.c
  * @author  Thomas W Liu
  * @brief   Source file of Next Generation Tick.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */
/*
IMPORTANT!!!!!!!!!!!!!!!! 
YOU MUST HAVE THE FULL BLE STACK ENABLED IN THE PRE-COMPLIER DEFINES IN ORDER 
FOR THE RF TEST COMMANDS TO WORK.  IF YOU HAVE THE BASIC BLE STACK CONFIGURED
THE CODE WILL NOT COMPILE
**/

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_rcc.h"
#include "NG_TICK_main.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_Acc.h"
#include "NG_Tick_NFC.h"
#include "NG_Tick_IWDG_Reset.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_wakeup.h"
#include "NG_Tick_LOL_LMCmd_T.h"
#include "NG_Tick_LOL_Func.h"
#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_BLE.h"
#include "NG_Tick_ADC.h"
#include "NG_Tick_app.h"

/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/
#if(CONFIG_OTA_USE_SERVICE_MANAGER) 
typedef struct Application_Header
{
    uint32_t  Product_Id;             /* Product Identification */
    uint32_t  Timestamp;              /* Time stamp of signature */
    uint32_t  Firmware_Version;       /* Firmware Version */
    uint32_t  Hardware_Version;       /* Hardware Version */
    uint32_t  Compatability_Version;  /* Compatability Version */
    uint32_t  Destination;            /* Starting Address of Image */
    uint32_t  Length;                 /* Length of Image */
    uint8_t   Firmware_Digest[64];    /* Key for Image decryption */
    uint32_t  Fingerprint;            /* Signature Hash of Image */
    uint32_t  Ivect_1;                /* 16 byte of Vectors */
    uint32_t  Ivect_2;
    uint32_t  Ivect_3;
    uint32_t  Ivect_4;
    uint8_t   Pad[80];                /* Pad area */
    uint32_t  Header_Image_Signature; /* Signature of Header */
    uint8_t   Header_Signature[60];   /* Header signature */      
} APPLICATION_HEADER_T;
#endif

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint32_t uiRAMRetention;        /* RAM bank retention in PWRC, CR2 */

#if(CONFIG_OTA_USE_SERVICE_MANAGER) 
SECTION(".app_header")
const APPLICATION_HEADER_T App_Header = 
{
  SYS_LOL_PRODID_VAL, /*  .Product_Id - Product Identification */
  0x00000000, /*  .Timestamp - Time stamp of signature */
  SYS_LOL_FWVER_VAL, /*  .Firmware_Version - Firmware Version */
  SYS_LOL_HWVER_VAL, /*  .Hardware_Version - Hardware Version */
  0x00000000, /*  .Compatability_Version - Compatability Version */
  0x10056900, /*  .Destination - Starting Address of Image */
  0x00000000, /*  .Length - Length of Image */
  0x00,       /*  .Firmware_Digest[64] - Key for Image decryption */
  0x00000000, /*  .Fingerprint - Signature Hash of Image */
  0x00000000, /*  .Ivect_1 - 16 byte of Vectors */
  0x00000000, /*  .Ivect_2 */
  0x00000000, /*  .Ivect_3 */
  0x00000000, /*  .Ivect_4 */
  0x00,       /*  .Pad[80] - Pad area */
  0x00000000, /*  .Header_Image_Signature - Signature of Header */
  0x00000000  /*  .Header_Signature[60] - Header signature */      
};
#endif

/* Private function prototypes -----------------------------------------------*/
static void LL_Init(void);
static void CheckPOR(void);

/* function prototype --------------------------------------------------------*/

/* Private user code ---------------------------------------------------------*/
extern void CheckApp(void);


/**
  ******************************************************************************
  * @func   main()
  * @brief  application entery point
  * @param  None
  * @gvar   SystemCoreClock, uiRAMRetention
  * @gfunc  SystemInit(), LL_Init(), LL_Init1msTick(), SystemVar_Init_All(),
  *         UART_Init_All(), AudioEngine_Init_All(), Acc_Init_All(), NFC_Init_All(),
  *         LOL_Init_All(), CheckApp(), LL_PWR_GetRAMBankRet(), LL_PWR_DisableRAMBankRet()
  * @retval None
  ******************************************************************************
  */
int main(void)
{
    /* System initialization function */
    if (SystemInit(SYSCLK_32M, BLE_SYSCLK_16M) != SUCCESS)

    {
        /* Error during system clock configuration take appropriate action */
        while (1);
    }

#if 1
    uiRAMRetention = LL_PWR_GetRAMBankRet();
    uiRAMRetention = LL_PWR_RAMRET_2 | LL_PWR_RAMRET_3;
    LL_PWR_DisableRAMBankRet(uiRAMRetention);
#endif
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    LL_Init();
    
    /* check for power on reset status, clear flag afterward */
    CheckPOR();    
    LL_RCC_ClearResetFlags();

    /* Set systick to 1ms using syENDstem clock frequency */
    LL_Init1msTick(SystemCoreClock);        /* declared in bluenrg_lp_ll_utils.c */
    
    /* init system related information and control blocks */
    SystemVar_Init_All();
            
    /* init the ADC*/
    ADC_Init_All();

    /* init audio engine related */
    AudioEngine_Init_All();

    /* init uart related */
    UART_Init_All();
        
    /* init accelerometer, io, interrupt, variables, algorithm, double taps for now */
    Acc_Init_All();

    /* int NFC, io, interrupt, variables */
    NFC_Init_All();
    
    /* init LOL related buffers, variables */
    LOL_Init_All();

#ifdef ENABLE_BLE
    BLE_Init_All();
#endif

    CheckLastMode();  /* check last mode, return to previous mode */
      
    /* Infinite loop */
    while (1)
    {
        CheckApp();
    }  /* while() */
}  /* main() */


/**
  ******************************************************************************
  * @func   LL_Init()
  * @brief  set priority
  * @param  None
  * @gvar   None
  * @gfunc  NVIC_SetPriority()
  * @retval None
  ******************************************************************************
  */
static void LL_Init(void)
{
    /* System interrupt init*/
    /* SysTick_IRQn interrupt configuration */
    NVIC_SetPriority(SysTick_IRQn, IRQ_HIGH_PRIORITY);
}  /* LL_Init() */


/**
  ******************************************************************************
  * @func   CheckPOR()
  * @brief  check power on reset status register, error conditions are LOCKUPRSTF,
  *         WDGRSTF, SFTRSTF, or (PADRSTF and !PORRSTF)
  *         
  * @param  None
  * @gvar   SystemIB
  * @gfunc  SystemIB.SysRAMIB.uPORStatus: 1 = error, 0 = no error
  * @retval None
  ******************************************************************************
  */
void CheckPOR()
{
    SystemIB.SysRAMIB.uPORStatus = (uint16_t)((RCC->CSR >> 16) & 0x0000ffff);
}  /* CheckPOR() */

