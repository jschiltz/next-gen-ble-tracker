/**
  ******************************************************************************
  * @file    Sensor_Acc.c
  * @author  Thomas W Liu
  * @brief   Source file of LIS2DW12 accelerometer module.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  * Note 1   if EN_RD_ACC_MSG is defined, transmit accelerometer cofiguration
  *          values via UART.
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "bluenrg_lp.h"
#include "bluenrg_lp_hal.h"     /* required for uwTick*/
#include "bluenrg_lp_ll_dma.h"
#include "NG_Tick_main.h"
#include "lis2dw12_reg.h"
#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_i2c.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_ae.h"
#include "NG_Tick_BLE.h"


/* If TESTING = 1, test values will be used to speed up testing */
#define TESTING   0

/* Includes ------------------------------------------------------------------*/
/* Activity */

#define WKUP_DUR        0x03    /* Interval between absolute activity crossing 
                                   above the threshold and when the acceleration_mg 
                                   sends the wake up interrupt 
                                   1LSb = 1 / ODR (Seconds) Range: 0x0 - 0x3 */
#define WKUP_THRESHOLD  0x01    /* Activity/inactivity threshold.
                                   1LSB = FS/64 (micro-g) */

/* variable declaration ------------------------------------------------------*/
/* Accelorometer */
stmdev_ctx_t accelerometerDriver;
lis2dw12_all_sources_t all_source;

uint32_t motion_detected;
uint8_t whoamI_lis2dw12 = 0;    /* who am I, lis2dw12 */
uint8_t lis2dw12Found = 0;      /* device found, default to device not found */
uint16_t iAccInt1Cnt = 0;       /* @#$% debug acc int1 counter */
uint16_t iAccSecCnt = 0;        /* @#$% debug acc second counter */

//static ACCEL_SM_CTRL accel_sm;  /* Struct contains state machine variables */
ACCEL_SM_CTRL accel_sm;         /* Struct contains state machine variables */
static NGT_ACCINFO_TYPE sAccIB; /* Stuct contains Tool Last Used and Histogram */

const char uccNGTStr_Acc_InitMsg[] = "Accelerometer initialized\r\n";
const char uccNGTStr_Acc_InitErr[] = "Accelerometer not found\r\n";
const char uccNGTStr_Acc_Int1[] = "Acc Int1 <= T, DT\r\n";


/* function prototypes -------------------------------------------------------*/
void Acc_Init_All(void);
int32_t SearchAcc(void);
void CheckAcc(void);
//void NormalCheckAcc(void);
void CheckAccInt1(void);
void CheckAccSM(void);

void Init_Accelerometer(void);
void Init_Activity_Detection(void);

void Accelerometer_Enter_Shutdown(void);
void Accelerometer_Exit_Shutdown(void);
void Accelerometer_Enter_Idle(void);
void Accelerometer_Exit_Idle(void);

void Acc_TLU_H_Init(void);
void Tool_Last_Used(void);
void Update_Histogram(void);

/**
  ******************************************************************************
  * @func   Acc_Init_All()
  * @brief  init i2c and search for accelerometer.  if not found, retry again until
  *         reached max retry.  insert delay in between retry.
  * @param  None
  * @gvar   SystemCB
  * @gfunc  NGT_I2C_Init(), SearchAcc(), Init_LIS2DW12_Int1(), Init_LIS2DW12_Int2()
  * @retval None
  ******************************************************************************
  */
void Acc_Init_All(void)
{
    NGT_I2C_Init();       /* BSP_I2C_Init(), accelerometer i2c init */
    
    SystemCB.AccCB.iRetryCnt = SystemCB.AccCB.iMaxRetry;
    while ((!SystemCB.AccCB.uFound) && (SystemCB.AccCB.iRetryCnt-- > 0))
    {
        if (SearchAcc())
        {
            SystemCB.AccCB.uFound = TRUE;
        }
        else
        {
            uint32_t uwFirstTick;       /* local var declaration */
            uwFirstTick = uwTick;
            while ((uwTick - uwFirstTick) < RETRYDLY_FINDACC);  /* delay before next attempt */
        }
    }  /* while */
    
    if (TRUE == SystemCB.AccCB.uFound)
    {
        /* init accelerometer interrupt */
        Init_Accelerometer();
        Init_Activity_Detection();
        SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_ACC;
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx(uccNGTStr_Acc_InitMsg);  /* send success message */
#endif
    }
    else
    {
#ifdef ENABLE_RND_MSG
        cStartTx_WaitTilEndOfTx(uccNGTStr_Acc_InitErr);  /* send error message */
#endif
    }  /* if */

    Acc_TLU_H_Init();  /* init tool last used & histogram */
}  /* Acc_Init_All() */


/**
  ******************************************************************************
  * @func   SearchAcc()
  * @brief  Search NFC, st25dv, via i2c bus
  * @param  None
  * @gvar   accelerometerDriver 
  * @gfunc  lis2dw12_device_id_get()
  * @retval TRUE if lis2dw12 is found
  *         FALSE if lis2dw12 is not found
  ******************************************************************************
  */
int32_t SearchAcc(void)
{
    uint8_t whoamI_lis2dw12 = 0;        /* who am I, lis2dw12 */
    uint8_t lis2dw12Found = FALSE;      /* device found, default to device not found */

    /* Initialize the handle of the LIS2DW12 driver */
    accelerometerDriver.write_reg = NGT_I2C_Write;
    accelerometerDriver.read_reg = NGT_I2C_Read;
    
    /* Check device ID for Accelerometer */
    lis2dw12Found = FALSE;      /* device is not found */
    whoamI_lis2dw12 = 0;        /* device id returned */
    lis2dw12_device_id_get(&accelerometerDriver, &whoamI_lis2dw12);
  
    /* check Accelerometer id*/
    if (whoamI_lis2dw12 == LIS2DW12_ID)
    {
        lis2dw12Found = TRUE;  /* device is found */
    }  /* if */
    return(lis2dw12Found);
}  /* SearchAcc() */

/**
  ******************************************************************************
  * @func   CheckAccInt1()
  * @brief  check accelerometer interrupt 1
  * @param  None
  * @gvar   motion_detected, iAccInt1Cnt, SystemIB, accel_sm
  * @gfunc  LL_GPIO_IsInputPinSet(), lis2dw12_all_sources_get()
  * @retval None
  ******************************************************************************
  */
void CheckAccInt1(void)
{
    motion_detected = LL_GPIO_IsInputPinSet(LIS2DW12_INT1_GPIO_PORT, LL_GPIO_PIN_7);
    
    if (1 == motion_detected)
    {
        iAccInt1Cnt++;
        /* check for i2c token */
        if (SystemIB.SysRAMIB.uI2CToken != I2C_TOKEN_ACC)
        {
            SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_ACC;
        }  /* if */
        
        /* This clears the interrupt flag on the accelerometer */
        lis2dw12_all_sources_get(&accelerometerDriver, &all_source);
        motion_detected = 0;
        accel_sm.bMyThreshold = TRUE;
    } 
    else
    {
        accel_sm.bMyThreshold = FALSE;
    }  
}  /* CheckAccInt1() */

/**
  ******************************************************************************
  * @func   Init_LIS2DW12_Int1()
  * @brief  initialize lis2dw12 i/o for interrupt.
  * @param  None
  * @gvar   accelerometerDriver, whoamI_lis2dw12
  * @gfunc  LIS2DW12_INT1_GPIO_CLK_ENABLE(), LIS2DW12_INT1_SYSCFG_CLK_ENABLE(),
  *          LL_GPIO_SetPinMode(), LL_GPIO_SetPinPull()
  * @retval None
  ******************************************************************************
  */
void Init_Activity_Detection(void)
{
    /* Enable the Vibration Wakup GPIO Clock */
    LIS2DW12_INT1_GPIO_CLK_ENABLE();
    LIS2DW12_INT1_SYSCFG_CLK_ENABLE();
    LL_GPIO_SetPinMode(LIS2DW12_INT1_GPIO_PORT, LIS2DW12_INT1_PIN, LL_GPIO_MODE_INPUT);
//    LL_GPIO_SetPinPull(LIS2DW12_INT1_GPIO_PORT, LIS2DW12_INT1_PIN, LL_GPIO_PULL_NO);  /* requires pull down cap & resistor */
    LL_GPIO_SetPinPull(LIS2DW12_INT1_GPIO_PORT, LIS2DW12_INT1_PIN, LL_GPIO_PULL_DOWN);
    
    /* Initialize activity detection state machine variables */
    accel_sm.myState = ACCEL_STATE_NOT_MOVING;
    accel_sm.moving_counter = MAX_MOVE_COUNTER;
    accel_sm.not_moving_counter = MAX_NOT_MOVE_COUNTER;
    accel_sm.bMyThreshold = FALSE;
    accel_sm.histogram_activity_counter = 0;

}  /* Init_Activity_Detection() */

/**
  ******************************************************************************
  * @func   Init_Accelerometer()
  * @brief  initialize lis2dw12 i/o for interrupt.
  * @param  None
  * @gvar   accelerometerDriver, whoamI_lis2dw12
  * @gfunc  LIS2DW12_INT1_GPIO_CLK_ENABLE(), LIS2DW12_INT1_SYSCFG_CLK_ENABLE(), LL_GPIO_SetPinMode(),
  *         LL_GPIO_SetPinPull(), LIS2DW12_INT1_EXTI_LINE_ENABLE(), LIS2DW12_INT1_EXTI_RISING_TRIG_ENABLE(),
  *         LL_EXTI_IsInterruptPending(), LL_EXTI_ClearInterrupt(), NVIC_SetPriority(),
  *         NVIC_EnableIRQ(), NVIC_SetPriority()
  * @retval None
  ******************************************************************************
  */
void Init_Accelerometer()
{
    uint8_t rst;
    lis2dw12_reg_t int_route;
    motion_detected = 0;

    /* Restore default configuration */
    lis2dw12_reset_set(&accelerometerDriver, PROPERTY_ENABLE);
    do
    {
        lis2dw12_reset_get(&accelerometerDriver, &rst);
    } while (rst); 
    
    /* Enable Block Data Update */
    lis2dw12_block_data_update_set(&accelerometerDriver, PROPERTY_ENABLE);
    /* Set Full Scale*/
    lis2dw12_full_scale_set(&accelerometerDriver, LIS2DW12_2g);
    /* Configure filter chain - HP filter to remove DC component of acceleration */
    lis2dw12_filter_path_set(&accelerometerDriver, LIS2DW12_HIGH_PASS_ON_OUT);
    /* Set filter cuttof frequency */
    lis2dw12_filter_bandwidth_set(&accelerometerDriver, LIS2DW12_ODR_DIV_4);
    /* Configure power mode */
    lis2dw12_power_mode_set(&accelerometerDriver, LIS2DW12_CONT_LOW_PWR_12bit);
    /* Set Output Data Rate */
    lis2dw12_data_rate_set(&accelerometerDriver, LIS2DW12_XL_ODR_12Hz5);

    /* Set wake up duration
       1LSb = 1 / ODR
       Range: 0-3*/
    lis2dw12_wkup_dur_set(&accelerometerDriver, WKUP_DUR);  /* 0x3 */

    /* Set activity/inactivity threshold
       threshold: 1LSB = FS/64*/
    lis2dw12_wkup_threshold_set(&accelerometerDriver, WKUP_THRESHOLD); /* 0x01 */    
       
    lis2dw12_int_notification_set(&accelerometerDriver, LIS2DW12_INT_LATCHED);
    lis2dw12_pin_mode_set(&accelerometerDriver, LIS2DW12_PUSH_PULL);
    lis2dw12_pin_polarity_set(&accelerometerDriver, LIS2DW12_ACTIVE_HIGH);
    
    /* Enable interrupt routing to Int1, not sure this is needed */
    lis2dw12_pin_int1_route_get(&accelerometerDriver, &int_route.ctrl4_int1_pad_ctrl);
    int_route.ctrl4_int1_pad_ctrl.int1_wu = PROPERTY_ENABLE;
    lis2dw12_pin_int1_route_set(&accelerometerDriver, &int_route.ctrl4_int1_pad_ctrl);
}  /* Init_Accelerometer() */

/**
  ******************************************************************************
  * @func   Accelerometer_Enter_Shutdown()
  * @brief  Configure accelerometer by putting it into shutdown mode
  * @param  None
  * @gvar   accelerometerDriver, accel_sm
  * @gfunc  LIS2DW12_DATA_RATE_SET()
  * @retval None
  ******************************************************************************
  */
void Accelerometer_Enter_Shutdown(void)
{
    /* Set Output Data Rate for shutdown mode */
    lis2dw12_data_rate_set(&accelerometerDriver, LIS2DW12_XL_ODR_OFF);
    
    accel_sm.myState = ACCEL_STATE_SHUTDOWN; /* Set state for state machine */
} /* Accelerometer_Enter_Shutdown() */

/**
  ******************************************************************************
  * @func   Accelerometer_Exit_Shutdown()
  * @brief  Configure accelerometer by putting it into shutdown mode
  * @param  None
  * @gvar   accelerometerDriver, accel_sm
  * @gfunc  LIS2DW12_DATA_RATE_SET()
  * @retval None
  ******************************************************************************
  */
void Accelerometer_Exit_Shutdown(void)
{
    motion_detected = 0;

    Init_Accelerometer(); /* Re-Initialize Accelerometer */
    Init_Activity_Detection(); /* Re-initialize movemement detection variables */
} /* Accelerometer_Exit_Shutdown() */

/**
  ******************************************************************************
  * @func   Accelerometer_Enter_Idle()
  * @brief  Configure Accelerometer to go into idle state
  * @param  None
  * @gvar   accel_sm
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void Accelerometer_Enter_Idle(void)
{
    accel_sm.myState = ACCEL_STATE_IDLE;
} /* Accelerometer_Enter_Idle() */

/**
  ******************************************************************************
  * @func   Accelerometer_Exit_Idle()
  * @brief  Configure Accelerometer to come out of idle state
  * @param  None
  * @gvar   accel_sm
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void Accelerometer_Exit_Idle(void)
{
    accel_sm.myState = ACCEL_STATE_NOT_MOVING;
} /* Accelerometer_Exit_Idle() */

#if 0
/**
  ******************************************************************************
  * @func   NormalCheckAcc()
  * @brief  check acc action.  if i2c init (normal mode), init i2c & search acc.
  *         if check int1 (production & normal mode), check int1 & state machine
  * @param  None
  * @gvar   SystemCB, iAccSecCnt
  * @gfunc  NGT_I2C_Init(), SearchAcc(), CheckAccInt1(), CheckAccSM()
  * @retval None
  * @note   ACCCB_ACT_I2C_INIT requires i2c initialization and search for accelerometer.
  *         ACCCB_ACT_CHK_INT1 is a sub set of ACCCB_ACT_I2C_INIT.  No break statement
  *         is required.
  ******************************************************************************
  */
void NormalCheckAcc(void)
{
    if (ACCCB_ACT_CHK_INT1 == SystemCB.AccCB.uAction)
    {
        iAccSecCnt++;
        SystemCB.AccCB.uAction = ACCCB_ACT_IDLE;
        CheckAccInt1();     /* check accelerometer interrupt */
//$%^&
        CheckAccSM();       /* check accelerometer state machine*/
    }  /* if */
}  /* NormalCheckAcc() */
#endif

/**
  ******************************************************************************
  * @func   CheckAcc()
  * @brief  check acc action.  if check int1, check int1 & state machine
  * @param  None
  * @gvar   SystemCB, iAccSecCnt
  * @gfunc  CheckAccInt1(), CheckAccSM()
  * @retval None
  ******************************************************************************
  */
void CheckAcc(void)
{
    if (ACCCB_ACT_CHK_INT1 == SystemCB.AccCB.uAction)
    {
        iAccSecCnt++;
        SystemCB.AccCB.uAction = ACCCB_ACT_IDLE;
        CheckAccInt1();     /* check accelerometer interrupt */
        CheckAccSM();       /* check accelerometer state machine*/
    }
}  /* CheckAcc() */

/**
  ******************************************************************************
  * @func   CheckAccSM()
  * @brief  State Machine for Accelerometer Movement Detection
  * @param  None
  * @gvar   accel_sm
  * @gfunc  Update_Histogram(), Tool_Last_Used()
  * @retval None
  ******************************************************************************
  */
void CheckAccSM(void)
{        
    switch (accel_sm.myState)
    {
        /* NOT_MOVING STATE */
        case ACCEL_STATE_NOT_MOVING:
            /* Remain in NOT_MOVING state */
            if (FALSE == accel_sm.bMyThreshold)
            {               
                /* Reset Moving Counter */
                accel_sm.moving_counter = MAX_MOVE_COUNTER;
            }
            /* NOT_MOVING --> MOVING state */
            else
            {
                accel_sm.moving_counter--;
                if (0 == accel_sm.moving_counter)
                {
                    accel_sm.myState = ACCEL_STATE_MOVING;
                    
                    /* Reset Counters */
                    accel_sm.moving_counter = MAX_MOVE_COUNTER;
                    accel_sm.not_moving_counter = MAX_NOT_MOVE_COUNTER;
                }
            }
        break;  /* ACCEL_STATE_NOT_MOVING */
            
        /* MOVING STATE */
        case ACCEL_STATE_MOVING:
            accel_sm.histogram_activity_counter++; /* Increment Counter to determine how long moving (used for histogram) */
            /* MOVING --> NOT_MOVING state */
            if (FALSE == accel_sm.bMyThreshold)
            {
                accel_sm.not_moving_counter--;
                if (0 == accel_sm.not_moving_counter)
                {
                    accel_sm.myState = ACCEL_STATE_NOT_MOVING;
                    Update_Histogram(); /* UPDATE HISTOGRAM */
                    
                    /* Reset Counters */
                    accel_sm.moving_counter = MAX_MOVE_COUNTER;
                    accel_sm.not_moving_counter = MAX_NOT_MOVE_COUNTER;
                    accel_sm.histogram_activity_counter = 0;
                }
            }
            /* Remain in MOVING state */
            else
            {  
                /* Reset Not Moving Counter */
                accel_sm.not_moving_counter = MAX_NOT_MOVE_COUNTER;
            }
        break;  /* ACCEL_STATE_MOVING */

        /* IDLE STATE */
        case ACCEL_STATE_IDLE:
        break;  /* ACCEL_STATE_IDLE */
        
        /* SHUTDOWN STATE */
        case ACCEL_STATE_SHUTDOWN:
        break;  /* ACCEL_STATE_SHUTDOWN */
    } /* end-switch statement */
    
    Tool_Last_Used(); /* Update Tool_Last_Used every wakeup */
        
}  /* CheckAccSM() */
           
int32_t  time_periods[TIME_PERIOD_SIZE];  /* Number of consecutive no-movement detections to qualify for each bin */
uint16_t histo_limits[HISTOGRAM_SIZE_LIMIT];    /* Time values for each histogram bin */
/**
  ******************************************************************************
  * @func   Acc_TLU_H_Init()
  * @brief  Init tool last used and histogram
  * @param  None
  * @gvar   time_periods[], histo_limits[]
  * @gfunc  None
  * @retval None
  *         
  ******************************************************************************
  */
void Acc_TLU_H_Init(void)
{ 
#if(TESTING)
    /* Testing Values */
    time_periods[0] = 5 / SAMPLE_RATE;            /* 5 seconds */
    time_periods[1] = 10 / SAMPLE_RATE;           /* 10 seconds */
    time_periods[2] = 20 / SAMPLE_RATE;           /* 20 seconds */
    time_periods[3] = 30 / SAMPLE_RATE;           /* 30 seconds */
    time_periods[4] = 45 / SAMPLE_RATE;           /* 45 seconds */
    time_periods[5] = 60 / SAMPLE_RATE;           /* 1 minute */
    time_periods[6] = 90 / SAMPLE_RATE;           /* 1 minute 30 seconds */

    /* Testing Values */
    histo_limits[0] = (10 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* 10 seconds */
    histo_limits[1] = (20 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* 20 seconds */
    histo_limits[2] = (30 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* 30 seconds */
    histo_limits[3] = (40 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* 40 seconds */
    histo_limits[4] = (50 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* 50 seconds */
    histo_limits[5] = (60 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* 1 minute */
    histo_limits[6] = (60 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);           /* > 1 minute */
#else
    /* Real-Life Values */
    time_periods[0] = 3600 / SAMPLE_RATE;         /* 1 Hour */
    time_periods[1] = 86400 / SAMPLE_RATE;        /* 1 Day */
    time_periods[2] = 604800 / SAMPLE_RATE;       /* 7 Days */
    time_periods[3] = 2592000 / SAMPLE_RATE;      /* 30 Days */
    time_periods[4] = 7776000 / SAMPLE_RATE;      /* 90 Days */
    time_periods[5] = 15552000 / SAMPLE_RATE;     /* 180 Days */
    time_periods[6] = 31536000 / SAMPLE_RATE;     /* 365 Days */

    /* Real-Life Values */
    histo_limits[0] = (900 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);          /* 0-15 Minute */
    histo_limits[1] = (1800 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);         /* 15-30 Minutes */
    histo_limits[2] = (3600 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);         /* 30-60 Minutes */
    histo_limits[3] = (7200 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);         /* 1-2 Hours */
    histo_limits[4] = (10800 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);        /* 2-3 Hours */
    histo_limits[5] = (18000 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);        /* 3-5 Hours */
    histo_limits[6] = (18000 / SAMPLE_RATE) + (MAX_NOT_MOVE_COUNTER - MAX_MOVE_COUNTER);        /* > 5 Hours */
#endif
}  /* Acc_TLU_H_Init */

/**
  ******************************************************************************
  * @func   Tool_Last_Used()
  * @brief  Determine how long tool is inactive and update sAccIB.tool_last_used
  * @param  None
  * @gvar   accel_sm, sAccIB
  * @gfunc  None
  * @retval None
  *         
  ******************************************************************************
  */
void Tool_Last_Used(void)
{ 
    static uint32_t inactivity_counter = 0;
    uint8_t uTempVal;

    switch (accel_sm.myState)
    {
        case ACCEL_STATE_MOVING:
            inactivity_counter = 0; /* Movement exists, reset counter */
            sAccIB.tool_last_used = USAGE_LT_HOUR;
        break;  /* ACCEL_STATE_MOVING */

        case ACCEL_STATE_NOT_MOVING:
            /* Only count when < 1 year of inactivity */
            if (inactivity_counter < time_periods[6])
                inactivity_counter++;
            
            /* Determine amount of time tracker has been inactive */
            if (inactivity_counter < time_periods[0])
                sAccIB.tool_last_used = USAGE_LT_HOUR;
            else if (inactivity_counter < time_periods[1])
                sAccIB.tool_last_used = USAGE_LT_DAY;
            else if (inactivity_counter < time_periods[2])
                sAccIB.tool_last_used = USAGE_LT_7_DAYS;
            else if (inactivity_counter < time_periods[3])
                sAccIB.tool_last_used = USAGE_LT_30_DAYS;
            else if (inactivity_counter < time_periods[4])
                sAccIB.tool_last_used = USAGE_LT_90_DAYS;
            else if (inactivity_counter < time_periods[5])
                sAccIB.tool_last_used = USAGE_LT_180_DAYS;
            else if (inactivity_counter < time_periods[6])
                sAccIB.tool_last_used = USAGE_LT_365_DAYS;
            else if (inactivity_counter >= time_periods[6])
                sAccIB.tool_last_used = USAGE_GT_365_DAYS; /* > 1 Year Inactivity */
            else
            {
                inactivity_counter = 0; /* Movement, reset counter */
#ifdef ENABLE_RND_MSG
                StartTx_WaitTilEndOfTx("Tool_Last_Used ERROR: Invalid Counter Value\r\n");
#endif
            }
            
            /* update tool last used bits in NFC Data */
            uTempVal = SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_TOOLLASTUSED];
            uTempVal &= 0x1f;
            uTempVal |= (sAccIB.tool_last_used << 5);
            SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_TOOLLASTUSED] = uTempVal;
            
        break;  /* ACCEL_STATE_NOT_MOVING */
                
        case ACCEL_STATE_IDLE:
        break;  /* ACCEL_STATE_IDLE */
            
        case ACCEL_STATE_SHUTDOWN:
        break;  /* ACCEL_STATE_SHUTDOWN */
            
        default:
        break;
    } /* end-switch statement */
      
    /* Add Function call to write sAccIB.tool_last_used to byte  17*/
    add_last_used_to_advertising_data(sAccIB.tool_last_used);
}  /* Tool_Last_Used() */
                
/**
  ******************************************************************************
  * @func   Update_Histogram()
  * @brief  Determine percentages of usage and save to histogram
  * @param  None
  * @gvar   sAccIB, accel_sm
  * @gfunc  
  * @retval 0 = No issues in computing and saving histogram data
  *         1 = Something bad happened
  *         
  ******************************************************************************
  */
void Update_Histogram(void)
{
    static uint32_t histo_bin_counts[HISTOGRAM_SIZE_LIMIT] = {0, 0, 0, 0, 0, 0, 0};       /* Individual counters for each histogram bin */
    uint32_t bin_sum = 0;
    uint8_t percentage = 0;
    uint8_t *pHistogram;
  
    /* Update histogram when NO MOVEMENT in current state and moving in previous state */
    /* If no movement, don't do anything */
    if (0 == accel_sm.histogram_activity_counter)
    {
        accel_sm.histogram_activity_counter = 0;
    }
    /* Increment counters for each bin */
    else if (accel_sm.histogram_activity_counter < histo_limits[0])
    {
        histo_bin_counts[0]++;
    }
    else if (accel_sm.histogram_activity_counter < histo_limits[1])
    {
        histo_bin_counts[1]++;
    }
    else if (accel_sm.histogram_activity_counter < histo_limits[2])
    {
        histo_bin_counts[2]++;
    }
    else if (accel_sm.histogram_activity_counter < histo_limits[3])
    {
        histo_bin_counts[3]++;
    }
    else if (accel_sm.histogram_activity_counter < histo_limits[4])
    {
        histo_bin_counts[4]++;
    }
    else if (accel_sm.histogram_activity_counter < histo_limits[5])
    {
        histo_bin_counts[5]++;
    }
    else if (accel_sm.histogram_activity_counter > histo_limits[6])
    {
        histo_bin_counts[6]++;
    } 
    else
    {
#ifdef ENABLE_RND_MSG
        StartTx_WaitTilEndOfTx("Motion_Time ERROR: Invalid Counter\r\n");
#endif
    }
    
    /* Calculate sum of values in bins */
    for (int16_t i = 0; i < HISTOGRAM_SIZE_LIMIT; i++)
    {
        bin_sum += histo_bin_counts[i];
    }
    
    /* update histogram in BLE and NFC */
    pHistogram = &SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN1];
    
    /* Calculate and store percentages in histogram*/
    for (int16_t j = 0; j < HISTOGRAM_SIZE_LIMIT; j++)
    {
        percentage = (histo_bin_counts[j] * 100) / bin_sum;
        sAccIB.histogram[j] = percentage;
        *pHistogram-- = percentage;
    }
    
    /* The entire histogram gets re-written above, but we need to bring back in the firmware version since part of it is in the histogram */
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN7] = (SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN7] & HISTOGRAM_MASK) | ((FIRMWARE_VERSION_ID_ADVERTISING << 7) & FWVER_L_MASK);
    
    /* Add function call to write to Byte 18 (> 5 Hours) - Byte 24 (0-1 minute)*/
    add_histogram_to_advertising_data(&sAccIB.histogram[0]);
    
    
} /* Update_Histogram() */
