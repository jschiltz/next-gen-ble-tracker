/**
  ******************************************************************************
  * @file    NG_Tick_ae_tbl.c
  * @author  Thomas W Liu
  * @brief   Source file of audio engine table.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  * Note -  Rules for system counter
  *           count down, 1ms based, max 0x7fff.ffff
  * Note -  Rules to begin playing song
  *           Init uiAE_NewSongId, Set uiAE_NewCmd = TRUE
  * Note -  Rules to abort playing song
  *           Set uiAE_Abort = TRUE
  * Note -  About the chord table
  *          1st parameter = note
  *          2nd parameter = duration, value: 0 - 32767 (1ms based)
  *          Use NOTE_TERM in note as the end of table
  *          Use NOTE_DURATION_TERM_CNT in duration as the end of table
  * Note - About the progression table
  *          1st parameter = repeat
  *          2nd parameter = pointer to chord table
  *          Use 0 in repeat as the end of table
  *          Use NULL in pointer as the end of table
  * Note - About the song table
  *          1st parameter = song id
  *          2nd parameter = volume control, value: 0 - 3
  *          3rd parameter = repeat, value: 0 - 0x7fff.ffff
  *          4th parameter = pointer to pregression table
  *          Use SONG_INDEX_NONE in song id as the end of table
  *          Use AUDIO_LEVEL_0 in volume control as the end of table
  *          Use 0 in repeat as the end of table
  *          Use NULL in pointer as the end of table
  * Note - Rules for building notes, chords and progressions/songs
  *          1.  No deadband (frequency = 0) as the only chord
  *          2.  No deadband (frequency = 0) as the first note or chord
  * Note - Rules for volume override; it is defaulted to zero.
  *          if SongVolumeOverride == 0, then default volume (from table) is used.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include "NG_Tick_ae.h"

/* define declaration ------------------------------------------------------*/

/* type declaration ----------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/
/**
  * @brief  Pairing chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_Pair[] =
{
    /* Note Freq        On Duration */
    { NOTE_D8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 4699hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_C8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 4186hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_F8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 5588hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/**
  * @brief  Beacon 1 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_Beacon1[] =
{
    /* Note Freq        On Duration */
    { NOTE_G8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 6272hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_B8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 7902hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 6272hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_C9       ,   NOTE_DURATION_82MSEC_CNT  },  /* 8372hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/**
  * @brief  Beacon 2 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_Beacon2[] =
{
    /* Note Freq        On Duration */
    { NOTE_G8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 6272hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_B8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 7902hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 6272hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_C9       ,   NOTE_DURATION_82MSEC_CNT  },  /* 8372hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_808MSEC_CNT },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/**
  * @brief  Beacon 3 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_Beacon3[] =
{
    /* Note Freq        On Duration */
    { NOTE_C7       ,   NOTE_DURATION_95MSEC_CNT  },  /* 2093hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_MINMSEC_CNT },
    { NOTE_A7       ,   NOTE_DURATION_95MSEC_CNT  },  /* 3520hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_MINMSEC_CNT },
    { NOTE_B7       ,   NOTE_DURATION_95MSEC_CNT  },  /* 3951hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_MINMSEC_CNT },
    { NOTE_C8       ,   NOTE_DURATION_255MSEC_CNT },  /* 4186hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_776MSEC_CNT },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/**
  * @brief  Beacon 3 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_Beacon4[] =
{
    /* Note Freq        On Duration */
    { NOTE_C7       ,   NOTE_DURATION_95MSEC_CNT  },  /* 2093hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_MINMSEC_CNT },
    { NOTE_A7       ,   NOTE_DURATION_95MSEC_CNT  },  /* 3520hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_MINMSEC_CNT },
    { NOTE_B7       ,   NOTE_DURATION_95MSEC_CNT  },  /* 3951hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_MINMSEC_CNT },
    { NOTE_C8       ,   NOTE_DURATION_255MSEC_CNT },  /* 4186hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_26MSEC_CNT  },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/**
  * @brief  tick tok 1 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_TickTock0[] =
{
    /* Note Freq        On Duration */
    { NOTE_A3       ,   NOTE_DURATION_150MSEC_CNT  },   /* 220hz */
    { NOTE_C3       ,   NOTE_DURATION_200MSEC_CNT  },   /* 130hz */
    { NOTE_A7       ,   NOTE_DURATION_500MSEC_CNT  },   
    { NOTE_G8       ,   NOTE_DURATION_500MSEC_CNT  },   /* 6272hz */
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT     }
};

/**
  * @brief  tick tok 1 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_TickTock1[] =
{
    /* Note Freq        On Duration */
    { NOTE_A4       ,   NOTE_DURATION_400MSEC_CNT  },   /* 440hz */
    { NOTE_C4       ,   NOTE_DURATION_300MSEC_CNT  },   /* 262hz */
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT     }
};

/**
  * @brief  tick tok 2 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_TickTock2[] =
{
    /* Note Freq        On Duration */
    { NOTE_A5       ,   NOTE_DURATION_300MSEC_CNT  },   /* 880hz */
    { NOTE_C5       ,   NOTE_DURATION_300MSEC_CNT  },   /* 523hz */
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT     }
};

/**
  * @brief  tick tok 3 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_TickTock3[] =
{
    /* Note Freq        On Duration */
    { NOTE_A6       ,   NOTE_DURATION_200MSEC_CNT  },   /* 1760hz */
    { NOTE_C6       ,   NOTE_DURATION_200MSEC_CNT  },   /* 1047hz */
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT     }
};

/**
  * @brief  tick tok 4 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_TickTock4[] =
{
    /* Note Freq        On Duration */
    { NOTE_A7       ,   NOTE_DURATION_100MSEC_CNT  },   /* 3520hz */
    { NOTE_C7       ,   NOTE_DURATION_100MSEC_CNT  },   /* 2093hz */
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT     }
};

/* test variable declaration -------------------------------------------------*/
/**
  * @brief  Pairing chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_P2[] =
{
    /* Note Freq        On Duration */
    { NOTE_D4       ,   NOTE_DURATION_82MSEC_CNT  },  /* 294hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_D5       ,   NOTE_DURATION_82MSEC_CNT  },  /* 587hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_D6       ,   NOTE_DURATION_82MSEC_CNT  },  /* 1175hz */
    { NOTE_D7       ,   NOTE_DURATION_82MSEC_CNT  },  /* 2349hz */
    { NOTE_D8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 4699hz */
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/**
  * @brief  Beacon 1 chord - define chord control block value
  */
const STRUCT_CHORD_CB_TYPE   BuzzerChord_B2[] =
{
    /* Note Freq        On Duration */
    { NOTE_G4       ,   NOTE_DURATION_82MSEC_CNT  },  /* 391hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G5       ,   NOTE_DURATION_82MSEC_CNT  },  /* 784hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G6       ,   NOTE_DURATION_82MSEC_CNT  },  /* 1567hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G7       ,   NOTE_DURATION_82MSEC_CNT  },  /* 3136hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G8       ,   NOTE_DURATION_82MSEC_CNT  },  /* 6272hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_G9       ,   NOTE_DURATION_82MSEC_CNT  },  /* 12543hz */
    { NOTE_NO_NOTE  ,   NOTE_DURATION_58MSEC_CNT  },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT  }
};

const STRUCT_CHORD_CB_TYPE   BuzzerChord_4KHz[] =
{
    { NOTE_B7       ,   NOTE_DURATION_200MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_B7       ,   NOTE_DURATION_200MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_B7       ,   NOTE_DURATION_200MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_B7       ,   NOTE_DURATION_200MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_B7       ,   NOTE_DURATION_200MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/*This is the Find Me Chord for the Find Me #1 Logo*/
const STRUCT_CHORD_CB_TYPE   FindMeChord_1[] =
{
    { NOTE_C6       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_G5       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },    
    { NOTE_C6       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_G5       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },    
    { NOTE_C6       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_G5       ,   NOTE_DURATION_125MSEC_CNT },   
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },    
    { NOTE_C6       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_G5       ,   NOTE_DURATION_125MSEC_CNT },   
    { NOTE_NO_NOTE  ,   NOTE_DURATION_750MSEC_CNT },
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/*This is the Find Me Chord for the Find Me #2 Logo*/
const STRUCT_CHORD_CB_TYPE   FindMeChord_2[] =
{
    { NOTE_C6       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_C5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_C6       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_C5       ,   NOTE_DURATION_64MSEC_CNT }, 
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_C6       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_C5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_D3_SHARP ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_G5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_D3_SHARP ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_G5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_D3_SHARP ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_G5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_E6       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_E5       ,   NOTE_DURATION_64MSEC_CNT },    
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_E6       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_E5       ,   NOTE_DURATION_64MSEC_CNT },    
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },    
    { NOTE_E6       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_E5       ,   NOTE_DURATION_64MSEC_CNT }, 
    { NOTE_NO_NOTE  ,   NOTE_DURATION_750MSEC_CNT},
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT   }
};

/*This is the Find Me Chord for the Find Me #3 Logo*/
const STRUCT_CHORD_CB_TYPE   FindMeChord_3[] =
{
    { NOTE_D5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_E5       ,   NOTE_DURATION_64MSEC_CNT },       
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_G5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_A5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_B5       ,   NOTE_DURATION_64MSEC_CNT },   
    { NOTE_NO_NOTE  ,   NOTE_DURATION_300MSEC_CNT},
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT   }
};

/*This is the Success Chord for the Success Logo*/
const STRUCT_CHORD_CB_TYPE   SuccessChord[] =
{
    { NOTE_C5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_E5       ,   NOTE_DURATION_64MSEC_CNT },       
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_G5       ,   NOTE_DURATION_64MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT },
    { NOTE_C6       ,   NOTE_DURATION_64MSEC_CNT }, 
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT   }
};

/*This is the Failure Chord for the Failure Logo*/
const STRUCT_CHORD_CB_TYPE   FailureChord[] =
{
    { NOTE_F5       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_D5       ,   NOTE_DURATION_125MSEC_CNT },       
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/*This is the Acknowledgement Chord for the Acknoledgement Logo*/
const STRUCT_CHORD_CB_TYPE   AcknowledgementChord1[] =
{
    { NOTE_D6       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_G6       ,   NOTE_DURATION_125MSEC_CNT },     
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_F5       ,   NOTE_DURATION_125MSEC_CNT }, 
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_D5       ,   NOTE_DURATION_125MSEC_CNT },     
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};

/*This is the Acknowledgement Chord for the Acknoledgement Logo*/
const STRUCT_CHORD_CB_TYPE   AcknowledgementChord2[] =
{
    { NOTE_G5       ,   NOTE_DURATION_125MSEC_CNT },
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_D5       ,   NOTE_DURATION_125MSEC_CNT },     
    { NOTE_NO_NOTE  ,   NOTE_DURATION_20MSEC_CNT  },
    { NOTE_E5       ,   NOTE_DURATION_125MSEC_CNT },    
    { NOTE_TERM     ,   NOTE_DURATION_TERM_CNT    }
};


/* variable declaration ------------------------------------------------------*/
/**
  * @brief  Pairing song - define progression control block value, 420ms
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Pair[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Pair[0]},
    {   0,    NULL                }
};

/**
  * @brief  Beacon song - define progression control block value, 17.55 seconds
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Beacon[] =
{
    /* chord repeat, chord ptr */
    {   14,   (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Beacon1[0]},
    {   1,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Beacon2[0]},
    {   15,   (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Beacon1[0]},
    {   0,    NULL                   }
};

/**
  * @brief  alternate Beacon song - define progression control block value, 12 seconds
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_BeaconAlt[] =
{
    /* chord repeat, chord ptr */
    {   8,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Beacon4[0]},
    {   1,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Beacon3[0]},
    {   9,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_Beacon4[0]},
    {   0,    NULL                   }
};

/**
  * @brief  tick tock song - define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_TickTock[] =
{
    /* chord repeat, chord ptr */
    {   4,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock0[0]},
    {   4,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock1[0]},
    {   4,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock2[0]},
    {   6,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock3[0]},
    {   8,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock4[0]},
    {   0,    NULL                     }
};

/**
  * @brief  tick tock song - define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Tick0[] =
{
    /* chord repeat, chord ptr */
    {   2,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock0[0]},
    {   3,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock1[0]},
    {   0,    NULL                     }
};

/**
  * @brief  tick tock song - define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Tick1[] =
{
    /* chord repeat, chord ptr */
    {   4,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock1[0]},
    {   0,    NULL                     }
};

/**
  * @brief  tick tock song - define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_Tick2[] =
{
    /* chord repeat, chord ptr */
    {   3,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock0[0]},
    {   5,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock1[0]},
    {   1,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_TickTock2[0]},
    {   0,    NULL                     }
};

/**
  * @brief  tick tock song - define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_B2[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_B2[0]},
    {   0,    NULL              }
};

/**
  * @brief  tick tock song - define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_P2[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_P2[0]},
    {   0,    NULL              }
};


/**
  * @brief  4Khz song (mfg)- define progression control block value
  */
const STRUCT_PROGRESS_CB_TYPE  BuzzerSong_4KHz[] =
{
    /* chord repeat, chord ptr */
    {   5,    (STRUCT_CHORD_CB_TYPE *)&BuzzerChord_4KHz[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Find Me Song #1
  */
const STRUCT_PROGRESS_CB_TYPE  FindMeSong_1[] =
{
    /* chord repeat, chord ptr */
    {   2,    (STRUCT_CHORD_CB_TYPE *)&FindMeChord_1[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Find Me Song #2
  */
const STRUCT_PROGRESS_CB_TYPE  FindMeSong_2[] =
{
    /* chord repeat, chord ptr */
    {   2,    (STRUCT_CHORD_CB_TYPE *)&FindMeChord_2[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Find Me Song #3
  */
const STRUCT_PROGRESS_CB_TYPE  FindMeSong_3[] =
{
    /* chord repeat, chord ptr */
    {   2,    (STRUCT_CHORD_CB_TYPE *)&FindMeChord_3[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Success
  */
const STRUCT_PROGRESS_CB_TYPE  SuccessSong[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&SuccessChord[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Failure
  */
const STRUCT_PROGRESS_CB_TYPE  FailureSong[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&FailureChord[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Ack 1
  */
const STRUCT_PROGRESS_CB_TYPE  AcknowledgementSong1[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&AcknowledgementChord1[0] },
    {   0,    NULL                 }
};

/**
  * @brief  Ack 2
  */
const STRUCT_PROGRESS_CB_TYPE  AcknowledgementSong2[] =
{
    /* chord repeat, chord ptr */
    {   1,    (STRUCT_CHORD_CB_TYPE *)&AcknowledgementChord2[0] },
    {   0,    NULL                 }
};


/**
  * @brief  Beacon song - define progression control block value
  */
STRUCT_SONG_CB_TYPE  BuzzerSongTable[] =
{
        /* song id,          volume, progression repeat, pregression ptr */
    {SONG_INDEX_BEACON,     AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_Beacon[0]     },  /* from BGM11S */
    {SONG_INDEX_BEACON_ALT, AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_BeaconAlt[0]  },  /* from BGM11S */
    {SONG_INDEX_PAIR,       AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_Pair[0]       },  /* from BGM11S */
    {SONG_INDEX_FUN,        AUDIO_LEVEL_3, 3, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_TickTock[0]   },  /* test song */
    {SONG_INDEX_TT0,        AUDIO_LEVEL_3, 3, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_Tick0[0]      },  /* test song */
    {SONG_INDEX_TT1,        AUDIO_LEVEL_3, 2, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_Tick1[0]      },  /* test song */
    {SONG_INDEX_TT2,        AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_Tick2[0]      },  /* test song */
    {SONG_INDEX_B2,         AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_B2[0]         },  /* test song */
    {SONG_INDEX_P2,         AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_P2[0]         },  /* test song */
    {SONG_INDEX_4K,         AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&BuzzerSong_4KHz[0]       },  /* test song */
    {SONG_INDEX_ACK1,       AUDIO_LEVEL_2, 1, (STRUCT_PROGRESS_CB_TYPE *)&AcknowledgementSong1[0]  },    
    {SONG_INDEX_ACK2,       AUDIO_LEVEL_2, 1, (STRUCT_PROGRESS_CB_TYPE *)&AcknowledgementSong2[0]  },
    {SONG_INDEX_FAIL,       AUDIO_LEVEL_2, 1, (STRUCT_PROGRESS_CB_TYPE *)&FailureSong[0]           },
    {SONG_INDEX_SUCCESS,    AUDIO_LEVEL_2, 1, (STRUCT_PROGRESS_CB_TYPE *)&SuccessSong[0]           },
    {SONG_INDEX_FIND1,      AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&FindMeSong_1[0]          },
    {SONG_INDEX_FIND2,      AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&FindMeSong_2[0]          },
    {SONG_INDEX_FIND3,      AUDIO_LEVEL_3, 1, (STRUCT_PROGRESS_CB_TYPE *)&FindMeSong_3[0]          },
    {SONG_INDEX_NONE,       AUDIO_LEVEL_0, 0, NULL                      }

};

