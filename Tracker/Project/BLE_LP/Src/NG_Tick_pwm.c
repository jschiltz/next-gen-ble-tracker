/**
  ******************************************************************************
  * @file    NG_Tick_pwm.c
  * @author  Thomas W Liu
  * @brief   Source file of pwm.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_ll_bus.h"
#include "bluenrg_lp_ll_system.h"
#include "bluenrg_lp_ll_utils.h"
#include "bluenrg_lp_ll_tim.h"
#include "bluenrg_lp_ll_gpio.h"
#include "NG_Tick_pwm.h"
#include "NG_Tick_ae.h"

/* variable declaration ------------------------------------------------------*/
/* TIM1 Clock */
uint32_t timxPrescaler = 0;     /* prescaler */
uint32_t timxPeriod = 0;        /* time period */

#ifdef EN_RD_PWM_MSG
char    cPWMStr[80] = { 0 };   /* message to be transmitted */
#endif
    

/* function prototype --------------------------------------------------------*/
void Buzzer_TIM1_Var_Init(void);
void Buzzer_TIM1_Init(void);
void Buzzer_TIM1_Enable(void);
void Buzzer_TIM1_Disable(void);
void Configure_DutyCycle6(uint32_t);
void Configure_DutyCycle6_50DC(void);

/**
  ******************************************************************************
  * @func   Buzzer_TIM1_Var_Init()
  * @brief  Initialize TIM1 related variables
  * @param  None
  * @gvar   timxPrescaler, timxPeriod
  * @gfunc  __LL_TIM_CALC_PSC(), __LL_TIM_CALC_ARR()
  * @retval None
  * @note   prescaler = (((tmclk + ((hz * COUNTER_FREQ_F) / 2.0)) / (hz * COUNTER_FREQ_F)) - 1.0)
  *         to compensate round off error:  add 0.5 before truncate to uint32
  *         auto reload register: defaulted to COUNTER_FREQ_I or COUNTER_FREQ_F - 1
  ******************************************************************************
  */
void Buzzer_TIM1_Var_Init(void)
{
    float fFreq, fTimxPrescaler;

    fFreq = (float)440.0;  /* default to 440 hz */
    // to compensat division round off
    fTimxPrescaler = ((((float)TIM_PERIPHCLK + (((float)fFreq * COUNTER_FREQ_F) / 2.0)) / ((float)fFreq * COUNTER_FREQ_F)) - 1.0);
    timxPrescaler = (uint32_t)(fTimxPrescaler + 0.5);  /* to compensate division round off */
    LL_TIM_SetPrescaler( TIM1, timxPrescaler);  /* init PSC register */
    timxPeriod = COUNTER_AUTORELOAD;
    LL_TIM_SetAutoReload(TIM1, timxPeriod);  /* Change PWM signal frequency */
}       /* Buzzer_TIM1_Var_Init() */

/**
  ******************************************************************************
  * @func   Buzzer_TIM1_Init()
  * @brief  TIM1 Initialization Function.
  * @param  None
  * @gvar   None
  * @gfunc  LL_APB0_EnableClock(), LL_AHB_EnableClock(), LL_GPIO_Init(),
  *         NVIC_SetPriority(), NVIC_EnableIRQ(), LL_TIM_Init(), LL_TIM_OC_EnablePreload(),
  *         LL_TIM_OC_Init(), LL_TIM_OC_DisableFast()
  * @retval None
  ******************************************************************************
  */
void Buzzer_TIM1_Init(void)
{
    LL_TIM_InitTypeDef TIM_InitStruct = {0};
    LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};

    LL_GPIO_InitTypeDef GPIO_InitStruct;

    /* Peripheral clock enable */
    LL_APB0_EnableClock(LL_APB0_PERIPH_TIM1);
    LL_AHB_EnableClock(LL_AHB_PERIPH_GPIOB);

    /**TIM1 GPIO Configuration  
    pin/mode = PA11 / AF4   ------> TIM1_CH6
    */
    GPIO_InitStruct.Pin = BUZZER_PIN;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;  /* default output to low */
    GPIO_InitStruct.Alternate = BUZZER_AF;
    LL_GPIO_Init(BUZZZER_GPIO_PORT, &GPIO_InitStruct);

    /* TIM1 interrupt Init */
    NVIC_SetPriority(TIM1_IRQn, IRQ_HIGH_PRIORITY);
    NVIC_EnableIRQ(TIM1_IRQn);

    TIM_InitStruct.Prescaler = timxPrescaler;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.Autoreload = timxPeriod;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    TIM_InitStruct.RepetitionCounter = 0;
    LL_TIM_Init(TIM1, &TIM_InitStruct);
    LL_TIM_EnableARRPreload(TIM1);
//    LL_TIM_OC_EnablePreload(TIM1, LL_TIM_CHANNEL_CH1);
    /*  CCMR3   OC6PE   LL_TIM_OC_EnablePreload */
    LL_TIM_OC_EnablePreload(TIM1, LL_TIM_CHANNEL_CH6);
    TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
    TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE;
    TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
    TIM_OC_InitStruct.CompareValue = ((timxPeriod + 1 ) / 2);
    TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;  
    TIM_OC_InitStruct.OCNPolarity = LL_TIM_OCPOLARITY_LOW;
    TIM_OC_InitStruct.OCIdleState = LL_TIM_OCIDLESTATE_LOW;
    TIM_OC_InitStruct.OCNIdleState = LL_TIM_OCIDLESTATE_HIGH;
    LL_TIM_OC_Init(TIM1, LL_TIM_CHANNEL_CH6, &TIM_OC_InitStruct);
    LL_TIM_OC_DisableFast(TIM1, LL_TIM_CHANNEL_CH6);

    LL_TIM_ClearFlag_CC1(TIM1);
    LL_TIM_ClearFlag_CC2(TIM1);
    LL_TIM_ClearFlag_CC3(TIM1);
    LL_TIM_ClearFlag_CC4(TIM1);
    LL_TIM_ClearFlag_CC5(TIM1);
    LL_TIM_ClearFlag_CC6(TIM1);
}       /* Buzzer_TIM1_Init() */


/**
  ******************************************************************************
  * @func   Buzzer_TIM1_Enable()
  * @brief  Setup timer 1, set channel 6 as output, enable chan 6, enable counter
  * @param  none
  * @gvar   None
  * @gfunc  LL_TIM_CC_EnableChannel(), LL_TIM_EnableAllOutputs(),
  *         LL_TIM_EnableCounter(), LL_TIM_GenerateEvent_UPDATE()
  * @retval None
  ******************************************************************************
  */
void Buzzer_TIM1_Enable(void)
{
    /* ---- TIM1 interrupts set-up ---- */
    /* Enable the capture/compare interrupt for channel 1 */
//    LL_TIM_EnableIT_CC1(TIM1);

    /* ---- Start output signal generation ---- */
    /* CCER     CC6E    LL_TIM_CC_EnableChannel */
    LL_TIM_CC_EnableChannel(TIM1, LL_TIM_CHANNEL_CH6);  /* Enable output channel 6 */           

    /* Enable the outputs (set the MOE bit in TIMx_BDTR register). */
    /* @rmtoll BDTR     MOE     LL_TIM_EnableAllOutputs */
    LL_TIM_EnableAllOutputs(TIM1);

    /* @rmtoll CR1      CEN     LL_TIM_EnableCounter */
    LL_TIM_EnableCounter(TIM1);         /* Enable counter */
    LL_TIM_GenerateEvent_UPDATE(TIM1);  /* Force update generation */
}       /* Buzzer_TIM1_Enable() */
  

/**
  ******************************************************************************
  * @func   Buzzer_TIM1_Disable()
  * @brief  Setup timer 1, set channel 6 as output, enable chan 6, enable counter
  * @param  none
  * @gvar   None
  * @gfunc  LL_TIM_DisableCounter(), LL_TIM_DisableAllOutputs(),
  *         LL_TIM_CC_DisableChannel()
  * @retval None
  ******************************************************************************
  */
void Buzzer_TIM1_Disable(void)
{
    LL_TIM_DisableCounter(TIM1);    /* disable counter */
    /* disable the outputs (clear the MOE bit in TIMx_BDTR register). */
    LL_TIM_DisableAllOutputs(TIM1);
    /* ---- Stop output signal generation ---- */
    LL_TIM_CC_DisableChannel(TIM1, LL_TIM_CHANNEL_CH6);  /* disable output channel 6 */           
}       /* Buzzer_TIM1_Disable() */
  

/**
  ******************************************************************************
  * @func   Configure_DutyCycle()
  * @brief  Changes the duty cycle of the PWM signal
  *         D = (T/P)*100, where T = pulse duration, P = PWM signal period
  * @param  D Duty cycle
  * @gvar   None
  * @gfunc  LL_TIM_GetAutoReload(), LL_TIM_OC_SetCompareCH6()
  * @retval None
  ******************************************************************************
  */
void Configure_DutyCycle6(uint32_t D)
{
    uint32_t P;    /* Pulse duration */
    uint32_t T;    /* PWM signal period */

    /* PWM signal period is determined by the value of the auto-reload register */
    T = LL_TIM_GetAutoReload(TIM1) + 1;

    /* Pulse duration is determined by the value of the compare register.       */
    /* Its value is calculated in order to match the requested duty cycle.      */
    P = (D * T) / 100;
    LL_TIM_OC_SetCompareCH6(TIM1, P);
#ifdef EN_RD_PWM_MSG
//    printf("Changes the duty cycle of the PWM signal : %d \n\r", P);
    sprintf( cPWMStr, "New PWM duty: %d\r\n", P);
    StartTx_WaitTilEndOfTx(cPWMStr);
#endif
}       /* Configure_DutyCycle() */

  
/**
  ******************************************************************************
  * @func   Configure_DutyCycle6_50DC()
  * @brief  Changes the duty cycle of the PWM signal
  *         D = (T/P)*100, where T = pulse duration, P = PWM signal period
  * @param  D Duty cycle
  * @gvar   None
  * @gfunc  LL_TIM_GetAutoReload(), LL_TIM_OC_SetCompareCH6()
  * @retval None
  ******************************************************************************
  */
void Configure_DutyCycle6_50DC()
{
#if 1
    uint32_t P;    /* Pulse duration */
    uint32_t T;    /* PWM signal period */

    /* PWM signal period is determined by the value of the auto-reload register */
    /* pulse count is half for 50% duty cycle, shift right once */
    T = LL_TIM_GetAutoReload(TIM1) + 1;
    P = (50 * T) / 100;
    LL_TIM_OC_SetCompareCH6(TIM1, P);
#endif
    
#if 0
    uint32_t P;    /* Pulse duration */
    
    /* PWM signal period is determined by the value of the auto-reload register */
    /* pulse count is half for 50% duty cycle, shift right once */
    P = (LL_TIM_GetAutoReload(TIM1) + 1) >> 1;
    LL_TIM_OC_SetCompareCH6(TIM1, P);
#endif
}       /* Configure_DutyCycle() */

