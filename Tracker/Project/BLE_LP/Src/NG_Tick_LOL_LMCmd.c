/**
  ******************************************************************************
  * @file    NG_Tick_LOL_LMCmd.c
  * @author  Thomas W Liu
  * @brief   Source file of Lite Open Link commands, local memory
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LOLConfig.h"
#include "system_BlueNRG_LP.h"
#include "NG_Tick_LOL_Cmd.h"
#include "NG_Tick_LOL_CmdVar.h"
#include "NG_Tick_uart.h"
#include "NG_Tick_FlashUtil.h"    /* must come before NG_Tick_sys.h */
#include "NG_Tick_NFC.h"          /* must come before NG_Tick_sys.h */
#include "NG_Tick_Acc.h"          /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_ADC.h"
#include "NG_Tick_ble.h"
#include "NG_Tick_LOL_Func.h"

/* constant variable declaration ---------------------------------------------*/
//static const uint16_t memoryMapVersion = 0x0022;        // Test data for a read of the version of the virtual memory map

/* constant variable declaration ------------------------------------------------------*/

/* variable declaration ------------------------------------------------------*/

//uint8_t mailboxArray[4] = { 0x0A, 0x00, 0x01, 0x8A};       // data to test the mailbox functionality
uint8_t MBresponseArray[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

/* function prototype --------------------------------------------------------*/
void debug_assert(bool);
void begin_super_loop( void );
eCommsErr Tool_data_tx( uint8_t *, uint8_t, uint8_t);  /* !@#$ */
void sendDevNameChannel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMSWPN( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void slave5( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void simpleAck( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void sendMaxPktLn( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void MBCompleteCallback1( bool, uint8_t *, uint8_t, uint8_t);
void passwordEnable( uint8_t *, uint8_t, uint8_t , uint8_t, uint8_t );
eMM_Sec_Lev NGT_PwVerify(uint8_t *);
eMM_Sec_Lev PasswordVerification( uint8_t *);
void setSecLevel( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void readMemMapVersion( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void RWExtMemOffset( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMMPBID( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMFwVersion( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateOfMfg( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateOfService( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateLastUSed( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMDateTimeRef( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMPlatformId( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);
void fLMBLEMACAddr( uint8_t *, uint8_t, uint8_t, uint8_t, uint8_t);


/**
  ******************************************************************************
  * @func   debug_assert()
  * @brief  debugging
  * @param  None
  * @gvar   
  * @gfunc  fault_disable_system()
  * @retval None
  ******************************************************************************
  */
void debug_assert(bool expression)
{
    #if defined (DEBUG_BUILD)
        if(!expression)
        {
            fault_disable_system( );
        }
    #endif
}  /* debug_assert() */

/***************************************************************************//**
* \fn void begin_super_loop()
* \brief Invokes the start of the application's super loop
* \details Sets the super loop main status flag to invoke the execution of the
* super loop.
* @param[out] bsuper_loop_ready 1ms Super Loop Start Flag
*******************************************************************************/
void begin_super_loop( void )
{
    ms_time = true;
}  /* begin_super_loop() */

/******************************************************************************/
/** \fn	eCommsErr Tool_data_tx(uint8_t *data, uint8_t data_len, uint8_t target);
* \brief    Tool side function used by the library to transmit data
* \details  This function is exposed from the main calling program. It is expected to
*	be called by the library to send OL packets for data transmission
*       \param data	- input array/pointer of bytes
*	\param data_len	- length of input array
*	\param target	- source of packet (BTUART, BATTERY_TERMINAL, etc.)
*	\return		- a eCommsErr with the result of processing the send request.
* \author   Original by: Jay Walsh
* \author   Stolen by: Al Lukowitz
* \pre		none
* \warning	<b> <i> Must exist in main tool code, library expects it!  </i> </b>
*			\n declare your return variable \b volatile
* \code
* uint8_t * data[]={1,2,3,4,5,6,7,8};
* volatile eCommsErr ret_val;
* ret_val = Tool_data_tx(data, 8,BT_UART);
* prinf("%d",ret_val);
* >><some eComsErr value>
* \endcode
*******************************************************************************/
/* Function to send generic data out via the target source. */
eCommsErr Tool_data_tx(uint8_t *data, uint8_t packet_len, uint8_t target) 
{
//    uart_dma_tx(target, packet_len, data);  /* !@#$% insert tx here */
    switch (target)
    {
        case LOL_CHAN_UART:
            pStartNextTransfers( data, packet_len);
        break;  /* LOL_CHAN_UART */
        
        case LOL_CHAN_BLE:
            pBLEStartNextTransfers( data, packet_len);
        break;  /* LOL_CHAN_BLE */
    }  /* switch() */
    return(eCommsErr_NONE);
}  /* eCommsErr() */

/******************************************************************************/
/**
* \fn void sendDevNameChannel()
* \brief Process a Read/Write Common Memory Map
* \details Note, write is not legal by definition.  It will be rejected before
*          this routine is called because of the eMM_Sec_Lev__NONE secrutiy setting
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void sendDevNameChannel(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                        uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.ackNack = TRUE;
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {
        for (int16_t iCnt = 0; iCnt < payloadLength - 2; iCnt++)
        {
            // If we are writing, move the data over!
            deviceName[iCnt] = payloadPtr[iCnt + 2];
        }
    }
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = (uint8_t *)deviceName;
    slaveAckVals.payloadLength = SIZE_DEVICE_NAME_BUFF;
    LOLSendACK(&slaveAckVals);
}  /* sendDevNameChannel() */

/**
  ******************************************************************************
  * @func   fLMSWPN()
  * @brief  Read/Write local memory - SW Part Number Request
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMSWPN(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write MPBID, prep ptr to update internal value */
        if ((LOL_SWPN_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)  /* 6 */
        {
            EndianSwapRead32( &payloadPtr[2], 4);
            SystemIB.SysFlashIB.ulDeviceSWPN = (uint32_t)unDataSwap32.uiVal;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */
        }
        else
        {
            /* incorrect payload, NAK it!! */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read SWPN, prep ptr to send internal value*/
        EndianSwap32( &uiESwapData32, SystemIB.SysFlashIB.ulDeviceSWPN);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData32;
        slaveAckVals.payloadLength = LOL_SWPN_PAYLOAD_LEN;  /* 4 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMSWPN() */

/******************************************************************************/
/**
* \fn void slave5()
* \brief Call LOLSendACK to ACK the packet
* \details
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void slave5(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
            uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = payloadPtr;
    slaveAckVals.payloadLength = payloadLength-4;
    slaveAckVals.ackNack = TRUE;
    LOLSendACK(&slaveAckVals);
}  /* slave5() */

/******************************************************************************/
/**
* \fn void simpleAck()
* \brief Call LOLSendACK to ACK the packet
* \details
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void simpleAck(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
               uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = NULL;
    slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
    slaveAckVals.ackNack = TRUE;
    LOLSendACK(&slaveAckVals);
}  /* simpleAck() */

/******************************************************************************/
/**
* \fn void sendMaxPktLn()
* \brief Respond with the max packet length
* \details
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void sendMaxPktLn(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                  uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {
        slaveAckVals.data = NULL;
        slaveAckVals.payloadLength = 0;
        slaveAckVals.ackNack = FALSE;
    }
    else
    {
        slaveAckVals.data = &maxPacketLength;
        slaveAckVals.payloadLength = 1;
        slaveAckVals.ackNack = TRUE;
    }
    LOLSendACK(&slaveAckVals);
}  /* sendMaxPktLn() */

mailboxVals mailboxVariables;

/******************************************************************************/
/**
* \fn void MBCompleteCallback1()
* \brief Function that will be called on a mailbox transmission complete
* \details Recreate response from channel 1 but use original SID as requested from channel 0
* \details Store data from message, if any exists
* \details ACK original message on channel 0
* \param[in] ackNack - True if this is an ACK, else it's a NAK
* \param[in] payloadPtr - pointer to the payload
* \param[in] payloadLength - how many bytes are in the buffer?
* \return None
*******************************************************************************/
void MBCompleteCallback1(bool ackNack, uint8_t * payloadPtr, uint8_t payloadLength, uint8_t channelNumber)
{
    // We need to rebuild the message and store it in MBresponseArray, without the checksum
    mailboxVariables.data = MBresponseArray;    // Point to the array that will store recreated response message
    MBresponseArray[MESSAGESIDPOS] = mailboxVariables.BLESID;   // original SID for BLE from Channel 0
    // do we have data to store?
    if (payloadLength > 0)
    {
        // there is data, store it!
        mailboxVariables.length = payloadLength + TXMAILBOXOVERHEAD;        // This will be for the response on channel 0
        MBresponseArray[PAYLOADLENGTHPOS] = payloadLength;                  // Store length of message returned on channel 1
        memcpy(&MBresponseArray[PAYLOADPOS], payloadPtr, payloadLength);    // Move the payload data
        if (true == ackNack)
        {
            MBresponseArray[MESSAGEIDPOS] = eACK_WITH_DATA;                 // Set correct message ID
        }
        else
        {
            MBresponseArray[MESSAGEIDPOS] = eNAK_WITH_DATA;
        }
    }
    else
    {
        mailboxVariables.length = ACKNACKLENGTH;        // No data, it will just be a 2 byte response
        if (TRUE == ackNack)
        {
            MBresponseArray[MESSAGEIDPOS] = eACK;       // Set correct message ID
        }
        else
        {
            MBresponseArray[MESSAGEIDPOS] = eNAK;
        }
    }

    // send channel 0 the simple ACK so he can ask for the results
    slaveAckVals.channelNum = CHANNELZERO;              // respond for channel 0
    slaveAckVals.SID = mailboxVariables.originalSID;    // Use the orignial SID
    slaveAckVals.data = NULL;                           // No payload data, set to NULL
    slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   // No payload data, set to 0
    slaveAckVals.ackNack = TRUE;                        // It is an ACK
    LOLSendACK(&slaveAckVals);                          // Use LOL to send it
}  /* MBCompleteCallback1() */

/*******************************************************************************
* \fn void passwordEnable()
* \brief Update the password if it is valid
* \details Call PasswordVerification with the received data and update the password if appropriate
* \param[in] payloadPtr - pointer to where the password to be checked is
* \param[in] messageID - better be 0x01
* \param[in] SID - SID
* \param[in] payloadLength - better be 0x0A
* \param[in] channelNumber - Channel the command came in on
*******************************************************************************/
void passwordEnable(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                    uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    //    eMM_Sec_Lev newSecurityLevel = PasswordVerification(&payloadPtr[PAYLOADLENGTHPOS]);
    eMM_Sec_Lev newSecurityLevel = NGT_PwVerify(&payloadPtr[PAYLOADLENGTHPOS]);
    LOLChangeSecurity(channelNumber, newSecurityLevel, DEFAULTSECURITYTIMEOUT);

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    if (newSecurityLevel != eMM_Sec_Lev__ALL)
    {
        // Respond with a simple ACK
        slaveAckVals.data = NULL;
        slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        slaveAckVals.ackNack = TRUE;
    }
    // else just quietly go away, not any more
    else
    {
        /* NAK it!! if password is not found */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKILLEGALVALUE;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        slaveAckVals.ackNack = FALSE;
    }  /* if else */
        
    LOLSendACK(&slaveAckVals);
}  /* passwordEnable() */

/**
  ******************************************************************************
  * @func   NGT_PwVerify()
  * @brief  password verification.  get password into little endian before compare.
  * @param  payloadPtr - pointer to payload buffer (password)
  * @gvar   unDataSwap64, SystemIB
  * @gfunc  EndianSwapRead()
  * @retval eMM_Sec_Lev
  ******************************************************************************
  */
eMM_Sec_Lev NGT_PwVerify(uint8_t *pInPassword)
{
    eMM_Sec_Lev retVal = eMM_Sec_Lev__ALL;
    
    EndianSwapRead( pInPassword, 8);
    if (unDataSwap64.uiVal == SystemIB.SysFlashIB.uUserPassword)
    {
        retVal = eMM_Sec_Lev__USER;
    }
    else
    {
        if (unDataSwap64.uiVal == SystemIB.SysFlashIB.uAdminPassword)
        {
            retVal = eMM_Sec_Lev__USER_ADMIN;
        }  /* uAdminPassword */
        else
        {
            if (unDataSwap64.uiVal == SystemIB.SysFlashIB.uServicePassword)
            {
                retVal = eMM_Sec_Lev__SERVICE;
            }  /* uServicePassword */
            else
            {
                if (unDataSwap64.uiVal == SystemIB.SysFlashIB.uMETCOPassword)
                {
                    retVal = eMM_Sec_Lev__MANUFACTURING;
                }  /* uServicePassword */
            }   /* else uServicePassword */
        }   /* else uAdminPassword */
    }  /* else uUserPassword */
    return(retVal);
}  /* NGT_PwVerify() */


/******************************************************************************/
/**
* \fn eMM_Sec_Lev PasswordVerification(uint8_t * inputPassword)
* \brief Example code for Password Verification
* \details Will call LOLCompare function to securely validate a password
* \param[in] inputPassword - password to be verified
* \return eMM_Sec_Lev Will return the new level of software based on the input password
*******************************************************************************/
eMM_Sec_Lev PasswordVerification(uint8_t * inputPassword)
{
    eMM_Sec_Lev retVal = eMM_Sec_Lev__ALL;

    // Check the lowest level of security first and work our way up.
    if (LOLCompare(inputPassword, (uint8_t *)&SystemIB.SysFlashIB.uUserPassword, PASSWORDLENGTH))
    {   // was userPassword
        // Set to User
        retVal = eMM_Sec_Lev__USER;
    }
    else if (LOLCompare(inputPassword, (uint8_t *)&SystemIB.SysFlashIB.uAdminPassword, PASSWORDLENGTH))
    {   // was adminPassword
        // Set to Admin
        retVal = eMM_Sec_Lev__USER_ADMIN;
    }
    else if (LOLCompare(inputPassword, (uint8_t *)&SystemIB.SysFlashIB.uServicePassword, PASSWORDLENGTH))
    {   // was servicePassword
        // Set to Service
        retVal = eMM_Sec_Lev__SERVICE;
    }
    else if (LOLCompare(inputPassword, (uint8_t *)&SystemIB.SysFlashIB.uMETCOPassword, PASSWORDLENGTH))
    {   // was METCOPassword
        // Set to MANUFACTURING
        retVal = eMM_Sec_Lev__MANUFACTURING;
    }
    return(retVal);
}  /* PasswordVerification() */

/*******************************************************************************
* \fn setSecLevel()
* \brief changing the password for different security levels
* \param[in] payloadPtr - pointer to payload buffer that has address of which security level to use
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void setSecLevel(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                 uint8_t payloadLength, uint8_t channelNumber)
{
    /* Prepare the ACK/NACK value! */
    uint8_t nakPayload[LOCAL_RETPL_LEN];  /* [2] */
    
    nakPayload[0] = NAKMAXMESSAGE;
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.data = NULL;
    slaveAckVals.payloadLength = 0;
    slaveAckVals.ackNack = TRUE;    /* Assume ACK */

    /* Dig the address out of the payload */
    uint16_t address = (payloadPtr[PAYLOADADDRPOS0] << 8) | payloadPtr[PAYLOADADDRPOS1];
    uint64_t newPassword = 0;

    /* Is this a read or a write of the password? */
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {
        if (CHANGEPASSWORDLENGTH == payloadLength)
        {
            // Compute new password
//            newPassword = OL_Bytes2U64(&payloadPtr[2]);  /* convert to little endian */
            EndianSwapRead( &payloadPtr[2], 8);
            newPassword = unDataSwap64.uiVal;
            /* Check to make sure the password is not a duplicate */
//            if (newPassword != userPassword && newPassword != adminPassword && newPassword != servicePassword && newPassword != METCOPassword)
//if ((newPassword != userPassword) && (newPassword != adminPassword)
//                && (newPassword != servicePassword) && (newPassword != METCOPassword))
            if ((newPassword != SystemIB.SysFlashIB.uUserPassword)
                && (newPassword != SystemIB.SysFlashIB.uAdminPassword )
                && (newPassword != SystemIB.SysFlashIB.uServicePassword)
                && (newPassword != SystemIB.SysFlashIB.uMETCOPassword ))
            {
                /* Write the new password */
                switch (address)
                {
                    case eUSER_PASSWORD:
                        SystemIB.SysFlashIB.uUserPassword = newPassword;/* was userPassword */
                        SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE;     /* set flag, write to flash */
                    break;  /* eUSER_PASSWORD */
                    
                    case eADMIN_PASSWORD:
                        SystemIB.SysFlashIB.uAdminPassword = newPassword;       /* was adminPassword */
                        SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE;     /* set flag, write to flash */
                    break;  /* eADMIN_PASSWORD */
                    
                    case eSERVICE_PASSWORD:
                        SystemIB.SysFlashIB.uServicePassword = newPassword;     /* was servicePassword */
                        SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE;     /* set flag, write to flash */
                    break;  /* eSERVICE_PASSWORD */
                    
                    case eMETCO_PASSWORD:
                        SystemIB.SysFlashIB.uMETCOPassword  = newPassword;      /* was METCOPassword */
                        SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE;     /* set flag, write to flash */
                    break;  /* eMETCO_PASSWORD*/
                    
                    default:  /* Nothing we know about, NAK it!! */
                        slaveAckVals.ackNack = FALSE;
                        nakPayload[1] = NAKUNKNOWNSPECIFIER;
                    break;
                }  /* switch */
            }  /* if newPassword */
            else
            {
                /* NAK it!  No duplicate passwords allowed */
                slaveAckVals.ackNack = FALSE;
                nakPayload[1] = NAKILLEGALVALUE;
            }  /* if else newPassword */
        }  /* if payload length */
        else
        {
            /* NAK it!  Packet is not formated correctly */
            slaveAckVals.ackNack = FALSE;
            nakPayload[1] = NAKMALFORMEDPACKET;
        }  /* if else payload length */
    }  /* if SID */
    else
    {
        slaveAckVals.payloadLength = PASSWORDLENGTH;
        // Read the old password
        switch (address)
        {
            case eUSER_PASSWORD:
                EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.uUserPassword);  /* convert to big endian */
                slaveAckVals.data = (uint8_t *)&uiESwapData64;      /* was userPassword */
            break;  /* eUSER_PASSWORD */
            
            case eADMIN_PASSWORD:
                EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.uAdminPassword);  /* convert to big endian */
                slaveAckVals.data = (uint8_t *)&uiESwapData64;      /* was userPassword */
            break;  /* eADMIN_PASSWORD */
            
            case eSERVICE_PASSWORD:
                EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.uServicePassword);  /* convert to big endian */
                slaveAckVals.data = (uint8_t *)&uiESwapData64;      /* was userPassword */
            break;  /* eSERVICE_PASSWORD */
            
            case eMETCO_PASSWORD:
                EndianSwap64( &uiESwapData64, SystemIB.SysFlashIB.uMETCOPassword);  /* convert to big endian */
                slaveAckVals.data = (uint8_t *)&uiESwapData64;      /* was userPassword */
            break;  /* eMETCO_PASSWORD */
            
            default:
                // Nothing we know about, NAK it!!
                nakPayload[1] = NAKUNKNOWNSPECIFIER;
                slaveAckVals.ackNack = FALSE;
            break;
        }  /* switch */
    }  /* if else SID */

    if (FALSE == slaveAckVals.ackNack)
    {  /* Get the NAK set up! */
        slaveAckVals.data = nakPayload;
        slaveAckVals.payloadLength = 2;
    }

    LOLSendACK(&slaveAckVals);    /* Send the ACK/NAK */
}  /* setSecLevel() */

/******************************************************************************/
/**
* \fn void readMemMapVersion()
* \brief Example code for reading the Memory Map Version.
* \details Note, write is not legal by definition.  It will be rejected before
*                this routine is called because of the eMM_Sec_Lev__NONE secrutiy setting
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void readMemMapVersion(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                       uint8_t payloadLength, uint8_t channelNumber)
{
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    EndianSwap16( &uiESwapData16, SystemIB.SysFlashIB.uiMMVer);  /* convert to big endian */
    slaveAckVals.data = (uint8_t *)&uiESwapData16;
    slaveAckVals.payloadLength = LOL_MMVER_PAYLOAD_LEN;  /* 2 */
    slaveAckVals.ackNack = TRUE;
    LOLSendACK(&slaveAckVals);
}  /* readMemMapVersion() */

/******************************************************************************/
/**
* \fn void RWExtMemOffset()
* \brief reading and writing the Extended Memory Map Offset
* \param[in] payloadPtr - pointer to payload buffer
* \param[in] messageID - better be 0x01, not needed
* \param[in] SID - used for the ACK response, no NAK will be sent
* \param[in] payloadLength - length of the payload buffer
* \param[in] channelNumber - Channel the command came in on
* \return None
*******************************************************************************/
void RWExtMemOffset(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                    uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN]; /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    /* check for read/write */
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {
        /* write, check for payload length */
        if ((LOL_XMMOS_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)  /* 4 */
        {
            /* payload length is correct, write new value */
            EndianSwapRead16( &payloadPtr[2]);
            extendedMemoryOffset = unDataSwap16.uiVal;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }
        else
        {
            /* NAK it!! if payload length is not correct */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_XMMOS_PAYLOAD_LEN;  /* 2 */
            slaveAckVals.ackNack = FALSE;
        }  /* if else payload Length */
    }  /* if SID */
    else
    {
        // Read value
        EndianSwap16( &uiESwapData16, extendedMemoryOffset);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData16;
        slaveAckVals.payloadLength = LOL_XMMOS_PAYLOAD_LEN;  /* 2 */
    }  /* if else SID */

    LOLSendACK(&slaveAckVals);
}  /* RWExtMemOffset() */

/**
  ******************************************************************************
  * @func   fLMMPBID()
  * @brief  Write local memory - manufacturing test, buzzer
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMMPBID(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                      uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    uint8_t *pNFC;
    uint8_t uUval, uHval;
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write MPBID, prep ptr to update internal value */
        if ((SYS_LOL_MPBID_SIZE + LOL_LMADDR_SIZE) == payloadLength)  /* 7 */
        {
            for (int16_t iCnt = 0; iCnt < payloadLength - 2; iCnt++)
            {
                // If we are writing, move the data over!
                SystemIB.SysFlashIB.aMPBID[iCnt] = payloadPtr[iCnt + 2];  /* deviceMPBID[iCnt] */
            }
            add_MPBID_to_advertising_data(SystemIB.SysFlashIB.aMPBID);

            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */

            /* update MBPID in NFC field, split high & low nibble into an ascii bytes (2)  */
            pNFC = &iNFCRecord0[NFC_COS_MPBID]; /* ptr to destination */  /* #$%^ */
            for (int16_t iCnt = 0; iCnt < payloadLength - 2; iCnt++)
            {
                uUval = payloadPtr[iCnt + 2];   /* read MPBID */
                uUval = uUval >> 4;             /* high nibble only */
                if (uUval > 9)                  /* is between 'a' & 'f' */
                {
                    uHval = (uUval % 10) + 'a'; /* map to 'a' thru 'f' */
                }
                else
                {
                    uHval = uUval + '0';        /* map to '0' thru '9' */
                }
                *pNFC++ = uHval;                /* save high nibble to destination */
                
                uUval = payloadPtr[iCnt + 2];   /* read MPBID */
                uUval &= 0x0f;                  /* low nibble only */
                if (uUval > 9)                  /* is between 'a' & 'f' */
                {
                    uHval = (uUval % 10) + 'a'; /* map to 'a' thru 'f' */
                }
                else
                {
                    uHval = uUval + '0';        /* map to '0' thru '9' */
                }
                *pNFC++ = uHval;                /* save low nibble to destination */
            }  /* for */
//            SystemCB.NFCCB.uAction = NFCCB_ACT_WAITSYSF;        /* write new MPBID to NFC memory, wait for system finish writing to flash */
        }
        else
        {
            /* incorrect payload, NAK it!! */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
            slaveAckVals.ackNack = FALSE;
        }  /* if else payloadLength */
    }  /* if SID */
    else
    {   
        /* read MPBID, prep ptr to send internal value*/
        slaveAckVals.data = (uint8_t *)&SystemIB.SysFlashIB.aMPBID;  /* deviceMPBID */
        slaveAckVals.payloadLength = SYS_LOL_MPBID_SIZE;  /* 5 */
    }  /* else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMMPBID() */


/**
  ******************************************************************************
  * @func   fLMFwVersion()
  * @brief  Write local memory - manufacturing test, buzzer
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB, uiESwapData32
  * @gfunc  EndianSwap32(), LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMFwVersion(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                   uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */
    
    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    /* check for read/write, if write, response nack, else response ack with data */
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {
        /* write firmware version, return nack */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        slaveAckVals.ackNack = FALSE;
    }  /* if */
    else
    {
        EndianSwap32( &uiESwapData32, SystemIB.SysFlashIB.ulDeviceFwVer);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData32;
        slaveAckVals.payloadLength = LOL_FWVER_PAYLOAD_LEN;  /* 4 */
    }  /* else */
    LOLSendACK(&slaveAckVals);
}  /* fLMFwVersion() */

/**
  ******************************************************************************
  * @func   fLMDateOfMfg()
  * @brief  Read/Write local memory - date of manufacturing
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB, unDataSwap32
  * @gfunc  EndianSwapRead32(), EndianSwap32(), LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateOfMfg(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write BOD, prep ptr to update internal value */
        if ((LOL_BOD_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)  /* 6 */
        {
            EndianSwapRead32( &payloadPtr[2], 4);
            SystemIB.SysFlashIB.ulBornOnDate = (uint32_t)unDataSwap32.uiVal;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */
            AdcCB.uAction = ADCCB_ACT_CONVERT;  /* get born on coin cell voltage, set adc convert action */
        }
        else
        {
            /* wrong payload, NAK it!! */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read SWPN, prep ptr to send internal value*/
        EndianSwap32( &uiESwapData32, SystemIB.SysFlashIB.ulBornOnDate);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData32;
        slaveAckVals.payloadLength = LOL_BOD_PAYLOAD_LEN;  /* 4 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateOfMfg() */


/**
  ******************************************************************************
  * @func   fLMDateOfService()
  * @brief  Read/Write local memory - date of operation/service, activation
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB, unDataSwap32, uiESwapData32
  * @gfunc  EndianSwapRead32(), EndianSwap32(), LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateOfService(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write MPBID, prep ptr to update internal value */
        if ((LOL_DT_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)  /* 6 */
        {
            EndianSwapRead32( &payloadPtr[2], 4);
            SystemIB.SysFlashIB.ulDateFirstServ = (uint32_t)unDataSwap32.uiVal;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;   /* 0 */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE; /* set flag, write to flash */
        }
        else
        {
            /* wrong payload, NAK it!! */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if SID */
    else
    {   
        /* read DFO, prep ptr to send internal value*/
        EndianSwap32( &uiESwapData32, SystemIB.SysFlashIB.ulDateFirstServ);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData32;
        slaveAckVals.payloadLength = LOL_DT_PAYLOAD_LEN;  /* 4 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateOfService() */

//uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

/**
  ******************************************************************************
  * @func   fLMDateLastUSed()
  * @brief  read local memory - data last used or tool last used
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB, uiESwapData32
  * @gfunc  EndianSwap32(), LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateLastUSed(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* can't write, nack */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.ackNack = FALSE;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if write */
    else
    {   
        /* read date last used (tool last used, prep ptr to send internal value*/
        EndianSwap32( &uiESwapData32, SystemIB.SysRAMIB.ulDateLastUsed);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData32;
        slaveAckVals.payloadLength = LOL_DT_PAYLOAD_LEN;  /* 4 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateLastUSed() */


/**
  ******************************************************************************
  * @func   fLMDateTimeRef()
  * @brief  read/write local memory - date time reference
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB, unDataSwap32, uiESwapData32
  * @gfunc  EndianSwapRead32(), EndianSwap32(), LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMDateTimeRef(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* write date time reference, prep ptr to update internal value */
        if ((LOL_DT_PAYLOAD_LEN + LOL_LMADDR_SIZE) == payloadLength)  /* 6 */
        {
            EndianSwapRead32( &payloadPtr[2], 4);
            SystemIB.SysRAMIB.ulDateTimeRef = (uint32_t)unDataSwap32.uiVal;
            slaveAckVals.data = NULL;
            slaveAckVals.payloadLength = LOL_ACK_PAYLOAD_LEN;  /* 0 */
        }
        else
        {
            /* wrong payload, NAK it!! */
            returnPayload[0] = NAKMAXMESSAGE;
            returnPayload[1] = NAKMALFORMEDPACKET;
            slaveAckVals.ackNack = FALSE;
            slaveAckVals.data = returnPayload;
            slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
        }  /* if else payload */
    }  /* if write */
    else
    {   
        /* read date time reference */
        EndianSwap32( &uiESwapData32, SystemIB.SysRAMIB.ulDateTimeRef);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData32;
        slaveAckVals.payloadLength = LOL_DT_PAYLOAD_LEN;  /* 4 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMDateTimeRef() */


/**
  ******************************************************************************
  * @func   fLMPlatformId()
  * @brief  read local memory - platform identifier
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB, uiESwapData16
  * @gfunc  EndianSwap16(), LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMPlatformId(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;
    
    if (LOL_SID_WR == (SID & READWRITEBIT))
    {   
        /* if write, thennack */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.ackNack = FALSE;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if write */
    else
    {   
        /* read platform id, prep ptr to send internal value */
        EndianSwap16( &uiESwapData16, SystemIB.SysRAMIB.uPlatformId);  /* convert to big endian */
        slaveAckVals.data = (uint8_t *)&uiESwapData16;  /* outgoing variable address */
        slaveAckVals.payloadLength = LOL_PFID_PAYLOAD_LEN;  /* 2 */
    }  /* if else SID */
    LOLSendACK(&slaveAckVals);
}  /* fLMPlatformId() */


/**
  ******************************************************************************
  * @func   fLMBLEMACAddr()
  * @brief  read local memory - BLE MAC address
  * @param  payloadPtr - pointer to payload buffer
  * @param  messageID - better be 0x01, not needed
  * @param  SID - used for the ACK response, no NAK will be sent
  * @param  payloadLength - length of the payload buffer
  * @param  channelNumber - Channel the command came in on
  * @gvar   slaveAckVals, SystemIB
  * @gfunc  LOLSendACK()
  * @retval None
  ******************************************************************************
  */
void fLMBLEMACAddr(uint8_t * payloadPtr, uint8_t messageID, uint8_t SID,
                     uint8_t payloadLength, uint8_t channelNumber)
{
    uint8_t returnPayload[LOCAL_RETPL_LEN];  /* [2] */

    slaveAckVals.channelNum = channelNumber;
    slaveAckVals.SID = SID;
    slaveAckVals.ackNack = TRUE;

    if (LOL_SID_RD == (SID & READWRITEBIT))
    {   
        /* read BLE MAC address*/
        slaveAckVals.data = get_ble_MAC_data_pointer();
//        slaveAckVals.data = (uint8_t *)&SystemIB.SysRAMIB.aBLEMACAddr;
        slaveAckVals.payloadLength = LOL_BLEMAC_PAYLOAD_LEN;  /* 6 */
    }
    else
    {
        /* write, NAK it!! */
        returnPayload[0] = NAKMAXMESSAGE;
        returnPayload[1] = NAKMALFORMEDPACKET;
        slaveAckVals.ackNack = FALSE;
        slaveAckVals.data = returnPayload;
        slaveAckVals.payloadLength = LOL_NACK_PAYLOAD_LEN;  /* 2 */
    }  /* if else */
    LOLSendACK(&slaveAckVals);
}  /* fLMBLEMACAddr() */


