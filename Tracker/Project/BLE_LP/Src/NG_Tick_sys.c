/**
  ******************************************************************************
  * @file    NG_Tick_Sys.c
  * @author  Thomas W Liu
  * @brief   Source file of system information block.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

//#define EN_RD_FLASH_TEST /* read and print accelerometer parameters */

/* Includes ------------------------------------------------------------------*/
#include "system_BlueNRG_LP.h"
#include "bluenrg_lp.h"
#include "NG_Tick_FlashUtil.h"  /* must comes before sys.h */
#include "NG_Tick_NFC.h"        /* must comes before sys.h */
#include "NG_Tick_Acc.h"        /* must comes before sys.h */
#include "NG_Tick_sys.h"
#include "NG_Tick_Adc.h"
#include "LOLConfig.h"
#include "NG_Tick_LOL_CmdVar.h"
#include "NG_Tick_LOL_Func.h"

/* defines -------------------------------------------------------------------*/
#define FLASHSYS_INITKEY_BITS   0x0000ffff      /* only lsb 16 bits matters */
#define FLASHSYS_INITKEY_NEW    0x0000ffff      /* new, never used before */
#define FLASHSYS_INITKEY_INIT   0x00000000      /* flash initialized, written */

/* constant declaration ------------------------------------------------------*/
const uint8_t aUPN_UnDefined[] = { DEF_A_UNDEFINED };
const uint8_t caMPBID[SYS_LOL_MPBID_SIZE] = { 0x01, 0x00, 0xFF, 0xFF, 0xFF };
const uint8_t caUnitSN[] = {
    DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN,
    DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN,
    DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN,
    DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN, DEF_A_UNITSN
};

/* variable declaration ------------------------------------------------------*/
//uint32_t  uSysInfoFlashByteLen = (uint32_t)&SystemIB.aBLEMACAddr - (uint32_t)&SystemIB.uSysInitKey + 4;
//uint32_t  uSysInfoFlashByteLen;
NGT_SYSTEMINFO_TYPE SystemIB;   /* system information block */
NGT_SYSTEMCTRL_TYPE SystemCB;   /* system control block */
uint8_t SystemIBRdbk[sizeof(NGT_SYSFLASHINFO_TYPE)];
NGT_SYSFLASHINFO_TYPE *pSystemIBRdbk;

/* function prototype --------------------------------------------------------*/
void SystemVar_Init_All(void);
void NGT_SystemCB_init(void);
void NGT_FlashCB_init(void);
void NGT_NFCCB_init(void);
void NGT_AccCB_init(void);
void NGT_AdcCB_init(void);
void NGT_SystemIB_init(void);

void NGT_SystemIB1_init();  /* testing only */
void NGT_SystemIB2_init();  /* testing only */


/**
  ******************************************************************************
  * @func   SystemVar_Init_All()
  * @brief  system variable initialization
  * @param  none
  * @gvar   SystemIB
  * @gfunc  NGT_SystemCB_init(), NGT_SystemIB_init()
  * @retval None
  ******************************************************************************
  */
void SystemVar_Init_All()
{
    NGT_SystemIB_init();  /* must init system info block before control block */
    NGT_SystemCB_init();
}  /* SystemVar_Init_All() */


/**
  ******************************************************************************
  * @func   NGT_SystemCB_init()
  * @brief  NGT system control block variable initialization
  * @param  none
  * @gvar   SystemIB, SystemCB
  * @gfunc  memcpy()
  * @retval None
  ******************************************************************************
  */
void NGT_SystemCB_init()
{
    uint32_t  uFlashSysInitKey;

    /***************************************************************************
    *  ##$$ System flash info block is declared to 16 bte boundry ##$$
    *  The burst write is 16 bytes (or 4 32-bit long word) flash driver.
    ***************************************************************************/
    SystemCB.uSysFlashIBByteLen = sizeof(NGT_SYSFLASHINFO_TYPE);
    SystemCB.uSysFlashIBByteLen >>= 4;
    SystemCB.uSysFlashIBByteLen <<= 4;
    
    /***************************************************************************
    * flash memory control block init
    ***************************************************************************/
    NGT_FlashCB_init();  /* init flash control block */
    
    /***************************************************************************
    * NFC control block init
    ***************************************************************************/
    NGT_NFCCB_init();  /* init NFC control block */
    
    /***************************************************************************
    * accelerometer control block init.
    ***************************************************************************/
    NGT_AccCB_init();
    
    /***************************************************************************
    * adc control block init.
    ***************************************************************************/
    NGT_AdcCB_init();
    
    /***************************************************************************
    * read first word in flash memory.
    *   If value is:  0xffff = flash is first usage.
    *                 0x0000 = flash is initialized, must erase before updating
    ***************************************************************************/
    Flash_ReadNWord( &uFlashSysInitKey, FLASH_ADDR_SYSINFO, 1);
    switch (uFlashSysInitKey & FLASHSYS_INITKEY_BITS)
    {
        case FLASHSYS_INITKEY_INIT:
            SystemCB.FlashCB.uAction = FLASHCB_ACT_READ;
        break;
        
        default:
            SystemIB.SysFlashIB.uSysInitKey = FLASHSYS_INITKEY_INIT;   /* init system initialization key */
            SystemCB.FlashCB.uAction = FLASHCB_ACT_ERASE_WRITE;        /* erase then write */
//            SystemCB.FlashCB.uAction = FLASHCB_ACT_WRITE;        /* just write, no erase required */
            SystemCB.NFCCB.uAction = NFCCB_ACT_WAITSYSF;        /* write deafult to NFC memory, wait for system finish writing to flash */
        break;
    }  /* switch */
}  /* NGT_SystemCB_init() */


/**
  ******************************************************************************
  * @func   NGT_FlashCB_init()
  * @brief  NGT flash control block variable initialization
  * @param  None
  * @gvar   SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void NGT_FlashCB_init()
{
    SystemCB.FlashCB.uAction = FLASHCB_ACT_IDLE;        /* default, idle */
    SystemCB.FlashCB.uState = FLASHCB_ST_IDLE;          /* no action */
    SystemCB.FlashCB.uStatus = FALSE;                   /* default, FALSE */
    SystemCB.FlashCB.uErrCode = FLASHCB_ERR_NONE;       /* no error code */
    SystemCB.FlashCB.uwErrCnt = 0;                      /* error count = 0 */
}  /* NGT_FlashCB_init() */


/**
  ******************************************************************************
  * @func   NGT_NFCCB_init()
  * @brief  NGT NFC control block variable initialization
  * @param  None
  * @gvar   SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void NGT_NFCCB_init()
{
    SystemCB.NFCCB.uFound = FALSE;              /* default, not found */
    SystemCB.NFCCB.iMaxRetry = MAXRETRY_FINDNFC;/* init to max retry count */
    SystemCB.NFCCB.iRetryCnt = 0;               /* init retry counter */
    SystemCB.NFCCB.uAction = NFCCB_ACT_IDLE;    /* current action - none */
    SystemCB.NFCCB.uState = NFCCB_ST_IDLE;      /* state = idle */
    SystemCB.NFCCB.uStatus = FALSE;             /* status */
    SystemCB.NFCCB.uErrCode = NFCCB_ERR_NONE;   /* no error code */
    SystemCB.NFCCB.uwErrCnt = 0;                 /* init error counter */
}  /* NGT_NFCCB_init() */

/**
  ******************************************************************************
  * @func   NGT_AccCB_init()
  * @brief  NGT accelerometer control block variable initialization
  * @param  None
  * @gvar   SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void NGT_AccCB_init()
{
    SystemCB.AccCB.uFound = FALSE;              /* default, not found */
    SystemCB.AccCB.iMaxRetry = MAXRETRY_FINDACC;/* init to max retry count */
    SystemCB.AccCB.iRetryCnt = 0;               /* init retry counter */
    SystemCB.AccCB.uAction = ACCCB_ACT_IDLE;    /* current action - none */
    SystemCB.AccCB.uState = ACCCB_ST_IDLE;      /* state = idle */
    SystemCB.AccCB.uStatus = FALSE;             /* status */
    SystemCB.AccCB.uErrCode = ACCCB_ERR_NONE;   /* no error code */
    SystemCB.AccCB.uwErrCnt = 0;                /* init error counter */
}  /* NGT_AccCB_init() */

/**
  ******************************************************************************
  * @func   NGT_AdcCB_init()
  * @brief  NGT adc control block variable initialization
  * @param  None
  * @gvar   SystemCB
  * @gfunc  None
  * @retval None
  ******************************************************************************
  */
void NGT_AdcCB_init()
{
    /*initialize BattVoltage to default, 0x7F is default */
    AdcCB.uAction = ADCCB_ACT_IDLE;     /* init default action */
    AdcCB.uState = ADCCB_ST_IDLE;       /* init default state */
    AdcCB.pBattVoltage = &SystemIB.SysRAMIB.uCoinCellV;  /* init ptr */
    *AdcCB.pBattVoltage = ADCCB_ERR_DEF;        /* init default value */
}  /* NGT_AdcCB_init() */


/**
  ******************************************************************************
  * @func   NGT_SystemIB_init()
  * @brief  NGT system information block variable initialization
  * @param  none
  * @gvar   SystemIB
  * @gfunc  memcpy(), memset()
  * @retval None
  ******************************************************************************
  */
void NGT_SystemIB_init()
{
    SystemIB.SysFlashIB.uSysInitKey = FLASHSYS_INITKEY_INIT;  /* set system initialization key */
    SystemIB.SysFlashIB.uiMMVer = SYS_LOL_MMAPVER_VAL;     /* memory version */
    memcpy( SystemIB.SysFlashIB.aMPBID, caMPBID, SYS_LOL_MPBID_SIZE);      /* MPBID */
    SystemIB.SysFlashIB.ulDeviceSWPN = SYS_LOL_SWPN_VAL;   /* device software PN */
    SystemIB.SysFlashIB.ulDeviceFwVer = SYS_LOL_FWVER_VAL; /* device firmware version */
    SystemIB.SysFlashIB.ulBornOnDate = SYS_LOL_BOD_VAL;    /* born on date */
    SystemIB.SysFlashIB.ulDateFirstServ = SYS_LOL_DFOS_VAL;/* date first service */
    memset( SystemIB.SysFlashIB.sUniqueProdName, 0, SYS_LOL_MPBID_SIZE);   /* unique product name */
    memcpy( SystemIB.SysFlashIB.sUniqueProdName, aUPN_UnDefined, sizeof(aUPN_UnDefined) - 1);
    
    memset( SystemIB.SysFlashIB.aUnitSN, 0, SYS_LOL_USN_SIZE);     /* unit SN */
    memcpy( SystemIB.SysFlashIB.aUnitSN, caUnitSN, SYS_LOL_USN_SIZE);
    SystemIB.SysFlashIB.aAssetIdCode = DEF_A_ASSETCODE;
    SystemIB.SysFlashIB.aAssetIdCodePhone = DEF_A_ASSETCODE_PHONE;
    SystemIB.SysFlashIB.aOwnerClaimKey = DEF_A_OWNER_CLAIMKEY;    /* ownership claim key */
    SystemIB.SysFlashIB.uBLEShipMode = BLE_SHIP_MODE_DEF;  /* BLE ship mode */
    SystemIB.SysFlashIB.uBornOnCoinCellV = 0;              /* BLE ship mode - born on coin cell voltage (read adc) */
    SystemIB.SysFlashIB.uUserPassword = USERPW;            /* user password */
    SystemIB.SysFlashIB.uAdminPassword = ADMINPW;          /* admin password */
    SystemIB.SysFlashIB.uServicePassword = SERVICEPW;      /* service password */
    SystemIB.SysFlashIB.uMETCOPassword = METCOPW;          /* METCO password */
    SystemIB.SysFlashIB.uShipModeStatus = PRODUCTION_MODE; /* ship mode status */

    for (int8_t iCnt = 0; iCnt < SYS_LOL_BLANK_SIZE; iCnt++)
    {
        SystemIB.SysFlashIB.uBlank[iCnt] = 0xff;      /* preserved to structure aligned to 16 bytes */
    }  /* for */

    /* BLE MAC address default. BLE_Init will load real MAC */
    SystemIB.SysRAMIB.aBLEMACAddr[0] = SYS_LOL_BLEMAC_BYTE0;     
    SystemIB.SysRAMIB.aBLEMACAddr[1] = SYS_LOL_BLEMAC_BYTE1;
    SystemIB.SysRAMIB.aBLEMACAddr[2] = SYS_LOL_BLEMAC_BYTE2;
    SystemIB.SysRAMIB.aBLEMACAddr[3] = SYS_LOL_BLEMAC_BYTE3;
    SystemIB.SysRAMIB.aBLEMACAddr[4] = SYS_LOL_BLEMAC_BYTE4;
    SystemIB.SysRAMIB.aBLEMACAddr[5] = SYS_LOL_BLEMAC_BYTE5;

    //TODO:JRG - There is no BLE firmware version that is not linked to app version
    SystemIB.SysRAMIB.uBLEFwVer[0] = FIRMWARE_VERSION_REV;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[1] = FIRMWARE_VERSION_MAJOR;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[2] = FIRMWARE_VERSION_MINOR;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[3] = FIRMWARE_VERSION_SUB;      /* BLE firmware version */

    SystemIB.SysRAMIB.ulDateLastUsed = SYS_LOL_DLU_VAL;  /* date last used */
    SystemIB.SysRAMIB.ulDateTimeRef = SYS_LOL_DTREF;     /* date time reference */
    
    SystemIB.SysRAMIB.uPlatformId = SYS_LOL_PFID_NGT;       /* platform id = NGT */
    SystemIB.SysRAMIB.uUnitReset = DEF_A_UNIT_RST_NONE;          /* unit reset mode */
    SystemIB.SysRAMIB.uAttributeStream = DEF_A_ATT_STREAM_OFF;   /* streaming off */

    SystemIB.SysRAMIB.uMfgTst_BLESleepA = DEF_A_BLE_SLEEP_TM;    /* manufacturing test BLE sleep current test */
    SystemIB.SysRAMIB.uMfgTst_BuzzerTest = 0;            /* manufacturing test buzzer test*/
    SystemIB.SysRAMIB.uMfgTst_BLEReset = FALSE;          /* manufacturing test */
    SystemIB.SysRAMIB.uLastUsedTimeCValue = 0;           /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uLastUsedTimeC[0] = 0;             /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uLastUsedTimeC[1] = 0;             /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_ACT_INDX] = 0;     /* BLE generic command - buzzer, play/stop (write only) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_TONE_INDX] = 0;    /* BLE generic command - buzzer, tone id (write only) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_REPEAT_INDX] = 0;  /* BLE generic command - buzzer, repeat (write only) */
    SystemIB.SysRAMIB.uCoinCellV = 0;                    /* BLE generic command - coin cell voltage (read only) */

    for (int8_t iCnt = 0; iCnt < SYS_LOL_BBPARM_SIZE; iCnt++)
    {
        SystemIB.SysRAMIB.uBLEBeaconParm[iCnt] = 0;      /* BLE beacon parameter command - development */
    }
    SystemIB.SysRAMIB.uBLEBeaconID = 0;                  /* BLE beacon parameter command - beacon id */
    SystemIB.SysRAMIB.uBLEBeaconInterval = 0;            /* BLE beacon parameter command - beacon interval */
    SystemIB.SysRAMIB.uBLEDisconnect = FALSE;            /* BLE disconnect */

    SystemIB.SysRAMIB.uBLESyncToolPhone = BLE_BLUENRG_LP;   /* BLE blue nrg lp */
    
    /* NFC advertising default data init */
    SystemIB.SysRAMIB.uBLEAdv[0] = DEFAULT_COIN_CELL_ADVERTISING;         /* default coin cell voltage */
    SystemIB.SysRAMIB.uBLEAdv[1] = DEFAULT_TOOL_LAST_USED | (FIRMWARE_VERSION_ID_ADVERTISING >> 1); /* default Last Used and FW Version */
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN7] = (DEFAULT_HISTOGRAM >> 1) | (FIRMWARE_VERSION_ID_ADVERTISING << 7); /*Default FW Version and Histogram Bin 7 Default */
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN6] = DEFAULT_HISTOGRAM; /*Default  Histogram Bin 6  */
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN5] = DEFAULT_HISTOGRAM; /*Default  Histogram Bin 5 */
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN4] = DEFAULT_HISTOGRAM; /*Default  Histogram Bin 4 */
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN3] = DEFAULT_HISTOGRAM; /*Default  Histogram Bin 3*/
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN2] = DEFAULT_HISTOGRAM; /*Default  Histogram Bin 2*/
    SystemIB.SysRAMIB.uBLEAdv[NFC_BLE_ADV_INDEX_HIST_BIN1] = DEFAULT_HISTOGRAM; /*Default  Histogram Bin 1*/
    
    SystemIB.SysRAMIB.uNFCBuzzerEvents = 0;      /* buzzer events */
    SystemIB.SysRAMIB.uNFCEvents = 0;            /* NFC events */
    SystemIB.SysRAMIB.uNFCResetEvents = 0;       /* reset events */

    SystemIB.SysRAMIB.uI2CToken = I2C_TOKEN_DEF; /* reset events */
//    SystemIB.SysRAMIB.uPORStatus = 0;            /* power on reset status */ /* updated in main */
}  /* NGT_SystemIB_init() */


/**
  ******************************************************************************
  * @func   NGT_SystemIB1_init()
  * @brief  NGT system information block 1 variable initialization
  * @param  none
  * @gvar   SystemIB
  * @gfunc  memcpy(), memset()
  * @retval None
  ******************************************************************************
  */
void NGT_SystemIB1_init()
{
#if 0
    SystemIB.SysFlashIB.uSysInitKey = 0;                   /* set system initialization key */
    SystemIB.SysFlashIB.uiMMVer = SYS_LOL_MMAPVER_VAL+1;     /* memory version */
    memset( SystemIB.SysFlashIB.aMPBID, 0x54, SYS_LOL_MPBID_SIZE);      /* MPBID, "T" */
    SystemIB.SysFlashIB.ulDeviceSWPN = SYS_LOL_SWPN_VAL+1;   /* device software PN */
    SystemIB.SysFlashIB.ulDeviceFwVer = SYS_LOL_FWVER_VAL+1; /* device firmware version */
    SystemIB.SysFlashIB.ulBornOnDate = SYS_LOL_BOD_VAL+1;    /* born on date */
    SystemIB.SysFlashIB.ulDateFirstServ = SYS_LOL_DFOS_VAL+1;/* date first service */
    memset( SystemIB.SysFlashIB.sUniqueProdName, 0x4d, SYS_LOL_MPBID_SIZE);   /* unique product name "M" */
    
    memset( SystemIB.SysFlashIB.aUnitSN, 0x53, SYS_LOL_USN_SIZE);     /* unit SN, "S" */
    SystemIB.SysFlashIB.aAssetIdCodePhone = DEF_A_ASSETCODE | 2;
    SystemIB.SysFlashIB.aAssetIdCodePhone = DEF_A_ASSETCODE_PHONE | 2;   /* asset code, "P" */
    SystemIB.SysFlashIB.aOwnerClaimKey = DEF_A_OWNER_CLAIMKEY + 1;      /* ownership claim key */
    SystemIB.SysFlashIB.uBLEShipMode = BLE_SHIP_MODE_DEF+1;  /* BLE ship mode */
    SystemIB.SysFlashIB.uBornOnCoinCellV = 0x5f;             /* BLE ship mode - born on coin cell voltage (read adc) */
    SystemIB.SysFlashIB.uUserPassword = USERPW+1;            /* user password */
    SystemIB.SysFlashIB.uAdminPassword = ADMINPW+1;          /* admin password */
    SystemIB.SysFlashIB.uServicePassword = SERVICEPW+1;      /* service password */
    SystemIB.SysFlashIB.uMETCOPassword = METCOPW+1;          /* METCO password */

    for (int8_t iCnt = 0; iCnt < SYS_LOL_BLANK_SIZE; iCnt++)
    {
        SystemIB.SysFlashIB.uBlank[iCnt] = 0xff;      /* preserved to structure aligned to 16 bytes */
    }  /* for */

    /* BLE MAC address default. BLE_Init will load real MAC */
    SystemIB.SysRAMIB.aBLEMACAddr[0] = SYS_LOL_BLEMAC_BYTE0;     
    SystemIB.SysRAMIB.aBLEMACAddr[1] = SYS_LOL_BLEMAC_BYTE1+1;
    SystemIB.SysRAMIB.aBLEMACAddr[2] = SYS_LOL_BLEMAC_BYTE2+1;
    SystemIB.SysRAMIB.aBLEMACAddr[3] = SYS_LOL_BLEMAC_BYTE3+1;
    SystemIB.SysRAMIB.aBLEMACAddr[4] = SYS_LOL_BLEMAC_BYTE4+1;
    SystemIB.SysRAMIB.aBLEMACAddr[5] = SYS_LOL_BLEMAC_BYTE5+1;

    //TODO:JRG - There is no BLE firmware version that is not linked to app version
    SystemIB.SysRAMIB.uBLEFwVer[0] = FIRMWARE_VERSION_REV;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[1] = FIRMWARE_VERSION_MAJOR;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[2] = FIRMWARE_VERSION_MINOR;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[3] = FIRMWARE_VERSION_SUB;      /* BLE firmware version */

    SystemIB.SysRAMIB.ulDateLastUsed = SYS_LOL_DLU_VAL+1;  /* date last used */
    SystemIB.SysRAMIB.ulDateTimeRef = SYS_LOL_DTREF+1;     /* date time reference */
    SystemIB.SysRAMIB.uPlatformId = SYS_LOL_PFID_NGT + 1;  /* platform id = NGT */
    SystemIB.SysRAMIB.uUnitReset = DEF_A_UNIT_RST_NONE+1;  /* unit reset mode */
    SystemIB.SysRAMIB.uAttributeStream = DEF_A_ATT_STREAM_OFF+1;   /* streaming off */

    SystemIB.SysRAMIB.uMfgTst_BLESleepA = DEF_A_BLE_SLEEP_TM;    /* manufacturing test BLE sleep current test */
    SystemIB.SysRAMIB.uMfgTst_BuzzerTest = 0;            /* manufacturing test buzzer test*/
    SystemIB.SysRAMIB.uMfgTst_BLEReset = FALSE;          /* manufacturing test */
    SystemIB.SysRAMIB.uLastUsedTimeCValue = 0;           /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uLastUsedTimeC[0] = 0;             /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uLastUsedTimeC[1] = 0;             /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_ACT_INDX] = 0;     /* BLE generic command - buzzer, play/stop (write only) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_TONE_INDX] = 0;    /* BLE generic command - buzzer, tone id (write only) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_REPEAT_INDX] = 0;  /* BLE generic command - buzzer, repeat (write only) */
    SystemIB.SysRAMIB.uCoinCellV = 0;                    /* BLE generic command - coin cell voltage (read only) */

    for (int8_t iCnt = 0; iCnt < SYS_LOL_BBPARM_SIZE; iCnt++)
    {
        SystemIB.SysRAMIB.uBLEBeaconParm[iCnt] = 0;      /* BLE beacon parameter command - development */
    }
    SystemIB.SysRAMIB.uBLEBeaconID = 0;                  /* BLE beacon parameter command - beacon id */
    SystemIB.SysRAMIB.uBLEBeaconInterval = 0;            /* BLE beacon parameter command - beacon interval */
    SystemIB.SysRAMIB.uBLEDisconnect = FALSE;            /* BLE disconnect */

    SystemIB.SysRAMIB.uBLESyncToolPhone = BLE_BLUENRG_LP;   /* BLE blue nrg lp */
#endif
}  /* NGT_SystemIB1_init() */


/**
  ******************************************************************************
  * @func   NGT_SystemIB2_init()
  * @brief  NGT system information block 2 variable initialization
  * @param  none
  * @gvar   SystemIB
  * @gfunc  memcpy(), memset()
  * @retval None
  ******************************************************************************
  */
void NGT_SystemIB2_init()
{
#if 0
    SystemIB.SysFlashIB.uSysInitKey = 0;                   /* set system initialization key */
    SystemIB.SysFlashIB.uiMMVer = SYS_LOL_MMAPVER_VAL+2;   /* memory version */
    memset( SystemIB.SysFlashIB.aMPBID, 0x55, SYS_LOL_MPBID_SIZE);      /* MPBID, "U" */
    SystemIB.SysFlashIB.ulDeviceSWPN = SYS_LOL_SWPN_VAL+2;   /* device software PN */
    SystemIB.SysFlashIB.ulDeviceFwVer = SYS_LOL_FWVER_VAL+2; /* device firmware version */
    SystemIB.SysFlashIB.ulBornOnDate = SYS_LOL_BOD_VAL+2;    /* born on date */
    SystemIB.SysFlashIB.ulDateFirstServ = SYS_LOL_DFOS_VAL+2;/* date first service */
    memset( SystemIB.SysFlashIB.sUniqueProdName, 0x43, SYS_LOL_MPBID_SIZE);   /* unique product name "N" */
    
    memset( SystemIB.SysFlashIB.aUnitSN, 0x54, SYS_LOL_USN_SIZE);  /* unit SN, "T" */
    SystemIB.SysFlashIB.aAssetIdCode = DEF_A_ASSETCODE | 4;        /* asset code, "A" */
    SystemIB.SysFlashIB.aAssetIdCodePhone = DEF_A_ASSETCODE_PHONE | 4;   /* asset code, "P" */
    SystemIB.SysFlashIB.aOwnerClaimKey = DEF_A_OWNER_CLAIMKEY + 2; /* ownership claim key */
    
    SystemIB.SysFlashIB.uBLEShipMode = BLE_SHIP_MODE_DEF+2;  /* BLE ship mode */
    SystemIB.SysFlashIB.uBornOnCoinCellV = 0x60;             /* BLE ship mode - born on coin cell voltage (read adc) */
    SystemIB.SysFlashIB.uUserPassword = USERPW+2;            /* user password */
    SystemIB.SysFlashIB.uAdminPassword = ADMINPW+2;          /* admin password */
    SystemIB.SysFlashIB.uServicePassword = SERVICEPW+2;      /* service password */
    SystemIB.SysFlashIB.uMETCOPassword = METCOPW+2;          /* METCO password */

    for (int8_t iCnt = 0; iCnt < SYS_LOL_BLANK_SIZE; iCnt++)
    {
        SystemIB.SysFlashIB.uBlank[iCnt] = 0xff;      /* preserved to structure aligned to 16 bytes */
    }  /* for */

    /* BLE MAC address default. BLE_Init will load real MAC */
    SystemIB.SysRAMIB.aBLEMACAddr[0] = SYS_LOL_BLEMAC_BYTE0;     
    SystemIB.SysRAMIB.aBLEMACAddr[1] = SYS_LOL_BLEMAC_BYTE1+2;
    SystemIB.SysRAMIB.aBLEMACAddr[2] = SYS_LOL_BLEMAC_BYTE2+2;
    SystemIB.SysRAMIB.aBLEMACAddr[3] = SYS_LOL_BLEMAC_BYTE3+2;
    SystemIB.SysRAMIB.aBLEMACAddr[4] = SYS_LOL_BLEMAC_BYTE4+2;
    SystemIB.SysRAMIB.aBLEMACAddr[5] = SYS_LOL_BLEMAC_BYTE5+2;

    //TODO:JRG - There is no BLE firmware version that is not linked to app version
    SystemIB.SysRAMIB.uBLEFwVer[0] = FIRMWARE_VERSION_REV;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[1] = FIRMWARE_VERSION_MAJOR;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[2] = FIRMWARE_VERSION_MINOR;      /* BLE firmware version */
    SystemIB.SysRAMIB.uBLEFwVer[3] = FIRMWARE_VERSION_SUB;      /* BLE firmware version */

    SystemIB.SysRAMIB.ulDateLastUsed = SYS_LOL_DLU_VAL+2;  /* date last used */
    SystemIB.SysRAMIB.ulDateTimeRef = SYS_LOL_DTREF+2;     /* date time reference */
    SystemIB.SysRAMIB.uPlatformId = SYS_LOL_PFID_NGT+2;     /* platform id = NGT */
    SystemIB.SysRAMIB.uUnitReset = DEF_A_UNIT_RST_NONE+2;          /* unit reset mode */
    SystemIB.SysRAMIB.uAttributeStream = DEF_A_ATT_STREAM_OFF+2;   /* streaming off */

    SystemIB.SysRAMIB.uMfgTst_BLESleepA = DEF_A_BLE_SLEEP_TM;    /* manufacturing test BLE sleep current test */
    SystemIB.SysRAMIB.uMfgTst_BuzzerTest = 0;            /* manufacturing test buzzer test*/
    SystemIB.SysRAMIB.uMfgTst_BLEReset = FALSE;          /* manufacturing test */
    SystemIB.SysRAMIB.uLastUsedTimeCValue = 0;           /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uLastUsedTimeC[0] = 0;             /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uLastUsedTimeC[1] = 0;             /* BLE generic command - last used time constant (r/w) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_ACT_INDX] = 0;     /* BLE generic command - buzzer, play/stop (write only) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_TONE_INDX] = 0;    /* BLE generic command - buzzer, tone id (write only) */
    SystemIB.SysRAMIB.uBGCPlayBuzzer[SYS_LOL_BGC_BUZZ_REPEAT_INDX] = 0;  /* BLE generic command - buzzer, repeat (write only) */
    SystemIB.SysRAMIB.uCoinCellV = 0;                    /* BLE generic command - coin cell voltage (read only) */

    for (int8_t iCnt = 0; iCnt < SYS_LOL_BBPARM_SIZE; iCnt++)
    {
        SystemIB.SysRAMIB.uBLEBeaconParm[iCnt] = 0;      /* BLE beacon parameter command - development */
    }
    SystemIB.SysRAMIB.uBLEBeaconID = 0;                  /* BLE beacon parameter command - beacon id */
    SystemIB.SysRAMIB.uBLEBeaconInterval = 0;            /* BLE beacon parameter command - beacon interval */
    SystemIB.SysRAMIB.uBLEDisconnect = FALSE;            /* BLE disconnect */

    SystemIB.SysRAMIB.uBLESyncToolPhone = BLE_BLUENRG_LP;   /* BLE blue nrg lp */
#endif
}  /* NGT_SystemIB2_init() */


