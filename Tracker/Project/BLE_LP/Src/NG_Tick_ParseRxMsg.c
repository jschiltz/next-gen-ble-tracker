
#ifndef ENABLE_LOL

#include "bluenrg_lp_ll_tim.h"  /* !@#$ suppport sleep, delete later */
#include "NG_Tick_main.h"
#include "NG_Tick_pwm.h"        /* !@#$ ParseRxMsg(), may delete later */
#include "NG_Tick_ae.h"         /* !@#$ ParseRxMsg(), may delete later */
#include "NG_Tick_uart.h"
#include "NG_Tick_wakeup.h"     /* !@#$ ParseRxMsg(), suppport sleep, delete later */

extern char    ucDestStr[];

/* Duty cycles: D = T/P * 100%                                                */
/* where T is the pulse duration and P  the period of the PWM signal          */
static uint32_t aDutyCycle[] = {
    0,    /*  0% */
    10,   /* 10% */
    20,   /* 20% */
    30,   /* 30% */
    40,   /* 40% */
    50,   /* 50% */
    60,   /* 60% */
    70,   /* 70% */
    80,   /* 80% */
    90,   /* 90% */
    100   /* 100% */
};

static uint32_t aNoteFrequncy[] = {
    196,  /* G3 */
    220,  /* A3 */
    247,  /* B3 */
    262,  /* C4 */
    294,  /* D4 */
    330,  /* E4 */
    349,  /* F4 */
    392,  /* G4 */
    440,  /* A4 */
   7902   /* B4 */
//   494   /* B4 */
};

void ParseRxMsg()
{
    float fTmPrescaler;
    uint32_t iTmPrescaler;
    int8_t bytecnt;
    uint8_t *cptr;
    uint8_t cmd;
    uint8_t freq;
    int16_t hz;
    uint8_t uiParm;
    static uint8_t level = 5;
    int8_t icnt = 0;


//    cptr = (uint8_t *)ucDestStr;
    cptr = pBufferReadyForUser;
//    cptr = pBufferReadyForRx;
    bytecnt = (int)*cptr++ - 0x30;
    cmd = *cptr++;
    
    switch (cmd)
    {
        /* rx msg[] = 2an, where n = 0 - 9 */
        case 'a':  /* audio song */
            uiParm = *cptr++ - 0x30;      /* 1st parm = song id */
            if (3 == bytecnt)
            {
               uiParm = (uiParm * 10) + (*cptr - 0x30);
            }
            sAudioEngine.uiAE_NewSongId = uiParm;
            sAudioEngine.uiAE_NewCmd = TRUE;
        break;

        /* rx msg[] = 1d */
        case 'd':  /* disable TIM1 */
            Buzzer_TIM1_Disable();
        break;

        /* rx msg[] = 1e */
        case 'e':  /* enable TIM1 */
            Buzzer_TIM1_Enable();       /* enable TIM1 */
        break;

        /* rx msg[] = 2px, where x = 0 - 9 */
        case 'f':  /* pwm frequency */
            freq = *cptr++ - 0x30;
            freq %= 10;
            timxPeriod = __LL_TIM_CALC_ARR(TIM_PERIPHCLK, timxPrescaler, aNoteFrequncy[freq]); 
            LL_TIM_SetAutoReload(TIM1, timxPeriod);  /* Change PWM signal frequency */
            Configure_DutyCycle6(aDutyCycle[level]); /* Change PWM signal duty cycle */
            Buzzer_TIM1_Enable();       /* enable TIM1 */
        break;

        /* rx msg[] = 2px, where x = 100 - 20000 */
        /* prescaler = (((tmclk + ((hz * COUNTER_FREQ_F) / 2.0)) / (hz * COUNTER_FREQ_F)) - 1.0) */
        /* to compensate round off error:  add 0.5 before truncate to uint32 */
        /* auto reload register: defaulted to COUNTER_FREQ_I or COUNTER_FREQ_F - 1 */
        case 'h':  /* pwm frequency, hz */
            hz = *cptr++ - 0x30;
            for (icnt = 0; icnt < bytecnt - 2; icnt++)
            {
                hz = (hz * 10) + (*cptr++ - 0x30);
            }  /* for */
            fTmPrescaler = ((((float)TIM_PERIPHCLK + (((float)hz * COUNTER_FREQ_F) / 2.0)) / ((float)hz * COUNTER_FREQ_F)) - 1.0);  // to compensat division round off
            iTmPrescaler = (uint32_t)(fTmPrescaler + 0.5);      /* to compensat division round off */
            LL_TIM_SetPrescaler( TIM1, iTmPrescaler);  /* init PSC register */
            timxPeriod = COUNTER_AUTORELOAD;
            LL_TIM_SetAutoReload(TIM1, timxPeriod);  /* Change PWM signal frequency */
            Configure_DutyCycle6(50); /* Change PWM signal duty cycle */
        break;

        /* rx msg[] = 2px, where x = 0 - 9, : */
        case 'p':  /* pwm duty cycle */
            level = *cptr++ - 0x30;
            level %= 11;
            Configure_DutyCycle6(aDutyCycle[level]);  /* Change PWM signal duty cycle */
        break;

        /* rx msg[] = 1q */
        case 'q':  /* abort audio */
            sAudioEngine.uiAE_Abort = TRUE;      /* abort = true */
        break;

        /* rx msg[] = 2sx */
        case 's':  /* sleep mode select */
            uiParm = *cptr++ - 0x30;
            if (3 == bytecnt)
            {
               uiParm = (uiParm * 10) + (*cptr - 0x30);
            }
            uiSysSleepMode = uiParm;
        break;

        /* rx msg[] = 1u */
        case 'u':  /* write data to NFC memory */
            uiParm = *cptr++ - 0x30;
            switch (uiParm)
            {
                case 0:
//                    WipeNFCUserMemory();  /* wipe NFC memory */
                break;
              
                case 1:
//                    WaitTilEndOfTxTransfer();
//                    WriteNDEFURLDataToNFCMemory();  /* write NDEF data to NFC memory */
                break;
              
                case 2:
//                    WaitTilEndOfTxTransfer();
//                    WriteDataToNFCMemory();  /* write data to NFC memory */
                break;
              
                case 3:
//                    WriteAltDataToNFCMemory();  /* write data to NFC memory */
                break;
              
                case 4:
// %^&*
//                    WriteRec1DataToNFCMemory();  /* write rec1 data to NFC memory */
                break;
              
                case 5:
// %^&*
//                    WriteRec2DataToNFCMemory();  /* write rec2 data to NFC memory */
                break;
              
                case 6:
// %^&*
//                    WriteRec3DataToNFCMemory();  /* write rec3 data to NFC memory */
                break;
            }  /* switch() */
        break;

        /* rx msg[] = 2vx, where x = 0 - 3 */
        case 'v':  /* audio volume oerride */
            uiParm = *cptr - 0x30;
            uiParm %= 4;
            sAudioEngine.uiAE_SongVolumeOverride = (uint16_t)uiParm;      /* new volume - override */
        break;

        /* rx msg[] = 2wx, where x = 0 - 3 */
        case 'w':  /* audio volume oerride */
            uiParm = *cptr - 0x30;
            uiParm %= 4;
            switch (uiParm)
            {
                case 0:
                    LL_GPIO_ResetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
                    LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
                break;  /* volume = 0 */
                
                case 1:
                    LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
                    LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
                break;  /* volume = 0 */
                
                case 2:
                    LL_GPIO_ResetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
                    LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
                break;  /* volume = 0 */
                
                case 3:
                    LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
                    LL_GPIO_SetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
                break;  /* volume = 0 */
                
                default:
                    LL_GPIO_SetOutputPin(AUDIOAMP_EN2_GPIO_PORT, AUDIOAMP_EN2_PIN);
                    LL_GPIO_ResetOutputPin(AUDIOAMP_EN1_GPIO_PORT, AUDIOAMP_EN1_PIN);
                break;  /* default */
            }
        break;

        /* rx msg[] = 2wx, where x = 0 - 3 */
        case 'x':  /* audio volume oerride */
            uiParm = *cptr - 0x30;
            uiParm = (uint16_t)((float)(LL_TIM_GetAutoReload(TIM1) + 1) * ((float)uiParm / 10));
            LL_TIM_OC_SetCompareCH6(TIM1, uiParm);
        break;

    }  /* switch() */
}
#endif


