/**
  ******************************************************************************
  * @file    NG_Tick_NFC_i2c.c
  * @author  Thomas W Liu
  * @brief   Source file of NFC i2c driver.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "BLE_NFC.h"

/* variable declaration ------------------------------------------------------*/
I2C_HandleTypeDef hi2cx;

/* function prototype --------------------------------------------------------*/
int32_t MX_I2Cx_Init(void);
int32_t MX_I2Cx_Deinit(void);
int32_t BSP_I2C_IsReady(uint16_t, uint32_t);
int32_t BSP_I2C_WriteReg(uint16_t, uint16_t, uint8_t *, uint16_t);
int32_t BSP_I2C_ReadReg(uint16_t, uint16_t, uint8_t *, uint16_t);
int32_t BSP_I2C_WriteReg16(uint16_t, uint16_t, uint8_t *, uint16_t);
int32_t BSP_I2C_ReadReg16(uint16_t, uint16_t, uint8_t *, uint16_t);
int32_t BSP_I2C_Send(uint16_t, uint8_t *, uint16_t);
int32_t BSP_I2C_Recv(uint16_t, uint8_t *, uint16_t);
int32_t BSP_GetTick(void);

/* external variable declaration ---------------------------------------------*/

/* external function prototype -----------------------------------------------*/
  

/**
  ******************************************************************************
  * @func   MX_I2Cx_Init()
  * @brief  I2Cx Initialization Function for NFC st25dv
  * @param  None 
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Init(), HAL_I2CEx_ConfigAnalogFilter(), HAL_I2CEx_ConfigDigitalFilter()
  * @retval HAL_OK or HAL_ERROR 
  ******************************************************************************
  */
int32_t MX_I2Cx_Init(void)
{
    int32_t ret = HAL_OK;
/* (uint32_t)0x10320309 = I2C TIMING in Fast Mode                  */
/* (uint32_t)0x00200204 = I2C TIMING in Fast Mode plus             */

    /* 0x4100.0000, I2C1_BASE = APB1PERIPH_BASE + 0x0000U */
    /* APB1PERIPH_BASE = (PERIPH_BASE + 0x01000000U) */
    /* PERIPH_BASE = (0x40000000U) */
    hi2cx.Instance = I2Cx;
    hi2cx.Init.Timing = 0x10320309;
    hi2cx.Init.OwnAddress1 = 0;
    hi2cx.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;  /* =1 */
    hi2cx.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE; /* =0 */
    hi2cx.Init.OwnAddress2 = 0;
    hi2cx.Init.OwnAddress2Masks = I2C_OA2_NOMASK;         /* =0 */
    hi2cx.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE; /* =0 */
    hi2cx.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;     /* =0 */
    if (HAL_I2C_Init(&hi2cx) != HAL_OK)
    {
        return(HAL_ERROR);
    }
    
    /** Configure Analogue filter */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2cx, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        return(HAL_ERROR);
    }
    
    /** Configure Digital filter */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2cx, 0) != HAL_OK)
    {
        return(HAL_ERROR);
    }
    return(ret);
}       /* MX_I2Cx_Init() */


/**
  ******************************************************************************
  * @func   MX_I2Cx_Deinit()
  * @brief  I2Cx de-initialization Function for NFC st25dv
  * @param  None 
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_DeInit()
  * @retval HAL_OK or HAL_ERROR
  * @note   
  ******************************************************************************
  */
int32_t MX_I2Cx_Deinit(void)
{
    int32_t ret = HAL_OK;

    if (HAL_I2C_DeInit(&hi2cx) != HAL_OK)
    {
        ret = HAL_ERROR; 
    }
    return(ret);
}       /* MX_I2Cx_Deinit */


/* BUS IO driver over I2C Peripheral */
/*******************************************************************************
                            BUS OPERATIONS OVER I2C
*******************************************************************************/

/**
  ******************************************************************************
  * @func   void BSP_I2C_IsReady()
  * @brief  Check whether the I2C bus is ready.
  * @param  uint16_t DevAddr = device address
  * @param  uint16_t Trials = check trails number
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_IsDeviceReady()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t BSP_I2C_IsReady(uint16_t DevAddr, uint32_t Trials)
{
    int32_t ret = HAL_OK;

    if (HAL_I2C_IsDeviceReady(&hi2cx, DevAddr, Trials, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        ret = HAL_ERROR; /* BSP_ERROR_BUSY */
    }

    return(ret);
}  /* BSP_I2C_IsReady() */

/**
  ******************************************************************************
  * @func   void BSP_I2C_WriteReg()
  * @brief  Write a value in a register of the device through BUS.
  * @param  DevAddr Device address on Bus.
  * @param  Reg    The target register address to write
  * @param  pData  Pointer to data buffer to write
  * @param  Length Data Length
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Mem_Write(), HAL_I2C_GetError()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t BSP_I2C_WriteReg(uint16_t DevAddr, uint16_t Reg, uint8_t *pData, uint16_t Length)
{
    int32_t ret = HAL_ERROR;

    if (HAL_I2C_Mem_Write(&hi2cx, DevAddr,Reg, I2C_MEMADD_SIZE_8BIT,pData, Length, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        if (HAL_I2C_ERROR_AF == HAL_I2C_GetError(&hi2cx))
        {
            ret = HAL_ERROR; /* BSP_ERROR_BUS_ACKNOWLEDGE_FAILURE */
        }
        else
        {
            ret =  HAL_ERROR; /* BSP_ERROR_PERIPH_FAILURE */
        }
    }
    return(ret);
}  /* BSP_I2C_WriteReg() */

/**
  ******************************************************************************
  * @func   BSP_I2C_ReadReg()
  * @brief  Read a register of the device through BUS
  * @param  DevAddr Device address on Bus.
  * @param  Reg    The target register address to read
  * @param  pData  Pointer to data buffer to read
  * @param  Length Data Length
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Mem_Read(), HAL_I2C_GetError()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t  BSP_I2C_ReadReg(uint16_t DevAddr, uint16_t Reg, uint8_t *pData, uint16_t Length)
{
    int32_t ret = HAL_ERROR;

    if (HAL_I2C_Mem_Read(&hi2cx, DevAddr, Reg, I2C_MEMADD_SIZE_8BIT, pData, Length, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        if (HAL_I2C_ERROR_AF == HAL_I2C_GetError(&hi2cx))
        {
            ret = HAL_ERROR; /* BSP_ERROR_BUS_ACKNOWLEDGE_FAILURE */
        }
        else
        {
            ret = HAL_ERROR; /* BSP_ERROR_PERIPH_FAILURE */
        }
    }
    return(ret);
}  /* BSP_I2C_ReadReg() */

/**
  ******************************************************************************
  * @func   BSP_I2C_WriteReg16()
  * @brief  Write a value in a register of the device through BUS.
  * @param  DevAddr Device address on Bus.
  * @param  Reg    The target register address to write
  * @param  pData  Pointer to data buffer to write
  * @param  Length Data Length
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Mem_Write(), HAL_I2C_GetError()
  * @retval HAL_OK, -22 or -33
  ******************************************************************************
  */
int32_t BSP_I2C_WriteReg16(uint16_t DevAddr, uint16_t Reg, uint8_t *pData, uint16_t Length)
{
    int32_t ret = HAL_OK;

    if (HAL_I2C_Mem_Write(&hi2cx, DevAddr, Reg, I2C_MEMADD_SIZE_16BIT, pData, Length, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        if (HAL_I2C_ERROR_AF == HAL_I2C_GetError(&hi2cx))
        {
            ret = -33; /* BSP_ERROR_BUS_ACKNOWLEDGE_FAILURE */
        }
        else
        {
            ret = -22; /* BSP_ERROR_PERIPH_FAILURE */
        }
    }
    return(ret);
}  /* BSP_I2C_WriteReg16() */

/**
  ******************************************************************************
  * @func   BSP_I2C_ReadReg16()
  * @brief  Read registers through a bus (16 bits)
  * @param  DevAddr: Device address on BUS
  * @param  Reg: The target register address to read
  * @param  Length Data Length
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Mem_Read(), HAL_I2C_GetError()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t  BSP_I2C_ReadReg16(uint16_t DevAddr, uint16_t Reg, uint8_t *pData, uint16_t Length)
{
    int32_t ret = HAL_OK;

    if (HAL_I2C_Mem_Read(&hi2cx, DevAddr, Reg, I2C_MEMADD_SIZE_16BIT, pData, Length, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        if (HAL_I2C_GetError(&hi2cx) != HAL_I2C_ERROR_AF)
        {
            ret = HAL_ERROR; /* BSP_ERROR_BUS_ACKNOWLEDGE_FAILURE */
        }
        else
        {
            ret = HAL_ERROR; /* BSP_ERROR_PERIPH_FAILURE */
        }
    }
    return(ret);
}  /* BSP_I2C_ReadReg16() */

/**
  ******************************************************************************
  * @func   BSP_I2C_Send()
  * @brief  Send an amount width data through bus (Simplex)
  * @param  DevAddr: Device address on Bus.
  * @param  pData: Data pointer
  * @param  Length: Data length
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Master_Transmit(), HAL_I2C_GetError()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t BSP_I2C_Send(uint16_t DevAddr, uint8_t *pData, uint16_t Length)
{
    int32_t ret = HAL_ERROR;

    if (HAL_I2C_Master_Transmit(&hi2cx, DevAddr, pData, Length, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        if (HAL_I2C_GetError(&hi2cx) != HAL_I2C_ERROR_AF)
        {
            ret = HAL_ERROR; /* BSP_ERROR_BUS_ACKNOWLEDGE_FAILURE */
        }
        else
        {
            ret = HAL_ERROR; /*BSP_ERROR_PERIPH_FAILURE */
        }
    }  /* BSP_I2C_Send() */

    return(ret);
}  /* BSP_I2C_Send() */

/**
  ******************************************************************************
  * @func   BSP_I2C_Recv()
  * @brief  Receive an amount of data through a bus (Simplex)
  * @param  DevAddr: Device address on Bus.
  * @param  pData: Data pointer
  * @param  Length: Data length
  * @gvar   hi2cx
  * @gfunc  HAL_I2C_Master_Receive(), HAL_I2C_GetError()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t BSP_I2C_Recv(uint16_t DevAddr, uint8_t *pData, uint16_t Length)
{
    int32_t ret = HAL_ERROR;

    if (HAL_I2C_Master_Receive(&hi2cx, DevAddr, pData, Length, BUS_I2C_POLL_TIMEOUT) != HAL_OK)
    {
        if (HAL_I2C_GetError(&hi2cx) != HAL_I2C_ERROR_AF)
        {
            ret = HAL_ERROR; /* BSP_ERROR_BUS_ACKNOWLEDGE_FAILURE */
        }
        else
        {
            ret = HAL_ERROR; /* BSP_ERROR_PERIPH_FAILURE; */
        }
    }
    return(ret);
}  /* BSP_I2C_Recv() */

/* I2C1 init function */


#if (USE_HAL_I2C_REGISTER_CALLBACKS == 1U)
/**
  ******************************************************************************
  * @func   BSP_I2C_RegisterDefaultMspCallbacks()
  * @brief  Register Default BSP I2C1 Bus Msp Callbacks
  * @gvar   hi2cx, IsI2C1MspCbValid
  * @gfunc  __HAL_I2C_RESET_HANDLE_STATE(), HAL_I2C_RegisterCallback()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t BSP_I2C_RegisterDefaultMspCallbacks (void)
{
    __HAL_I2C_RESET_HANDLE_STATE(&hi2cx);

    /* Register MspInit Callback */
    if (HAL_I2C_RegisterCallback(&hi2cx, HAL_I2C_MSPINIT_CB_ID, I2C1_MspInit) != HAL_OK)
    {
        return(HAL_ERROR); /* BSP_ERROR_PERIPH_FAILURE */
    }

    /* Register MspDeInit Callback */
    if (HAL_I2C_RegisterCallback(&hi2cx, HAL_I2C_MSPDEINIT_CB_ID, I2C1_MspDeInit) != HAL_OK)
    {
        return(HAL_ERROR); /* BSP_ERROR_PERIPH_FAILURE; */
    }
    IsI2C1MspCbValid = 1;

    return(HAL_ERROR);
}  /* BSP_I2C_RegisterDefaultMspCallbacks() */

/**
  ******************************************************************************
  * @func   BSP_GetTick()
  * @brief  Return time tick in ms
  * @gvar   None
  * @gfunc  None
  * @retval uwTick
  ******************************************************************************
  */
int32_t BSP_GetTick(void)
{
    return(uwTick);
}  /* BSP_GetTick() */

/**
  ******************************************************************************
  * @func   BSP_I2C_RegisterMspCallbacks()
  * @brief  BSP I2C1 Bus Msp Callback registering
  * @param  Callbacks pointer to I2C1 MspInit/MspDeInit callback functions
  * @gvar   hi2cx, IsI2C1MspCbValid
  * @gfunc  __HAL_I2C_RESET_HANDLE_STATE(), HAL_I2C_RegisterCallback()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
int32_t BSP_I2C_RegisterMspCallbacks(BSP_I2C_Cb_t *Callbacks)
{
    /* Prevent unused argument(s) compilation warning */
    __HAL_I2C_RESET_HANDLE_STATE(&hi2cx);

     /* Register MspInit Callback */
    if (HAL_I2C_RegisterCallback(&hi2cx, HAL_I2C_MSPINIT_CB_ID, Callbacks->pMspInitCb) != HAL_OK)
    {
        return(HAL_ERROR); /* BSP_ERROR_PERIPH_FAILURE; */
    }

    /* Register MspDeInit Callback */
    if (HAL_I2C_RegisterCallback(&hi2cx, HAL_I2C_MSPDEINIT_CB_ID, Callbacks->pMspDeInitCb) != HAL_OK)
    {
        return(HAL_ERROR); /* BSP_ERROR_PERIPH_FAILURE; */
    }

    IsI2C1MspCbValid = 1;

    return(HAL_ERROR);
}  /* BSP_I2C_RegisterMspCallbacks() */
#endif /* USE_HAL_I2C_REGISTER_CALLBACKS */

/**
  ******************************************************************************
  * @func   MX_I2C_Init()
  * @brief  Register Default BSP I2C1 Bus Msp Callbacks
  * @gvar   hi2c
  * @gfunc  HAL_I2C_Init(), HAL_I2CEx_ConfigAnalogFilter(), HAL_I2CEx_ConfigDigitalFilter()
  * @retval HAL_ERROR or HAL_OK
  ******************************************************************************
  */
__weak HAL_StatusTypeDef MX_I2C_Init(I2C_HandleTypeDef* hi2c)
{
    HAL_StatusTypeDef ret = HAL_OK;

    if (HAL_I2C_Init(hi2c) != HAL_OK)
    {
        ret = HAL_ERROR;
    }

    if (HAL_I2CEx_ConfigAnalogFilter(hi2c, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        ret = HAL_ERROR;
    }

    if (HAL_I2CEx_ConfigDigitalFilter(hi2c, 2) != HAL_OK)
    {
        ret = HAL_ERROR;
    }

    //HAL_I2CEx_EnableFastModePlus(I2C_FASTMODEPLUS_I2C1);

    return(ret);
}  /* MX_I2C_Init() */

