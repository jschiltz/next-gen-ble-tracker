/**
  ******************************************************************************
  * @file    NG_Tick_I2C.c
  * @author  Thomas W Liu
  * @brief   Source file of i2c driver for accelerometer
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 Milwaukee Electric Tool Corporation
  * All rights reserved.
  *
  * 2020/10/16   TL      Added timeout in i2c when accelerometer is not found
  ******************************************************************************
  */

#define ENABLE_I2C_TIMEOUT      /* enable time out on i2c bus, in case device not found */

/* Includes ------------------------------------------------------------------*/
#include "bluenrg_lp_evb_i2c.h"
#include "bluenrg_lp_ll_i2c.h"

#define LIS2DW12_REGS_H
#include "lis2dw12_reg.h"       // must have define LIS2DW12_REGS_H before header
#include "NG_Tick_i2c.h"

/* variable declaration ------------------------------------------------------*/

/* function prototype --------------------------------------------------------*/
void NGT_I2C_Init(void);
void NGT_I2C_DeInit(void);
int32_t NGT_I2C_Write(void *, uint8_t, uint8_t *, uint16_t);
int32_t NGT_I2C_Read(void *, uint8_t, uint8_t *, uint16_t);


/**
  ******************************************************************************
  * @func   NGT_I2C_Init()
  * @brief  Configures the I2C interface used for the sensor.
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void NGT_I2C_Init(void)
{
    /* Initialize the GPIOs associated to the I2C port */
    NGT_I2C_DATA_GPIO_CLK_ENABLE();
    LL_GPIO_SetPinMode(NGT_I2C_DATA_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinSpeed(NGT_I2C_DATA_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(NGT_I2C_DATA_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(NGT_I2C_DATA_GPIO_PORT, NGT_I2C_DATA_PIN, NGT_I2C_DATA_GPIO_PULL);
    NGT_I2C_DATA_GPIO_AF();

    NGT_I2C_CLK_GPIO_CLK_ENABLE();
    LL_GPIO_SetPinMode(NGT_I2C_CLK_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinSpeed(NGT_I2C_CLK_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(NGT_I2C_CLK_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(NGT_I2C_CLK_GPIO_PORT, NGT_I2C_CLK_PIN, NGT_I2C_CLK_GPIO_PULL);
    NGT_I2C_CLK_GPIO_AF();

    /* Initialize the I2C clock */
    NGT_I2C_CLK_ENABLE();

    LL_I2C_Disable(NGT_I2C);
  
    /* Configure the SDA setup, hold time and the SCL high, low period
     * For Fast-mode 400 kHz, PRESC | 0h | SCLDEL | SDADEL | SCLH | SCLL
     *                          1h  | 0h |    3h  |   2h   |  03h |  09h
     */
    LL_I2C_SetTiming(NGT_I2C, 0x10320309);

    /* Enable Clock stretching */
    LL_I2C_EnableClockStretching(NGT_I2C);

    /* Enable Peripheral in I2C mode */
    LL_I2C_SetMode(NGT_I2C, LL_I2C_MODE_I2C);

    /* Enable the I2C peripheral */
    LL_I2C_Enable(NGT_I2C);

    /* Enable I2C transfer complete/error interrupts:
     *  - Enable Receive Interrupt
     *  - Enable Not acknowledge received interrupt
     *  - Enable Error interrupts
     *  - Enable Stop interrupt
     */
//  LL_I2C_EnableIT_TX(NGT_I2C);
//  LL_I2C_EnableIT_RX(NGT_I2C);
//  LL_I2C_EnableIT_TC(NGT_I2C);
//  LL_I2C_EnableIT_NACK(NGT_I2C);
//  LL_I2C_EnableIT_ERR(NGT_I2C);
//  LL_I2C_EnableIT_STOP(NGT_I2C);
}       /* NGT_I2C_Init() */


/**
  ******************************************************************************
  * @func   NGT_I2C_DeInit()
  * @brief  Deinitialize the I2C interface used for the sensor.
  * @param  None
  * @gvar   
  * @gfunc  
  * @retval None
  ******************************************************************************
  */
void NGT_I2C_DeInit(void)
{
    /* Disable I2C transfer complete/error interrupts */
    LL_I2C_ClearFlag_TXE(NGT_I2C);
    LL_I2C_ClearFlag_NACK(NGT_I2C);
    LL_I2C_ClearFlag_STOP(NGT_I2C);

    LL_I2C_DisableIT_TX(NGT_I2C);
    LL_I2C_DisableIT_RX(NGT_I2C);
    LL_I2C_DisableIT_TC(NGT_I2C);
    LL_I2C_DisableIT_NACK(NGT_I2C);
    LL_I2C_DisableIT_ERR(NGT_I2C);
    LL_I2C_DisableIT_STOP(NGT_I2C);

    /* Disable the I2C peripheral */
    LL_I2C_Disable(BSP_I2C);

    /* Disable the I2C clock */
    NGT_I2C_CLK_DISABLE();

    /* Reset the I2C pins */
    LL_GPIO_SetPinMode(NGT_LED1_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_MODE_ANALOG);
    LL_GPIO_SetPinSpeed(NGT_LED1_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_SPEED_FREQ_LOW);
    LL_GPIO_SetPinOutputType(NGT_LED1_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(NGT_LED1_GPIO_PORT, NGT_I2C_DATA_PIN, LL_GPIO_PULL_NO);

    LL_GPIO_SetPinMode(NGT_LED2_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_MODE_ANALOG);
    LL_GPIO_SetPinSpeed(NGT_LED2_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_SPEED_FREQ_LOW);
    LL_GPIO_SetPinOutputType(NGT_LED2_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinPull(NGT_LED2_GPIO_PORT, NGT_I2C_CLK_PIN, LL_GPIO_PULL_NO);
}       /* NGT_I2C_DeInit() */


/**
  ******************************************************************************
  * @func   NGT_I2C_Write()
  * @brief  i2c write function
  * @param  handle: handle. 
  * @param  Reg: Reg. 
  * @param  pBuff: pBuff. 
  * @param  nBuffSize: nBuffSize. 
  * @gvar   
  * @gfunc  LL_I2C_HandleTransfer(), LL_I2C_IsActiveFlag_TXE(), LL_I2C_TransmitData8(),
  *         LL_I2C_IsActiveFlag_TXIS()
  * @retval 0
  * @note   must use lis2dw12 H address
  ******************************************************************************
  */
#define MAX_I2C_DELAY_CNT       2048
int32_t NGT_I2C_Write(void *handle, uint8_t Reg, uint8_t *pBuff, uint16_t nBuffSize)
{
    int16_t iDelayCnt = MAX_I2C_DELAY_CNT;
    
    /* Initialize the handle transfer */
    LL_I2C_HandleTransfer(NGT_I2C, LIS2DW12_I2C_ADD_H, LL_I2C_ADDRSLAVE_7BIT, nBuffSize+1, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);  /* OK */
  
    /* Wait for the TX Empty flag */
#ifdef ENABLE_I2C_TIMEOUT
    while ((LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0) && (iDelayCnt-- > 0)); /* SW timeout */
#else
    while (LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0); /* TODO: add a SW timeout */
#endif
  
    /* Get the received byte from the RX FIFO */
    LL_I2C_TransmitData8(NGT_I2C, (Reg | 0x80)); // |0x80 auto-increment
    
    /* Wait for the TX Empty flag */
#ifdef ENABLE_I2C_TIMEOUT
    iDelayCnt = MAX_I2C_DELAY_CNT;
    while ((LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0) && (iDelayCnt-- > 0)); /* SW timeout */
#else
    while (LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0); /* TODO: add a SW timeout */
#endif
  
    for (uint16_t icnt = 0; icnt < nBuffSize; icnt++)
    {
      /* Wait for the TX flag */
#ifdef ENABLE_I2C_TIMEOUT
        iDelayCnt = MAX_I2C_DELAY_CNT;
        while ((LL_I2C_IsActiveFlag_TXIS(NGT_I2C) == 0) && (iDelayCnt-- > 0)); /* SW timeout */
#else
        while (LL_I2C_IsActiveFlag_TXIS(NGT_I2C) == 0); /* TODO: add a SW timeout */
#endif
        LL_I2C_TransmitData8(NGT_I2C, pBuff[icnt]);    /* Fill the TX FIFO with data */
    }

    /* Wait for the Transfer Complete flag */
//  while(LL_I2C_IsActiveFlag_TC(NGT_I2C) == 0);

    return(0);
}       /* NGT_I2C_Write() */


/**
  ******************************************************************************
  * @func   NGT_I2C_Read()
  * @brief  i2c read for lis2dw12, accelerometer.
  * @brief  I2C read function used for the LPS22HH pressure sensor.
  * @param  handle: handle. 
  * @param  Reg: Reg. 
  * @param  pBuff: pBuff. 
  * @param  nBuffSize: nBuffSize. 
  * @gvar   accelerometerDriver, whoamI_lis2dw12
  * @gfunc  LL_I2C_HandleTransfer(), LL_I2C_IsActiveFlag_TXE(), LL_I2C_TransmitData8(),
  *         LL_I2C_IsActiveFlag_RXNE(), LL_I2C_ReceiveData8()
  * @retval 0
  * @note   must use lis2dw12 H address
  ******************************************************************************
  */
int32_t NGT_I2C_Read(void *handle, uint8_t Reg, uint8_t *pBuff, uint16_t nBuffSize)
{
    int16_t iDelayCnt = MAX_I2C_DELAY_CNT;
    
    /* Initialize the handle transfer */
    LL_I2C_HandleTransfer(NGT_I2C, LIS2DW12_I2C_ADD_H, LL_I2C_ADDRSLAVE_7BIT, 1, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);  /* OK */
  
    /* Wait for the TX Empty flag */
#ifdef ENABLE_I2C_TIMEOUT
    iDelayCnt = MAX_I2C_DELAY_CNT;
    while ((LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0) && (iDelayCnt-- > 0)); /* SW timeout */
#else
    while (LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0); /* TODO: add a SW timeout */
#endif
  
    /* Get the received byte from the RX FIFO */
    LL_I2C_TransmitData8(NGT_I2C, (Reg | 0x80)); // |0x80 auto-increment
    
    /* Wait for the TX Empty flag */
#ifdef ENABLE_I2C_TIMEOUT
    iDelayCnt = MAX_I2C_DELAY_CNT;
    while ((LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0) && (iDelayCnt-- > 0)); /* SW timeout */
#else
    while (LL_I2C_IsActiveFlag_TXE(NGT_I2C) == 0); /* TODO: add a SW timeout */
#endif
  
    /* Wait for the Transfer Complete flag */
//      while(LL_I2C_IsActiveFlag_TC(NGT_I2C) == 0);  
  
    /* Initialize the handle transfer */
    LL_I2C_HandleTransfer(NGT_I2C, LIS2DW12_I2C_ADD_H, LL_I2C_ADDRSLAVE_7BIT, nBuffSize, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_READ);  /* OK */

    for (uint16_t icnt = 0; icnt < nBuffSize; icnt++)
    {
      /* Wait for the RX Not Empty flag */
#ifdef ENABLE_I2C_TIMEOUT
        iDelayCnt = MAX_I2C_DELAY_CNT;
        while ((LL_I2C_IsActiveFlag_RXNE(NGT_I2C) == 0) && (iDelayCnt-- > 0)); /* SW timeout */
#else
        while (LL_I2C_IsActiveFlag_RXNE(NGT_I2C) == 0); /* TODO: add a SW timeout */
#endif
        pBuff[icnt] = LL_I2C_ReceiveData8(NGT_I2C);    /* Get the received byte from the RX FIFO */
    }
  
  /* Wait for the Transfer Complete flag */
//    while(LL_I2C_IsActiveFlag_TC(NGT_I2C) == 0);

    return(0);
}       /* NGT_I2C_Read() */

